/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUtilities/temp_file_system.h"

#include <algorithm>
#include <random>
#include "AccerionSensorAPI/utils/filesystem.h"

namespace detail {

std::string GenerateRandomString(size_t length) {
    const char characters[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> dist(0, sizeof(characters) - 2); // -2 to exclude null terminator

    std::string result;
    result.reserve(length);
    for (size_t i = 0; i < length; ++i) {
        result += characters[dist(generator)];
    }
    return result;
}

std::string GetTempDirectory() {
#ifdef _WIN32
    auto temp_dir = std::getenv("TEMP");
    return temp_dir ? temp_dir : std::getenv("TMP");
#else
    auto temp_dir = std::getenv("TMPDIR");
    return temp_dir ? temp_dir : "/tmp";
#endif
}

}  //  namespace detail

std::string TempFileSystem::Create(const std::string& ext, const std::string& folder) {
    auto folder_path = folder.empty() ? detail::GetTempDirectory() : folder;
    const std::string model = detail::GenerateRandomString(16) + (ext.empty() ? ext : "." + ext);
    temp_files_.emplace_back(folder_path + "/" + model);
    return temp_files_.back();
}

void TempFileSystem::Clean() {
    // Although the names are misleading, fileExists and removeFile also handle directories
    std::for_each(std::begin(temp_files_), std::end(temp_files_), [](const auto &path) {
        if (accerion::api::FileExists(path)) {
            accerion::api::RemoveFile(path);
        }
    });
    temp_files_.clear();
}
