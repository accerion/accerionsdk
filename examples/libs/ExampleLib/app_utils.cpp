/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "app_utils.h"

#include <iostream>


void PauseApp(std::string const& message) {
    // There is not a single cross-platform way to wait for one key, without pressing enter.
    std::cout << message << std::endl;
    while (std::cin.get() != '\n') {}
}
