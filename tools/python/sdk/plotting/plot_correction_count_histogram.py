#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from math import ceil
from pathlib import Path

from matplotlib import pyplot as plt

from sdk.data.data_container import DataContainer
from sdk.plotting.utils.plotting import save_or_show, Colors
from sdk.processing.correction_count import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v

from sdk.utils.version import python_version, Version
assert python_version() >= Version("3.7"), "Assumption that dictionary keys are ordered."


def plot_correction_count_histogram(data: DataContainer,
                                    output_folder: Optional[Path] = None,
                                    conf: Configuration = Configuration.default(),
                                    *,
                                    verbose: bool = True):
    _, _, dc_counts, velocity_bounds = get_correction_count(data.arams_original, conf, verbose=verbose)
    if not dc_counts:
        print_v(verbose, "No drift correction per cluster crossing.")
        return

    total = len(dc_counts)
    max_count = max(dc_counts)

    # Generate histogram
    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Number of drift corrections per cluster crossing", fontweight='bold')
    axis.set_title(f'Cluster crossings: {total}, velocity interval: ({velocity_bounds[0]:.3f},{velocity_bounds[1]:.3f})')

    bins = range(1, max_count + 2)
    axis.hist(dc_counts, bins=bins, histtype='bar', rwidth=0.75, align='left')
    if max_count > 20:
        tick_step = ceil(max_count / 20)
        bins = list(reversed(range(max_count, 0, -tick_step)))
    axis.set_xticks(bins)

    avg_count = sum(dc_counts) / total
    axis.axvline(avg_count, color=Colors.BLACK.value, linestyle='dashed', linewidth=1, label="Mean")
    min_ylim, max_ylim = axis.get_ylim()
    axis.text(avg_count * 1.1, max_ylim * 0.95, f'Mean: {avg_count:.1f}')

    axis.set_xlabel('Number of drift corrections per cluster crossing')
    axis.set_ylabel("Frequency")

    save_or_show(fig, output_folder, "correction_count_histogram.png", conf.get_prefix(data.arams_original.get_input_files()))


if __name__ == "__main__":
    from sdk.plotting.utils.plotting import call_arams_plot_function
    call_arams_plot_function(plot_correction_count_histogram)
