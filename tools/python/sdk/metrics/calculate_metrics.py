#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from pathlib import Path

from sdk.data.data_container import DataContainer
from sdk.metrics.metrics import Metrics
from sdk.metrics.reader import Reader as MetricsReader
from sdk.metrics.writer import Writer as MetricsWriter
from sdk.utils.print import print_v
from sdk.utils.argument_parser import create_default_arg_parser, add_spec
from sdk.utils.configuration import Configuration


__all__ = []


"""
With this script the yaml test conditions can be checked for the given arams logging data.
"""


if __name__ == "__main__":
    parser = create_default_arg_parser(output_required=True)
    add_spec(parser, False)
    args = vars(parser.parse_args())

    conf = Configuration.load_configuration()
    if args["skip"] is not None:
        conf[Configuration.Name.SKIP_INITIAL_DRIFT_CORRECTIONS] = int(args["skip"])
    data = DataContainer.create([Path(f) for f in args["input"]], conf)

    metrics = MetricsReader.read(args["spec"]) if "spec" in args else MetricsReader.read_default()

    metrics.evaluate(data)
    MetricsWriter.write(metrics, args["output"])
