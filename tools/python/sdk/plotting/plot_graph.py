#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import argparse
from typing import Set

from sdk.arams.arams import Arams
from sdk.graph.graph import Graph
from sdk.graph.g2o_reader import Reader
from sdk.plotting.utils.graph import *
from sdk.plotting.utils.plotting import *


"""
With this script one graph can be visualized.
Toggling can be done on different types of vertices and edges in the graph by clicking the corresponding legend item.
"""


def __split_graph(graph: Graph) -> List[Tuple[str, Graph]]:
    """
    Note that the graph instances (fixes, vertices and edges) are 'references' to the original graph, so if a change is
    made to the input graph or one of the output ones, the change is visible in all graphs.
    """
    sets = [
        ("absolute constraints",
         {Graph.Vertex.Type.Fixed, Graph.Vertex.Type.Absolute, Graph.Vertex.Type.Interpolated},
         {Graph.Edge.Type.Absolute, Graph.Edge.Type.Identity}),
        ("relative constraints",
         {Graph.Vertex.Type.Signature, Graph.Vertex.Type.Interpolated},
         {Graph.Edge.Type.Relative}),
        ("loop closures",
         {Graph.Vertex.Type.Signature},
         {Graph.Edge.Type.Loop})
    ]

    def filter_graph(vertex_type_set: Set[Graph.Vertex.Type], edge_type_set: Set[Graph.Edge.Type]):
        filtered_graph = Graph()
        filtered_graph.fixes = graph.fixes
        filtered_graph.vertices = [v for v in graph.vertices if v.type in vertex_type_set]
        filtered_graph.edges = [e for e in graph.edges if e.type in edge_type_set]
        return filtered_graph

    return [(name, filter_graph(vertex_types, edge_types)) for name, vertex_types, edge_types in sets]


def plot_graph(graph: Graph, signatures: List[Signature] = None, *, draw_heading: bool = False):
    # Store all information regarding the plots
    plot_elements = PlotElements()

    # Create one figure with the different graph types
    fig, ax = plt.subplots(1, 1)

    graphs = __split_graph(graph)
    settings = PlotGraphsSettings.create(len(graphs))
    for index, (name, graph) in enumerate(graphs):
        handles = add_graph(ax, graph, settings[index], draw_heading=draw_heading)
        plot_elements.append(name, settings[index], handles)

    add_signature_poses(ax, signatures, (138. / 256, 43. / 256, 226. / 256))  # blue violet

    add_legend(fig, ax, plot_elements)

    ax.set_aspect('equal', adjustable='datalim')
    ax.set_xlabel("x-coordinate [m]")
    ax.set_ylabel("y-coordinate [m]")
    ax.set_title("Pose graph displaying different vertex/edge sets")
    fig.tight_layout()
    plt.show()


def __parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", required=True, nargs='+', help="Input g2o files, up to three")
    parser.add_argument("-c", "--coords", required=False, help="Input map coordinates csv file")
    parser.add_argument("-dh", "--drawheading", required=False, action="store_true",
                        help="Enable drawing of heading")

    return vars(parser.parse_args())


def __main():
    args = __parse_arguments()

    graph, *others = [Reader.read(file_name) for file_name in args["input"]]
    assert len(others) == 0
    assert all(v.type != Graph.Vertex.Type.Unknown for v in graph.vertices)
    assert all(e.type != Graph.Edge.Type.Unknown for e in graph.edges)

    arams = Arams(args["coords"]) if args["coords"] is not None else None

    plot_graph(graph, arams, draw_heading=args["drawheading"])


if __name__ == "__main__":
    __main()
