/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <unordered_map>

#include "AccerionPgo/pose_graph.h"


namespace accerion {
namespace pgo {

Eigen::Matrix3d CreateInformationMatrix(double standard_deviation_position, double standard_deviation_rotation);

std::unordered_map<accerion::api::SignatureId, std::size_t> CreateNodeIdToIndexMap(
        const std::vector<accerion::pgo::PoseGraph::Node>& nodes);

std::vector<accerion::pgo::PoseGraph::Edge> IdentifyLoopClosureEdges(
        const std::vector<accerion::pgo::PoseGraph::Edge>& edges);

}  // namespace accerion
}  // namespace pgo
