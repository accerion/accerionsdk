# Resources
In this folder particular files can be found of which the details are explained here.

## Test specification files
`test_template.yml` is the default test specification file that will be used by the calculate_metrics.py script.

One remark about the format. As the yaml file basically contains a nested dictionaries, every dictionary on its own may
only contain unique keys. Therefore the following is not allowed:
```yml
A: x
B: y
A: z
```
Similarly also the following definition is incorrect:
```yml
A:
    x: 'x'
    y: 'y'
    x: 'z'
```

## Configuration template file
`conf_template.yml` is the default configuration file used for setting plotting, filtering and reporting specifications. Same format restrictions apply as for `test_template.yml`.

## Report template markdown file
`report_*_template.md` files contain the report content that can be used by the create_report.py script. It is copied
locally, customized and subsequently used by the script.

The file is a regular markdown file, with the exception that the chunks (the code blocks between '```') are actually
executed when the file is converted into an actual pdf report. Some helper functions are defined in `preparations.py`
and `markdown_utils.py` in the folder `report`. Note that any code should output/print markdown or directly html
compatible results in order to be processed properly.

The package used for the Python interpretation is [Pweave](https://mpastell.com/pweave/). On this website more
information can be found, how the code chunks are used.

Note that the `results = 'markdown'` option is added, but the actual value `markdown` does not have any effect.
Check [this page](https://mpastell.com/pweave/chunks.html#envvar-results='verbatim') for more details.
