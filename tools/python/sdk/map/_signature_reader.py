#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from enum import Enum
from enum import auto as enum_auto
from typing import Any, List, Optional, Type

from sdk.data.pose import Pose
from sdk.data.signature import Signature
from sdk.database.database import Database
from sdk.map.versions.map_specification import MapSpecification


__all__ = ["MapSignatureReader"]


class MapSignatureReader:
    class Info(Enum):
        IDS = enum_auto()
        POSE = enum_auto()
        ALL = enum_auto()

    def __init__(self, db: Database, map_spec: Type[MapSpecification]):
        self.__db = db
        self.Signatures = map_spec.Signatures

    def __get_columns(self, spec: Info) -> List[str]:
        result = [self.Signatures.ID, self.Signatures.CLUSTER_ID,
                  self.Signatures.INDEX_IN_CLUSTER]

        if spec == self.Info.POSE:
            result.extend(
                [self.Signatures.X, self.Signatures.Y, self.Signatures.TH])

        return [str(i) for i in result]

    def __extract_signature(self, row: Any, spec: Info) -> Signature:
        result = Signature()

        result.id = int(row[self.Signatures.ID])
        result.cluster_id = int(row[self.Signatures.CLUSTER_ID])
        result.index_in_cluster = int(row[self.Signatures.INDEX_IN_CLUSTER])

        if spec == self.Info.POSE:
            result.pose = Pose(
                row[self.Signatures.X],
                row[self.Signatures.Y],
                row[self.Signatures.TH]
            )

        return result

    def get_signature(self, signature_id, spec: Info) -> Optional[Signature]:
        columns = self.__get_columns(spec)

        cursor = self.__db.select(f"""
                SELECT {', '.join(columns)}
                  FROM {self.Signatures.table_name()}
                 WHERE {self.Signatures.ID} = :id; 
            """, {"id": signature_id})

        row = cursor.fetchone()
        return row if row is None else self.__extract_signature(row, spec)

    def get_all_signatures(self, spec: Info) -> List[Signature]:
        columns = self.__get_columns(spec)

        cursor = self.__db.connection.execute(f"""
                SELECT {', '.join(columns)}
                  FROM {self.Signatures.table_name()}
            """)

        return [self.__extract_signature(row, spec) for row in cursor]
