/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once
#include <map>
#include <optional>
#include <string>
#include <utility>
#include <vector>
#include "AccerionDatabaseHandling/database.h"
#include "AccerionMap/map_version.h"
#include "AccerionSensorAPI/structs/version.h"

namespace accerion {

namespace map {

struct TableDescription {
    std::string name;
    std::string columns;  // Also includes constraints
};
struct IndexDescription {
    std::string name;
    std::string specification;
};
struct Schema {
    std::optional<TableDescription> FindTable(const std::string& name);
    std::optional<IndexDescription> FindIndex(const std::string& name);

    std::vector<TableDescription> tables;
    std::vector<IndexDescription> indices;
};

static const accerion::map::Schema kLatestSchema = {
    // Initialize tables
    {
        {"signatures",
            "id                  INTEGER     PRIMARY KEY AUTOINCREMENT NOT NULL,"
            "x                   DOUBLE      NOT NULL,"
            "y                   DOUBLE      NOT NULL,"
            "th                  DOUBLE      NOT NULL,"
            "cluster_id          INTEGER     NOT NULL,"
            "index_in_cluster    INTEGER     NOT NULL"},

        {"data",
            "id    INTEGER     PRIMARY KEY NOT NULL,"
            "a     BLOB        NOT NULL,"
            "b     BLOB        NOT NULL,"
            "FOREIGN KEY(id) REFERENCES signatures(id) ON DELETE CASCADE ON UPDATE CASCADE"},

        {"images",
            "id      INTEGER     PRIMARY KEY NOT NULL,"
            "image   BLOB        NOT NULL,"
            "serial  INTEGER     NOT NULL,"
            "FOREIGN KEY(id) REFERENCES signatures(id) ON DELETE CASCADE ON UPDATE CASCADE"},

        {"metadata",
            "key       BLOB    PRIMARY KEY NOT NULL,"
            "value     BLOB    NOT NULL"}
    },

    // Initialize indices
    {
        {"pos_index",     "signatures(x, y)"},
        {"cluster_index", "signatures(cluster_id)"}
    }
};

class SchemaCreator {
 public:
    SchemaCreator(std::map<accerion::api::Version, accerion::map::Schema> = {{accerion::map::LatestVersion(),
                                                                                std::move(kLatestSchema)}});

    std::optional<Schema> FindSchema(const accerion::api::Version& map_version) const;
    // Creates tables and sets the map version in the given database
    void AddToDatabase(const accerion::sqlite::Database& db, const accerion::api::Version& map_version) const;
    void DropIndices(const accerion::sqlite::Database& db,
                        const accerion::api::Version& map_version = accerion::map::LatestVersion()) const;
    void CreateIndices(const accerion::sqlite::Database& db,
                        const accerion::api::Version& map_version = accerion::map::LatestVersion()) const;

 private:
    std::map<accerion::api::Version, accerion::map::Schema> schemas_;
};

}  // namespace map
}  // namespace accerion
