#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.data.data_container import DataContainer
from sdk.plotting.utils.plotting import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_heading_corrections_heatmap(data: DataContainer,
                                     output_folder: Optional[Path] = None,
                                     conf: Configuration = Configuration.default(),
                                     *,
                                     verbose: bool = True):
    if "LDC" not in data.arams:
        print_v(verbose, __name__, ": Could not find logDriftCorrections file or the arams version does not support the file type.")
        return None
    LDC = data.arams.LDC

    arams = data.arams  # shallow copy
    
    df = arams[LDC]
    df = df.sort_values(by=[LDC.ERROR_THETA.value], key=lambda e: abs(e), ascending=False)

    # Create plot
    fig, axis = plt.subplots(1, 1)

    if conf[Configuration.Name.OVERLAY_MAP]:
        # adding alpha channel to prevent the heatmap from being lost under the scatter map
        add_signature_poses(axis, data.signatures, (.5, .5, .7, .15))

    array = create_binned_data(df[LDC.NEW_XPOS.value], df[LDC.NEW_YPOS.value], abs(df[LDC.ERROR_THETA.value]),
                                     bin_size=.3)
    heatmap_handle = add_heat_map(axis, df[LDC.NEW_XPOS.value], df[LDC.NEW_YPOS.value], array)
    visualize_first_dc(axis, arams[LDC][LDC.NEW_XPOS.value].to_list(), arams[LDC][LDC.NEW_YPOS.value].to_list(), conf)

    fig.colorbar(heatmap_handle, ax=axis)
    axis.set_aspect('equal', adjustable='datalim')
    axis.set_xlabel("x-coordinate [m]")
    axis.set_ylabel("y-coordinate [m]")
    axis.set_title("Heat map of maximum heading corrections [deg]")
    fig.tight_layout()

    save_or_show(fig, output_folder, "heading_corrections_heatmap.png", conf.get_prefix(arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_heading_corrections_heatmap)
