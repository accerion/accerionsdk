#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from packaging.version import Version
from pathlib import Path
from typing import Optional

from matplotlib.figure import SubplotParams
import matplotlib.pyplot as plt
import math, copy
import numpy as np
import pandas as pd
from scipy.optimize import minimize

from sdk.arams.arams import Arams
from sdk.data.data_container import DataContainer
from sdk.plotting.utils.plotting import save_or_show, Colors, drop_nan
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v

"""
Python tool corresponding to the mount pose validation test procedure as described in Accerion's documentation on the 
Knowledge Base. Goal of the tool is to align velocities from the external reference pose provided by the user with 
the velocities derived from Triton's visual odometry. The result is an estimate of the error in transformation between 
where the user describes where Triton is via its robot description, and where Triton is estimated to be. 
"""

MIN_LINEAR_VELOCITY_THRESHOLD = 0.001  # m/s
MIN_ANGULAR_VELOCITY_THRESHOLD = 0.05  # rad/s
MOVING_AVERAGE_WINDOW = 50


class Objective:
    def __init__(self, velocities):
        # Get Triton velocities and calculate equivalent velocities based on external reference poses
        _, triton_vel_x, triton_vel_y, _, input_vel_x, input_vel_y, self.input_vel_th = velocities

        # Combine velocities into matrix form
        self.v_t = np.transpose(np.vstack((triton_vel_x, triton_vel_y)))
        self.v_b = np.transpose(np.vstack((input_vel_x, input_vel_y)))

        # Transformation and helper matrices
        self.S = np.array([[0, -1],
                           [1, 0]])

    def __call__(self, x, args):
        # Compute errors
        errors = self.v_t - self.calculate_estimated_velocity_in_triton_frame(x)

        return np.sum(errors ** 2)

    def calculate_estimated_velocity_in_triton_frame(self, x):
        # Transformation and helper matrices
        r = np.array([x[0], x[1]])
        Ainv = np.array([[np.cos(x[2]), np.sin(x[2])],
                        [-np.sin(x[2]), np.cos(x[2])]])

        # Vectorized calculation of v_t_est
        cross_r = np.dot(self.S, r)  # Cross product with mount offset

        # The expression below is a faster way of performing the following operations:
        # v_t_est = np.empty([2, (len(self.v_t))])
        # for i in range(len(self.v_t)):
        #     v_t_est[:, i] = np.dot(Ainv, self.v_b[i, :]) + np.dot(Ainv, self.input_vel_th[i] * np.dot(self.S, r))

        v_t_est = self.v_b @ Ainv.T + (self.input_vel_th[:, None] * (cross_r @ Ainv.T))

        return v_t_est


def copy_to_rad(arams):
    """
    Creates a new arams objects with all data in degrees converted to radians.
    """
    copied_arams = copy.deepcopy(arams)

    assert copied_arams.get_version() >= Version("7.0.0")

    LRT = copied_arams.LRT
    LER = copied_arams.LER

    table_columns = {
        LRT: [LRT.ODOM_VEL_TH.value],
        LER: [LER.INPUT_POSE_TH.value]
    }

    for key, values in table_columns.items():
        for value in values:
            copied_arams[key][value] *= math.pi / 180

    return copied_arams


def moving_average(data, window_size=MOVING_AVERAGE_WINDOW):
    """Compute the moving average of a 1D array."""
    assert window_size <= len(data), "Window size must not exceed the length of the data."

    cumulative_sum = np.cumsum(data, dtype=float)
    # Subtract cumulative sums to get sums over the sliding window
    cumulative_sum[window_size:] = cumulative_sum[window_size:] - cumulative_sum[:-window_size]
    # Return the mean values for each window
    return cumulative_sum[window_size - 1:] / window_size


def apply_filters(*arrays, window_size=MOVING_AVERAGE_WINDOW):
    """Apply moving average filter to arrays."""
    return [moving_average(arr, window_size) for arr in arrays if len(arr) > window_size]


def calculate_and_filter_velocities(merged_df: pd.DataFrame, arams_df: Arams):
    input_pose_x = merged_df[arams_df.LER.INPUT_POSE_X].to_numpy()
    input_pose_y = merged_df[arams_df.LER.INPUT_POSE_Y].to_numpy()
    input_pose_th = np.unwrap(merged_df[arams_df.LER.INPUT_POSE_TH].to_numpy(), period=2*np.pi)

    time = merged_df[arams_df.ARAMS_TIME].to_numpy()/1.e3

    triton_vel_x = merged_df[arams_df.LRT.ODOM_VEL_X].to_numpy()
    triton_vel_y = merged_df[arams_df.LRT.ODOM_VEL_Y].to_numpy()
    triton_vel_th = merged_df[arams_df.LRT.ODOM_VEL_TH].to_numpy()

    # Pre-allocate velocity arrays
    input_vel_x = np.empty_like(input_pose_x)
    input_vel_y = np.empty_like(input_pose_y)
    input_vel_th = np.empty_like(input_pose_th)

    # Calculate differences
    dt = np.diff(time)
    dx = np.diff(input_pose_x)
    dy = np.diff(input_pose_y)
    dth = np.diff(input_pose_th)

    # Calculate global frame velocities
    input_vel_x_glob = dx / dt
    input_vel_y_glob = dy / dt
    input_vel_th[1:] = dth / dt  # Skip the first entry

    # Convert to Triton equivalent frame
    cos_th = np.cos(input_pose_th[1:])
    sin_th = np.sin(input_pose_th[1:])
    input_vel_x[1:] = cos_th * input_vel_x_glob + sin_th * input_vel_y_glob
    input_vel_y[1:] = -sin_th * input_vel_x_glob + cos_th * input_vel_y_glob

    # Handle the first entry (optional, set to 0 or NaN)
    input_vel_x[0] = input_vel_y[0] = input_vel_th[0] = 0  # Or np.nan


    assert len(triton_vel_x) == len(input_vel_x)

    # Apply moving average filter
    time, triton_vel_x, triton_vel_y, triton_vel_th, input_vel_x, input_vel_y, input_vel_th = apply_filters(
        time, triton_vel_x, triton_vel_y, triton_vel_th, input_vel_x, input_vel_y, input_vel_th)

    # Create a boolean mask for entries to keep  (above velocity thresholds)
    keep_mask = ~(
            (np.abs(triton_vel_x) < MIN_LINEAR_VELOCITY_THRESHOLD) &
            (np.abs(triton_vel_y) < MIN_LINEAR_VELOCITY_THRESHOLD) &
            (np.abs(triton_vel_th) < MIN_ANGULAR_VELOCITY_THRESHOLD)
    )

    return (time[keep_mask], triton_vel_x[keep_mask], triton_vel_y[keep_mask], triton_vel_th[keep_mask],
            input_vel_x[keep_mask], input_vel_y[keep_mask], input_vel_th[keep_mask])


def calculate_mount_pose(velocities):
    # Optimization of trajectories
    bounds = [(-np.infty, np.infty),
              (-np.infty, np.infty),
              (-np.pi, np.pi)]

    objective = Objective(velocities)

    result = minimize(objective, x0=np.array([0, 0, 0]), args=None, method='SLSQP', bounds=bounds, options={'maxiter': 100})
    if result:
        print("Estimated remaining mount pose error:")
        labels = ["x [m]", "y [m]", "Heading [rad]"]
        for label, value, precision in zip(labels, result.x, [3, 3, 4]):
            print(f"{label}: {value:.{precision}f}")
    else:
        print("No solution found.")
        return None

    return result.x


def plot_calculate_mount_pose(data: DataContainer,
                              output_folder: Optional[Path] = None,
                              conf: Configuration = Configuration.default(),
                              verbose: bool = True):

    if data.arams.get_version() < Version("7.0.0"):
        print_v(verbose, __name__, ": This tool only support version 7.0.0 and higher.")
        return

    arams = copy_to_rad(data.arams)

    LER = arams.LER
    if LER not in arams:
        print_v(verbose, __name__,
                f": Could not find {LER.full_name()} file or the arams version does not support the file type.")
        return

    df_ler = arams[LER]
    df_ler = df_ler.set_index(df_ler[LER.INDEX], drop=True)

    LRT = arams.LRT
    if LRT not in arams:
        print_v(verbose, __name__, ": Could not find logRuntimeTrackingFast file or the arams version does not support the file type.")
        return

    df_lrt = arams[LRT]
    df_lrt = df_lrt.set_index(df_lrt[LRT.INDEX], drop=True)

    # Remove duplicate columns from LRT that are also present in LER
    df_lrt = df_lrt.drop(columns=[arams.ARAMS_TIME])

    # Merge LRT and LER dataframes, as there is not always an external reference for every Triton frame
    merged_df = pd.merge(df_ler, df_lrt, left_index=True, right_index=True, how="inner")

    if len(merged_df) == 0:
        print_v(verbose, __name__,
                f": No overlap between external reference poses and Triton velocities.")
        return

    time, triton_vel_x, triton_vel_y, triton_vel_th, input_vel_x, input_vel_y, input_vel_th = calculate_and_filter_velocities(merged_df, arams)
    if len(time) == 0:
        print_v(verbose, __name__,
                f": No velocities above threshold.")
        return

    x = calculate_mount_pose(velocities=(time, triton_vel_x, triton_vel_y, triton_vel_th, input_vel_x, input_vel_y, input_vel_th))

    if x is None:
        print_v(verbose, __name__,
                f": No solution found.")
        return

    # Use optimal solution x to calculate velocity using optimized mount pose
    objective = Objective(velocities=(time, triton_vel_x, triton_vel_y, triton_vel_th, input_vel_x, input_vel_y, input_vel_th))
    v_t_sol = objective.calculate_estimated_velocity_in_triton_frame(x)

    # Plot results
    def create_subplot(axis, axis_label, x_data, data_1, data_2, data_3):
        axis.set_xlabel("Time since retrieval of logs [s]")
        axis.set_ylabel(axis_label)
        axis.plot(x_data, data_1, marker='.', linestyle='None', markersize=8, color=Colors.BLACK.value)
        axis.plot(x_data, data_2, marker='.', linestyle='None', markersize=5, color=Colors.RED.value)
        axis.plot(x_data, data_3, marker='.', linestyle='None', markersize=2, color=Colors.GREEN.value)
        axis.grid()

    fig, axes = plt.subplots(nrows=3, ncols=1, figsize=[8, 8], sharex='all', subplotpars=SubplotParams(hspace=.4))
    fig.suptitle("Triton vs input vs optimized velocities")

    time_to_end = time - time[-1]
    create_subplot(axes[0], "Vx [m/s]", time_to_end, triton_vel_x, input_vel_x, v_t_sol[:, 0])
    create_subplot(axes[1], "Vy [m/s]", time_to_end, triton_vel_y, input_vel_y, v_t_sol[:, 1])
    create_subplot(axes[2], "Angular velocity [rad/s]", time_to_end, triton_vel_th, input_vel_th, input_vel_th)

    fig.legend(labels=["Triton", "Input", "Optimized"], loc="upper right", borderaxespad=1.0)
    save_or_show(fig, output_folder, "mount_pose_validation.png", conf.get_prefix(arams.get_input_files()))


def main():
    from sdk.plotting.utils.plotting import call_arams_plot_function
    call_arams_plot_function(plot_calculate_mount_pose)


if __name__ == "__main__":
    main()
