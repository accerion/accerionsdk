#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.geometry.bounding_box import BoundingBox
from sdk.geometry.interval import Interval
from sdk.geometry.point import Point


def test_containment():
    bbox = BoundingBox(Interval(0, 1), Interval(0, 1))
    assert Point(0, 0) in bbox
    assert Point(-2, 0) not in bbox


def test_create_from_points():
    bbox = BoundingBox.create_from_points(
        Point(0, 0), Point(0.5, 0), Point(1, 0))

    assert Point(0.5, 0) in bbox
    assert not Point(1.1, 0) in bbox
