#!/usr/bin/env python3
"""
 Copyright (c) 2021-2022, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import Dict

from sdk.metrics.common import ComparisonType, NO_AGGREGATION_DATA, parse_condition, Result


class MetricCondition:
    def __init__(self, condition_str: str):
        self.__str = condition_str
        self.__comparisons, self.__func = parse_condition(self.__str)

    def __repr__(self):
        return self.__str

    @property
    def comparisons(self) -> Dict[ComparisonType, float]:
        return self.__comparisons

    def __call__(self, data: float) -> Result:
        if data == NO_AGGREGATION_DATA:
            return Result.NoData
        return self.__func(data)
