#!/usr/bin/env python3
"""
 Copyright (c) 2021-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Union

from yaml import safe_load

from sdk import ROOT_PATH
from sdk.metrics.common import NO_AGGREGATION_DATA, Result
from sdk.metrics.metrics import Metrics
from sdk.metrics.metrics_data import MetricsData


__all__ = ["Reader"]


class Reader:
    _MetricsData = MetricsData

    @classmethod
    def read_default(cls) -> Metrics:
        return cls.read(ROOT_PATH / "resources" / "test_template.yml")

    @classmethod
    def read(cls, file_name: Union[str, Path]) -> Metrics:
        assert Path(file_name).is_file()

        result = Metrics()
        with open(file_name, 'r') as input_file:
            test_yaml = safe_load(input_file)

            for data_tag in test_yaml:
                data = cls._MetricsData(data_tag)

                conditions = []
                for agg_tag, test_dict in test_yaml[data_tag].items():
                    assert "cond" in test_dict, "Missing condition in test"
                    conditions.append(Metrics.Condition.create(
                        test_dict["id"],
                        agg_tag,
                        test_dict["cond"],
                        test_dict["result"] if "result" in test_dict else Result.NoData,
                        test_dict["value"] if "value" in test_dict else NO_AGGREGATION_DATA))
                result.data_conditions[data] = conditions
        return result
