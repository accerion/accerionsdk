/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/optional.h"
#include "AccerionSensorAPI/utils/string.h"

namespace accerion {

namespace utilities {

template <typename... Types>
class CsvReader {
public:
    using Line = std::tuple<Types...>;

    explicit CsvReader(std::string file_location, char delimiter = ','):
                        delimiter_string_{delimiter},
                        file_handle_(file_location, std::ios::in) {
        if (!accerion::api::FileExists(file_location)) {
            std::stringstream ss;
            ss << "Trying to open non-existing file " << file_location;
            throw std::runtime_error(ss.str());
        }
        if (!IsOpen()) {
            std::stringstream ss;
            ss << "Could not open file " << file_location;
            throw std::runtime_error(ss.str());
        }
        ReadHeader();
    }

    ~CsvReader() {
        CloseFile();
    }

    void CloseFile() {
        if (IsOpen()) {
            file_handle_.close();
        }
    }

    std::vector<std::string> ReadHeader() {
        // Only read it if it hasn't been read yet, otherwise it will parse data as the header
        if (!header_vector_) {
            std::string fullHeaderLine;
            GetStrippedLine(fullHeaderLine);
            header_vector_ = accerion::api::Split(fullHeaderLine, delimiter_string_);
        }
        return header_vector_.value();
    }

    std::istream& ReadLine(Line& tuple) {
        std::string line;
        std::istream& stream = GetStrippedLine(line);
        // some files have an empty line at the end, so ignore that line too
        if (line.length() != 0) tuple = ParseStringToTuple<Types...>(line);
        return stream;
    }

    std::vector<Line> ReadLines() {
        std::vector<Line> lines;
        Line line;
        while (ReadLine(line)) {
            lines.push_back(line);
        }
        return lines;
    }

private:
    template <typename T>
    static T ParseAs(std::string data) {
        static_assert(!std::is_pointer<T>::value, "CsvReader does not support pointers");
        static_assert(!std::is_enum<T>::value, "CsvReader does not support enums");
        T t;
        std::stringstream ss;
        ss << data;
        ss >> t;
        return t;
    }

    static void StripLine(std::string &line) {
        // Trim to remove '\r\n' on Windows
        std::stringstream(line) >> line;
    }

    std::istream& GetStrippedLine(std::string &line) {
        std::istream &str = std::getline(file_handle_, line);
        StripLine(line);
        return str;
    }

    bool IsOpen() const {
        return file_handle_.is_open();
    }

    template <std::size_t I = 0, typename... Args>
    typename std::enable_if<I == sizeof...(Args), void>::type
    static FillTuple(std::tuple<Args...> &t, const std::vector<std::string> &data) {}

    template <std::size_t I = 0, typename... Args>
    typename std::enable_if < I<sizeof...(Args), void>::type
    static FillTuple(std::tuple<Args...> &t, const std::vector<std::string> &data) {
        if (!std::is_same<typename std::tuple_element<I,
            std::tuple<Args...>>::type, std::string>::value) {
            if (data[I].length() == 0) {
                throw std::runtime_error("Parsing error: Empty string in integral type");
            }
        }
        std::get<I>(t) = ParseAs<typename std::tuple_element<I, std::tuple<Args...>>::type>(data[I]);
        FillTuple<I + 1, Args...>(t, data);
    }

    template <typename... Args>
    std::tuple<Args...> ParseStringToTuple(std::string is) const {
        std::vector<std::string> data = accerion::api::Split(is, delimiter_string_);
        std::tuple<Args...> tuple;
        FillTuple(tuple, data);
        return tuple;
    }

    const std::string delimiter_string_;
    std::ifstream file_handle_;
    accerion::api::Optional<std::vector<std::string>> header_vector_;
};

}  // namespace utilities

}  // namespace accerion
