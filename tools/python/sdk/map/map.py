#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import List, Optional, Set, Union

from packaging.version import Version
from sdk.data.signature import Signature
from sdk.database.database import Database
from sdk.map._signature_reader import MapSignatureReader
from sdk.map.versions.map_specification import MapSpecification, MapSpecificationRegister


__all__ = ["Map"]


class Map:
    """
    Wrapper around a Database instance, that reads the database as a map
    """
    __MapSpecificationRegister = MapSpecificationRegister
    __SignatureReader = MapSignatureReader
    Info = MapSignatureReader.Info

    def __init__(self, db_file: Union[str, Path]):
        self.__database = Database(db_file)
        self.__database.execute("PRAGMA foreign_keys = 1")

        self.__version = self.__get_map_version()
        self.__map_specification = self.__MapSpecificationRegister.get_supported_specification(self.__version)

        self.__signature_reader = self.__SignatureReader(
            self.__database, self.__map_specification)

    def __get_map_version(self) -> Version:
        """
        Return a Version with only the major, minor and patch values set.
        """
        cursor = self.database().get_cursor(f"""
            SELECT {MapSpecification.MetaData.VALUE}
              FROM {MapSpecification.MetaData.table_name()}
             WHERE {MapSpecification.MetaData.KEY} = "map_version"
            """)
        rows = cursor.fetchone()
        assert rows

        # Remove everything after the patch part
        version_string = rows[0]
        version_string = '.'.join(version_string.split('.')[:3])
        return Version(version_string)

    @property
    def version(self):
        return self.__version

    def database(self):
        # To be deleted in the future?
        return self.__database

    def get_signature(self, signature_id, spec: Info) -> Optional[Signature]:
        return self.__signature_reader.get_signature(signature_id, spec)

    def get_signatures(self, signature_ids: List[int], spec: Info) -> List[Signature]:
        signatures = []
        for signature_id in signature_ids:
            if signature := self.get_signature(signature_id, spec):
                signatures.append(signature)

        return signatures

    def get_all_signatures(self, spec: Info) -> List[Signature]:
        return self.__signature_reader.get_all_signatures(spec)

    def remove_signatures(self, signature_ids: Set[int]) -> None:
        if len(signature_ids) == 0:
            return

        self.__database.execute(f"""
            DELETE
              FROM {self.__map_specification.Signatures.table_name()}
             WHERE {self.__map_specification.Signatures.ID} IN ({','.join(map(str, signature_ids))})
            """)
        self.__database.connection.commit()

    def __cluster_reindex_required(self) -> bool:
        s = self.__map_specification.Signatures
        rows = self.__database.execute(f"""
            SELECT {s.CLUSTER_ID},
                   min({s.INDEX_IN_CLUSTER}) AS minimum,
                   max({s.INDEX_IN_CLUSTER}) AS maximum,
                   count(DISTINCT {s.INDEX_IN_CLUSTER}) AS unique_count
              FROM {s.table_name()}
             GROUP BY {s.CLUSTER_ID}
            HAVING    minimum <> 0
                   OR maximum + 1 <> unique_count
             LIMIT 1;
            """)
        return rows.fetchone()

    def reindex_cluster_index(self):
        """
        Function currently relies on the assumption that the signatures ids can be used to determine the order of the
        signatures within a cluster.
        """
        if not self.__cluster_reindex_required():
            return

        s = self.__map_specification.Signatures
        self.__database.execute(f"""
            UPDATE {s.table_name()}
               SET {s.INDEX_IN_CLUSTER} = (SELECT new_index
                                             FROM (SELECT {s.ID},
                                                          {s.INDEX_IN_CLUSTER}  AS old_index,
                                                          (SELECT COUNT(*)
                                                             FROM {s.table_name()} AS s2
                                                            WHERE     s2.{s.CLUSTER_ID} = {s.table_name()}.{s.CLUSTER_ID}
                                                                  AND s2.{s.ID} <= {s.table_name()}.{s.ID}) - 1  AS new_index
                                                     FROM {s.table_name()}) AS indices
                                            WHERE indices.{s.ID} = {s.table_name()}.{s.ID});
            """)
        self.__database.connection.commit()
