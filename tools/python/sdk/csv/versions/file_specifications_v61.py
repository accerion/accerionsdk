#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.csv.versions.base import FmcFileSpecification
from sdk.csv.versions.file_specifications_v6 import ARAMS_INDEX, ARAMS_TIME, LDC, LER, LRT, LSD


__all__ = ["ARAMS_INDEX", "ARAMS_TIME", "FMC", "FILES_SUPPORTED"]


# Floor map coordinates .csv file column headers used
class FMC(FmcFileSpecification):
    SIGNATURE_ID = "signature_id"
    CLUSTER_ID = "cluster_id"
    SIG_STATUS = "status"
    SIG_POS_X = "pos_x[m]"
    SIG_POS_Y = "pos_y[m]"
    STD_DEV_POS_X = "std_dev_pos_x[m]"
    STD_DEV_POS_Y = "std_dev_pos_y[m]"


# List of supported ARAMS files
FILES_SUPPORTED = [LRT, LDC, LER, LSD]
