#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.csv.versions.base import FmcFileSpecification, LogFileSpecification
from sdk.csv.versions.file_specifications_v615 import ARAMS_INDEX, ARAMS_TIME, LDC, LSD


__all__ = ["ARAMS_INDEX", "ARAMS_TIME", "FMC", "FILES_SUPPORTED"]


class LER(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME

    INPUT_POSE_X = "pose_x[m]"
    INPUT_POSE_Y = "pose_y[m]"
    INPUT_POSE_TH = "pose_theta[deg]"
    TIME_OFFSET = "time_offset[s]"

    @staticmethod
    def full_name():
        return "logExtRefInputFast"


class FMC(FmcFileSpecification):
    SIGNATURE_ID = "signature_id"
    CLUSTER_ID = "cluster_id"
    SIG_POS_X = "x[m]"
    SIG_POS_Y = "y[m]"
    SIG_POSE_TH = "th[deg]"


class LRT(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME

    STEP_TIME = "step_time[s]"
    IS_TRACKING_GOOD = "is_tracking_good[0-1]"
    CORRECTED_ODOM_POSE_X = "corrected_odom_pos_x[m]"
    CORRECTED_ODOM_POSE_Y = "corrected_odom_pos_y[m]"
    CORRECTED_ODOM_POSE_TH = "corrected_odom_pos_theta[deg]"
    ODOM_POSE_X = "odom_pos_x[m]"
    ODOM_POSE_Y = "odom_pos_y[m]"
    ODOM_POSE_TH = "odom_pos_theta[deg]"
    ODOM_VEL_X = "odom_vel_x[m/s]"
    ODOM_VEL_Y = "odom_vel_y[m/s]"
    ODOM_VEL_TH = "odom_vel_theta[deg/s]"
    SEARCH_RADIUS = "search_radius[m]"
    SEARCH_ANGLE = "search_angle[deg]"

    @staticmethod
    def full_name():
        return "logRuntimeTrackingFast"


# List of supported ARAMS files
FILES_SUPPORTED = [LRT, LDC, LER, LSD]
