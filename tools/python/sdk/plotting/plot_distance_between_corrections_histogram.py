#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.plotting.utils.plotting import *
from sdk.processing.distance_between_corrections import get_distance_between_corrections
from sdk.utils.configuration import Configuration


def plot_distance_between_corrections_histogram(data: DataContainer,
                                                output_folder: Optional[Path] = None,
                                                conf: Configuration = Configuration.default(),
                                                *,
                                                verbose: bool = True):
    from sdk.metrics.metrics_data import MetricsData

    _, _, dist = get_distance_between_corrections(data.arams, verbose=verbose)
    if not dist:
        return
    corrections_total = len(dist)

    n_bins = 6

    # Bin ranges; e.g. [5, 6] --> first bin includes values of 5 but excludes values of 6
    max_dist_metric = conf[Configuration.Name.METRICS].get_constraint_value(MetricsData.Tag.DistCorrections,
                                                                            MetricDataAggregation.Type.Maximum,
                                                                            ComparisonType.Smaller)

    # Calculate max distance in dataset. If above max metric, add extra bin
    max_dist = max(dist)
    extra_bin = max_dist > max_dist_metric
    n_bins += int(extra_bin)

    bins = [i * max_dist_metric / n_bins for i in range(0, n_bins + 1)]
    x_labels = [f"{x:.2f}" for x in bins]

    # Combine values outside of bin range in final bin
    dist = np.clip(dist, bins[0], bins[-1])

    # Generate histogram
    fig, axis = plt.subplots()
    fig.suptitle("Distance between consecutive corrections", fontweight='bold')
    axis.set_title(f'Total detected signatures: {corrections_total}')

    hist_counts, _, patches = axis.hist(dist, bins=bins, histtype='bar', rwidth=0.75, log=True)

    # Put values above max metric in the extra bin and show in red
    if extra_bin:
        x_labels[-1] = f"{max_dist:.2f}"
        patches[-1].set_facecolor('red')

    # Annotate percentages to each bin
    for i, n_corrections in enumerate(hist_counts):
        annotation = f"{n_corrections / corrections_total * 100:.2f}%"
        bin_center = (bins[i] + bins[i + 1]) / 2
        axis.text(bin_center, n_corrections, annotation,
                  horizontalalignment='center',
                  verticalalignment='baseline',
                  fontweight='bold')

    axis.set_xticks(bins)
    axis.set_xticklabels(x_labels)
    # Remove y ticks
    axis.set_yticks([])

    # Relabel the axis as "Frequency"
    axis.set_ylabel("Frequency (log scale)")
    axis.set_xlabel("Distance [m]")

    save_or_show(fig, output_folder, "distance_between_corrections_histogram.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_distance_between_corrections_histogram)
