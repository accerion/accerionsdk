/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "stream_parsing.h"

#include "AccerionSensorAPI/utils/string.h"


// To not clutter up the cli_command.cpp, the stream parsing helper functions are stored here.
namespace detail {

std::string GetString(std::istream& is, bool lower) {
    if (!is.good()) {
        return {};
    }
    std::string result;
    is >> result;
    if (lower) {
        accerion::api::ToLower(result);
    }
    return result;
}

ResultGet<std::string> GetQuotedString(std::istream& is) {
    constexpr auto kQuote = '\"';

    auto result = GetString(is, false);
    if (result.empty()) {
        // Empty string
        return {true, result};
    }

    if (result.front() == kQuote) {
        result.erase(result.begin());
    } else {
        // Plain string
        return {true, result};
    }
    if (result.back() == kQuote) {
        // Quoted string without spaces
        result.erase(std::prev(result.end()));
        return {true, result};
    }

    // Quoted string with spaces
    is.unsetf(std::ios_base::skipws);
    while (is && is.peek() != kQuote) {
        // Next character is not the expected quote, so just add it to the resulting string
        result.push_back(is.get());
    }

    if (!is || is.peek() != kQuote) {
        std::cout << "Invalid string, unpaired quote found." << std::endl;
        return {false, {}};
    }

    std::ignore = is.get();
    is.setf(std::ios_base::skipws);

    return {true, result};
}

}  // namespace detail
