#!/usr/bin/env python3
"""
 Copyright (c) 2021-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from enum import Enum
from typing import Callable, List, Tuple

from sdk.metrics.common import NO_AGGREGATION_DATA, parse_condition, Result


class MetricDataAggregation:
    class Type(Enum):
        Minimum = 'minimum'
        Maximum = 'maximum'
        Range = 'range'
        Average = 'average'
        Percentile = 'percentile'

    def __init__(self, aggregation_str: str):
        self.__str = aggregation_str.lower()
        self.__type, self.__func = self.__parse(self.__str)

    def __repr__(self):
        return self.__str

    @property
    def type(self):
        return self.__type

    def __call__(self, data: List[float]) -> float:
        if data is None or len(data) == 0:
            return NO_AGGREGATION_DATA
        return self.__func(data)

    @staticmethod
    def __parse(s: str) -> Tuple[Type, Callable[[List[float]], float]]:
        if s in ['min', 'minimum']:
            return MetricDataAggregation.Type.Minimum, min
        elif s in ['max', 'maximum']:
            return MetricDataAggregation.Type.Maximum, max
        elif s in ['range']:
            return MetricDataAggregation.Type.Range, lambda l: max(l) - min(l)
        elif s in ['mean', 'avg', 'average']:
            return MetricDataAggregation.Type.Average, lambda l: sum(l) / len(l)
        elif s.startswith('percentile'):
            parts = s.split(' ', 1)
            assert len(parts) == 2, "Percentile should specify a condition as well"
            condition_function = parse_condition(parts[1])[1]
            return MetricDataAggregation.Type.Percentile, \
                   lambda l: len([1 for i in l if condition_function(i) == Result.Pass]) / len(l) * 100.0  # percentage
        else:
            raise Exception(f"Unknown aggregation specifier '{s}'")
