/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionUtilities/string_parser.h"

TEST(AccerionUtilities, ParseConnectionType) {
    std::vector<std::string> test_values {"tcp", "tcp_no_udp", "udp_broad", "udp_uni", "udp_uni_no_hb", "tcp_", "TCP"};
    std::vector<accerion::api::ConnectionType> expected_value {accerion::api::ConnectionType::kConnectionTcp,
                                                               accerion::api::ConnectionType::kConnectionTcpDisableUdp,
                                                               accerion::api::ConnectionType::kConnectionUdpBroadcast,
                                                               accerion::api::ConnectionType::kConnectionUdpUnicast,
                                                               accerion::api::ConnectionType::kConnectionUdpUnicastNoHb};
    std::vector<bool> expected_value_bool;
    expected_value_bool.insert(expected_value_bool.end(), expected_value.size(), true);
    expected_value_bool.insert(expected_value_bool.end(), (test_values.size() - expected_value.size()), false);

    for (int i; i < test_values.size(); i++) {
        auto result = ParseConnectionType(test_values[i]);
        if (result && expected_value_bool[i]) {
            EXPECT_EQ(expected_value[i], result.value());
        } else {
            EXPECT_FALSE(result);
        }
    }
}