#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from pathlib import Path
from typing import List

from sdk.csv.reader import read
from sdk.csv.versions import Version, determine_version, get_supported_version_module
from sdk.data.pose import Pose
from sdk.data.signature import Signature
from sdk.map.map import Map
from sdk.utils.path import path_list


__all__ = ["SignatureReader"]


class SignatureReader:
    _Map = Map

    @classmethod
    def read(cls, paths: List[Path]) -> List[Signature]:
        """
        Create a (potentially empty) list of signatures from either the map database .db file or the floor map
        coordinates .csv file. If both are available, then the information from the database will be used.
        """
        result = []

        paths = path_list(paths)
        db_paths = [p for p in paths if p.suffix == ".db"]
        if db_paths:
            result = cls._Map(db_paths[0]).get_all_signatures(Map.Info.POSE)

        if result:
            return result

        # Arams-version-based csv reading
        version = determine_version(paths)
        version_module = get_supported_version_module(version)
        fmc = version_module.FMC
        paths = filter(lambda f: fmc.can_parse(str(f)), paths)
        for path in paths:
            df = read(path, fmc)
            if df is None:
                continue

            for _, row in df.iterrows():
                signature = Signature(id=row[fmc.SIGNATURE_ID],
                                      cluster_id=row[fmc.CLUSTER_ID],
                                      index_in_cluster=None,
                                      pose=Pose(row[fmc.SIG_POS_X], row[fmc.SIG_POS_Y], 0))
                if version >= Version("7.0"):
                    signature.pose.th = row[fmc.SIG_POSE_TH]
                result.append(signature)

            if result:
                break

        return result
