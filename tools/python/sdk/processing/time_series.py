#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from bisect import bisect_left
from dataclasses import dataclass
from typing import Any, List


class TimeSeries:
    """
    Create a time series of some property, such that time-based look up is possible.
    If the time value is not exactly present in the series, then interpolation is performed
    """

    def __init__(self, times: List[float], values: List[Any]):
        assert len(times) > 0
        assert len(times) == len(values)
        non_monotonic_timestamps = [times[i + 1] for i in range(len(times) - 1) if times[i] > times[i + 1]]
        assert len(non_monotonic_timestamps) == 0, f"Times are not sorted, example {non_monotonic_timestamps[0]}."

        self.__times = times
        self.__values = values

    @dataclass
    class _LookUpResult:
        ratio: float
        value_before: Any
        value_after: Any

    def __getitem__(self, t: float) -> _LookUpResult:
        if t <= self.__times[0]:
            return self._LookUpResult(0, self.__values[0], None)
        elif t >= self.__times[-1]:
            return self._LookUpResult(1, None, self.__values[-1])

        index_before = bisect_left(self.__times, t) - 1
        index_after = index_before + 1

        time_before = self.__times[index_before]
        value_before = self.__values[index_before]
        time_after = self.__times[index_after]
        value_after = self.__values[index_after]

        if t == time_before:
            return self._LookUpResult(0, value_before, None)
        elif t == time_after:
            return self._LookUpResult(1, None, value_after)

        # To be interpolated
        ratio = (t - time_before) / (time_after - time_before)
        return self._LookUpResult(ratio, value_before, value_after)
