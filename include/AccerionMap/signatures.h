/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once
#include <optional>
#include "AccerionDatabaseHandling/database.h"
#include "AccerionSensorAPI/structs/signature.h"

namespace accerion {
namespace map {

std::vector<accerion::api::Signature> GetSignaturesFromDatabase(accerion::sqlite::Database& db);

}  //  namespace map
}  //  namespace accerion