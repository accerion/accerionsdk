/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include <fstream>
#include <string>

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionUtilities/csv_reader.h"
#include "AccerionUtilities/csv_writer.h"

static std::string GetLineTrimmed(std::istream &file_handle) {
    std::string line;
    std::getline(file_handle, line);
    // Trim line read to remove '\r\n' on Windows
    std::stringstream(line) >> line;
    return line;
}

void WriteDummyCSVFile(const std::string &file_path) {
    std::vector<std::string> headers{"signature_id", "cluster_id", "status",
                                        "pos_x[m]", "pos_y[m]",
                                        "std_dev_pos_x[m]", "std_dev_pos_y[m]"};
    accerion::utilities::CsvWriter<int, double, double, std::string, int, std::string,
                                    std::string>
                                    writer(file_path, headers, ',');

    writer.WriteLine(1, 2.1, 3.5, "4", 5, "6", "7");
    writer.WriteLine(8, 9.1, 10.5, "11", 12, "13", "14");
    writer.WriteLine(15, 16, 17, "18", 19, "", "test");
    writer.CloseFile();
}

void RemoveFile(const std::string &file_path) {
    if (accerion::api::FileExists(file_path)) {
        EXPECT_TRUE(accerion::api::RemoveFile(file_path));
    }
}

TEST(AccerionUtilities, WriteCsvFile) {

    constexpr auto test_file_path = "test_csv_file.csv";
    WriteDummyCSVFile(test_file_path);
    
    std::ifstream file_handle(test_file_path, std::ios::binary);
    EXPECT_TRUE(file_handle.is_open());

    constexpr auto header = "signature_id,cluster_id,status,pos_x[m],pos_y[m],std_dev_pos_x[m],std_dev_pos_y[m]";
    constexpr auto expected_line = "1,2.1,3.5,4,5,6,7";

    EXPECT_EQ(header, GetLineTrimmed(file_handle));
    EXPECT_EQ(expected_line, GetLineTrimmed(file_handle));

    file_handle.close();
    RemoveFile(test_file_path);
}

TEST(AccerionUtilities, ReadHeaderAndSingleLineFromCsvFile) {
    constexpr auto test_file_path = "test_csv_file.csv";
    WriteDummyCSVFile(test_file_path);

    accerion::utilities::CsvReader<int, double, double, int, int, std::string, std::string>
                                                    reader(test_file_path, ',');
    std::vector<std::string> csv_header =
                            reader.ReadHeader();
    
    std::tuple<int, double, double, int, int, std::string, std::string> line1;
    std::tuple<int, double, double, int, int, std::string, std::string> line2;
    reader.ReadLine(line1);
    reader.ReadLine(line2);

    EXPECT_EQ("signature_id",       csv_header[0]);
    EXPECT_EQ("cluster_id",         csv_header[1]);
    EXPECT_EQ("status",             csv_header[2]);
    EXPECT_EQ("pos_x[m]",           csv_header[3]);
    EXPECT_EQ("pos_y[m]",           csv_header[4]);
    EXPECT_EQ("std_dev_pos_x[m]",   csv_header[5]);
    EXPECT_EQ("std_dev_pos_y[m]",   csv_header[6]);

    EXPECT_EQ(1, std::get<0>(line1));
    EXPECT_EQ(2.1, std::get<1>(line1));
    EXPECT_EQ(3.5, std::get<2>(line1));
    EXPECT_EQ(4, std::get<3>(line1));
    EXPECT_EQ(5, std::get<4>(line1));
    EXPECT_EQ("6", std::get<5>(line1));
    EXPECT_EQ("7", std::get<6>(line1));

    EXPECT_EQ(8, std::get<0>(line2));
    EXPECT_EQ(9.1, std::get<1>(line2));
    EXPECT_EQ(10.5, std::get<2>(line2));
    EXPECT_EQ(11, std::get<3>(line2));
    EXPECT_EQ(12, std::get<4>(line2));
    EXPECT_EQ("13", std::get<5>(line2));
    EXPECT_EQ("14", std::get<6>(line2));

    reader.CloseFile();
    RemoveFile(test_file_path);
}

TEST(AccerionUtilities, ReadAllLinesFromCsvFile) {
    constexpr auto test_file_path = "test_csv_file.csv";
    WriteDummyCSVFile(test_file_path);

    accerion::utilities::CsvReader<int, double, double, int, int, std::string, std::string>
                                                reader(test_file_path, ',');
    auto lines = reader.ReadLines();
    
    EXPECT_EQ(lines.size(), 3);
    EXPECT_EQ(1,    std::get<0>(lines[0]));
    EXPECT_EQ(2.1,  std::get<1>(lines[0]));
    EXPECT_EQ(3.5,  std::get<2>(lines[0]));
    EXPECT_EQ(4,    std::get<3>(lines[0]));
    EXPECT_EQ(5,    std::get<4>(lines[0]));
    EXPECT_EQ("6",  std::get<5>(lines[0]));
    EXPECT_EQ("7",  std::get<6>(lines[0]));

    EXPECT_EQ(8,    std::get<0>(lines[1]));
    EXPECT_EQ(9.1,  std::get<1>(lines[1]));
    EXPECT_EQ(10.5, std::get<2>(lines[1]));
    EXPECT_EQ(11,   std::get<3>(lines[1]));
    EXPECT_EQ(12,   std::get<4>(lines[1]));
    EXPECT_EQ("13", std::get<5>(lines[1]));
    EXPECT_EQ("14", std::get<6>(lines[1]));

    EXPECT_EQ(15,   std::get<0>(lines[2]));
    EXPECT_EQ(16,   std::get<1>(lines[2]));
    EXPECT_EQ(17,   std::get<2>(lines[2]));
    EXPECT_EQ(18,   std::get<3>(lines[2]));
    EXPECT_EQ(19,   std::get<4>(lines[2]));
    EXPECT_EQ("",   std::get<5>(lines[2]));
    EXPECT_EQ("test",   std::get<6>(lines[2]));

    reader.CloseFile();
    RemoveFile(test_file_path);
}

TEST(AccerionUtilties, TestReadFileWithEmptyField) {
    constexpr auto test_file_path = "test_csv_file.csv";
    WriteDummyCSVFile(test_file_path);
    accerion::utilities::CsvReader<int, double, double, int, int, int,
                                    std::string> reader(test_file_path, ',');
    EXPECT_ANY_THROW(reader.ReadLines());
    reader.CloseFile();
    RemoveFile(test_file_path);
}
