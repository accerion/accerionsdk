/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <vector>

#include "Eigen/Core"

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/created_signature.h"

namespace accerion {

namespace pgo {

class PoseGraph {
 public:
    enum class NodeType { kUnknown, kSignature, kAbsolute, kFixed, kInterpolated };
    enum class EdgeType { kUnknown, kRelative, kAbsolute, kLoopClosure, kIdentity };

    struct Node {
        api::SignatureId id;
        api::Pose_<double, false> pose;
        NodeType type;
    };

    struct Edge {
        api::SignatureId id_a;
        api::SignatureId id_b;
        api::Pose_<double, false> offset;
        Eigen::Matrix3d info_matrix;
        EdgeType type;
    };

    std::vector<api::SignatureId> fixed_node_ids;
    std::vector<Edge> edges;
    std::vector<Node> nodes;
};

std::ostream& operator<<(std::ostream& os, PoseGraph::NodeType node_type);
std::istream& operator>>(std::istream& is, PoseGraph::NodeType& node_type);

std::ostream& operator<<(std::ostream& os, PoseGraph::EdgeType edge_type);
std::istream& operator>>(std::istream& is, PoseGraph::EdgeType& edge_type);

}  // namespace pgo

}  // namespace accerion
