/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cstddef>

#include "gtest/gtest.h"

#include "AccerionDatabaseHandling/wal_mode.h"
#include "AccerionSensorAPI/utils/units.h"
#include "AccerionDatabaseHandling/prepared_statement.h"
#include "AccerionDatabaseHandling/database.h"
#include "AccerionUtilities/temp_file_system.h"

namespace detail {

accerion::sqlite::Database CreateDatabase(const std::string& path) {
    accerion::sqlite::Database db {path, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE};

    db.Execute(R"SQL(
                CREATE TABLE IF NOT EXISTS data (
                    id              INTEGER PRIMARY KEY,
                    blob            BLOB
                );
            )SQL");

    db.Execute(R"SQL(
                INSERT INTO data
                       (id, blob)
                VALUES (0,  NULL);
            )SQL");
    return db;
}

accerion::sqlite::PreparedStatement CreateStatement(accerion::sqlite::Database& db) {
    return db.PrepareStatement(R"SQL(
                UPDATE data
                   SET blob = ?2
                 WHERE id = ?1;
            )SQL");
}

}  // namespace detail

class SqliteWalCheckpointerTest : public ::testing::Test {
 protected:
    SqliteWalCheckpointerTest()
        : temp_files_{}, db_{detail::CreateDatabase(temp_files_.Create())}, stmt_{detail::CreateStatement(db_)} {
        // Database configuration expected by 'WalCheckpointer'
        db_.Execute("PRAGMA journal_mode = WAL;");
    }

    void WriteBlob(int id, const std::vector<std::byte>& bytes) {
        const auto bind_blob {accerion::sqlite::PreparedStatement::BindBlob::Create(bytes.data(), bytes.size())};

        stmt_.Bind(1, id);
        stmt_.Bind(2, bind_blob);
        stmt_.Step();
        stmt_.Reset();
    }

    TempFileSystem temp_files_;
    accerion::sqlite::Database db_;

 private:
    // PreparedStatement to update blob data
    accerion::sqlite::PreparedStatement stmt_;
};

TEST_F(SqliteWalCheckpointerTest, WalFileIsCheckedIn) {
    std::vector<std::byte> bytes(accerion::api::kKiloByte);

    accerion::sqlite::WalCheckpointer checkptr {db_};

    constexpr auto kIterations = 10;
    for (std::uint8_t i = 0; i < kIterations; ++i) {
        // Modify previous blob, to add entry within Wal file
        bytes.push_back(std::byte{i});
        WriteBlob(0, bytes);

        std::promise<std::optional<accerion::sqlite::WalCheckpointer::Info>> promise;
        auto future = promise.get_future();
        checkptr.AttemptToCheckpoint([&promise](const std::optional<accerion::sqlite::WalCheckpointer::Info>&info) {
                                                promise.set_value(info);
                                                });

        ASSERT_EQ(future.wait_for(std::chrono::milliseconds(100)), std::future_status::ready)
            << "Checkpointing timed out or task not scheduled";

        const auto opt_info = future.get();
        ASSERT_TRUE(opt_info);
        EXPECT_EQ(opt_info.value().pages_total, 1);
        EXPECT_EQ(opt_info.value().pages_checkpointed, 1);
    }
}

TEST_F(SqliteWalCheckpointerTest, CheckpointCompletionWithinLifetime) {
    std::atomic<bool> task_done {false};
    {
        accerion::sqlite::WalCheckpointer checkpointer(db_);

        const std::vector<std::byte> bytes(accerion::api::kKiloByte);
        WriteBlob(0, bytes);

        checkpointer.AttemptToCheckpoint([&task_done](const auto) { task_done = true; });
    }
    ASSERT_TRUE(task_done);
}

TEST_F(SqliteWalCheckpointerTest, DatabaseNotInWalMode) {
    const auto journal_mode = db_.ExecuteAndGetValue<std::string>("PRAGMA journal_mode = DELETE;");
    ASSERT_EQ(journal_mode, "delete");

    EXPECT_ANY_THROW(accerion::sqlite::WalCheckpointer {db_});
}
