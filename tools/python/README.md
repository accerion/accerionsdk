# Python tools
Please use `python3 -m <(sub)module name> -h` to get information on the input needed per script.
Note that scripts need to be executed from the `.../tools/python` folder.

## Plotting scripts
Call the scripts using `python3 -m sdk.plotting.<script name>`.

When an output folder is supplied, then the plots will be saved as files in that folder. If not, then the plots will be
shown immediately in an interactive window. When plots are saved, the argument `prefix` controls the file naming. The
argument can be used in the following ways:
* Omitting the flag will give files the default generic name like `residual_errors_g2o_map.png`.
* When the flag is provided without a value, two prefixed can be used:
  * The input file paths will be searched for the sensor-timestamp prefix (like `598100475-22-10-2021-08-53`). When this
prefix is found, then it will be used.
  * In absense of a sensor-timestamp prefix, a default timestamp prefix will be generated, resulting in a file name like
`22-10-2021-08-53_residual_errors_g2o_map.png`.
* When the flag is provided with a value, then the name will have specified prefix like
`prefix_residual_errors_g2o_map.png` when `prefix` is provided as part of the argument.

To skip the first n records for the drift corrections, then add the `--skip <n>` argument.

Histograms and colorbars use limits from the `test_template.yml` inside the `resources` folder. A user can modify this
file or create its own an pass this along with the `-s` flag.

Below examples are shown for the figures that can be created using the scripts in the plotting folder.
### Map quality

#### plot_map_visualization.py
* Input: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to visualize a map, where every cluster has a distinct color. 

| Map Visualization                        |
|------------------------------------------|
| ![Map](sdk/images/map_visualization.png) |

#### plot_mapping_direction.py
* Input: floor map coordinates (or .db map file for v7.0.0 and higher) 
* Use to visualize a map, where color indicates start and end of clusters (based on index_in_cluster if available, else signature id). 

| Map Visualization                        |
|------------------------------------------|
| ![Map](sdk/images/mapping_direction.png) |

#### plot_distance_between_signatures_{map,histogram,histogram_cum}.py
* Input: floor map coordinates (or .db map file for v7.0.0 and higher) 
* Use to check if distance between consecutive signatures is acceptable.

| Map Visualization                                      | Histogram                                    | Cumulative Histogram                        |
|--------------------------------------------------------|---------------------------------------------|---------------------------------------------|
| ![Map](sdk/images/distance_between_signatures_map.png) | ![Histogram](sdk/images/distance_between_signatures_histogram.png) | ![Cumulative Histogram](sdk/images/distance_between_signatures_histogram_cum.png) |

#### plot_residual_position_errors_g2o.py
* Input: .g2o file with loop closures, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check residual position errors in map based on Triton loop closures. 

| Map Visualization                                | Histogram                     |
|---------------------------------------------------|-----------------------------------------------|
| ![Map](sdk/images/residual_position_errors_g2o_map.png) | ![Histogram](sdk/images/residual_position_errors_g2o_hist.png) |

#### plot_residual_heading_errors_g2o.py
* Input: .g2o file with loop closures, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check residual heading errors in map based on Triton loop closures. 

| Map Visualization                                | Histogram                                                     |
|---------------------------------------------------|---------------------------------------------------------------|
| ![Map](sdk/images/residual_heading_errors_g2o_map.png) | ![Histogram](sdk/images/residual_heading_errors_g2o_hist.png) |

---

### Localization

#### plot_distance_between_corrections_{map,histogram}.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check distance between consecutive drift corrections.

| Map Visualization                          | Histogram               |
|---------------------------------------------------|----------------------------------------------|
| ![Map](sdk/images/distance_between_corrections_map.png) | ![Histogram](sdk/images/distance_between_corrections_histogram.png) |

#### plot_position_corrections_{map,histogram}.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check to position error between external reference pose and drift correction.
* Large values can be caused by for example inaccuracies in the map or inaccurate external reference pose.

| Map Visualization                       | Histogram               |
|---------------------------------------------------|-------------------------------------------|
| ![Map](sdk/images/position_corrections_map.png) | ![Histogram](sdk/images/position_corrections_histogram.png) |

#### plot_heading_corrections_{map,histogram,heatmap}.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check to heading error between external reference pose and drift correction.
* Large values can be caused by for example inaccuracies in the map or inaccurate external reference pose.

| Map Visualization                           | Histogram                | Heatmap                 |
|---------------------------------------------------|----------------------------------------------|--------------------------------------------|
| ![Map](sdk/images/heading_corrections_map.png) | ![Histogram](sdk/images/heading_corrections_histogram.png) | ![Heatmap](sdk/images/heading_corrections_heatmap.png) |

#### plot_correction_count_{map,histogram,heatmap}.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to check the number of drift corrections when crossing a line segment perpendicularly. Works best for maps containing straight lines.
* Values are influenced by a range of factors, such as the search radius, velocity and compatibility of the floor. 

 
| Map Visualization                              | Histogram                   | Heatmap                    |
|---------------------------------------------------|---------------------------------------------|---------------------------------------------|
| ![Map](sdk/images/correction_count_map.png) | ![Histogram](sdk/images/correction_count_histogram.png) | ![Heatmap](sdk/images/correction_count_heatmap.png) |

#### plot_sensor_poses.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to show external reference pose and corrections, optionally with the map displayed in the background.


| Map Visualization                             |
|---------------------------------------------------|
| ![Map](sdk/images/map_with_sensor_poses.png) |

#### plot_external_reference_timeseries.py
* Input: arams users logs folder, optional: floor map coordinates (or .db map file for v7.0.0 and higher)
* Use to show external reference pose, corrections and Triton velocities as function of time. 

| Timeseries                    |
|--------------------------------------------------|
| ![Map](sdk/images/external_reference_timeseries.png) |


## Pose graph module
The main functionality is in `optimize_graph.py`. It can be used like
```Bash
python3 -m sdk.graph.optimize_graph \
  -i "/.../graph.g2o" \
  -o "/.../graph_optimized.g2o"
```
This script will apply the following validations:
- Duplicate edges: When an edge is present twice in the pose graph (two edges having the same id_begin and id_end), then
  only the first edge is kept (regardless of the remainder of the content in the edge record).
- Connectivity: When there are multiple disconnected parts in the graph, then a part is removed when it does not contain
  a fixed pose (in absence of fixed poses, then the first pose of the complete graph is taken to be fixed).

By default, when the graph is not valid, the script will ask the user whether the graph should be corrected.
To automatically let the script correct it, the `-y` option can be added when calling the script.

The script also contains a loop closure edge outlier rejection mechanism, but it is still under development. It can be
turned on using the `-r` flag, but by default it is not applied. The mechanism follows the next steps to identify
outliers:
- Select candidate loop closure edges when the residual is above a certain threshold.
- For each candidate:
  - A maximum number of neighbouring loop closure edges within a certain distance will be searched for.
  - Calculate the average edge residual of all neighbours combined.
  - Calculate the ratio of the candidate edge residual over the average neighbouring edge residual.
  - When the ratio is larger by a certain factor, then the candidate edge is marked as outlier.

When outlier rejection is turned on, still first the graph is optimized. When the outlier rejection then identifies
outliers, then these outliers are removed from the graph after which the graph is optimized again. This continues until
there are no outliers identified anymore.

The `-v` and `-d` flags can be used to toggle verbose and debug logging.

Currently, only the g2o format is supported.


## ARAMS module
Note that the input can be given as a list of files, but also a folder that would contain the files. However, the folder
will not be searched recursively. 

## Calculating metrics
The main script is `calculate_metrics.py`. It can be used like
```Bash
python3 -m metrics.calculate_metrics \
  -i "/.../arams_user_logs/" \
     "/.../floor_map_coordinates.csv" \
  -o "/.../test_results.yml"
```
This will use the default test specification file found under resources.
When a custom test specification should be used, then the command becomes
```Bash
python3 -m metrics.calculate_metrics \
  -i "/.../arams_user_logs/" \
     "/.../floor_map_coordinates.csv" \
  -s "/.../test_spec.yml" \
  -o "/.../test_results.yml"
```

Some more details about the test specification file found in the
[README file in the resources folder](sdk/resources/README.md).

To skip the first n records for the drift corrections, then add the `--skip <n>` argument.

## Generating reports
The main script for creating a report will do so based on a report template markdown file, `create_from_template.py` and
it can be used in the following way:
```Bash
python3 -m sdk.report.create_from_template \
  -t "/.../report.md" \
  -o "/.../results_multiple_runs/"
```
Note that there is no input data given. It is expected that all processing is handled in the template file. The template
file can refer to other files relative to where the report will be created as that will be the working directory.

For convenience also a default report can be generated for a single run using the following script:
```Bash
python3 -m sdk.report.create_single_run \
  -i "/.../floor_map_coordinates.csv" \
     "/.../arams_user_logs/" \
     "/.../pose_graph_optimized.g2o" \
  -o "/.../results_one_run/"
```
When calling this script, the tool will use a default template to create a report for one run. This default
template file can be found in the folder `resources`.

The single run script will calculate the metrics (calling functionality from `sdk/metrics/calculate_metrics.py`) and
generate all plots (calling functionality from `sdk/plotting/plot_all.py`) before generating a report. A default metrics
file will be used, unless a custom test file is provided using the `-s` argument is provided (same as for the
`calculate_metrics` script).

The script will store the plots in a dedicated folder called `plots` in the output directory. The test results yaml file
will be stored in the output folder. The file name of the output report pdf can be controlled using the `prefix`
argument as done for plots. Note however that the plots that are generated for the report will not have this template
(otherwise the report needs to know the prefix or start matching etc.).

In general, there is a configuration framework created to inject information into the template markdown file using an
external file. Some more details about the report template markdown file found in the
[README file in the resources folder](sdk/resources/README.md). 

In the current implementation, the compilation of the code chunks in the markdown file is independent of
the code that triggered the compilation. Therefore, in order to add information to the markdown compilation it is chosen
to add processing of an external configuration file. 

Configuration can be done by putting a 'conf.yml' file in results folder. The configuration file provides parameters
used for setting preferences for plotting, data processing and report generation. The default configuration is stored in
resources/conf_template.yml and it is formatted like
```yaml
skip initial drift corrections: 0
reuse results: false
omit passed results: false
```
Check [README file in the resources folder](sdk/resources/README.md) for format restrictions on the configuration file. 

Subsequently the information can be retrieved like
```python
conf = load_configuration(output_folder)
omit_pass = conf[Configuration.Name.OMIT_PASSED_RESULTS]
```
Note that the resources/conf_template.yml yaml file is the default configuration, which will be used when no file is
provided by the user. Regarding the configuration parameters, an explanation will be given for each, grouped on the
applicable domain:
* maps plotting
  * `overlay map`: When set to `true` and a csv file for the map coordinates is provided, it plots the map in the
    `distance_between_corrections_map`, `heading_corrections_map`, `position_corrections_map` plots.
  * ```yaml
    zoom to region of interest:
      mode: 'auto'
      xlim: [.0, .0] # [min, max]
      ylim: [.0, .0]
    ``` 
    The `zoom to region of interest` parameter allows the user to provide a region of interest to which the
    plot_distance_between_corrections, plot_heading_corrections, plot_map_with_sensor_poses and
    plot_position_corrections will be restricted. In addition, the `mode` subparameter takes the values `manual`, `auto`,
    `auto_equal` or `equal`. The `auto` mode limits x and y based on the min and max values in the data with added 0.5m
    space around the boundary data points. The `auto_equal` mode is a hybrid mode between `auto` and `equal` - the x and
    y limits will be fitted to the data but the aspect ratio will also be set to `equal`. This means the plot might not
    be tightly around the data but will adjust to the data while retaining the `equal` aspect ratio. 
    
    The `mode` values can be provided both with or without quotes i.e. both 'auto' and auto are valid. Based on that the
    following cases are distinguished:
    * ```yaml
      zoom to region of interest:
        mode: auto
      ``` 
      The x and y limits are determined based on the smallest and largest values in x and y data points. The parameters
      `xlim` and `ylim` can be omitted as in the example. The `auto` mode is the default setting of the configuration.
    * ```yaml
      zoom to region of interest:
        mode: 'manual'
        xlim: [0, 1]
        ylim: [-1, 1]
      ```
      The user provided `xlim` and `ylim` parameters are used for determining the x and y limits of the plots. 
    * ```yaml
      zoom to region of interest:
        mode: 'manual'
        xlim: [1, -1]
        ylim: [-1, 1]
      ``` 
      When the values in `xlim` and `ylim` are all set to 0 or are set to impossible values (see example), the values
      are ignored and the plots are plotted based on the map size if a map file is provided and plotted, or based on
      the x and y data points min/max values if no map is provided or plotted.
    * ```yaml
      zoom to region of interest:
        mode: 'equal'
      ```
      The aspect ratio between the x and y axis is set to equal.
  * `visualize outliers`: When set to `true` the color bar is restricted based on the minimum and/or maximum conditions
    specified in the `metrics_results.yml` file. Everything outside those limits is plotted as a cross with a color bar
    of its own. Limits not specified in the file are taken based on the min/max values in the displayed data.
  * `visualize first drift correction`: When set to `true` the location of the first drift correction will be circled.
* filter options
  * `plot last N runs`: Allows the user to specify the data for how many power cycles should be assessed. If set to `0`,
    the data from all power cycles will be processed. The timestamps of the drift corrections log and the tracking log
    (fast) will be compared to prevent mismatch between the data. The csv files are reset once they reach certain size
    and as the tracking log records data with higher frequency it will reach that size faster than the drift corrections
    log, which can cause the two files to fall out of synk. Currently only the last power cycle is filtered based on the
    `skip initial drift corrections` settings.
  * `plot time interval`: allows user to specify a time interval for which data will be loaded. If a list with start and end time is provided, those will be used.  
  If an int, float or list with single entry is provided with time T, it will load data from the last T milliseconds.

    The filtering based on this parameter is done outside the arams object initialization and before applying the
    plotting specific processing. 
* drift correction options
  * `skip initial drift corrections`: The number of initial drift correction to be skipped in the processing. As for
    some earlier scripts, the `create_single_run` accepts the `--skip <n>` argument to skip the first n records of the
    drift corrections. This values will also be written to the newly-created or already-present configuration file. (So
    the argument overrules the setting already present in a file.) In case of the `create_from_template` script, the
    only way is to use a configuration file or handle it in the template itself.
  * `first correction on line`: Allows the user to only show data from the first drift correction when crossing a line. 
    Relevant if you have a grid style map and only want to calculate metrics based on the first drift correction when
    crossing a line.
  * `drift correction error based on external reference`: Calculate position and heading errors by looking up external reference pose for 
    every drift correction. Interpolate between external reference poses if needed.
* correction count per crossing options 
  * `maximum signature ids per cluster crossing`: Specifies the maximum range (maximum - minimum) of signature ids to be
    detected by drift corrections, when crossing a cluster. Otherwise, these drift corrections are discarded in these
    plots.
  * `minimum distance between cluster crossings`: Specifies the minimum distance between the groups of drift corrections
    per cluster crossing. It is advised to keep have a value >= 10 cm, otherwise the distance could become smaller than
  the potential match range.
* report generation 
  * `reuse results`: Flag to reuse the metrics file and any plots when already present in the output directory.
  * `omit passed results`: In case of the single report generation, this flag indicates whether a section can be omitted
    from the report when its metric all passed.
  * `metrics`: Not to be provided by the user. Metrics to be used as already explained in this section.
  * `prefix`: Prefix to be prepended to names. Same behavior as with command line argument `prefix`, where `False`
    corresponds to not passing the `--prefix` flag and `True` to passing the flag without a value. Please refer to
    `Plotting scripts` section for further information on behavior. Can take the following values: false or False, true
    or True, string with user_value. Passing and empty string (i.e. '') is equivalent as only passing the flag without a
    value on the command line.

## Requirements
There are a couple of packages and configurations required to make the reporting work.
Besides the requirements.txt file, the following is required:
* Package wkhtmltopdf (`sudo apt install wkhtmltopdf`)
* Python3 version >= 3.7 (library dataclasses is used), this dependency is checked for in the code, so an assertion
  error will occur when the version does not match.

It is recommended to use the requirements file in combination with a virtual environment. More information can be found on
[this website](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#create-and-use-virtual-environments).


## Testing
The Python module can be tested using pytest. The tests are not run as part of a local build, but are run in GitLab. 

In order to be able to run the tests locally you need to install `pytest`. Furthermore, under windows you need to
install `wkhtmltopdf` (version 0.12.5), `awk` and `xpdf-utils`.

### Create a test
Follow the next steps in order to create a new test:
* Create a `test_<name>.py` in the `tests` directory. Note that this file prefix `test_` is mandatory.
* A test function named like `test_<name>` contains the tests to be performed. Note that this function prefix `test_` is mandatory.
  To actually perform tests, just use the regular python `assert` keyword.
* To use data in the tests, add the file (small only) to the `tests/data` folder and update the `tests/data/files.py` file.
  Then the `TestData` object from this file can be used to retrieve the file with correct full path.
* The new test will now automatically be picked up by the PyTest framework. The framework will recursively scan the test folder. 

For more details on the possibilities, check the PyTest documentation.

### Run all tests
There are different ways of executing the tests, but the most straightforward way is to use the following convenience script:
```shell
sdk/tools/python$ python3 -m sdk.tests.run_all_tests [-r results.xml]
```
When using an IDE like PyCharm, also then, by default, this script can be run directly.
Note that by default PyCharm adds the content roots to the `PYTHONPATH` variable. For more details see the implementation note in next section.

Note that the script can optionally output the results to a xml file using the `-r` option, which uses the PyTest argument `junitxml` internally.

### Implementation note
PyTest completely relies on the `PYTHONPATH` variable to be set. In the convenience script discussed above, it is set/updated for the user. 
Basically that script sets this variable and runs PyTest, like:
```shell
sdk/tools/python$ PYTHONPATH=`readlink -f .` pytest
```
Therefore, when directly calling `pytest` or running the tests from a different folder, then this variable needs to be set by the user, as in the following:
```shell
sdk$ PYTHONPATH="tools/python" python3 -m sdk.tests.run_all_tests
```
or:
```shell
sdk$ PYTHONPATH=`readlink -f tools/python` pytest
```
But note that the `PYTHONPATH` variable is required to have an absolute path (because of working directory changes by the testing framework).
