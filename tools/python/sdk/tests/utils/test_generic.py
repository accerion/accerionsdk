#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from typing import Any

from sdk.utils.generic import flatten


def test_flatten():
    @dataclass
    class Test:
        expectation: Any
        input: Any

    tests = [
        Test(0, []),
        Test(0, [[]]),
        Test(1, [0]),
        Test(1, [[0]]),
        Test(2, [0, 1]),
        Test(2, [[0, 1]]),
        Test(2, [0, [1]]),
        Test(2, [0, [[1]]]),
        Test(8, [0, 1, [2, 3], 4, [[5]], [6, [7]]])
    ]

    for index, test in enumerate(tests):
        assert list(flatten(test.input)) == list(range(test.expectation)), f"Test at index '{index}' failed"
