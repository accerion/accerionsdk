#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from packaging.version import Version
from typing import List, Optional, Tuple, Union, Any

import numpy as np
import numbers

import pandas as pd

from sdk.arams.arams import Arams


class TimeIntervalFilter:
    """
    Class that filters the data in an ARAMS class instance to only keep data between a specific time interval
    """
    @staticmethod
    def filter_all(arams: Arams, time_interval: list):
        file_types_str = ["LRT", "LDC"]

        if arams.get_version() >= Version("7.0.0"):  # LER did not have index column pre 7.0.0
            file_types_str.append("LER")

        file_types = [arams.get_type(t) for t in file_types_str if arams.has_type(t)]
        file_types = [t for t in file_types if t in arams]

        if isinstance(time_interval, numbers.Number):
            time_interval = [time_interval]

        assert len(time_interval) <= 2, "Invalid length of time interval list"  # List should be 1 or 2 values

        if len(time_interval) == 1:
            if time_interval[0] is not None:
                # Determine last time
                last_arams_time = 0
                for file_type in file_types:
                    arams_times = arams[file_type][arams.ARAMS_TIME]
                    last_arams_time = max(last_arams_time, arams_times.iloc[-1])
                time_interval = [last_arams_time - time_interval[0], None]
            else:
                time_interval = [time_interval[0], None]

        for file_type in file_types:
            TimeIntervalFilter.filter(arams, file_type, time_interval)

    @staticmethod
    def filter(arams: Arams, file_type, time_interval: list):
        if file_type not in arams:
            raise Exception("Filter data of supplied type not possible as data is not available.")

        data = arams[file_type]
        if len(time_interval) == 1:
            if time_interval[0] is not None:
                # Determine time_interval
                arams_times = arams[file_type][arams.ARAMS_TIME]
                last_arams_time = arams_times.iloc[-1]
                time_interval = [last_arams_time - time_interval[0], None]
            else:
                time_interval = [time_interval[0], None]

        if time_interval[0] is not None:
            data = data[time_interval[0] < data[arams.ARAMS_TIME]]
        if time_interval[1] is not None:
            data = data[data[arams.ARAMS_TIME] < time_interval[1]]

        arams[file_type] = data


class RegionFilter:
    @dataclass
    class Region:
        x_range: Optional[Tuple[float, float]] = None
        y_range: Optional[Tuple[float, float]] = None

        def contains(self, x: pd.Series, y: pd.Series) -> pd.Series:
            result = pd.Series(True, index=x.index)
            if self.x_range is not None:
                result &= (self.x_range[0] <= x) & (x <= self.x_range[1])
            if self.y_range is not None:
                result &= (self.y_range[0] <= y) & (y <= self.y_range[1])
            return result

    """
    Class that filters the data in an ARAMS class instance to only keep data within supplied regions.
    """
    @classmethod
    def filter_all(cls, arams: Arams, regions: Union[Region, List[Region]]):
        def get_type(type_str: str) -> Optional[Any]:
            if not arams.has_type(type_str):
                return None
            type_enum = arams.get_type(type_str)
            return type_enum if type_enum in arams else None

        LDC = get_type("LDC")
        if LDC is None:
            return
        LRT = get_type("LRT")
        if LRT is None:
            return

        if isinstance(regions, cls.Region):
            regions = [regions]

        arams[LDC] = cls.filter(arams[LDC], LDC.NEW_XPOS.value, LDC.NEW_YPOS.value, regions)
        arams[LRT] = cls.filter(arams[LRT], LRT.CORRECTED_ODOM_POSE_X.value, LRT.CORRECTED_ODOM_POSE_Y.value, regions)

    @classmethod
    def filter(cls, data: pd.DataFrame, x_col: str, y_col: str, regions: List[Region]) -> pd.DataFrame:
        assert isinstance(regions, list)

        mask = pd.Series(False, index=data.index)
        for region in regions:
            mask |= region.contains(data[x_col], data[y_col])
        return data[mask].copy()


class PowerCycleFilter:
    """
    Class that filters the data in an ARAMS class instance to only keep the Nth last power cycle data.
    """

    @classmethod
    def filter_all(cls, arams: Arams, requested_runs: int):
        file_types_str = ["LRT", "LDC"]
        if arams.get_version() >= Version("7.0.0"):  # LER did not have index column pre 7.0.0
            file_types_str.append("LER")
        file_types = [arams.get_type(t) for t in file_types_str if arams.has_type(t)]
        file_types = [t for t in file_types if t in arams]

        if len(file_types) == len(file_types_str):
            requested_runs = cls.__check_timestamp(arams, requested_runs, file_types)
        for file_type in file_types:
            cls.filter(arams, file_type, requested_runs)

    @classmethod
    def filter(cls, arams: Arams, file_type, requested_runs: int):
        if file_type not in arams:
            raise Exception("Filter data of supplied type not possible as data is not available.")

        if not any(e for e in file_type if e.value.lower() == "index"):
            raise Exception("No 'index' field in supplied type.")

        data = arams[file_type]
        index_data = data[arams.ARAMS_INDEX]
        power_cycle_indices = [len(index_data)]
        if requested_runs != 0:
            for _ in range(0, requested_runs):
                last_power_cycle_index = cls.__get_power_cycle_indices(index_data[:power_cycle_indices[-1]])[-1]
                power_cycle_indices.append(last_power_cycle_index)
            if power_cycle_indices != len(index_data):
                arams[file_type] = data.iloc[power_cycle_indices[-1]:]

    @staticmethod
    def __get_power_cycle_indices(data: pd.DataFrame):
        """
        Get the row indices in the column where the data makes a drop in value.
        For example, row index 345647 is returned for the following data:
            row index   data
            ...         ...
            345         9463.0
            346         694.0
            ...         ...
            345646      70560.0
            345647      2598.0          <===
            345648      2599.0
            ...         ...

        :param data: DataFrame containing data of one column
        """
        data = np.array(data)
        data_diff = np.diff(data)
        row_indices = np.where(data_diff < 0)[0].tolist()

        # Note that the + 1 is required as the row index was determined based on the data difference list which has one
        # element less
        row_indices = [e+1 for e in row_indices]
        row_indices.insert(0, 0) 
        return row_indices 

    @classmethod
    def __check_timestamp(cls, arams: Arams, requested_runs: int, keys):
        runs_ind = {}
        runs_ind.fromkeys(keys) # = {e: [] for e in keys}
        for key in keys:
            if key not in arams:
                raise Exception("Filter data of supplied type not possible as data is not available.")
            index_data = arams[key][arams.ARAMS_INDEX]
            runs_ind[key] = cls.__get_power_cycle_indices(index_data)
        num_runs = min(len(runs_ind[keys[0]]), len(runs_ind[keys[1]])) 
        if requested_runs == 0 or num_runs < requested_runs:
            diff_ms = arams[keys[0]][arams.ARAMS_TIME].iloc[0] - arams[keys[1]][arams.ARAMS_TIME].iloc[0]
            step = 100 # if we don't skip won't finish looping
            if diff_ms/(1000*60) > 15:
                # if more than 15 min diff in timestamps find the closest matching timestamp and start from there
                time_lrt = arams[keys[0]][arams.ARAMS_TIME].iloc[runs_ind[keys[0]][-num_runs]::step]
                time_ldc = arams[keys[1]][arams.ARAMS_TIME].iloc[runs_ind[keys[1]][-num_runs]::step]
                for t_ind, t in enumerate(time_ldc):
                    diff_min = [abs(t - e)/(1000*60) for e in time_lrt]
                    res = [ind for ind, val in enumerate(diff_min) if val < 15]
                    if res:
                        arams[keys[0]] = arams[keys[0]].iloc[res[0]*step:]
                        arams[keys[1]] = arams[keys[1]].iloc[t_ind*step:]
                        break
                    
        elif len(runs_ind[keys[0]]) != len(runs_ind[keys[1]]):
            requested_runs = min(num_runs, requested_runs)
        
        return requested_runs
        

class InitialRecordFilter:
    """
    Class that filters the data in an ARAMS class instance which removes the initial n records.
    """

    @classmethod
    def filter_drift_corrections(cls, arams: Arams, n: int):
        file_type = "LDC"
        if file_type in arams:
            cls.filter(arams, arams.get_type(file_type), n)

    @staticmethod
    def filter(arams: Arams, file_type, n: int):
        if file_type not in arams:
            raise Exception("Filter data of supplied type not possible as data is not available.")

        arams[file_type] = arams[file_type].iloc[n:]


class InitialCorrectionFilter:
    """
    Class that filters the data in an ARAMS class instance which removes all but the first drift correction received
    when crossing a line
    """

    @classmethod
    def filter_unique_consecutive_corrections(cls, arams: Arams):
        file_type = "LDC"
        if file_type in arams:
            cls.filter(arams, arams.get_type(file_type))

    @staticmethod
    def filter(arams: Arams, file_type):
        """
        The first drift correction on a line is identified by a unique cluster ID
        """
        if file_type not in arams:
            raise Exception("Filter data of supplied type not possible as data is not available.")

        df = arams[file_type]

        cluster_id = df[file_type.CLUSTER_ID.value].to_numpy()
        temp = np.insert(cluster_id, 0, 0)
        cluster_id_diff = cluster_id[:] - temp[0:-1]

        unique_indices = np.where(np.abs(cluster_id_diff) != 0)

        arams[file_type] = arams[file_type].iloc[unique_indices]
