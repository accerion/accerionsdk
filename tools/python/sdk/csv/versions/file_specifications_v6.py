#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.csv.versions.base import FmcFileSpecification, LogFileSpecification


__all__ = ["ARAMS_INDEX", "ARAMS_TIME", "FMC", "FILES_SUPPORTED"]


ARAMS_INDEX = "index"
ARAMS_TIME = "time[ms]"

# Floor map coordinates .csv file column headers used
class FMC(FmcFileSpecification):
    SIGNATURE_ID = "idxInMap"
    CLUSTER_ID = "clusterID"
    SIG_STATUS = "sigStatus"
    SIG_POS_X = "sigPosX"
    SIG_POS_Y = "sigPosY"
    STD_DEV_POS_X = "stdDevPosX"
    STD_DEV_POS_Y = "stdDevPosY"


# Log runtime tracking .csv file column headers used
class LRT(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME
    
    STEP_TIME = "step_time[s]"
    IS_TRACKING_GOOD = "is_tracking_good[0-1]"
    INPUT_POSE_X  = "input_pos_x[m]"
    INPUT_POSE_Y  = "input_pos_y[m]"
    INPUT_POSE_TH = "input_pose_theta[deg]"
    CORRECTED_ODOM_POSE_X = "sensor_pos_x[m]"
    CORRECTED_ODOM_POSE_Y = "sensor_pos_y[m]"
    CORRECTED_ODOM_POSE_TH = "sensor_pos_theta[deg]"
    RAW_SENSOR_OFFSET_X  = "raw_sensor_offset_pos_x[m]"
    RAW_SENSOR_OFFSET_Y  = "raw_sensor_offset_pos_y[m]"
    RAW_SENSOR_OFFSET_TH = "raw_sensor_offset_pos_theta[deg]"
    ODOM_VEL_X = "raw_output_vel_x[m/s]"
    ODOM_VEL_Y = "raw_output_vel_y[m/s]"
    ODOM_VEL_TH = "raw_output_vel_theta[deg/s]"
    STD_DEV_X  = "std_dev_pos_x[m]"
    STD_DEV_Y  = "std_dev_pos_y[m]"
    STD_DEV_TH = "std_dev_pos_theta[deg]"

    @staticmethod
    def full_name():
        return "logRuntimeTrackingFast"


# Log drift corrections .csv file column headers used
class LDC(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME

    NEW_XPOS = "sensor_pos_x[m]"
    NEW_YPOS = "sensor_pos_y[m]"
    NEW_THETA = "sensor_pos_theta[deg]"
    ERROR_X = "error_x[m]"
    ERROR_Y = "error_y[m]"
    ERROR_THETA = "error_theta[deg]"
    CUMU_DIST = "cumulative_distance[m]"
    CUMU_YAW = "cumulative_yaw[deg]"
    ERROR_PERCENTAGE = "error_percentage[%]"
    CLUSTER_ID = "cluster_id"
    SIGNATURE_ID = "signature_id"

    @staticmethod
    def full_name():
        return "logDriftCorrections"


# Log external reference input .csv file column headers used
class LER(LogFileSpecification):
    TIME = ARAMS_TIME

    TIME_OFFSET = "time_offset(s)"
    INPUT_POSE_X = "input_pos_x[m]"
    INPUT_POSE_Y = "input_pos_y[m]"
    INPUT_POSE_TH = "input_pos_theta[deg]"
    ODOM_VEL_X = "raw_output_vel_x[m/s]"
    ODOM_VEL_Y = "raw_output_vel_y[m/s]"
    ODOM_VEL_TH = "raw_output_vel_theta[deg/s]"
    STD_DEV_POS_X = "std_dev_pos_x[m]"
    STD_DEV_POS_Y = "std_dev_pos_y[m]"
    STD_DEV_POS_THETA = "std_dev_pos_theta[deg]"

    @staticmethod
    def full_name():
        return "logExtRefInputFast"


# Log sensor details .csv file 
class LSD(LogFileSpecification):
    SOFTWARE_VERSION = "Software version"
    SERIAL_NUMBER = "Serial number"

    @staticmethod
    def full_name():
        return "log_sensor_details"


# List of supported ARAMS files
FILES_SUPPORTED = [LRT, LDC, LER, LSD]
