#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Union
from yaml import add_representer, dump

from sdk.metrics.metrics import Metrics
from sdk.utils.path import ensure_parent_directory


# Mechanism to be able to print strings with quotes, usage _Quoted(variable)
class _Quoted(str):
    @staticmethod
    def presenter(dumper, data):
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='"')
add_representer(_Quoted, _Quoted.presenter)


class Writer:
    @staticmethod
    def write(metrics: Metrics, file_name: Union[str, Path]):
        yaml = dict()
        for data, conditions in metrics.data_conditions.items():
            conditions_yaml = dict()
            for condition in conditions:
                condition_yaml = dict()
                condition_yaml["id"] = _Quoted(condition.name)
                condition_yaml["cond"] = _Quoted(condition.condition)
                condition_yaml["value"] = condition.aggregation_result
                condition_yaml["result"] = condition.condition_result.value
                conditions_yaml[str(condition.aggregation)] = condition_yaml
            yaml[data.tag.value] = conditions_yaml

        ensure_parent_directory(file_name)
        with open(file_name, 'w') as output_file:
            output_file.write(dump(yaml, indent=4, default_flow_style=False, sort_keys=False))
