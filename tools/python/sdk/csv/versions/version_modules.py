#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import os
from packaging.version import Version
from pathlib import Path
from typing import List, Union

from sdk.csv.reader import read_lsd
from sdk.csv.versions import file_specifications_v6 as file_spec_v6
from sdk.csv.versions import file_specifications_v61 as file_spec_v61
from sdk.csv.versions import file_specifications_v615 as file_spec_v615
from sdk.csv.versions import file_specifications_v700 as file_spec_v700


__all__ = ["Version", "get_supported_version_module", "get_latest_version", "determine_version"]


_version_modules = {
    Version("6.0.0"): file_spec_v6,
    Version("6.1.0"): file_spec_v61,
    Version("6.1.5"): file_spec_v615,
    Version("7.0.0"): file_spec_v700
}

assert all(all(f.__name__.isupper() for f in module.FILES_SUPPORTED) for module in _version_modules.values()), \
        "All file specifications are expected to be all caps."


def get_supported_version_module(version: Version):
    for supported_version, module in sorted(_version_modules.items(), reverse=True):
        if version >= supported_version:
            return module

    raise Exception(f"Unsupported version {version}.")


def get_latest_version() -> Version:
    return max(_version_modules.keys())


def determine_version(input_paths: Union[Path, List[Path]]) -> Version:
    """
    The version check is handled based on the existence of the log_sensor_details.csv file introduced for version 6.
    """
    if isinstance(input_paths, List):
        folders = [x for x in input_paths if os.path.isdir(x)]
    else:
        folders = [input_paths] if os.path.isdir(input_paths) else []

    latest_version = get_latest_version()

    # Option 1) Latest version (default)
    if len(folders) == 0:
        return latest_version
    # Option 2) Version from 'log_sensor_details'
    version_module = get_supported_version_module(latest_version)
    for folder in folders:
        for file_folder, _, files in os.walk(folder):
            sensor_details = [f for f in files if version_module.LSD.can_parse(f)]
            if sensor_details:
                assert len(sensor_details) == 1, "Only a single log_sensor_details file expected."
                data_dict = read_lsd(os.path.join(file_folder, sensor_details[0]))
                return Version(data_dict["Software version"])
    # Option 3) Version without 'log_sensor_details' (unsupported version before version 6.0)
    raise Exception("Unsupported version (before 6.0).")
