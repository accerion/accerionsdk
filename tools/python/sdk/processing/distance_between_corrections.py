#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from typing import List, Tuple

import numpy as np
from sdk.arams.arams import Arams
from sdk.utils.print import print_v


def get_distance_between_corrections(arams: Arams, *, verbose: bool = True) -> Tuple[List[float], List[float], List[float]]:
    if "LDC" not in arams:
        print_v(verbose, __name__,
                ": Could not find logDriftCorrections file or the arams version does not support the file type.")
        return [], [], []

    LDC = arams.LDC
    df = arams[LDC]

    pos_x = df[LDC.NEW_XPOS].to_list()
    pos_y = df[LDC.NEW_YPOS].to_list()

    # Euclidean distance between consecutive corrections
    dist = np.sqrt(np.diff(pos_x) ** 2 + np.diff(pos_y) ** 2).tolist()

    return pos_x[1:], pos_y[1:], dist
