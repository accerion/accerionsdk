project(AccerionDatabaseHandling LANGUAGES CXX)

include(${CMAKE_CURRENT_SOURCE_DIR}/third_party/sqlite3.cmake)

add_library(${PROJECT_NAME}
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionDatabaseHandling/prepared_statement.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionDatabaseHandling/database.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionDatabaseHandling/integrity.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionDatabaseHandling/wal_mode.cpp)

target_link_libraries(${PROJECT_NAME} sqlite3 AccerionSensorAPI)

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

add_compiler_warnings(${PROJECT_NAME})
