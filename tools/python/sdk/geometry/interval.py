#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from typing import Optional


class Interval:
    def __init__(self, minimum: float, maximum: float):
        self.__min = minimum
        self.__max = maximum
        self.__norm()

    @property
    def min(self) -> float:
        return self.__min

    @min.setter
    def min(self, minimum: float):
        self.__min = minimum
        self.__norm()

    @property
    def max(self) -> float:
        return self.__max

    @property
    def length(self) -> float:
        return self.__max - self.__min

    @max.setter
    def max(self, maximum: float):
        self.__max = maximum
        self.__norm()

    def __eq__(self, other) -> bool:
        if isinstance(self, Interval):
            return self.min == other.min and self.max == other.max
        return False

    def __norm(self) -> None:
        if self.__min > self.__max:
            self.__min, self.__max = self.__max, self.__min

    def __contains__(self, value: float) -> bool:
        """Check if a value lies within the interval"""
        return self.min <= value <= self.max

    def include(self, value: float):
        self.__min = min(self.__min, value)
        self.__max = max(self.__max, value)

    def overlap(self, other: Interval) -> Optional[Interval]:
        if self.min > other.max or self.max < other.min:
            return None
        return Interval(max(self.min, other.min), min(self.max, other.max))
