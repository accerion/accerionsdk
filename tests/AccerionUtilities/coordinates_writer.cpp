/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include <fstream>
#include <string>
#include <vector>

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionUtilities/coordinates_writer.h"

static std::string GetLineTrimmed(std::istream &file_handle) {
    std::string line;
    std::getline(file_handle, line);
    // Trim line read to remove '\r\n' on Windows
    std::stringstream(line) >> line;
    return line;
}

TEST(AccerionUtilities, WriteSignatureInformation) {
    constexpr auto test_file_path = "test_floor_map_coordinates.csv";

    constexpr auto header =
        "signature_id,cluster_id,x[m],y[m],th[deg]";
    constexpr auto expected_line = "1,2,3,4,5";
    const accerion::api::Signature test_signature{{3, 4, 5}, 2, 1};

    const std::vector<accerion::api::Signature> signatures{test_signature};

    accerion::utilities::WriteCoordinates(signatures, test_file_path);

    std::ifstream file_handle(test_file_path, std::ios::binary);
    ASSERT_TRUE(file_handle.is_open());

    EXPECT_EQ(header, GetLineTrimmed(file_handle));
    EXPECT_EQ(expected_line, GetLineTrimmed(file_handle));

    file_handle.close();
    if (accerion::api::FileExists(test_file_path)) {
        ASSERT_TRUE(accerion::api::RemoveFile(test_file_path));
    }
}
