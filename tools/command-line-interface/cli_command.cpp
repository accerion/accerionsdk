/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "cli_command.h"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/file_transfer_task.h"
#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/software_details.h"
#include "AccerionSensorAPI/structs/version.h"
#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/sensor.h"
#include "AccerionSensorAPI/utils/string.h"
#include "AccerionSensorAPI/utils/optional.h"
#include "stream_parsing.h"

namespace detail {

template<typename T = uint8_t>
void PrintIdsWithRecordings(const std::vector<std::string>& recordings, std::vector<T> ids = {}) {
    std::cout << "    id    recording name         \n"
              << "    " << std::string(40, '-') << "\n";
    if (ids.empty()) {
        ids.resize(recordings.size());
        std::iota(ids.begin(), ids.end(), 0);
    }
    for (const auto id: ids) {
        std::cout << "    " << std::right << std::setw(2) << std::setfill(' ') << +id << "    " << recordings.at(id) << " \n";
    }
}

bool IsNumber(const std::string& s) {
    return std::all_of(s.cbegin(), s.cend(), isdigit);
}

}  // namespace detail

CliCommand::CliCommand(
        std::string n, std::vector<std::string> a, std::vector<std::string> u, std::vector<std::string> d)
        : name{std::move(n)}, alias{std::move(a)}, usage{std::move(u)}, description{std::move(d)} {}


CliCommandSetMode::CliCommandSetMode()
        : CliCommand{"setmode", {"sm"},
                     {"internal <on/off>",
                      "mapping <on/off> <cluster id, when enabling>",
                      "cont-sig-updating <on/off>",
                      "localizing <on/off>",
                      "line <on/off> <cluster id, when enabling>",
                      "recording <on/off>",
                      "aruco <on/off>",
                      "idle <on/off>",
                      "expert <on/off>"},
                     {"set internal mode",
                      "set mapping mode",
                      "set continuous signature updating mode",
                      "set localization mode",
                      "set line following mode",
                      "set recording mode",
                      "set aruco marker detection mode",
                      "set idle mode",
                      "set expert mode"}} {}

constexpr CliCommandId CliCommandSetMode::Id;  // C++17 won't require this

bool CliCommandSetMode::Execute(const BlockingSensor &sensor, std::istream &is) {
    // It is not documented, but actually toggling the reboot mode, is also possible.

    auto result_mode = detail::Get<BlockingSensor::Mode>(is);
    if (!result_mode.success) {
        return false;
    }
    auto result_enabled = detail::Get<bool>(is);
    if (!result_enabled.success) {
        return false;
    }

    if (result_mode.value == BlockingSensor::Mode::Mapping) {
        if (result_enabled.value) {
            auto result_id = detail::GetInt<std::uint16_t>(is);
            if (!result_id.success) {
                return false;
            }
            sensor.SetMappingOn(result_id.value);
        } else {
            sensor.SetMappingOff();
        }
    } else if (result_mode.value == BlockingSensor::Mode::LineFollowing) {
        if (result_enabled.value) {
            auto result_id = detail::GetInt<std::uint16_t>(is);
            if (!result_id.success) {
                return false;
            }
            sensor.SetLineFollowingOn(result_id.value);
        } else {
            sensor.SetLineFollowingOff();
        }
    } else {
        sensor.Set(result_mode.value, result_enabled.value);
    }

    // In case of reboot of sensor, stop the application
    return result_mode.value == BlockingSensor::Mode::Reboot;
}

CliCommandReboot::CliCommandReboot()
        : CliCommand{"reboot", {}, {}, {"reboot the sensor, cli will stop"}} {}

constexpr CliCommandId CliCommandReboot::Id;  // C++17 won't require this

bool CliCommandReboot::Execute(const BlockingSensor &sensor, std::istream &is) {
    sensor.Set(BlockingSensor::Mode::Reboot, true);
    // Stop the application
    return true;
}

CliCommandGetFile::CliCommandGetFile()
    : CliCommand{
        "getfile",
        {"gf"},
        {"<map/logs> [<output file/folder name/path>]",
         "coords [<output file/folder name/path>] [<cluster id>]",
         "loops [<output file/folder name/path>] <search radius [m]>",
         "recordings [<output file/folder name/path>] [<recording ids>]"},
        {"retrieve file",
         "retrieve map coordinates, default for all clusters",
         "retrieve loop closures",
         "retrieve recordings, default retrieve all recordings"}} {}

constexpr CliCommandId CliCommandGetFile::Id;  // C++17 won't require this

bool CliCommandGetFile::Execute(const BlockingSensor &sensor, std::istream &is) {
    // Determine the type of the file to be requested
    const auto result_type = detail::Get<accerion::api::FileType>(is);
    if (!result_type.success) {
        std::cout << "Unknown file type, expecting 'map', 'coords', 'logs', 'loops' or 'recordings'." << std::endl;
        return false;
    }
    const auto& file_type = result_type.value;

    // Determine the output file name
    auto result_file_name = detail::GetQuotedString(is);
    if (!result_file_name.success) {
        return false;
    }
    auto file_name = result_file_name.value;

    // Helper function
    auto is_csv_or_number_func = [](const std::string& s) -> bool {
        return std::all_of(s.cbegin(), s.cend(),
                           [](const auto c) { return isdigit(c) || ispunct(c); });
    };

    // Take into account that an option was potentially read as file name, therefore correct if that is the case
    std::string option{};
    if (!file_name.empty()) {
        // Check that supplied string it is not an additional option, such as:
        //  - search radius in case of loops
        //  - cluster id in case of floor map coordinates
        //  - recording id's list in case of recordings
        auto tmp = file_name;
        accerion::api::ToLower(tmp);
        const bool tmp_is_digit = detail::IsNumber(tmp);
        const bool tmp_is_csv_or_number = is_csv_or_number_func(tmp);
        if (file_type == accerion::api::FileType::LoopClosures && (tmp_is_csv_or_number) ||
            file_type == accerion::api::FileType::Coordinates && tmp_is_digit ||
            file_type == accerion::api::FileType::Recording && tmp_is_csv_or_number) {
            file_name.clear();
            option = std::move(tmp);
        }
    }

    if (file_name.empty()) {
        // No output file name supplied, so generate one.
        file_name += "./" + accerion::api::GenerateFileName(file_type, sensor.GetSerialNumber());
    } else {
        const auto supplied_extension = accerion::api::GetExtension(file_name);
        const auto& expected_extension = accerion::api::kFileExtensions.at(file_type);
        if (supplied_extension.empty()) {
            // Assume that output folder is given
            file_name += "/" + accerion::api::GenerateFileName(file_type, sensor.GetSerialNumber());
        } else if (supplied_extension != expected_extension) {
            std::cout << "The output file specified has wrong extension, expecting '" << expected_extension << "'."
                      << std::endl;
            return false;
        }
    }
    std::cout << "Output file name will be '" << file_name << "'." << std::endl;

    switch (file_type) {
        case accerion::api::FileType::Coordinates: {
            detail::ResultGet<accerion::api::ClusterId> result{};
            if (!option.empty()) {
                result.value = std::stoul(option);
                result.success = true;
            } else {
                result = detail::Get<accerion::api::ClusterId>(is);
            }
            auto opt_cluster_id = result.success ? accerion::api::Optional<accerion::api::ClusterId>{result.value}
                                                 : accerion::api::Optional<accerion::api::ClusterId>{};
            sensor.GetCoordinatesFile(file_name, opt_cluster_id);
            break;
        }
        case accerion::api::FileType::LoopClosures: {
            if (option.empty()) {
                option = detail::GetString(is, true);
            }
            if (!option.empty()) {
                if (is_csv_or_number_func(option)) {
                    sensor.GetLoopClosuresFile(file_name, std::stod(option));
                    break;
                }
            }
            std::cout << "A search radius needs to be supplied, i.e. 0.5" << std::endl;
            return false;
        }
        case accerion::api::FileType::Recording: {
            if (option.empty()) {
                auto result_get = detail::Get<std::string>(is);
                if (result_get.success) { option = std::move(result_get.value); }
            }
            const auto recordings = sensor.GetRecordings();
            auto requested_ids = accerion::api::StringToIntVector<uint8_t>(std::move(option));

            if (requested_ids.empty()) {
                // Request all recordings
                requested_ids.resize(recordings.size());
                std::iota(requested_ids.begin(), requested_ids.end(), 0);
            } else {
                // Remove requested ids that do not exist on the sensor
                requested_ids.erase(std::remove_if(requested_ids.begin(), requested_ids.end(),
                                                   [id_max = recordings.size() - 1](auto id) { return id > id_max; }),
                                    requested_ids.end());
                // Return in case none of the requested ids exist on the sensor
                if (requested_ids.empty()) {
                    std::cout << "Error, please enter list of valid recording ids from the overview below"
                              << " (e.g. '0,1,2')." << std::endl;
                    detail::PrintIdsWithRecordings(recordings);
                    return false;
                }
            }
            std::cout << "Retrieving following recordings: \n";
            detail::PrintIdsWithRecordings(recordings, requested_ids);
            sensor.GetRecordingsFile(file_name, std::move(recordings));
            break;
        }
        default: {
            sensor.GetFile(file_type, file_name);
            break;
        }
    }
    std::cout << "Output file name written to '" << file_name << "'." << std::endl;
    return false;
}

CliCommandSendFile::CliCommandSendFile()
        : CliCommand{"sendfile", {"sf"},
                     {"map <input file name/path> [<replace, merge, update>]",
                      "update <input file name/path>"},
                     {"send map, default strategy replace",
                      "send asu file firmware update"}} {}

constexpr CliCommandId CliCommandSendFile::Id;  // C++17 won't require this

bool CliCommandSendFile::Execute(const BlockingSensor &sensor, std::istream &is) {
    static const std::string kErrorMessage{"Unknown file type, expecting 'map' or 'update'."};

    auto result_type = detail::Get<accerion::api::FileType>(is);
    if (!result_type.success) {
        std::cout << kErrorMessage << std::endl;
        return false;
    }

    bool stop_application = false;

    auto result_file_name = detail::GetQuotedString(is);
    if (!result_file_name.success) {
        return false;
    }
    const auto& file_name = result_file_name.value;
    if (file_name.empty()) {
        std::cout << "Please provide input file name." << std::endl;
        return false;
    }

    switch (result_type.value) {
        case accerion::api::FileType::Map: {
            auto result_strategy = detail::Get<accerion::api::FileTransferTask>(is);
            if (result_strategy.success) {
                sensor.SendMapFile(file_name, result_strategy.value);
            }
            break;
        }
        case accerion::api::FileType::Update: {
            sensor.SendUpdateFile(file_name);
            stop_application = true;  // As the update will trigger a reboot, stop the app
        }
        default:
            break;
    }
    return stop_application;
}

CliCommandGet::CliCommandGet()
    : CliCommand{
        "get",
        {"g"},
        {"areas", "clusters", "diagnostics", "ip", "map-info [<file name/path>]", "recordings", "software"},
        {"get information"}} {}

constexpr CliCommandId CliCommandGet::Id;  // C++17 won't require this

bool CliCommandGet::Execute(const BlockingSensor &sensor, std::istream &is) {
    static const std::string kErrorMessage{"Unknown property, expecting 'areas', 'clusters', 'diagnostics', 'ip' or 'software'."};
    auto result_type = detail::Get<detail::PropertyType>(is);
    if (!result_type.success) {
        std::cout << kErrorMessage << std::endl;
        return false;
    }
    switch (result_type.value) {
        case detail::PropertyType::Areas: {
            const auto ids = sensor.GetAreaIds();
            std::cout << accerion::api::Join(ids.all_areas.cbegin(), ids.all_areas.cend(), ", ") << '\n'
                      << "  of which " << ids.active_area << " is active" << std::endl;
            break;
        }
        case detail::PropertyType::Clusters: {
            const auto ids = sensor.GetClusterIds();
            std::cout << accerion::api::Join(ids.cbegin(), ids.cend(), ", ") << std::endl;
            break;
        }
        case detail::PropertyType::Diagnostics: {
            std::cout << sensor.GetDiagnostics();
            break;
        }
        case detail::PropertyType::Ip: {
            auto ip_info = sensor.GetIpInfo();
            std::cout << "Ip information\n";
            std::cout << "  address: " << ip_info.address.AlignedString() << "\n";
            std::cout << "  netmask: " << ip_info.netmask.AlignedString() << "\n";
            std::cout << "  gateway: " << ip_info.gateway.AlignedString() << std::endl;
            break;
        }
        case detail::PropertyType::MapInfo: {
            const auto map_file_path = detail::GetQuotedString(is);
            if (!map_file_path.success) { return false; }

            if (map_file_path.value.empty()) {
                std::cout << sensor.GetMapSummary() << std::endl;
                break;
            }
            if (accerion::api::DoesStringEndWith(map_file_path.value, ".amf")) {
                std::cout << accerion::api::FileHeader::Unpack(map_file_path.value).header << std::endl;
                break;
            }
            std::cout << "This filetype does not have a header." << std::endl;
            break;
        }
        case detail::PropertyType::RecordingsList: {
            std::cout << "Recordings\n";
            detail::PrintIdsWithRecordings(sensor.GetRecordings());
            break;
        }
        case detail::PropertyType::Software: {
            const auto sw_info = sensor.GetSoftwareInfo();
            if (sw_info.first == accerion::api::SoftwareVersion{}) {
                std::cout << "No response" << std::endl;
            } else {
                std::cout << sw_info.first << ", " << sw_info.second << std::endl;
            }
            break;
        }
        default: {
            std::cout << kErrorMessage << std::endl;
            break;
        }
    }
    return false;
}

CliCommandSet::CliCommandSet()
        : CliCommand{"set", {"s"},
                     {"area <area id>",
                      "buffer <buffered recovery buffer length [m]>",
                      "connection <tcp/tcp_no_udp/udp_broad/udp_uni/udp_uni_no_hb> [<local ip>]",
                      "ip <address/netmask/gateway> <address> [...]",
                      "pose <x [m]> <y [m]> <theta [deg]>"},
                     {"set property value",
                      "",
                      "local ip is only needed for udp"}} {}

constexpr CliCommandId CliCommandSet::Id;  // C++17 won't require this

bool CliCommandSet::Execute(const BlockingSensor &sensor, std::istream &is) {
    static const std::string kErrorMessage{"Unknown property, expecting 'area', 'buffer', 'connection', 'ip' or 'pose'."};
    auto result_type = detail::Get<detail::PropertyType>(is);
    if (!result_type.success) {
        std::cout << kErrorMessage << std::endl;
        return false;
    }
    bool stop_app = false;
    switch (result_type.value) {
        case detail::PropertyType::Area: {
            auto result_area = detail::GetInt<accerion::api::AreaId>(is);
            if (result_area.success) {
                sensor.SetArea(result_area.value);
            } else {
                return stop_app;
            }
            break;
        }
        case detail::PropertyType::BufferLength: {
            auto result_length = detail::Get<float>(is);
            if (result_length.success) {
                sensor.SetBufferLength(result_length.value);
            } else {
                std::cout << "No buffer length supplied." << std::endl;
                return stop_app;
            }
            break;
        }
        case detail::PropertyType::Connection: {
            auto result_connection = detail::Get<accerion::api::ConnectionType>(is);
            if (result_connection.success) {
                if (result_connection.value == 
                    accerion::api::ConnectionType::kConnectionUdpUnicast ||
                    result_connection.value == 
                    accerion::api::ConnectionType::kConnectionUdpUnicastNoHb ||
                    result_connection.value == 
                    accerion::api::ConnectionType::kConnectionUdpBroadcast)
                {
                    auto ip_str = detail::GetString(is, true);
                    if (ip_str.empty()) {
                        std::cout << "No correct address supplied." << std::endl;
                        return stop_app;
                    }
                    const auto local_ip = accerion::api::Address::Parse(ip_str);
                    sensor.SetConnectionType(result_connection.value, local_ip);
                } else {
                    sensor.SetConnectionType(result_connection.value);
                }
            } else {
                std::cout << "No correct connection type supplied." << std::endl;
                return stop_app;
            }
            break;
        }
        case detail::PropertyType::Ip: {
            accerion::api::Optional<accerion::api::Address> address{};
            accerion::api::Optional<accerion::api::Address> netmask{};
            accerion::api::Optional<accerion::api::Address> gateway{};
            while (true) {
                const auto option = detail::GetString(is, true);
                if (option.empty()) {
                    break;
                }
                const auto new_value_string = detail::GetString(is, false);
                if (new_value_string.empty()) {
                    std::cout << "No correct address supplied." << std::endl;
                    return stop_app;
                }
                const auto new_value = accerion::api::Address::Parse(new_value_string);
                if (option == "address") {
                    address = new_value;
                    stop_app = true;
                } else if (option == "netmask") {
                    netmask = new_value;
                } else if (option == "gateway") {
                    gateway = new_value;
                } else {
                    std::cout << "No correct ip option supplied, should be 'address', 'netmask' or 'gateway'." << std::endl;
                    return stop_app;
                }
            }

            if (!address && !netmask && !gateway) {
                std::cout << "No ip options supplied." << std::endl;
                return stop_app;
            }
            sensor.SetIpInfo(address, netmask, gateway);
            stop_app = true;
            break;
        }
        case detail::PropertyType::Pose: {
            auto result_pose = detail::Get<accerion::api::Pose>(is);
            if (result_pose.success) {
                sensor.SetPose(result_pose.value);
            } else {
                return stop_app;
            }
            break;
        }
        default: {
            std::cout << kErrorMessage << std::endl;
            return stop_app;
        }
    }
    std::cout << "Property set." << std::endl;
    if (stop_app) {
        std::cout << "Application will be stopped because of change." << std::endl;
    }
    return stop_app;
}

CliCommandBufferedRecovery::CliCommandBufferedRecovery()
        : CliCommand{"recover", {}, {"<x [m]> <y [m]> [<radius [m]>]"},
                     {"perform buffered recovery, no radius means full map"}} {}

constexpr CliCommandId CliCommandBufferedRecovery::Id;  // C++17 won't require this

bool CliCommandBufferedRecovery::Execute(const BlockingSensor &sensor, std::istream &is) {
    auto result_x = detail::Get<double>(is);
    auto result_y = detail::Get<double>(is);
    if (result_x.success && result_y.success) {
        auto result_r = detail::Get<double>(is);
        auto opt_radius = result_r.success ? accerion::api::Optional<double>{result_r.value}
                                           : accerion::api::Optional<double>{};
        sensor.PerformBufferedRecovery(result_x.value, result_y.value, opt_radius);
    } else {
        std::cout << "Please provide the initial estimate for the position like '<x [m]> <y [m]>'." << std::endl;
    }
    return false;
}

CliCommandDelete::CliCommandDelete()
        : CliCommand{"delete", {"d"},
                     {"area <id/" + kAllOption + ">",
                      "cluster <id>"},
                     {"delete one area or delete all areas",
                      "delete one cluster"}} {}

constexpr CliCommandId CliCommandDelete::Id;  // C++17 won't require this
const std::string CliCommandDelete::kAllOption{"all"};

bool CliCommandDelete::Execute(const BlockingSensor &sensor, std::istream &is) {
    static const std::string kTypeErrorMessage{"Unknown property, expecting 'area' or 'cluster'."};
    auto result_type = detail::Get<detail::PropertyType>(is);
    if (!result_type.success) {
        std::cout << kTypeErrorMessage << std::endl;
        return false;
    }
    switch (result_type.value) {
        case detail::PropertyType::Area: {
            static const std::string kValueErrorMessage{"Please provide an id or the all keyword."};
            const auto option = detail::GetString(is, true);
            if (option.empty()) {
                std::cout << kValueErrorMessage << std::endl;
                return false;
            }
            accerion::api::Optional<accerion::api::AreaId> opt_id;
            if (option != kAllOption) {
                if (!detail::IsNumber(option)) {
                    std::cout << kValueErrorMessage << std::endl;
                    return false;
                }
                opt_id = accerion::api::Optional<accerion::api::AreaId>{
                    static_cast<accerion::api::AreaId>(std::stoull(option))};
            }
            sensor.DeleteArea(opt_id);
            std::cout << (opt_id ? "Area" : "Areas") << " deleted" << std::endl;
            break;
        }
        case detail::PropertyType::Cluster: {
            auto result_cluster = detail::GetInt<accerion::api::ClusterId>(is);
            if (!result_cluster.success) {
                std::cout << "Please provide an id" << std::endl;
                return false;
            }
            sensor.DeleteCluster(result_cluster.value);
            std::cout << "Cluster deleted" << std::endl;
            break;
        }
        default: {
            std::cout << kTypeErrorMessage << std::endl;
            return false;
        }
    }
    return false;
}

CliCommandDev::CliCommandDev()
        : CliCommand{"dev", {},
                     {"get-frame <dev key> <file name>"},
                     {"get frame and store as file"}} {}

constexpr CliCommandId CliCommandDev::Id;  // C++17 won't require this

bool CliCommandDev::Execute(const BlockingSensor &sensor, std::istream &is) {
    static const std::string kCommandErrorMessage{"Unknown command, expecting 'get-frame'."};
    static const std::string kKeyErrorMessage{"Please provide the dev key."};
    static const std::string kFileNameErrorMessage{"Please provide output file name."};
    const auto command = detail::GetString(is, true);
    if (command.empty() && command != "get-frame") {
        std::cout << kCommandErrorMessage << std::endl;
        return false;
    }
    const auto key = detail::GetString(is, false);
    if (key.empty()) {
        std::cout << kKeyErrorMessage << std::endl;
        return false;
    }
    auto result_file_name = detail::GetQuotedString(is);
    if (!result_file_name.success) {
        std::cout << kFileNameErrorMessage << std::endl;
        return false;
    }
    auto file_name = result_file_name.value;
    if (file_name.empty()) {
        std::cout << kFileNameErrorMessage << std::endl;
        return false;
    }
    if (!accerion::api::DoesStringEndWith(file_name, ".png")) {
        file_name += ".png";
    }
    const auto raw_frame = sensor.GetFrame(key);
    if (raw_frame.empty()) {
        std::cout << "Did not receive data, is the development key correct?" << std::endl;
        return false;
    }
    std::ofstream ofs{file_name, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary};
    ofs.write(reinterpret_cast<const char*>(raw_frame.data()), raw_frame.size());
    std::cout << "Frame saved as '" << file_name << "'" << std::endl;
    return false;
}
