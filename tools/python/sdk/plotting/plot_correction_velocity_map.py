#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.plotting.utils.plotting import *
from sdk.processing.correction_count import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_correction_velocity_map(data: DataContainer,
                                 output_folder: Optional[Path] = None,
                                 conf: Configuration = Configuration.default(),
                                 *,
                                 verbose: bool = True):
    arams = data.arams_original  # shallow copy
        
    info = get_drift_correction_info_cluster(arams, conf, verbose=verbose)
    if info is None or len(info) == 0:
        print_v(verbose, "No drift correction per cluster crossing.")
        return
    pos_x, pos_y, dc_counts = zip(*[(i.pose.x, i.pose.y, i.mean_velocity) for i in info])

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Average velocities at cluster crossing", fontweight='bold')
    axis.set_title(f'Total detected signatures: {len(dc_counts)}')

    if conf[Configuration.Name.OVERLAY_INPUT_POSES]:
        add_input_poses(axis, data.arams)

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)
    _, data_cbar_handle = scatter_data(axis, pos_x, pos_y, dc_counts, factor=15, reverse=True)
 
    ticks = np.linspace(data_cbar_handle.vmin.astype(float), data_cbar_handle.vmax.astype(float), 10)
    data_cbar_handle.ax.set_yticklabels([f"{t:.2f}" for t in ticks])
    data_cbar_handle.ax.set_ylabel("velocity [m/s]")

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")
    axis.grid()
    set_xylim(axis, conf, [min(pos_x), max(pos_x)], [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "correction_velocity_map.png", conf.get_prefix(arams.get_input_files()))


if __name__ == "__main__":
    from sdk.plotting.utils.plotting import call_arams_plot_function
    call_arams_plot_function(plot_correction_velocity_map)
