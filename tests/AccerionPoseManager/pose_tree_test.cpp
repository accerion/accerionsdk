/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "pose_tree_fixture.h"
#include "pose_utils.h"


using PoseTree = PoseTreeFixture::PoseTree;

accerion::api::Pose const kDefaultPose{1.0, 1.0, 1.0};
accerion::api::Pose const kDefaultPoseSqr{kDefaultPose * kDefaultPose};


TEST(PoseTree, InvalidIds) {
    PoseTree pt;
    // Empty id not allowed
    ASSERT_ANY_THROW(pt.Set("", "B", kDefaultPose));
    // Not known id cannot be used in request
    ASSERT_ANY_THROW(pt.Get("A", "B"));
}

TEST(PoseTree, EqualIds) {
    PoseTree pt;
    // Basically add an id to the tree to use it in tests
    pt.Set("A", "B", kDefaultPose);

    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("A", "A"), accerion::api::Pose::Identity());
    ASSERT_ANY_THROW(pt.Set("A", "A", accerion::api::Pose::Identity()));
}

TEST(PoseTree, Contains) {
    PoseTree pt;
    // Basically add an id to the tree to use it in tests
    pt.Set("A", "B", kDefaultPose);

    ASSERT_TRUE(pt.Contains("A"));
    ASSERT_TRUE(pt.Contains("B"));
    ASSERT_FALSE(pt.Contains("C"));
}

TEST(PoseTree, SetGet) {
    PoseTree pt;
    pt.Set("A", "B", kDefaultPose);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("A", "B"), kDefaultPose);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("B", "A"), kDefaultPose.Inverse());
}

TEST(PoseTree, ReplaceGet) {
    PoseTree pt;
    pt.Set("B", "A", accerion::api::Pose{100, 100, 100});
    pt.Set("B", "A", kDefaultPose);
    PosesAreApproxEqual(pt.Get("B", "A"), kDefaultPose);

    // Setting the reverse of a branch is possible
    pt.Set("A", "B", kDefaultPose);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("A", "B"), kDefaultPose);
}

TEST(PoseTree, CyclicBranches) {
    PoseTree pt;
    pt.Set("B", "A", kDefaultPose);
    pt.Set("C", "B", kDefaultPose);
    ASSERT_ANY_THROW(pt.Set("C", "A", kDefaultPose));
    ASSERT_ANY_THROW(pt.Set("A", "C", kDefaultPose));
}

TEST(PoseTree, IsolatedBranches) {
    PoseTree pt;
    pt.Set("B", "A", kDefaultPose);
    ASSERT_ANY_THROW(pt.Set("D", "C", kDefaultPose));
}

TEST(PoseTree, DirectionTest) {
    // Original implementation was dependent on the sorting of Id's, that should not be the case, therefore this test is
    // created.
    //       L
    //     /   \
    //    K     M
    //   / \   / \
    //  A   Y B   Z
    //  |
    //  P
    PoseTree pt;
    pt.Set("K", "L", kDefaultPose);
    pt.Set("M", "L", kDefaultPose);

    pt.Set("A", "K", kDefaultPose);
    pt.Set("Y", "K", kDefaultPose);
    pt.Set("B", "M", kDefaultPose);
    pt.Set("Z", "M", kDefaultPose);

    pt.Set("P", "A", kDefaultPose);

    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("K", "L"), kDefaultPose);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("M", "L"), kDefaultPose);

    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("A", "L"), kDefaultPoseSqr);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("B", "L"), kDefaultPoseSqr);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("Y", "L"), kDefaultPoseSqr);
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("Z", "L"), kDefaultPoseSqr);

    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("P", "L"), kDefaultPoseSqr * kDefaultPose);
}

TEST(PoseTree, SingleRoot_RootReplacement) {
    //    X
    //   /
    //  A
    //   \
    //    B
    PoseTree pt;
    pt.Set("B", "A", kDefaultPose);
    ASSERT_NO_THROW(pt.Set("A", "X", kDefaultPose));
    ASSERT_PRED2(PosesAreApproxEqual, pt.Get("B", "X"), kDefaultPoseSqr);
}

TEST(PoseTree, SingleRoot_AdditonalRoot) {
    //  A   X  <-  not allowed
    //   \ /
    //    B
    PoseTree pt;
    pt.Set("B", "A", kDefaultPose);
    ASSERT_ANY_THROW(pt.Set("B", "X", kDefaultPose));

    //  A   X  <-  not allowed
    //   \ /
    //    B
    //     \
    //      C
    pt.Set("C", "B", kDefaultPose);
    ASSERT_ANY_THROW(pt.Set("B", "X", kDefaultPose));
}

TEST_F(PoseTreeFixture, SequentialBranches) {
    //     0
    //     |
    //     A
    //    / \
    //   B   C
    //       |
    //       D
    const accerion::api::Pose p_a0 = tree_.Get("A", "0");
    const accerion::api::Pose p_da = tree_.Get("D", "A");
    ASSERT_PRED2(PosesAreApproxEqual, p_da * p_a0, p_d0_);
}

TEST_F(PoseTreeFixture, ParallelBranches) {
    //     0
    //     |
    //     A
    //    / \
    //   B   C
    //       |
    //       D
    const accerion::api::Pose p_b0 = tree_.Get("B", "0");
    ASSERT_PRED2(PosesAreApproxEqual, p_b0 * p_d0_.Inverse(), p_bd_);
}