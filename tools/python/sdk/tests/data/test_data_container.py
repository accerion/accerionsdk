#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from packaging.version import Version
from pathlib import Path

from sdk.data.data_container import DataContainer
from sdk.tests.test_data.files import TestData
from sdk.utils.configuration import Configuration


def test_data_container():
    versions_to_test = [
        (Version("6.0.0"), [TestData.File.ARAMS_USER_LOGS_V6,
                            TestData.File.FLOOR_MAP_COORDINATES,
                            TestData.File.POSE_GRAPH]),
        (Version("6.1.0"), [TestData.File.ARAMS_USER_LOGS_V61,
                            TestData.File.FLOOR_MAP_COORDINATES_V61,
                            TestData.File.POSE_GRAPH]),
        (Version("6.1.5"), [TestData.File.ARAMS_USER_LOGS_V615,
                            TestData.File.FLOOR_MAP_COORDINATES_V61,
                            TestData.File.POSE_GRAPH]),
        (Version("7.0.0"), [TestData.File.ARAMS_USER_LOGS_V700,
                            TestData.File.FLOOR_MAP_COORDINATES_V700,
                            TestData.File.POSE_GRAPH]),
        (Version("7.0.0"), [TestData.File.ARAMS_USER_LOGS_V700,
                            TestData.File.IMPORT_G2O_DB_200,
                            TestData.File.POSE_GRAPH]),
        (Version("7.0.0"), [TestData.File.ARAMS_USER_LOGS_V700,
                            TestData.File.FLOOR_MAP_COORDINATES_V700,
                            TestData.File.IMPORT_G2O_DB_200,
                            TestData.File.POSE_GRAPH])
    ]
    data_containers = [
        DataContainer.create(TestData.get_list(test_data_files), Configuration.default())
            for _, test_data_files in versions_to_test
    ]
    # All data is supplied, so should be read by DataContainer
    for container in data_containers:
        assert container.arams_original
        assert container.arams
        assert container.signatures
        assert container.graph

    # In case there are both floor map coordinates and a map available, then the map should be used
    assert data_containers[-2].signatures == data_containers[-1].signatures
