#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from argparse import Action, ArgumentError, ArgumentParser
from typing import Dict, List
import os.path


def validate_argument_is_dir(path: str) -> str:
    """
    Function to be used as argparse.ArgumentParser validator, like
        parser.add_argument("--output_folder", type=validate_argument_is_dir)
    """
    if not os.path.isdir(path):
        raise ValueError("Supplied output folder does not exist.")
    return path


class ValidateInputPaths(Action):
    """
    Check that the input paths are actually existing. The input can be a file, a folder or a set of them.
    """
    def __call__(self, parser, namespace, values: List[str], option_string=None):
        def check_path_exists(v):
            if not os.path.exists(v):
                raise ArgumentError(self, f"Input path '{v}' does not exist")

        if isinstance(values, list):
            for value in values:
                check_path_exists(value)
        else:
            check_path_exists(values)
        setattr(namespace, self.dest, values)


def create_default_arg_parser(*, output_required: bool = False) -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, nargs="+", action=ValidateInputPaths,
                        help="Input file(s) and folder(s)")
    parser.add_argument("-o", "--output", required=output_required, type=str,
                        help="Output folder in which figures are stored")
    parser.add_argument("--skip", type=int, required=False, default=None,
                        help="Skip as many initial drift corrections as specified")
    return parser


def add_prefix(parser, is_required = False):
    parser.add_argument("-p", "--prefix", type=str, required=is_required, nargs='?', const="",
                        help="Prefix to be added to the results, default none, empty string means generated prefix")
    

def add_spec(parser, is_required = False):
    parser.add_argument("-s", "--spec", required=is_required, action=ValidateInputPaths, help="Input test specification file")
