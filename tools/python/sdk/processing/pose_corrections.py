#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from typing import List, Tuple

import numpy as np

from sdk.arams.arams import Arams
from sdk.graph.graph import Pose
from sdk.utils.print import print_v


def get_pose_corrections(arams: Arams, *, verbose: bool = True) -> Tuple[List[float], List[float], List[Pose]]:
    if "LDC" not in arams:
        print_v(verbose, __name__,
                ": Could not find logDriftCorrections file or the arams version does not support the file type.")
        return [], [], []

    LDC = arams.LDC
    df_ldc = arams[LDC]

    pos_x = df_ldc[LDC.NEW_XPOS].to_list()
    pos_y = df_ldc[LDC.NEW_YPOS].to_list()

    errors = []

    # Convert heading errors to radians (according to Pose definition)
    ldc_heading_errors_rad = np.deg2rad(df_ldc[LDC.ERROR_THETA])
    for error_x, error_y, error_th in zip(df_ldc[LDC.ERROR_X], df_ldc[LDC.ERROR_Y], ldc_heading_errors_rad):
        errors.append(Pose(x = error_x, y = error_y, th = error_th))

    return pos_x, pos_y, errors
