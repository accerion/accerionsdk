#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from abc import ABCMeta, abstractmethod
from enum import Enum, EnumMeta


class ABCEnumMeta(EnumMeta, ABCMeta):
    """Mixin between Enum- and AbstractBaseClass metaclasses"""
    pass


class StrEnum(str, Enum):
    """Mixin between 'str' and 'Enum' class"""

    def __str__(self) -> str:
        return self.value


class LogFileSpecification(StrEnum, metaclass=ABCEnumMeta):
    @staticmethod
    @abstractmethod
    def full_name() -> str:
        pass

    @classmethod
    def can_parse(cls, file_name: str):
        return file_name.startswith(cls.full_name() + ".") and file_name.endswith(".csv")


class FmcFileSpecification(LogFileSpecification):
    @staticmethod
    def full_name():
        return "FloorMapCoordinates"

    @classmethod
    def can_parse(cls, file_name: str):
        file_name = file_name.lower()
        return ("coordinates" in file_name or "coords" in file_name) and file_name.endswith(".csv")
