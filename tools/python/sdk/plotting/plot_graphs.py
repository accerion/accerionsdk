#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import argparse
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt

from sdk.arams.arams import Arams
from sdk.data.signature import Signature
from sdk.graph.graph import Graph
from sdk.graph.filter import identify_edges
from sdk.graph.g2o_reader import Reader
from sdk.plotting.utils.graph import *
from sdk.plotting.utils.plotting import add_signature_poses

"""
With this script graphs from up to three g2o files can be visualized within one figure.
Toggling can be done on a complete graph by clicking the corresponding legend item.
"""


def plot_graphs(graphs: List[Graph], names: List[str] = None, signatures: List[Signature] = None):
    assert all(len(g.vertices) != 0 for g in graphs), "Not all graphs contain vertices."

    if names is None:
        names = [""] * len(graphs)

    settings = PlotGraphsSettings.create(len(graphs))

    # Store all information regarding the plots
    plot_elements = PlotElements()

    # Create one figure with the graph plots
    fig, ax = plt.subplots(1, 1)
    for index, graph in enumerate(graphs):
        if all(v.type != Graph.Vertex.Type.Unknown for v in graph.vertices):
            graph.vertices = [v for v in graph.vertices if v.type == Graph.Vertex.Type.Signature]
        loop_indices = identify_edges(graph, Graph.Edge.Type.Loop)
        graph.edges = [e for i, e in enumerate(graph.edges) if i in set(loop_indices)]
        handles = add_graph(ax, graph, settings[index])
        plot_elements.append(names[index], settings[index], handles)

    add_signature_poses(ax, signatures, (138. / 256, 43. / 256, 226. / 256))  # blue violet

    add_legend(fig, ax, plot_elements)

    ax.set_aspect('equal', adjustable='datalim')
    ax.set_xlabel("x-coordinate [m]")
    ax.set_ylabel("y-coordinate [m]")
    ax.set_title("Pose graphs displaying loop closure edges")
    fig.tight_layout()
    plt.show()


def __parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", required=True, nargs='+', help="Input g2o files, up to three")
    parser.add_argument("-c", "--coords", required=False, help="Input map coordinates csv file")

    return vars(parser.parse_args())


def __main():
    args = __parse_arguments()
    file_names = args["input"]
    graphs = [Reader.read(file_name) for file_name in args["input"]]

    names = [""] if len(file_names) == 1 else [Path(f).stem for f in file_names]

    arams = Arams(args["coords"]) if args["coords"] is not None else None

    plot_graphs(graphs, names, arams)


if __name__ == "__main__":
    __main()
