#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from collections import defaultdict
from dataclasses import dataclass
from math import ceil
from typing import Dict, List, Optional, Tuple

from sdk.data.signature import Signature
from sdk.geometry.bounding_box import BoundingBox
from sdk.geometry.point import Point


__all__ = ["BoundingBoxWithRadius", "Pixel", "SpatialMesh",
           "rasterize_circle", "calculate_signature_density_dict", "get_max_radius_in_bounding_boxes"]


Pixel = Tuple[int, int]


@dataclass
class BoundingBoxWithRadius:
    bounding_box: BoundingBox
    radius: float


class SpatialMesh:
    def __init__(self, cell_size: float, signatures: List[Signature]):
        self.cell_size = cell_size
        self.min_cell_x = min(s.pose.x for s in signatures)
        self.min_cell_y = min(s.pose.y for s in signatures)

    def calculate_cell_dict(self, signatures: List[Signature]) -> Dict[Pixel, List[int]]:
        cell_dict = defaultdict(list)
        for signature_index, signature in enumerate(signatures):
            pixel = self.__calculate_pixel(signature.pose.x, signature.pose.y)
            cell_dict[pixel].append(signature_index)
        return cell_dict

    def get_cell_center(self, c: int, r: int) -> Point:
        return Point(self.min_cell_x + (c + .5) * self.cell_size,
                     self.min_cell_y + (r + .5) * self.cell_size)

    def __calculate_pixel(self, x: float, y: float) -> Pixel:
        return int((x - self.min_cell_x) / self.cell_size), int((y - self.min_cell_y) / self.cell_size)


def rasterize_circle(radius: float, cell_size: float) -> List[Pixel]:
    """Rasterize a filled circle with a given radius and cell size.

    Args:
        radius: The radius of the circle
        cell_size: The size of each square cell

    Returns:
        A list of (x, y) coordinates of the cells within the circle
    """
    assert cell_size <= radius, "Cell size must be smaller than or equal to the radius for accurate representation."

    # Convert radius to cell units
    radius_in_cells = int(ceil(radius / cell_size))
    radius_in_cells_squared = radius_in_cells ** 2

    # List to hold the IDs of cells inside the circle
    pattern = []

    # Iterate through a square bounding box that contains the circle
    for i in range(-radius_in_cells, radius_in_cells + 1):
        for j in range(-radius_in_cells, radius_in_cells + 1):
            # For each cell, check if the cell center is within radius of the center of the circle
            if i ** 2 + j ** 2 <= radius_in_cells_squared:
                pattern.append((i, j))

    return pattern


def __count_signatures_in_pattern(occupancy_grid: Dict[Pixel, List[int]],
                                  pixel_of_interest_r: int,
                                  pixel_of_interest_c: int,
                                  pattern: List[Pixel]):
    """Calculate the total count of signatures in a pattern of cells."""
    count = 0
    for r, c in pattern:
        neighbor_key = (pixel_of_interest_r + r, pixel_of_interest_c + c)
        count += len(occupancy_grid[neighbor_key]) if neighbor_key in occupancy_grid else 0
    return count


def calculate_signature_density_dict(mesh: SpatialMesh,
                                     signatures: List[Signature],
                                     patterns: Dict[Optional[float], List[Pixel]],
                                     bounding_boxes: List[BoundingBoxWithRadius]) -> Dict[Pixel, int]:
    occupancy_grid = mesh.calculate_cell_dict(signatures)

    signature_count_per_cell = dict()
    # Loop over all pixel that contain signatures
    for pixel_with_signatures_r, pixel_with_signatures_c in occupancy_grid.keys():
        cell_center = mesh.get_cell_center(pixel_with_signatures_r, pixel_with_signatures_c)
        radius = get_max_radius_in_bounding_boxes(cell_center, bounding_boxes)

        pattern = patterns[radius]
        # Loop over all pixels surrounding the pixel that contain signatures
        for pattern_r, pattern_c in pattern:
            pixel_of_interest = (pixel_with_signatures_r + pattern_r, pixel_with_signatures_c + pattern_c)

            # Skip calculation if pixel is covered in previous iterations already
            if pixel_of_interest in signature_count_per_cell:
                continue

            count = __count_signatures_in_pattern(
                occupancy_grid, pixel_of_interest[0], pixel_of_interest[1], pattern)
            signature_count_per_cell[pixel_of_interest] = count

    return signature_count_per_cell


def get_max_radius_in_bounding_boxes(position: Point,
                                     boxes: List[BoundingBoxWithRadius],
                                     default_radius: float = None) -> Optional[float]:
    """
    Find the maximum radius for a signature position based on bounding boxes.

    Args:
        position: The signature position to check
        boxes: A list of dictionaries containing 'bounding_box' and 'radius'
        default_radius: The default radius

    Returns:
        The maximum radius of the bounding boxes containing the signature position, or default
         if no bounding box contains the point
    """
    # Extract radii from bounding boxes that contain the signature position
    radii = [box.radius for box in boxes if position in box.bounding_box]

    # Return the maximum radius (most restrictive), or default_radius if no radii were found
    radius = max(radii) if len(radii) > 0 else default_radius

    return radius
