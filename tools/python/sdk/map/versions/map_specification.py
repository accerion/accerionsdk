#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import Dict, Generic, List, Tuple, TypeVar

from packaging.version import Version
from sdk.map.versions._map_specification_base import MapSpecification
from sdk.map.versions._map_specification_v200 import MapSpecificationV200

MapSpecificationType = TypeVar("MapSpecificationType")


class MapSpecificationRegister(Generic[MapSpecificationType]):
    __MapSpecification = MapSpecification
    __map_specifications: Dict[Version, MapSpecificationType] = {}

    @classmethod
    def __valid_specification(cls, map_spec: MapSpecificationType):
        assert isinstance(
            map_spec, cls.__MapSpecification), f"{map_spec} does not fullfill the 'MapSpecification' protocol requirements"

    @classmethod
    def add_specification(cls, version: Version, map_spec: MapSpecificationType):
        cls.__valid_specification(map_spec)
        cls.__map_specifications[version] = map_spec

    @classmethod
    def add_specifications(cls, map_spec_list: List[Tuple[Version, MapSpecificationType]]):
        for version, map_spec in map_spec_list:
            cls.add_specification(version, map_spec)

    @classmethod
    def get_supported_specification(cls, version: Version):
        assert version in cls.__map_specifications, f"Map version '{version} is not supported"

        return cls.__map_specifications[version]


MapSpecificationRegister.add_specification(
    Version("2.0.0"), MapSpecificationV200)
