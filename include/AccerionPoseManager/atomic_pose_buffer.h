/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <atomic>
#include <ostream>
#include <map>

#include "AccerionPoseManager/pose_buffer.h"

namespace accerion {

namespace pose_manager {

template <typename PoseType, typename TimeType, typename TimeDiffConverter = TimeDiffCaster<TimeType>>
class AtomicPoseBuffer_ {
 public:
    using PoseBuffer = PoseBuffer_<PoseType, TimeType, TimeDiffConverter>;
    using TimeDiff = typename PoseBuffer::TimeDiff;

    explicit AtomicPoseBuffer_(const TimeDiff& buffer_length) : pose_buffer_(buffer_length) {}

    void Set(const PoseType& pose, const TimeType& time) {
        // acquire lock
        while (flag_.test_and_set(std::memory_order_acquire)) {}  // spin
        pose_buffer_.Set(pose, time);
        flag_.clear(std::memory_order_release);  // release lock
    }

    PoseType Get(const TimeType& time) const {
        // acquire lock
        while (flag_.test_and_set(std::memory_order_acquire)) {}  // spin

        PoseType t = pose_buffer_.Get(time);
        flag_.clear(std::memory_order_release);  // release lock
        return t;
    }

 private:
    PoseBuffer_<PoseType, TimeType> pose_buffer_;
    mutable std::atomic_flag flag_{false};
};

}  // namespace pose_manager

}  // namespace accerion
