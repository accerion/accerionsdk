/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <algorithm>
#include <cstdint>
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "sqlite3.h"

namespace accerion {

namespace sqlite {

// Forward declaration
class Database;

class PreparedStatement {
 public:
    PreparedStatement(const Database& db, const std::string& query);

    ~PreparedStatement() = default;
    PreparedStatement(const PreparedStatement& other) = delete;
    PreparedStatement& operator=(const PreparedStatement& other) = delete;
    PreparedStatement(PreparedStatement&& other) = default;
    PreparedStatement& operator=(PreparedStatement&& other) = delete;

    // Helper types to be used to handle raw data blob fields
    struct Blob {
        std::vector<std::uint8_t> bytes;
    };
    // Use the following object for binding when the data is already stored somewhere and it remains available while
    // executing the query. In that case a pointer and a size will suffice. This is the preferred way to avoid copying.
    struct BindBlob {
        template <typename PointerType, typename SizeType>
        static BindBlob Create(PointerType p, SizeType s) {
            return {static_cast<const void *>(p), static_cast<int>(s)};
        }

        const void* pointer;
        int size;
    };


    // Bind value to the statement based on position, starting from 1
    template <typename Type>
    void Bind(int position, const Type& value) const {
        if (position == 0) {
            std::stringstream ss;
            ss << "Prepared statement binding error: got binding position 0" << "\n";
            ss << "Query: " << sqlite3_sql(statement_.get()) << "\n";
            throw std::runtime_error(ss.str());
        }

        const auto return_code = BindValue<Type>(position, value);
        if (return_code != SQLITE_OK) {
            std::stringstream ss;
            ss << "Prepared statement binding error: " << sqlite3_errmsg(database_) << "\n";
            ss << "Query: " << sqlite3_sql(statement_.get()) << "\n";
            throw std::runtime_error(ss.str());
        }
    }

    // Bind-by-name function that will reuse the Bind-by-position function, so bind based on the name of the parameter.
    // When a query is constructed like
    //     sqlite3* db{...};
    //     auto statement = PreparedStatement{db, R"SQL(
    //         SELECT *
    //           FROM signatures
    //          WHERE id = @id;
    //     )SQL"};
    // then this function can be used like
    //     int id{...};
    //     statement.Bind("@id", id);
    template <typename Type>
    void Bind(const std::string& name, const Type& value) const {
        auto position = sqlite3_bind_parameter_index(statement_.get(), name.c_str());
        if (position == 0) {
            std::stringstream ss;
            ss << "Prepared statement binding error: binding variable '" << name << "' not known" << "\n";
            ss << "Query: " << sqlite3_sql(statement_.get()) << "\n";
            throw std::runtime_error(ss.str());
        }
        Bind<Type>(position, value);
    }

    // Two actions are performed as part of the reset function call:
    //   1. Reset statement to its initial state, ready for re-execution
    //   2. Drop all bound variables
    void Reset() const;

    // Returns whether the query is ongoing, so not done yet, such that it can be used like
    //     PreparedStatement statement{...};
    //     while (statement.Step()) {
    //         ...
    //     }
    bool Step() const;
    // Returns whether the query is ongoing, so not done yet, such that it can be used like
    //     PreparedStatement statement{...};
    //     std::tuple<double, str::string> row
    //     while (statement.Step(row)) {
    //         ...
    //     }
    template <typename... Types>
    bool Step(std::tuple<Types...>& tuple) const {
        if (sqlite3_column_count(statement_.get()) < sizeof...(Types)) {
            std::stringstream ss;
            ss << "Prepared statement binding error: trying to bind more variables (" << sizeof...(Types) << ")";
            ss << " than the query returns columns (" << sqlite3_column_count(statement_.get()) << ")\n";
            ss << "Query: " << sqlite3_sql(statement_.get()) << "\n";
            throw std::runtime_error(ss.str());
        }
        const auto result = Step();
        if constexpr (sizeof...(Types) > 0) {
            if (result) {
                tuple = GetValues<Types...>(std::make_index_sequence<sizeof...(Types)>{});
            }
        }
        return result;
    }

    // The return code from SQLite for the last execution using Step
    // Not optimal, but this can be used to perform extended error handling
    int LastReturnCode() const;

    // Get all query result rows.
    // When a value in the database is null, then a default constructed object is returned, being
    //     integer 0, float 0.0, text "", blob empty
    // Use function in the following way:
    //     PreparedStatement statement{...};
    //     std::vector<std::tuple<double, str::string>> rows = statement.GetRows<double, str::string>();
    template <typename... Types, typename Tuple = std::tuple<Types...>>
    std::vector<Tuple> GetRows() const {
        Tuple row;
        std::vector<Tuple> rows;
        while (Step(row)) {
            rows.emplace_back(std::move(row));
        }
        Reset();
        return rows;
    }

    template <typename Type>
    Type GetValue() const {
        const auto rows = GetRows<Type>();
        if (rows.size() != 1) {
            std::stringstream ss;
            ss << "Query resulted in " << rows.size() << " rows, expecting exactly one, query:\n"
               << sqlite3_sql(statement_.get()) << "\n";
            throw std::runtime_error(ss.str());
        }
        return std::get<0>(rows.front());
    }

    template <typename Type>
    std::vector<Type> GetVector() const {
        const auto rows = GetRows<Type>();
        std::vector<Type> values;
        values.reserve(rows.size());
        std::transform(rows.cbegin(), rows.cend(), std::back_inserter(values),
                       [](const auto& row) { return std::get<0>(row); });
        return values;
    }

private:
    // Helper class for Sqlite statement pointer handling
    class SqliteStatementPtrDeleter {
    public:
        explicit SqliteStatementPtrDeleter(sqlite3* database);

        void operator()(sqlite3_stmt* statement) const;

    private:
        sqlite3* const database_;
    };
    using SqliteStatementPtr = std::unique_ptr<sqlite3_stmt, SqliteStatementPtrDeleter>;

    template <typename Type>
    int BindValue(int position, const Type& value) const {
        if constexpr (std::is_integral_v<Type>) {
            return sqlite3_bind_int(statement_.get(), position, value);
        } else if constexpr (std::is_floating_point_v<Type>) {
            return sqlite3_bind_double(statement_.get(), position, value);
        } else if constexpr (std::is_same_v<Type, std::string>) {
            return sqlite3_bind_text(statement_.get(), position, value.data(),
                                     static_cast<int>(value.size()), SQLITE_TRANSIENT);
        } else if constexpr (std::is_same_v<Type, Blob>) {
            return sqlite3_bind_blob(statement_.get(), position, static_cast<const void*>(value.bytes.data()),
                                     static_cast<int>(value.bytes.size()), SQLITE_TRANSIENT);
        } else if constexpr (std::is_same_v<Type, BindBlob>) {
            return sqlite3_bind_blob(statement_.get(), position, value.pointer, value.size, nullptr);
        } else {
            static_assert(std::is_integral_v<Type>, "Trying to use PreparedStatement::Bind with an unknown type");
        }
    }

    template <typename Type>
    Type GetValue(int position) const {
        const auto column_type = sqlite3_column_type(statement_.get(), position);
        if (column_type == SQLITE_NULL) { return {}; }

        const auto wrong_type_error_message = [position, &stmt = statement_](const auto& type_name) {
            std::stringstream ss;
            ss << "Returned column type at position " << position << " is not of expected type " << type_name
               << ", query:\n" << sqlite3_sql(stmt.get()) << "\n";
            return ss.str();
        };

        if constexpr (std::is_integral_v<Type>) {
            if (column_type != SQLITE_INTEGER) { throw std::runtime_error(wrong_type_error_message("integer")); }
            return static_cast<Type>(sqlite3_column_int(statement_.get(), position));
        } else if constexpr (std::is_floating_point_v<Type>) {
            if (column_type != SQLITE_FLOAT) { throw std::runtime_error(wrong_type_error_message("floating point")); }
            return static_cast<Type>(sqlite3_column_double(statement_.get(), position));
        } else if constexpr (std::is_same_v<Type, std::string>) {
            if (column_type != SQLITE_TEXT) { throw std::runtime_error(wrong_type_error_message("text")); }
            const auto raw_ptr = sqlite3_column_text(statement_.get(), position);
            if (raw_ptr == nullptr) { return {}; }
            return {reinterpret_cast<const char*>(raw_ptr)};
        } else if constexpr (std::is_same_v<Type, Blob>) {
            if (column_type != SQLITE_BLOB) { throw std::runtime_error(wrong_type_error_message("blob")); }
            const auto raw_ptr = sqlite3_column_blob(statement_.get(), position);
            if (raw_ptr == nullptr) { return {}; }
            const auto data_ptr = static_cast<const std::uint8_t*>(raw_ptr);
            const auto data_size = sqlite3_column_bytes(statement_.get(), position);
            return {{data_ptr, data_ptr + data_size}};
        } else {
            static_assert(std::is_integral_v<Type>, "Trying to get unknown type from query in PreparedStatement");
        }
    }

    template <typename... Types, std::size_t... Indices, typename Tuple = std::tuple<Types...>>
    Tuple GetValues(std::index_sequence<Indices...>) const {
        return std::make_tuple(GetValue<Types>(Indices)...);
    }

    sqlite3* const database_;
    SqliteStatementPtr statement_;  // Should be treated as const, except that it can't as it needs to be movable
    mutable int last_return_code_;
};

}  // namespace sqlite
}  // namespace accerion
