#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.plotting.utils.plotting import *
from sdk.processing.match_quality import get_match_quality
from sdk.utils.configuration import Configuration


def plot_match_quality_map(data: DataContainer,
                           output_folder: Optional[Path] = None,
                           conf: Configuration = Configuration.default(),
                           *,
                           verbose: bool = True):

    pos_x, pos_y, match_quality = get_match_quality(
        data.arams, verbose=verbose)
    if not match_quality:
        return

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Match quality [%]", fontweight='bold')
    axis.set_title(f'Total detected signatures: {len(match_quality)}')

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)

    _, cbar_handle = scatter_data(axis, pos_x, pos_y, match_quality, conf,
                                  tag=MetricsData.Tag.MatchQuality, min_colorbar=0, max_colorbar=100, reverse=True, factor=0.5)
    # No decimals in colorbar labels
    cbar_handle.ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")
    axis.grid()
    set_xylim(axis, conf, [min(pos_x), max(pos_x)],
              [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "match_quality_map.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_match_quality_map)
