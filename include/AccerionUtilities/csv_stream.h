/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <fstream>
#include <iostream>

namespace accerion {

namespace utilities {

class CsvStream {
 public:
    CsvStream(std::ostream& os, char del)
        : del_(del), os_{os} {
    }

    template <typename Type, typename... Others>
    void Write(Type value, Others... others) const {
        WriteValue(value);
        os_  << del_;
        Write(others...);
    }

    template <typename Type>
    void Write(Type value) const {
        WriteValue(value);
        os_ << '\n';
    }

 private:
    template <class Type>
    typename std::enable_if<!std::is_integral<Type>::value>::type
    WriteValue(Type value) const {
        static_assert(!std::is_pointer<Type>::value,
                                        "CsvStream does not support pointers");
        static_assert(!std::is_enum<Type>::value,
                                        "CsvStream does not support enums");
        os_ << value;
    }

    template <class Type>
    typename std::enable_if<std::is_integral<Type>::value>::type
    WriteValue(Type value) const {
        static_assert(!std::is_pointer<Type>::value,
                                        "CsvStream does not support pointers");
        static_assert(!std::is_enum<Type>::value,
                                        "CsvStream does not support enums");
        os_ << +value;
    }

    const char del_;
    std::ostream& os_;
};

}  // end of namespace utilities

}  // end of namespace accerion
