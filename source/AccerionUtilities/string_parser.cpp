/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionUtilities/string_parser.h"

#include <iostream>


accerion::api::Optional<accerion::api::ConnectionType> ParseConnectionType(const std::string& connection_type) {
    if (connection_type == "tcp") {
        return accerion::api::ConnectionType::kConnectionTcp;
    } else if (connection_type == "tcp_no_udp") {
        return accerion::api::ConnectionType::kConnectionTcpDisableUdp;
    } else if (connection_type == "udp_broad") {
        return accerion::api::ConnectionType::kConnectionUdpBroadcast;
    } else if (connection_type == "udp_uni") {
        return accerion::api::ConnectionType::kConnectionUdpUnicast;
    } else if (connection_type == "udp_uni_no_hb") {
        return accerion::api::ConnectionType::kConnectionUdpUnicastNoHb;
    } else {
        std::cout << "Connection type '" << connection_type << "' is not valid, expecting "
                  << " 'tcp', 'tcp_no_udp', 'udp_broad', 'udp_uni' or 'udp_uni_no_hb'." << std::endl;
        return {};
    }
}
