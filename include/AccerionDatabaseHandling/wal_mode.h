/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <future>
#include <optional>
#include <string>

#include "AccerionDatabaseHandling/database.h"

namespace accerion {

namespace sqlite {

void SwitchToWalJournaling(const accerion::sqlite::Database& db);

void SwitchToDeleteJournaling(const accerion::sqlite::Database& db);

class WalCheckpointer {
 public:
    explicit WalCheckpointer(const accerion::sqlite::Database& db);

    struct Info {
        // Total number of unsaved db pages before the checkpointing action
        int pages_total {};
        // Number of pages saved during the checkpointing action
        int pages_checkpointed {};
    };
    // Schedules an async task to checkpoint the wall file without blocking,
    // or it returns directly if the checkpointing is in progress, calling the provided callback if specified.
    void AttemptToCheckpoint(std::function<void(const std::optional<Info>&)> callback = {});

 private:
    Info Checkpoint() const;

    accerion::sqlite::Database db_;
    // Guaranteed to wait for running task to complete when object is destroyed
    std::future<void> busy_;
};


}  // namespace sqlite
}  // namespace accerion
