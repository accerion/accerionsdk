/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <optional>
#include <string>

#include "AccerionMap/schema_creator.h"
#include "AccerionSensorAPI/structs/acknowledgement.h"

namespace accerion {

namespace map {

class Validator {
 public:
    /**
     * Test whether the database is valid up to the point that the map version can be safely read.
     *
     * Still the full validation using Validate should be used before a map can be considered acceptable for production!
     *
     * @param path The database to be checked
     * @param creator SchemaCreator containing information about the database structure/layout
     * @param all_checks Flag to indicate that all checks should be run, also the lengthy ones, default is false.
     */
    static accerion::api::Acknowledgement ValidateSafety(const std::string& path,
                                                            const accerion::map::SchemaCreator& creator,
                                                            bool all_checks = false);

    /**
     * Test whether the database is acceptable
     *
     * Note that it assumed that ValidateSafety was ran before this method!
     *
     * @param path The database to be checked
     * @param creator SchemaCreator containing information about the database structure/layout
     * @param all_checks Flag to indicate that all checks should be run, also the lengthy ones, default is false.
     */
    static accerion::api::Acknowledgement Validate(const std::string& path,
                                                    const accerion::map::SchemaCreator& creator,
                                                    bool all_checks = false);

    /**
     * Clean the database
     *
     * It will make sure that any wrong or unknown indices are removed and the proper ones created
     *
     * @param path The database to be cleaned
     * @param creator SchemaCreator containing information about the database structure/layout
     */
    static void Clean(const std::string& path, const accerion::map::SchemaCreator& creator);
};

}  // namespace map
}  // namespace accerion
