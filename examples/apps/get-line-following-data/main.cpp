/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/connection_manager.h"

#include "app_utils.h"


int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cout << "Please include sensor ip address and cluster id; e.g. " << argv[0] << " 10.42.0.116 35" << std::endl;
        return 1;
    }
    const auto sensor_ip = accerion::api::Address::Parse({argv[1]});
    const auto cluster_id = std::stoi({argv[2]});

    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(sensor_ip, accerion::api::ConnectionType::kConnectionTcp);
    auto sensor = future_sensor.WaitFor(std::chrono::seconds{1});
    if (!sensor) {
        std::cout << "Could not connect to sensor" << std::endl;
        return 1;
    }

    sensor->SetLineFollowingMode(true, cluster_id).WaitFor();

    std::atomic_uint8_t counter{0};
    auto last_time = std::chrono::steady_clock::now();
    sensor->SubscribeToLineFollowerData().Callback([&last_time, &counter](const accerion::api::LineFollowerData& data) {
        // Print once a second
        auto current_time = std::chrono::steady_clock::now();
        if ((current_time - last_time) < std::chrono::seconds{1}) {
            return;
        }
        last_time = current_time;

        // Print header once in 20 seconds
        if (counter % 20 == 0) {
            counter = 0;
            std::cout << "\ntime since boot [s], (current sensor pose), (closest point on cluster, cluster heading)\n";
        }
        ++counter;

        std::cout << std::chrono::duration_cast<std::chrono::duration<double>>(data.timestamp.time_since_epoch()).count() * 1e-6
                  << ", (" << data.pose << "), (" << data.closest_point << ")\n";
    });

    PauseApp();

    sensor->SetLineFollowingMode(false, {}).WaitFor();

    return 0;
}
