/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include <chrono>
#include <cmath>
#include <numeric>
#include <thread>

#include "AccerionPoseManager/atomic_pose_buffer.h"
#include "AccerionSensorAPI/structs/pose.h"

using namespace accerion;

using Time = double;
using AtomicPoseBuffer = pose_manager::AtomicPoseBuffer_<api::Pose, Time>;

void AtomicPoseBufferThread(const int desired_throughput, const int iterations,
                            AtomicPoseBuffer& buffer,
                            std::function<void(int, AtomicPoseBuffer&)> func) {
    const std::uint64_t max_total_duration_ms = std::floor(iterations * 1000 / desired_throughput);
    const std::uint64_t max_duration_nanoseconds = std::ceil(1e9 / desired_throughput);

    std::vector<std::uint64_t> durations;
    durations.reserve(iterations);
    const auto test_start = std::chrono::steady_clock::now();
    for (int i = 0; i < iterations; ++i) {
        const auto start_time = std::chrono::steady_clock::now();

        func(i, buffer);

        const auto end_time = std::chrono::steady_clock::now();
        durations.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count());
    }
    const auto test_end = std::chrono::steady_clock::now();
    const auto test_duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(test_end - test_start).count();

    std::uint64_t durations_accumulated = std::accumulate(durations.begin(), durations.end(), std::uint64_t(0));
    std::uint64_t average = durations_accumulated / durations.size();
    EXPECT_TRUE(average <= max_duration_nanoseconds)
            << "Average: " << average << "ns, max: " << max_duration_nanoseconds << "ns";
    EXPECT_TRUE(test_duration_ms <= max_total_duration_ms)
            << "Duration: " << test_duration_ms << "ms, max: " << max_total_duration_ms << "ms";
}

TEST(AtomicPoseBuffer, ThroughPutTest) {
    const AtomicPoseBuffer::TimeDiff buffer_size = 10.0;
    AtomicPoseBuffer buffer{buffer_size};
    const int desired_throughput = 100000;
    const int desired_iterations = 500000;

    auto setter_lambda = [](int i, AtomicPoseBuffer& buffer) {
        api::Pose pose;
        buffer.Set(pose, i);
    };
    std::thread setter_thread(AtomicPoseBufferThread, desired_throughput,
                                desired_iterations, std::ref(buffer),
                                setter_lambda);

    auto getter_lambda = [](int i, AtomicPoseBuffer& buffer) {
        api::Pose pose = buffer.Get(i);
    };
    std::thread getter_thread(AtomicPoseBufferThread, desired_throughput,
                                desired_iterations, std::ref(buffer),
                                getter_lambda);

    setter_thread.join();
    getter_thread.join();
}

