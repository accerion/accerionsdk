#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from argparse import ArgumentParser
from csv import DictWriter
from math import pi

from sdk.csv.versions import *
from sdk.map.map import Map
from sdk.utils.argument_parser import ValidateInputPaths


def export_coordinates(map_db: Map, csv_path: str):
    fmc = get_supported_version_module(get_latest_version()).FMC

    with open(csv_path, 'w') as output_file_handle:
        writer = DictWriter(output_file_handle, fieldnames=[f.value for f in fmc])
        writer.writeheader()

        for signature in map_db.get_all_signatures(Map.Info.POSE):
            writer.writerow({
                fmc.SIGNATURE_ID: str(signature.id),
                fmc.CLUSTER_ID: str(signature.cluster_id),
                fmc.SIG_POS_X: str(signature.pose.x),
                fmc.SIG_POS_Y: str(signature.pose.y),
                fmc.SIG_POSE_TH: str(signature.pose.th * 180 / pi)
            })


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, type=str,
                        help="Input db file", action=ValidateInputPaths)
    parser.add_argument("-o", "--output", required=True,
                        type=str, help="Output csv file")
    args = vars(parser.parse_args())

    export_coordinates(Map(args["input"]), args["output"])
