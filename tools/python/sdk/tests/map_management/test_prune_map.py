#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import logging
from math import pi
from sys import platform
from time import time
from typing import Dict
from unittest.mock import patch

import numpy as np

from sdk.data.pose import Pose
from sdk.data.signature import Signature
from sdk.map_management.prune_tool.plotting import Plotting
from sdk.map_management.prune_tool.prune_map import create_metrics, report_metrics
from sdk.map_management.prune_tool.pruning import Settings, prune
from sdk.map_management.prune_tool.utils import Pixel
from sdk.metrics.common import Result


def test_prune_tool_benchmark():
    map_size = 5  # [meter]
    signature_distance = 0.025  # [meter]
    signatures_per_meter = int(1 / signature_distance)

    search_radius = 1  # [meter]
    reduction_factor = 2

    signatures_in_search_radius = pi * signatures_per_meter ** 2

    signatures = [Signature(id = i, pose=Pose(x * signature_distance, y * signature_distance))
                  for i, (x, y) in enumerate(np.ndindex(map_size * signatures_per_meter, map_size * signatures_per_meter))]

    settings = Settings(default_radius = search_radius,
                        max_signatures_in_radius = int(signatures_in_search_radius / reduction_factor),
                        bounding_boxes = [],
                        cluster_ids_to_keep = set())

    ### Test pruning

    start_time = time()
    ids_to_keep = prune(signatures, settings)
    duration = time() - start_time  # seconds

    # This is a more than just the signature count divided by the reduction factor, because on the edges of the map less
    # signature need to be removed
    expected_signatures_to_keep = 23750
    assert abs(len(ids_to_keep) - expected_signatures_to_keep) / len(signatures) < 0.01  # [percent]

    expected_duration = 24 if platform == "linux" else 50  # [second]
    assert duration < expected_duration

    ### Test plotting

    with patch("matplotlib.pyplot.show"):
        start_time = time()
        plotting = Plotting(signatures, ids_to_keep, settings)
        plotting.plot_prune_overview()
        duration = time() - start_time  # [second]
        expected_duration = 1.4 if platform == "linux" else 5.5  # [second]
        assert duration < expected_duration

        start_time = time()
        plotting.plot_signature_densities()
        duration = time() - start_time  # [second]
        expected_duration = 1.0  # [second]
        assert duration < expected_duration

        start_time = time()
        plotting.plot_signature_distances()
        duration = time() - start_time  # [second]
        expected_duration = 1.1 if platform == "linux" else 3.5  # [second]
        assert duration < expected_duration


def test_metrics():
    settings = Settings()
    settings.max_signatures_in_radius = 20
    signature_density: Dict[Pixel, int] = {(0, i): i for i in range(20)}

    logger = logging.getLogger('dummy')

    # Passing test
    metrics = create_metrics(settings, signature_density)
    assert len(metrics) == 3
    assert all(m.condition_result == Result.Pass for m in metrics)
    assert report_metrics(metrics, logger) == False

    # Failing test, some value is not smaller than required maximum
    settings.max_signatures_in_radius = int(settings.max_signatures_in_radius / 2 - 1)
    metrics = create_metrics(settings, signature_density)
    assert all(m.condition_result == Result.Fail for m in metrics)
    assert report_metrics(metrics, logger) == True
