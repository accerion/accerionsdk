#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.csv.versions import Version, determine_version
from sdk.tests.test_data.files import TestData


def test_determine_version():
    versions_to_test = {
        Version("6.1.0"): TestData.File.ARAMS_USER_LOGS_V61,
        Version("6.1.2"): TestData.File.ARAMS_USER_LOGS_V612,
        Version("6.1.5"): TestData.File.ARAMS_USER_LOGS_V615,
        Version("7.0.0"): TestData.File.ARAMS_USER_LOGS_V700
    }
    for expected_version, test_file in versions_to_test.items():
        version = determine_version(TestData.get(test_file))
        assert version == expected_version
