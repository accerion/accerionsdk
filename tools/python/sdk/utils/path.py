#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from datetime import datetime
from itertools import zip_longest
import os
from pathlib import Path
import re
from typing import List, Union, Optional, Tuple


PathList = List[Path]
AnyPath = Union[str, Path, List[str], PathList]


def ensure_directory(path: Union[str, Path]):
    """
    Check whether a folder is present, and if not, then create it.
    Done recursively to naturally also ensure parent directories.
    """
    path = Path(path).absolute()
    if not os.path.exists(path):
        ensure_directory(path.parent)
        os.mkdir(path)


def ensure_parent_directory(path: Union[str, Path]):
    ensure_directory(Path(path).parent)


def path_list(inputs: AnyPath) -> PathList:
    """Create from any (list of) path-like-typed variable a list of Paths."""
    assert inputs is not None

    if not isinstance(inputs, Path):
        assert len(inputs) != 0  # no empty string and no empty list

    if not isinstance(inputs, List):
        inputs = [inputs]
    if not isinstance(inputs[0], Path):
        inputs = [Path(i) for i in inputs]
    return inputs


def add_to_stem(path: Path, *, prefix: str = None, suffix: str = None) -> Path:
    assert isinstance(path, Path)
    if not prefix and not suffix:
        return path

    # Split stem and suffixes
    parts = path.name.split('.', 1)
    assert len(parts) in {1, 2}
    stem = parts[0]
    suffixes = f".{parts[1]}" if len(parts) == 2 else ""

    # Change stem
    if prefix:
        stem = prefix + stem
    if suffix:
        stem = stem + suffix

    # Create result
    return (path.parent / stem).with_suffix(suffixes)


def determine_prefix(file_names: Optional[AnyPath] = None) -> str:
    prefix = None if file_names is None else extract_sensor_time(file_names)
    return prefix if prefix is not None else dmy_hm_str()


SENSOR_TIME_REGEX = re.compile("\\d{9}-\\d{2}-\\d{2}-\\d{4}-\\d{2}-\\d{2}")


def extract_sensor_time(file_names: AnyPath) -> Optional[str]:
    file_names = [p.resolve() for p in path_list(file_names)]
    for file_name in file_names:
        result = SENSOR_TIME_REGEX.search(str(file_name))
        if result is not None:
            return result[0]
    return None


def datetime_str(format: str) -> str:
    return datetime.now().strftime(format)


def dmy_hm_str() -> str:
    """Return string with date and time like 'dd-mm-yyyy-HH-MM'."""
    return datetime_str("%d-%m-%Y-%H-%M")


def separate_common_parent(input_files: AnyPath) -> Tuple[str, List[str]]:
    """It is assumed that all input files are with respect to some common location."""

    if len(input_files) in {0, 1}:
        return "", [str(f) for f in input_files]
    input_files: PathList = path_list(input_files)

    split_input_files = [Path(f).parts for f in input_files]
    # "Transpose", go from
    #  [
    #      [folder11, folder12,    name1],
    #      [folder21, folder22, folder23, name2]
    #  ]
    #  to
    #  [
    #      [folder11, folder21],
    #      [folder12, folder22],
    #      [   name1, folder23],
    #      [      "",    name2]
    #  ]
    split_input_files_transposed = list(map(list, zip_longest(*split_input_files, fillvalue="")))
    common_parent_index = 0
    for index, values in enumerate(split_input_files_transposed):
        if not all(values[0] == v for v in values[1:]):
            common_parent_index = index
            break

    base_folder = ""
    if common_parent_index != 0:
        base_folder = os.path.join(*[str(p) for p in split_input_files[0][:common_parent_index]])

    return base_folder, [os.path.join(*[str(p) for p in parts[common_parent_index:]]) for parts in split_input_files]
