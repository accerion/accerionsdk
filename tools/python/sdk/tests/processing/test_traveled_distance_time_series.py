#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import pytest

from sdk.graph.graph import Pose
from sdk.processing.traveled_distance_time_series import TraveledDistanceTimeSeries


def test_traveled_distance_time_series():
    factor = 1.2
    times = list(range(1, 11, 2))  # 1, 3, 5, 7, 9
    poses = [Pose(x * factor) for x in times]

    print(times)
    print(poses)

    series = TraveledDistanceTimeSeries(times, poses)

    for t in range(12):
        value = series[t] + times[0] * factor
        print(f"{t}: {value}")
        if t <= times[0]:
            assert value == pytest.approx(times[0] * factor)
        elif t >= times[-1]:
            assert value == pytest.approx(times[-1] * factor)
        else:
            assert value == pytest.approx(t * factor)
