/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionMap/validator.h"

#include <cctype>
#include <functional>
#include <list>

#include "AccerionDatabaseHandling/integrity.h"
#include "AccerionMap/map_version.h"
#include "AccerionMap/schema_creator.h"

#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/string.h"
#include "AccerionSensorAPI/utils/units.h"

namespace detail {

std::string CleanQueryForCompare(const std::string& s) {
    // Remove comments
    auto lines = accerion::api::Split(s, "\n");
    for (auto& line : lines) {
        if (line.empty()) {
            continue;
        }
        auto parts = accerion::api::Split(line, "--");
        if (parts.size() > 2) {
            // No clue what is input here, so should not be accepted
            return "";
        } else if (parts.size() == 2) {
            line = parts[0];
        }
    }

    // Remove ALL whitespaces
    auto result = accerion::api::Join(lines.begin(), lines.end(), "");
    result.erase(std::remove_if(result.begin(), result.end(),
                                [](const auto& c) { return std::isspace(static_cast<unsigned char>(c)); }),
                 result.end());
    return std::move(result);
}

using Check = std::function<accerion::api::Acknowledgement(const accerion::sqlite::Database&,
                                                            const accerion::map::SchemaCreator&)>;

accerion::api::Acknowledgement Validate(const std::string& path, const accerion::map::SchemaCreator& creator,
                                            const std::list<detail::Check>& checks) {
    // Sanity check that database can be opened
    if (accerion::sqlite::IsCorrupt(path)) {
        return {false, "Database is corrupt"};
    }
    accerion::sqlite::Database database{path, SQLITE_OPEN_READONLY};
    database.Execute("PRAGMA trusted_schema = OFF");  // safety measure
    for (const auto &check : checks) {
        const auto ack = check(database, creator);
        if (!ack.accepted) {
            return ack;
        }
    }
    return {true, ""};
}

struct SqliteSchemaRow {
    std::string type;
    std::string name;
    std::string table;
    std::size_t page;
    std::string sql;
};
std::vector<SqliteSchemaRow> QuerySqliteSchema(const accerion::sqlite::Database& database) {
    // For more information on the structure and content to be expected from sqlite_master,
    // check https://www.sqlite.org/schematab.html

    const auto statement = database.PrepareStatement(R"SQL(
        SELECT type, name, tbl_name, rootpage, sql
          FROM sqlite_schema
         WHERE sql IS NOT NULL  -- omit internal/automatic indices
    )SQL");

    // Firstly, convert the raw rows to named rows, for less error-prone filtering
    auto rows = statement.GetRows<std::string, std::string, std::string, std::size_t, std::string>();
    std::vector<SqliteSchemaRow> items;
    items.reserve(rows.size());
    for (auto& row : rows) {
        items.push_back({
            std::move(std::get<0>(row)),
            std::move(std::get<1>(row)),
            std::move(std::get<2>(row)),
            std::get<3>(row),
            std::move(std::get<4>(row))
        });
    }

    // Secondly, filtering rows that are sqlite specific
    const auto filter_item = [](const auto& item) {
        if (item.type == "table" &&
                item.name == "sqlite_sequence" &&
                item.table == "sqlite_sequence" &&
                item.sql == "CREATE TABLE sqlite_sequence(name,seq)") { return true; }
        return false;
    };
    accerion::api::Erase(items, filter_item);
    return items;
}

}  // namespace detail

namespace check {

accerion::api::Acknowledgement NoUnexpectedElements(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    const auto rows = detail::QuerySqliteSchema(database);
    if (!std::all_of(rows.cbegin(), rows.cend(),
                     [](const auto& row) { return row.type == "table" && row.page != 0 || row.type == "index"; })) {
        // Types like 'view' or 'trigger' can be present and are caught here
        // The page is zero or null in case of views, triggers and virtual tables, so also virtual tables are caught
        return {false, "Database contains other elements than regular tables and indices"};
    }

    auto modules = database.PrepareStatement("PRAGMA module_list").GetVector<std::string>();
    accerion::api::Erase(modules, [](const auto& module) { return module == "json_tree" || module == "json_each"; });
    if (!modules.empty()) {
        // Modules can be user defined, but anything else than the default are caught here. Moreover, virtual tables can
        // only be made using modules, although modules are not always used for that (like the json ones).
        return {false, "Database has unknown modules/extensions (" + std::to_string(modules.size()) +
                       "), like '" + modules.front() + "'"};
    }

    return {true, ""};
}

accerion::api::Acknowledgement RunSqliteQuickCheck(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    // On sensor, the takes just over 5 minutes for a 2.6 GB map with 300.000 signatures
    const auto rows = database.PrepareStatement("PRAGMA quick_check").GetVector<std::string>();
    if (!rows.empty() && rows.front() != "ok") {
        return {false, "Database does not pass quick check: " + rows.front()};
    }
    return {true, ""};
}

accerion::api::Acknowledgement MetadataCorrect(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    const auto table_stmt = database.
                            PrepareStatement("SELECT name FROM sqlite_master WHERE type='table' AND name='metadata';");
    if (table_stmt.GetRows<std::string>().empty()) {
        return {false, "Metadata is not valid"};
    }
    const auto version_string = database.PrepareStatement("SELECT value FROM metadata WHERE key = 'map_version'").
                                    GetValue<std::string>();
    if (version_string.empty() || !creator.FindSchema(accerion::api::Version::Parse(version_string))) {
        return {false, "Metadata is not valid"};
    }
    return {true, ""};
}

accerion::api::Acknowledgement ExpectedTablesPresent(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    auto rows = detail::QuerySqliteSchema(database);
    accerion::api::Erase(rows, [](const auto& item) { return item.type != "table"; });
    std::unordered_set<std::string> tables_in_database{};
    tables_in_database.reserve(rows.size());
    std::transform(rows.cbegin(), rows.cend(), std::inserter(tables_in_database, tables_in_database.end()),
                   [](const auto& row) { return row.table; });

    const auto expected_schema = creator.FindSchema(accerion::map::LatestVersion());
    for (const auto& expected_table : expected_schema.value().tables) {
        if (tables_in_database.count(expected_table.name) != 1) {
            return {false, "Database does not contain table '" + expected_table.name + "'"};
        }
    }

    return {true, ""};
}

accerion::api::Acknowledgement TablesKnownAndCorrect(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    auto rows = detail::QuerySqliteSchema(database);
    accerion::api::Erase(rows, [](const auto& item) { return item.type != "table"; });

    // To check if a table is valid, resort to "exactly" matching of the sql field to the SQL creation statement
    auto schema = creator.FindSchema(accerion::map::LatestVersion());
    for (const auto& row : rows) {
        const auto opt_table_description = schema.value().FindTable(row.name);
        if (!opt_table_description) {
            return {false, "Database contains unknown table '" + row.name + "'"};
        }
        const auto create_table_stmt = "CREATE TABLE " + row.name + " (" + opt_table_description.value().columns + ")";
        if (detail::CleanQueryForCompare(row.sql) != detail::CleanQueryForCompare(create_table_stmt)) {
            return {false, "Database table '" + row.name + "' has wrong definition"};
        }
    }

    return {true, ""};
}

accerion::api::Acknowledgement SettingsCorrect(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    const std::unordered_set<std::string> kPragmasNotOfInterestExpectedValues{
            "application_id", "case_sensitive_like", "collation_list", "compile_options", "count_changes",
            "data_version", "database_list", "default_cache_size", "empty_result_callbacks", "encoding",
            "foreign_key_check", "foreign_key_list", "freelist_count", "full_column_names", "function_list", "hexkey",
            "hexrekey", "incremental_vacuum", "index_info", "index_list", "index_xinfo", "integrity_check", "key",
            "mmap_size", "module_list", "optimize", "page_count", "page_size", "pragma_list", "quick_check", "rekey",
            "schema_version", "short_column_names", "shrink_memory", "table_info", "table_list", "table_xinfo",
            "temp_store_directory", "textkey", "textrekey", "trusted_schema", "user_version", "wal_autocheckpoint",
            "wal_checkpoint"
    };
    // Values stored in container:
    //   - pragma name (used like "pragma_<name>" as query table)
    //   - expected value as text
    //   - field name in query table (if not the pragma name)
    struct PragmaDetails {
        std::string expected_value;
        std::string pragma_table_field;
    };
    // Lots of these values are just the default ones when opening a connection. There are only a few persistent
    // settings that are set in an earlier connection, that remain in a later connection. To not find this out for every
    // setting, almost all plausible ones are mentioned here.
    const std::unordered_map<std::string, PragmaDetails> kExpectedPragmaValues{
            {"analysis_limit",              {"0"}},
            {"auto_vacuum",                 {"0"}},
            {"automatic_index",             {"1"}},
            {"busy_timeout",                {"0",           "timeout"}},
            {"cache_size",                  {"-2000"}},
            {"cache_spill",                 {"483"}},
            {"cell_size_check",             {"0"}},
            {"checkpoint_fullfsync",        {"0"}},
            {"defer_foreign_keys",          {"0"}},
            {"foreign_keys",                {"1"}},
            {"fullfsync",                   {"0"}},
            {"hard_heap_limit",             {"0"}},
            {"ignore_check_constraints",    {"0"}},
            {"journal_mode",                {"delete"}},
            {"journal_size_limit",          {"-1"}},
            {"legacy_alter_table",          {"0"}},
            {"locking_mode",                {"normal"}},
            {"max_page_count",              {"1073741823"}},
            {"query_only",                  {"0"}},
            {"read_uncommitted",            {"0"}},
            {"recursive_triggers",          {"0"}},
            {"reverse_unordered_selects",   {"0"}},
            {"secure_delete",               {"0"}},
            {"soft_heap_limit",             {"0"}},
            {"synchronous",                 {"2"}},
            {"temp_store",                  {"0"}},
            {"threads",                     {"0"}},
            {"writable_schema",             {"0"}}
    };

    const auto get_pragma_value = [&database](const std::string& pragma_name, const std::string& field) {
        const auto statement = database.PrepareStatement(
                "SELECT CAST(" + (field.empty() ? pragma_name: field) + " AS TEXT) FROM pragma_" + pragma_name);
        // Try to retrieve string or integer, but returned as string
        try {
            return statement.GetValue<std::string>();
        } catch (const std::runtime_error&) {}
        try {
            return std::to_string(statement.GetValue<int>());
        } catch (const std::runtime_error&) {
            throw std::runtime_error("Pragma '" + pragma_name + "' returns unknown type");
        }
    };

    const auto pragmas = database.PrepareStatement("pragma pragma_list").GetVector<std::string>();
    std::unordered_set<std::string> pragmas_in_db{pragmas.cbegin(), pragmas.cend()};
    accerion::api::Erase(pragmas_in_db,
                         [&to_filter = kPragmasNotOfInterestExpectedValues](const auto& p) {
                             return to_filter.count(p) > 0;
                         });
    for (const auto& pragma : pragmas_in_db) {
        const auto value_iter = kExpectedPragmaValues.find(pragma);
        if (value_iter == kExpectedPragmaValues.cend()) {
            return {false, "No expected value for pragma '" + pragma + "'"};
        }
        const auto pragma_value = get_pragma_value(pragma, value_iter->second.pragma_table_field);
        if (pragma_value != value_iter->second.expected_value) {
            return {false, "Value for pragma '" + pragma + "' is not correct, got '"
                           + pragma_value + "' while expecting '" + value_iter->second.expected_value + "'"};
        }
    }
    return {true, ""};
}

accerion::api::Acknowledgement NoForeignKeyViolations(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    if (accerion::sqlite::AnyForeignKeyViolations(database)) {
        return {false, "There are foreign key violations"};
    }
    return {true};
}

accerion::api::Acknowledgement DataFilled(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    // Assumes that there are no foreign key violations present
    const auto counts = database.PrepareStatement(R"SQL(
		SELECT (SELECT count()
                  FROM signatures) AS signature_count,
		       (SELECT count()
                  FROM data) AS data_count
    )SQL").GetRows<std::size_t, std::size_t>();
    const auto signature_count = std::get<0>(counts.front());
    const auto data_count = std::get<1>(counts.front());
    if (signature_count != data_count) {
        return {false, "Not all signatures have data"};
    }
    return {true, ""};
}

accerion::api::Acknowledgement IndexInClusterCorrect(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    {
        accerion::sqlite::Database temp(database.GetFilePath(), SQLITE_OPEN_READWRITE);
        creator.CreateIndices(temp);
    }
    const auto rows = database.PrepareStatement(R"SQL(
        SELECT cluster_id,
               min(index_in_cluster) as minimum,
               max(index_in_cluster) as maximum,
               count(DISTINCT index_in_cluster) as unique_count
          FROM signatures
         GROUP BY cluster_id
        HAVING    minimum <> 0
               OR maximum + 1 <> unique_count
         LIMIT 1;
    )SQL").GetVector<accerion::api::ClusterId>();
    if (!rows.empty()) {
        return {false, "Field 'index_in_cluster' is not correctly filled for cluster " + std::to_string(rows.front())};
    }
    return {true, ""};
}

accerion::api::Acknowledgement NoVacuumRequired(const accerion::sqlite::Database& database,
                                                            const accerion::map::SchemaCreator& creator) {
    // Check based on amount of empty pages, does not take fragmented pages into account
    const auto page_size_in_bytes = database.PrepareStatement("PRAGMA page_size").GetValue<std::size_t>();
    const auto page_count = database.PrepareStatement("PRAGMA page_count").GetValue<std::size_t>();
    const auto free_pages = database.PrepareStatement("PRAGMA freelist_count").GetValue<std::size_t>();
    if (free_pages * page_size_in_bytes >= accerion::api::kGigaByte) {
        return {false, "Database has more than 1 GB of unused space allocated, VACUUM the database"};
    }
    if (static_cast<double>(free_pages) / static_cast<double>(page_count) >= .5) {
        return {false, "Database has more than 50% of unused space allocated, VACUUM the database"};
    }
    return {true, ""};
}

}  // namespace check

namespace accerion {

namespace map {

accerion::api::Acknowledgement Validator::ValidateSafety(const std::string& path,
                                                            const accerion::map::SchemaCreator& creator,
                                                            bool all_checks) {
    if (!creator.FindSchema(accerion::map::LatestVersion())) return {false, "No schema found"};
    return detail::Validate(path, creator, [&all_checks]() {
        std::list<detail::Check> checks{};
        checks.emplace_back(check::NoUnexpectedElements);  // for security reasons, first do this check
        if (all_checks) checks.emplace_back(check::RunSqliteQuickCheck);
        checks.emplace_back(check::MetadataCorrect);
        return checks;
    }());
}

accerion::api::Acknowledgement Validator::Validate(const std::string& path,
                                                    const accerion::map::SchemaCreator& creator,
                                                    bool all_checks) {
    if (!creator.FindSchema(accerion::map::LatestVersion())) return {false, "No schema found"};
    return detail::Validate(path, creator, [&all_checks]() {
        std::list<detail::Check> checks{};
        checks.emplace_back(check::ExpectedTablesPresent);
        checks.emplace_back(check::TablesKnownAndCorrect);
        checks.emplace_back(check::SettingsCorrect);
        if (all_checks) checks.emplace_back(check::NoForeignKeyViolations);
        if (all_checks) checks.emplace_back(check::DataFilled);  // needs to be done after foreign key violations check
        checks.emplace_back(check::IndexInClusterCorrect);
        checks.emplace_back(check::NoVacuumRequired);
        // No check for indices, Clean will create the ones required and just delete the rest
        return checks;
    }());
}

void Validator::Clean(const std::string& path, const accerion::map::SchemaCreator& creator) {
    // Remove any unknown or wrong indices
    auto schema_opt = creator.FindSchema(accerion::map::LatestVersion());
    if (!schema_opt) throw std::runtime_error("No schema found");
    accerion::sqlite::Database database{path, SQLITE_OPEN_READWRITE};

    auto rows = detail::QuerySqliteSchema(database);
    accerion::api::Erase(rows, [](const auto &item) { return item.type != "index"; });

    for (const auto& row : rows) {
        const auto opt_index_descr = schema_opt.value().FindIndex(row.name);
        if (!opt_index_descr) {
            // Unknown index
            database.Execute("DROP INDEX " + row.name);
            continue;
        }
        const auto create_index_stmt = "CREATE INDEX " + row.name + " ON " + opt_index_descr.value().specification;
        if (detail::CleanQueryForCompare(row.sql) != detail::CleanQueryForCompare(create_index_stmt)) {
            // Wrong index
            database.Execute("DROP INDEX " + row.name);
        }
    }
    creator.CreateIndices(database);
}

}  // namespace map
}  // namespace accerion
