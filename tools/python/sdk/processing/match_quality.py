#!/usr/bin/env python3
"""
 Copyright (c) 2024 Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from typing import List, Tuple

from packaging.version import Version
from sdk.arams.arams import Arams
from sdk.utils.print import print_v


def get_match_quality(arams: Arams, *, verbose: bool = True) -> Tuple[List[float], List[float], List[float]]:
    if arams.get_version() < Version("6.1.5"):
        print_v(verbose, __name__,
                ": 'Match quality' data not supported within Arams before v6.1.5.")
        return [], [], []
    elif "LDC" not in arams:
        print_v(verbose, __name__,
                ": Could not find log_drift_corrections file.")
        return [], [], []

    LDC = arams.LDC
    df = arams[LDC]

    pos_x = df[LDC.NEW_XPOS].to_list()
    pos_y = df[LDC.NEW_YPOS].to_list()
    match_quality = df[LDC.MATCH_QUALITY].to_list()

    return pos_x, pos_y, match_quality
