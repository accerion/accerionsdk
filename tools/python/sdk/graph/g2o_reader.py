#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from enum import Enum
from pathlib import Path
from typing import Optional

from sdk.graph.graph import Graph, InfMatrix, Pose
from sdk.utils.path import AnyPath, path_list


class G2oTags(Enum):
    VERTEX = "VERTEX_SE2"
    EDGE = "EDGE_SE2"
    FIX = "FIX"


class Reader:
    @staticmethod
    def read(file_name):
        return Reader(file_name).graph

    def __init__(self, file_name):
        self.graph = Graph()
        self.__read_file(file_name)

    def __read_file(self, file_name):
        with open(file_name, 'r') as input_file:
            # Read all lines in memory and loop over them for efficiency
            for line in input_file.readlines():
                if len(line.strip()) == 0:
                    continue

                # Use any number of spaces as separator to split the line
                parts = line.split()

                # Based on the tag at the beginning the line, parse the remainder of the line
                tag = G2oTags(parts[0])
                if tag == G2oTags.VERTEX:
                    self.__read_vertex(parts)
                elif tag == G2oTags.EDGE:
                    self.__read_edge(parts)
                elif tag == G2oTags.FIX:
                    self.__read_fix(parts)

    def __read_vertex(self, parts):
        # For efficiency, explicitly address every field
        pose = Pose(x=float(parts[2]), y=float(parts[3]), th=float(parts[4]))
        vertex = Graph.Vertex(id=int(parts[1]), pose=pose)
        if len(parts) == 7 and parts[5] == "#":
            vertex.type = Graph.Vertex.Type(parts[6])
        self.graph.vertices.append(vertex)

    def __read_edge(self, parts):
        # For efficiency, explicitly address every field
        pose = Pose(x=float(parts[3]), y=float(parts[4]), th=float(parts[5]))
        inf = InfMatrix(x_x=float(parts[6]), x_y=float(parts[7]), x_th=float(parts[8]),
                        y_y=float(parts[9]), y_th=float(parts[10]), th_th=float(parts[11]))
        edge = Graph.Edge(id_begin=int(parts[1]), id_end=int(parts[2]), pose=pose, inf=inf)
        if len(parts) == 14 and parts[12] == "#":
            edge.type = Graph.Edge.Type(parts[13])
        self.graph.edges.append(edge)

    def __read_fix(self, parts):
        f = Graph.Fix(int(parts[1]))
        self.graph.fixes.append(f)


def is_g2o_file(path: Path) -> bool:
    return path.suffix == ".g2o"


def find_g2o_file(paths: AnyPath) -> Optional[Path]:
    for path in path_list(paths):
        if is_g2o_file(path):
            return path
    return None


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="Input g2o file")
    args = vars(parser.parse_args())

    g2o_file = find_g2o_file(args["input"])
    if not g2o_file:
        raise Exception("Could not find g2o file.")

    graph = Reader.read(g2o_file)

    print("Sizes")
    print(f"    vertices array: {len(graph.vertices)}")
    print(f"    edges array: {len(graph.edges)}")
    print(f"    fixes array: {len(graph.fixes)}")

    print("First entries")
    if graph.vertices:
        print(f"    {graph.vertices[0]}")
    if graph.edges:
        print(f"    {graph.edges[0]}")
    if graph.fixes:
        print(f"    {graph.fixes[0]}")
