#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""


class MonitoredData:
    """
    Wrapper class that can be used to monitor the progress of the data being processed. Can be reused.
    The constructor accepts the iterable data container and a progress call back function. The latter should accept the
    progress value as a percentage (integer value) and will only be called when it is different from the previous value.
    """
    # Implementation inspired by
    # https://stackoverflow.com/questions/24956928/using-a-progress-handler-in-python-sqlite-executemany-query#comment38798165_24957207

    def __init__(self, data, progress_cb):
        self.__data = data
        self.__progress_cb = progress_cb

        self.__index = 0
        self.__previous_progress_percentage = None

    def __len__(self) -> int:
        return len(self.__data)

    def __getitem__(self, index: int):
        if self.__index >= len(self.__data):
            raise IndexError("Index out of range")
        return self.__data[index]

    def __iter__(self):
        self.__index = 0
        self.__previous_progress_percentage = None
        self.__report_progress()
        return self

    def __next__(self):
        if self.__index >= len(self):
            self.__index = 0
            self.__previous_progress_percentage = None
            raise StopIteration

        value = self[self.__index]
        self.__index += 1
        self.__report_progress()

        return value

    def __report_progress(self):
        if not self.__progress_cb:
            # No call back, nothing to do
            return

        if self.__previous_progress_percentage is None:
            current_progress = 0
        else:
            current_progress = int(
                round(self.__index / len(self) * 100))

        if current_progress != self.__previous_progress_percentage:
            self.__progress_cb(current_progress)
            self.__previous_progress_percentage = current_progress
