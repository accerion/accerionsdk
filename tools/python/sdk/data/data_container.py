#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass, field
from pathlib import Path
from typing import List

from sdk.arams.arams import Arams
from sdk.data.signature import Signature
from sdk.data.signature_reader import SignatureReader
from sdk.graph.g2o_reader import find_g2o_file, Reader as G2oReader
from sdk.graph.graph import Graph
from sdk.utils.configuration import Configuration
from sdk.utils.preparations import apply_configuration


__all__ = ["DataContainer"]


@dataclass
class DataContainer:
    _Arams = Arams
    _SignatureReader = SignatureReader

    arams_original: _Arams = None
    arams: _Arams = None
    signatures: List[Signature] = field(default_factory=list)
    graph: Graph = None

    @classmethod
    def create(cls, input_files: List[Path], conf: Configuration = None):
        data = DataContainer()

        data.arams_original = cls._Arams(input_files)

        data.arams = data.arams_original.get_copy()
        if conf is not None:
            data.arams = apply_configuration(data.arams, conf)

        data.signatures = cls._SignatureReader.read(input_files)

        g2o_file = find_g2o_file(input_files)
        if g2o_file is not None:
            data.graph = G2oReader.read(g2o_file)

        return data
