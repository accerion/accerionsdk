#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import Dict, List

from sdk.data.signature import Signature
from sdk.graph.graph import Graph


def create_vertex_id_dict(graph: Graph) -> Dict:
    """
    Create dictionary relating the vertex id to the index in the vertex list of supplied graph.
    Usage:
        dict_vertex_id_to_index = create_vertex_id_dict(graph)
        vertex_index = dict_vertex_id_to_index[graph.vertices[10].id]
    so vertex_index should be 10, provided that graph has enough vertices.
    """
    return dict((v.id, i) for i, v in enumerate(graph.vertices))


def create_edge_vertex_ids_dict(graph: Graph, *, sort: bool = False) -> Dict:
    """
    Create dictionary relating the vertex indices of the edges to the index in the edge list of supplied graph.
    Usage:
        dict_edge_vertices_to_index = create_edge_vertex_ids_dict(graph)
        edge = graph.edge[10]
        edge_index = dict_edge_vertices_to_index[(edge.id_begin, edge.id_end)]
    so edge_index should be 10, provided that graph has enough edges.

    When the sort flag is set to True, then the vertex indices will be sorted, so (23, 4) will become (4, 23).

    Note that duplicate edges are not supported (two edges having the same id_begin and id_end).
    """
    def sorted_node_indices(e: Graph.Edge):
        return (e.id_begin, e.id_end) if e.id_begin < e.id_end else (e.id_end, e.id_begin)
    return dict((sorted_node_indices(e) if sort else (e.id_begin, e.id_end), i) for i, e in enumerate(graph.edges))


def create_vertices(signatures: List[Signature]) -> List[Graph.Vertex]:
    """
    Transforms a list of signatures into a list of graph vertices.

    Each vertex is created from the corresponding signature's ID and pose,
    and is assigned the type 'Signature'.
    """
    vertices = []
    for sig in signatures:
        vertex = Graph.Vertex()
        vertex.id = sig.id
        vertex.pose = sig.pose
        vertex.type = Graph.Vertex.Type.Signature
        vertices.append(vertex)
    return vertices
