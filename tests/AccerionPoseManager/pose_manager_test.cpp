/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionPoseManager/pose_manager.h"
#include "AccerionSensorAPI/structs/pose.h"

#include "pose_utils.h"


using PoseManager2d = accerion::pose_manager::PoseManager<accerion::api::Pose>;
using PoseTree2d = PoseManager2d::PoseTreeType;


TEST(PoseManager, SetGetCommonChild) {
    // Trees:          |------- map ------|- robot -|
    // Tree id's:    global -> local -> common -> child
    // Distances:           4        2         1

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id child_id = "child";

    // Set up PoseManager
    PoseTree2d robot;
    robot.Set(child_id, common_id, {-1, 0, 0});

    PoseManager2d manager(std::move(robot), common_id, child_id, child_id);
    manager.SetLocal({2+1, 0, 0});
    manager.SetGlobal({4+2+1, 0, 0});

    // Perform tests
    accerion::api::Pose p = {1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, child_id));
    p = {2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, common_id));
    p = {4, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, manager.local_id));

    p = {4+2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, common_id));
    p = {4+2+1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, child_id));
}

TEST(PoseManager, SetGetDifferentChild) {
    // Trees:          |------- map ------|- robot -|
    // Tree id's:    global -> local -> common -> local_child
    // Distances:           4        2         1
    // Tree id's:                       common -> global_child
    // Distances:                              3

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id local_child_id = "local_child";
    const PoseManager2d::Id global_child_id = "global_child";

    // Set up PoseManager
    PoseTree2d robot;
    robot.Set(local_child_id, common_id, {-1, 0, 0});
    robot.Set(global_child_id, common_id, {-3, 0, 0});

    PoseManager2d manager{std::move(robot), common_id, global_child_id, local_child_id};
    manager.SetLocal({2+1, 0, 0});
    manager.SetGlobal({4+2+3, 0, 0});

    // Perform tests
    accerion::api::Pose p = {1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, local_child_id));
    p = {3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, global_child_id));

    p = {2+1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id));
    p = {4+2+3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id));

    p = {4+2+1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, local_child_id));
}

TEST(PoseManager, SetGetRotatedLocalChild) {
    // Trees:          |------- map ------|- robot -|
    // Tree id's:    global -> local -> common -> local_child
    // (x,y,th):       (1,-1,-90) (1,-2,90) (0,1,-90)
    // Tree id's:                       common -> global_child
    // (x,y,th):                            (3,0,0)

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id local_child_id = "local_child";
    const PoseManager2d::Id global_child_id = "global_child";

    // Set up PoseManager
    PoseTree2d robot;
    robot.Set(local_child_id, common_id, {1, 0, 90});
    robot.Set(global_child_id, common_id, {-3, 0, 0});

    PoseManager2d manager{std::move(robot), common_id, global_child_id, local_child_id};
    manager.SetLocal({0, -2, 0});
    manager.SetGlobal({2, 0, 0});

    // Perform tests
    accerion::api::Pose p = {0, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, local_child_id));
    p = {3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, global_child_id));

    p = {0, -2, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id));
    p = {2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id));

    p = {-1, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, local_child_id));
}

TEST(PoseManager, CreateSlam) {
    // Trees:          |------- map ---|- robot -|
    // Tree id's:    global -> common -> sensor
    // (x,y,th):         (1,1,0)   (0,1,-90)
    // Tree id's:              common -> global_child
    // (x,y,th):                    (3,0,0)

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id sensor_id = "sensor";
    const PoseManager2d::Id global_child_id = "global_child";

    accerion::api::Pose p_common_sensor = {1, 0, 90};
    accerion::api::Pose p_common_global = {-3, 0, 0};

    // Set up PoseManager using CreateSlam
    auto manager = PoseManager2d::CreateSlam(common_id,
                              sensor_id, p_common_sensor,
                              global_child_id, p_common_global);
    manager.SetGlobal({2, 0, 0});

    // Perform tests
    accerion::api::Pose p = {0, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, sensor_id));
    p = {3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, global_child_id));

    p = {2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id));

    p = {-1, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, sensor_id));
}

TEST(PoseManager, Create) {
    // Trees:          |------- map ------|- robot -|
    // Tree id's:    global -> local -> common -> local_child
    // (x,y,th):       (1,-1,-90) (1,-2,90) (0,1,-90)
    // Tree id's:                       common -> global_child
    // (x,y,th):                            (3,0,0)

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id local_child_id = "local_child";
    const PoseManager2d::Id global_child_id = "global_child";

    accerion::api::Pose p_common_sensor = {1, 0, 90};
    accerion::api::Pose p_common_global_child = {-3, 0, 0};

    // Set up PoseManager using CreateSlam
    auto manager = PoseManager2d::Create(common_id,
                                         "dummy", accerion::api::Pose::Identity(),
                                         global_child_id, p_common_global_child,
                                         local_child_id, p_common_sensor);
    manager.SetLocal({0, -2, 0});
    manager.SetGlobal({2, 0, 0});

    // Perform tests
    accerion::api::Pose p = {0, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, local_child_id));
    p = {3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(common_id, global_child_id));

    p = {0, -2, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id));
    p = {2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id));

    p = {-1, 1, -90};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, local_child_id));
}
