/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <iostream>
#include <string>

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/file_transfer_task.h"
#include "AccerionSensorAPI/utils/sensor.h"

#include "blocking_sensor.h"

#include "AccerionUtilities/string_parser.h"


// To not clutter up the cli_command.cpp, the stream parsing helper functions are stored here.
namespace detail {

template <typename T>
struct ResultGet {
    bool success;
    T value;
};

std::string GetString(std::istream& is, bool lower);

template <typename T>
ResultGet<T> Get(std::istream& is) {
    if (!is.good()) {
        return {false, {}};
    }
    try {
        ResultGet<T> result{true, {}};
        is >> result.value;
        return result;
    } catch (...) {
        return {false, {}};
    }
}

template <>
inline ResultGet<bool> Get(std::istream& is) {
    std::string result = GetString(is, true);
    if (result == "on" || result == "off") {
        return {true, result == "on"};
    }
    std::cout << "Boolean input not valid, expecting 'on' or 'off'." << std::endl;
    return {false, {}};
}

template <typename Integer, typename = typename std::enable_if<std::is_integral<Integer>::value>::type>
ResultGet<Integer> GetInt(std::istream& is) {
    auto result = Get<Integer>(is);
    if (!result.success) {
        std::cout << "Integer input not given or invalid." << std::endl;
    }
    return result;
}

template <>
inline ResultGet<BlockingSensor::Mode> Get(std::istream& is) {
    std::string result = GetString(is, true);

    BlockingSensor::Mode mode;
    if (result == "internal") {
        mode = BlockingSensor::Mode::Internal;
    } else if (result == "mapping") {
        mode = BlockingSensor::Mode::Mapping;
    } else if (result == "cont-sig-updating") {
        mode = BlockingSensor::Mode::ContSignatureUpdating;
    } else if (result == "localizing") {
        mode = BlockingSensor::Mode::AbsLocalization;
    } else if (result == "line") {
        mode = BlockingSensor::Mode::LineFollowing;
    } else if (result == "recording") {
        mode = BlockingSensor::Mode::Recording;
    } else if (result == "aruco") {
        mode = BlockingSensor::Mode::Aruco;
    } else if (result == "idle") {
        mode = BlockingSensor::Mode::Idle;
    } else if (result == "expert") {
        mode = BlockingSensor::Mode::Expert;
    } else if (result == "reboot") {
        mode = BlockingSensor::Mode::Reboot;
    } else {
        std::cout << "Mode input not valid, expecting 'internal', 'mapping', 'cont-sig-updating', 'localizing'"
                  << ", 'line', 'recording', 'aruco', 'idle' or 'expert'." << std::endl;
        return {false, {}};
    }
    return {true, mode};
}

template <>
inline ResultGet<accerion::api::ConnectionType> Get(std::istream& is) {
    std::string result = GetString(is, true);

    accerion::api::Optional<accerion::api::ConnectionType> type = ParseConnectionType(result);
    if (type) return {true, type.value()};
    return {false, {}};
}

template <>
inline ResultGet<accerion::api::FileTransferTask> Get(std::istream& is) {
    std::string result = GetString(is, true);
    if (result.empty() || result == "replace") {
        return {true, accerion::api::FileTransferTask::kReplaceMap};
    } else if (result == "merge") {
        return {true, accerion::api::FileTransferTask::kMergeMap};
    } else if (result == "update") {
        return {true, accerion::api::FileTransferTask::kUpdateMap};
    }
    std::cout << "Strategy input not valid, expecting 'replace', 'merge' or 'update'." << std::endl;
    return {false, {}};
}

template <>
inline ResultGet<accerion::api::FileType> Get(std::istream& is) {
    std::string result = GetString(is, true);

    accerion::api::FileType type;
    if (result == "map") {
        type = accerion::api::FileType::Map;
    } else if (result == "coords") {
        type = accerion::api::FileType::Coordinates;
    } else if (result == "loops") {
        type = accerion::api::FileType::LoopClosures;
    } else if (result == "logs") {
        type = accerion::api::FileType::Logs;
    } else if (result == "recordings") {
        type = accerion::api::FileType::Recording;
    } else if (result == "update") {
        type = accerion::api::FileType::Update;
    } else {
        return {false, {}};
    }
    return {true, type};
}

// Note that the function does not require the string to be quoted
ResultGet<std::string> GetQuotedString(std::istream& is);

enum class PropertyType : std::uint8_t {
    Area = 0,
    Areas,
    BufferLength,
    Cluster,
    Clusters,
    Connection,
    Diagnostics,
    Ip,
    MapInfo,
    Pose,
    RecordingsList,
    Software
};

template <>
inline ResultGet<PropertyType> Get(std::istream& is) {
    std::string result = GetString(is, true);

    PropertyType type;
    if (result == "area") {
        type = PropertyType::Area;
    } else if (result == "areas") {
        type = PropertyType::Areas;
    } else if (result == "buffer") {
        type = PropertyType::BufferLength;
    } else if (result == "cluster") {
        type = PropertyType::Cluster;
    } else if (result == "clusters") {
        type = PropertyType::Clusters;
    } else if (result == "connection") {
        type = PropertyType::Connection;
    } else if (result == "diagnostics") {
        type = PropertyType::Diagnostics;
    } else if (result == "ip") {
        type = PropertyType::Ip;
    } else if (result == "map-info") {
        type = PropertyType::MapInfo;
    } else if (result == "pose") {
        type = PropertyType::Pose;
    } else if (result == "software") {
        type = PropertyType::Software;
    } else if (result == "recordings") {
        type = PropertyType::RecordingsList;
    } else {
        return {false, {}};
    }
    return {true, type};
}

template <>
inline ResultGet<accerion::api::Pose> Get(std::istream& is) {
    bool success = true;
    std::array<double, 3> doubles{};
    std::for_each(doubles.begin(), doubles.end(), [&is, &success](double& v) {
        auto result = Get<double>(is);
        if (result.success) {
            v = result.value;
        } else {
            success = false;
        }
    });
    accerion::api::Pose pose{};
    if (success) {
        pose.x = doubles[0];
        pose.y = doubles[1];
        pose.th = doubles[2];
    }
    else {
        std::cout << "Could not parse pose, expecting 3 values for x, y and heading." << std::endl;
    }
    return {success, pose};
}

}  // namespace detail
