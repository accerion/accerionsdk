#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from dataclasses import dataclass, field
from math import floor
from statistics import mean
from typing import Optional, List, Sequence, Tuple
import warnings

import numpy as np
from numpy import argmax
import pandas as pd

from sdk.arams.arams import Arams
from sdk.graph.graph import Pose
from sdk.processing.generic import get_input_poses
from sdk.processing.traveled_distance_time_series import TraveledDistanceTimeSeries
from sdk.processing.pose_time_series import PoseTimeSeries
from sdk.utils.configuration import Configuration
from sdk.utils.math import rounded_floor, rounded_ceil
from sdk.utils.print import print_v


@dataclass
class AggregateInfo:
    """The results provided to the user."""
    pose: Pose = field(default_factory=Pose)
    dc_count: int = 0
    mean_velocity: float = 0


def get_drift_correction_info_cluster(arams_original: Arams,
                                      conf: Configuration,
                                      *,
                                      verbose: bool = True) -> Optional[List[AggregateInfo]]:

    from sdk.utils.preparations import apply_configuration
    # The correction count plots should only use all data and corrections from the last run
    conf[Configuration.Name.PLOT_LAST_N_RUNS] = 1
    conf[Configuration.Name.FIRST_CORRECTION_ON_LINE] = False
    arams = apply_configuration(arams_original.get_copy(), conf)

    if "LDC" not in arams:
        print_v(verbose, __name__, ": Could not find logDriftCorrections file or the arams version does not support the file type.")
        return None
    LDC = arams.LDC
    df_ldc = arams[LDC]

    indices = df_ldc[LDC.INDEX].to_list()
    assert all(indices[i] < indices[i + 1] for i in range(len(indices) - 1))

    if "LRT" not in arams:
        print_v(verbose, __name__, ": Could not find logRuntimeTracking file or the arams version does not support the file type.")
        return None
    LRT = arams.LRT
    df_lrt = arams[LRT]

    # Misusing the Pose object to store velocities (with angular velocities in radians/s)
    lrt_angular_velocities_rad = np.deg2rad(df_lrt[LRT.ODOM_VEL_TH])
    velocities = [Pose(x=x, y=y, th=th)
                  for x, y, th in zip(df_lrt[LRT.ODOM_VEL_X],
                                      df_lrt[LRT.ODOM_VEL_Y],
                                      lrt_angular_velocities_rad)]

    velocity_time_series = PoseTimeSeries(df_lrt[LRT.TIME].to_list(), velocities)

    # Use 'logRuntimeTracking' indices to find correspondences between the indices from the input poses series
    indices = df_lrt[LRT.INDEX]

    # Specify the desired indices and return NaN for indices that do not exist.
    x_input, y_input, _ = [pose_data.reindex(indices) for pose_data in get_input_poses(arams)]

    # Use data from input poses, unless no correspondences could be found;
    # in that case fall back to sensor poses (corrected odometry)
    if x_input.isna().all():
        warnings.warn(
                "Could not read poses from 'logExtRefInput' file, falling back to 'logRuntimeTracking' for sensor poses.")
        x_input, y_input = df_lrt[LRT.CORRECTED_ODOM_POSE_X], df_lrt[LRT.CORRECTED_ODOM_POSE_Y]

    poses = [Pose(x, y) for x, y in zip(x_input, y_input)]

    traveled_distance_time_series = TraveledDistanceTimeSeries(df_lrt[LRT.TIME].to_list(), poses)

    def get_traveled_distance(prev_time: float, curr_time: float) -> float:
        return traveled_distance_time_series[curr_time] - traveled_distance_time_series[prev_time]

    max_sign_ids_per_cluster_crossing = conf[Configuration.Name.MAX_SIGN_IDS_PER_CLUSTER_CROSSING]
    min_dist_between_cluster_crossings = conf[Configuration.Name.MIN_DIST_BETWEEN_CLUSTER_CROSSINGS]

    # Convenience type used during the processing
    @dataclass
    class DcGroup:
        last_time: Optional[float] = None
        poses: List[Pose] = field(default_factory=list)
        velocities: List[Pose] = field(default_factory=list)

    # Collect all the information
    dc_groups: List[DcGroup] = []

    # Approach is to first collect all the drift correction information for the current group/crossing. When certain
    # conditions are met, then the group collected so far will be added to the final result.
    current_dc_group = DcGroup()
    signature_id_buffer: List[int] = []
    for index, signature_id, time, detected_x, detected_y in zip(df_ldc[LDC.INDEX],
                                                          df_ldc[LDC.SIGNATURE_ID],
                                                          df_ldc[LDC.TIME],
                                                          df_ldc[LDC.NEW_XPOS],
                                                          df_ldc[LDC.NEW_YPOS]):
        detected_pose = Pose(detected_x, detected_y)
        # In order to start a new group and potentially store the group detected so far, the traveled distance between
        # this correction group and the next group should be larger than a certain threshold
        if current_dc_group.last_time is not None and \
                get_traveled_distance(current_dc_group.last_time, time) > min_dist_between_cluster_crossings:
            # If the maximum number of unique signatures encountered is below a certain threshold, then the group
            # detected so far will be stored
            if max(signature_id_buffer) - min(signature_id_buffer) <= max_sign_ids_per_cluster_crossing:
                dc_groups.append(current_dc_group)

            signature_id_buffer.clear()
            current_dc_group = DcGroup()

        signature_id_buffer.append(signature_id)
        current_dc_group.last_time = time
        current_dc_group.poses.append(detected_pose)
        current_dc_group.velocities.append(velocity_time_series[time])

    # Post-processing
    result: List[AggregateInfo] = []

    for g in dc_groups:
        agg_info = AggregateInfo()
        agg_info.pose.x = mean(p.x for p in g.poses)
        agg_info.pose.y = mean(p.y for p in g.poses)
        agg_info.dc_count = len(g.poses)
        agg_info.mean_velocity = mean(v.norm for v in g.velocities)

        result.append(agg_info)

    return result


def determine_driving_velocity(values: List[float], bin_size: float = 0.15):
    """
    Practically, taking the middle of the fullest bin in a velocity histogram.
    """

    min_value = rounded_floor(min(values), bin_size)
    max_value = rounded_ceil(max(values), bin_size)
    n_bins = int((max_value - min_value) / bin_size)

    def index(v: float):
        i = floor((v - min_value) / bin_size)
        return i - 1 if i == n_bins else i

    bin_values = [0] * n_bins
    for value in values:
        bin_values[index(value)] += 1

    max_index = argmax(bin_values)
    return min_value + (max_index + 0.5) * bin_size


def get_correction_count(arams: Arams, conf: Configuration, velocity_margin: float = 0.1, *, verbose: bool = True
                         ) -> Tuple[List[float], List[float], List[float], Sequence[float]]:
    info: Optional[List[AggregateInfo]] = get_drift_correction_info_cluster(
        arams, conf, verbose=verbose)
    if not info:
        return [], [], [], []

    driving_velocity = determine_driving_velocity([i.mean_velocity for i in info])
    velocity_bounds = [driving_velocity - velocity_margin, driving_velocity + velocity_margin]

    pos_x, pos_y, dc_counts = zip(*[(i.pose.x, i.pose.y, i.dc_count) for i in info if velocity_bounds[0] < i.mean_velocity < velocity_bounds[1]])

    return pos_x, pos_y, dc_counts, velocity_bounds
