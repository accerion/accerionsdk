#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import argparse
from enum import Enum
from pathlib import Path
from typing import Dict, List, Tuple

import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.collections import LineCollection

from sdk.arams.arams import Arams
from sdk.data.signature import Signature
from sdk.graph.graph import Graph
from sdk.graph.filter import identify_edges
from sdk.graph.g2o_reader import Reader
from sdk.graph.graph_compare import IndexList, get_edge_diff_sets, POSE_ABS_TOL
from sdk.plotting.utils.graph import PlotGraphsSettings, PlotElements, __toggle_visibility
from sdk.plotting.utils.plotting import add_signature_poses

"""
With this script graphs from two g2o files can be visualized side-by-side with similarities and differences highlighted.
Toggling can be done on every all elements in each graph by clicking the corresponding legend item.
"""

# In the future, this script should reuse the same util functions from sdk.plotting.utils.graph


class Name(Enum):
    VERTEX = "vertex"
    EDGE = "edge"
    SAME = "same"
    DIFF = "different"
    UNIQ = "unique"
    REL = "relative"
    ABS = "absolute"


class PlotGraphsDiffSettings(PlotGraphsSettings):
    @classmethod
    def create(cls, _: int = None) -> Dict:
        return {
            Name.VERTEX: cls('k', 'o', 3)._get_plt_dict_vertices(),  # k = black
            Name.EDGE: {
                (Name.SAME, Name.REL): cls('g', ':', 1)._get_plt_dict_edges(),  # g = green
                (Name.SAME, Name.ABS): cls('g', '-', 1)._get_plt_dict_edges(),  # g = green
                (Name.DIFF, Name.REL): cls('r', ':', 1)._get_plt_dict_edges(),  # r = red
                (Name.DIFF, Name.ABS): cls('r', '-', 1)._get_plt_dict_edges(),  # r = red
                (Name.UNIQ, Name.REL): cls('b', ':', 1)._get_plt_dict_edges(),  # b = blue
                (Name.UNIQ, Name.ABS): cls('b', '-', 1)._get_plt_dict_edges()   # b = blue
            }
        }


def plot_graph(graph: Graph, index_sets: Tuple[IndexList, IndexList, IndexList], ax, settings: dict,
               draw_vertices: bool = True) -> PlotElements:
    plot_elements = PlotElements([], [], [])

    # Draw vertices
    vertex_id_to_index = {v.id: i for i, v in enumerate(graph.vertices)}

    if draw_vertices:
        vertices = np.array([[v.pose.x, v.pose.y] for v in graph.vertices])
        plot_elements.append(
            Name.VERTEX.value,
            settings[Name.VERTEX],
            ax.plot(vertices[:, 0], vertices[:, 1], **settings[Name.VERTEX]))

    # Draw loop closure edges based on 3 sets: same, different and unique edges
    edge_settings = settings[Name.EDGE]
    for name, indices in zip([Name.SAME, Name.DIFF, Name.UNIQ], index_sets):
        n_indices = len(indices)
        # Array containing all line segments, consisting of a start and end points, where each point has two
        # coordinates, so the array is used like "array[line_index, point_index, coordinate_index]"
        edges_rel = np.zeros((n_indices, 2, 2))
        edges_geom = np.zeros((n_indices, 2, 2))
        for line_segment_index in range(n_indices):
            e = graph.edges[indices[line_segment_index]]  # pose change between a and b, going from b to a

            p_a = graph.vertices[vertex_id_to_index[e.id_begin]].pose  # pose a
            p_b = graph.vertices[vertex_id_to_index[e.id_end]].pose  # pose b
            p_b_e = p_a * e.pose  # pose b applying the edge/pose change to pose a

            # Define line segments
            # Define starting point of line segment
            edges_rel[line_segment_index, 0, :] = [p_a.x, p_a.y]
            edges_geom[line_segment_index, 0, :] = [p_a.x, p_a.y]

            # Define ending point of line segment
            edges_rel[line_segment_index, 1, :] = [p_b.x, p_b.y]
            edges_geom[line_segment_index, 1, :] = [p_b_e.x, p_b_e.y]

        plot_elements.append(
            " ".join([str(n.value) for n in [Name.EDGE, name, Name.REL]]),
            edge_settings[(name, Name.REL)],
            ax.add_collection(LineCollection(edges_rel, **edge_settings[(name, Name.REL)])))
        plot_elements.append(
            " ".join([str(n.value) for n in [Name.EDGE, name, Name.ABS]]),
            edge_settings[(name, Name.ABS)],
            ax.add_collection(LineCollection(edges_geom, **edge_settings[(name, Name.ABS)])))

    return plot_elements


def add_legend(fig, ax, plot_elements: PlotElements):
    legend_items = [mlines.Line2D([], [], **s) for s in plot_elements.settings]
    legend = ax.legend(legend_items, plot_elements.names, loc='upper right')

    # Add visibility toggling functionality
    legend_handles = legend.get_lines()
    for item in legend_handles:
        item.set_picker(True)
        item.set_pickradius(10)
    legend_plots_map = dict(zip(legend_handles, plot_elements.handles))
    fig.canvas.mpl_connect('pick_event', lambda event: __toggle_visibility(legend_plots_map, event))


def plot_graphs(graphs: List[Graph], names: List[str] = None, signatures: List[Signature] = None):
    assert len(graphs) == 2, "Can only plot two graphs."
    assert all(len(g.vertices) != 0 for g in graphs), "Both graphs should contain vertices."

    loop_a = identify_edges(graphs[0], Graph.Edge.Type.Loop)
    loop_b = identify_edges(graphs[1], Graph.Edge.Type.Loop)
    both_same, both_diff, only_a, only_b = get_edge_diff_sets(graphs[0], loop_a, graphs[1], loop_b)

    settings = PlotGraphsDiffSettings.create()

    # Store all information regarding the plots
    plot_elements_list = []

    # Create two side-by-side plots
    fig, axs = plt.subplots(1, 2, sharex='all', sharey='all')
    for i in range(2):
        ax = axs[i]

        sets = (
            [index_pair[i] for index_pair in both_same],
            [index_pair[i] for index_pair in both_diff],
            only_a if i == 0 else only_b
        )

        plot_elements = plot_graph(graphs[i], sets, ax, settings)
        add_signature_poses(ax, signatures)

        add_legend(fig, ax, plot_elements)
        plot_elements_list.append(plot_elements)

        ax.set_aspect("equal")
        ax.set_xlabel("x-coordinate [m]")
        ax.set_ylabel("y-coordinate [m]")
        ax.set_title(names[i])

    fig.tight_layout()
    fig.suptitle(f"Pose graphs displaying loop closure edges\n"
                 f"same < {POSE_ABS_TOL}: {len(both_same)}, diff > {POSE_ABS_TOL}: {len(both_diff)}, "
                 f"unique {len(only_a)} vs. {len(only_b)}")
    plt.show()


def __parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", required=True, nargs=2, help="Input g2o files, exactly two")
    parser.add_argument("-c", "--coords", required=False, help="Input map coordinates csv file")

    return vars(parser.parse_args())


def __main():
    args = __parse_arguments()
    file_names = args["input"]
    graphs = [Reader.read(file_name) for file_name in args["input"]]

    names = [Path(f).stem for f in file_names]
    if names[0] == names[-1]:
        names = [Path(f) for f in file_names]

    arams = Arams(args["coords"]) if args["coords"] is not None else None

    plot_graphs(graphs, names, arams)


if __name__ == "__main__":
    __main()
