/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/version.h"

namespace accerion {

namespace map {

inline accerion::api::Version LatestVersion() { return {2, 0, 0}; }


}  // namespace sqlite
}  // namespace accerion
