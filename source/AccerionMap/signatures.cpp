/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionMap/signatures.h"
#include "AccerionDatabaseHandling/prepared_statement.h"
#include "AccerionSensorAPI/structs/signature.h"

namespace accerion {
namespace map {

std::vector<accerion::api::Signature> GetSignaturesFromDatabase(accerion::sqlite::Database& db) {
    std::string query = R"SQL(
                SELECT id, cluster_id, x, y, th
                  FROM signatures;
        )SQL";
    const auto rows = accerion::sqlite::PreparedStatement{db, query}.GetRows<accerion::api::SignatureId,
                                                                                accerion::api::ClusterId, double,
                                                                                double, double>();
    std::vector<accerion::api::Signature> signatures;
    signatures.reserve(rows.size());
    std::transform(rows.begin(), rows.end(), std::back_inserter(signatures),
                   [](const auto& row) {
                       accerion::api::Signature signature{};
                       signature.signature_id = std::get<0>(row);
                       signature.cluster_id = std::get<1>(row);
                       signature.pose.x = std::get<2>(row);
                       signature.pose.y = std::get<3>(row);
                       signature.pose.th = std::get<4>(row);
                       return signature;
                   });
    return signatures;
}

}  //  namespace map
}  //  namespace accerion
