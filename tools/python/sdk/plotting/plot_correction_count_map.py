#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.plotting.utils.plotting import *
from sdk.processing.correction_count import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_correction_count_map(data: DataContainer,
                              output_folder: Optional[Path] = None,
                              conf: Configuration = Configuration.default(),
                              *,
                              verbose: bool = True):
    pos_x, pos_y, dc_counts, velocity_bounds = get_correction_count(data.arams_original, conf, verbose=verbose)
    if not dc_counts:
        print_v(verbose, "No drift correction per cluster crossing.")
        return

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Number of drift corrections per cluster crossing", fontweight='bold')
    axis.set_title(f'Cluster crossings: {len(dc_counts)}, velocity interval: ({velocity_bounds[0]:.3f},{velocity_bounds[1]:.3f})')

    if conf[Configuration.Name.OVERLAY_INPUT_POSES]:
        add_input_poses(axis, data.arams)

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)
    _, data_cbar_handle = scatter_data(axis, pos_x, pos_y, dc_counts, factor=1, reverse=True, min_colorbar=1)

    max_count = max(dc_counts)
    tick_step = 1
    if max_count > 20:
        tick_step = ceil(max_count / 20)
    ticks = list(reversed(range(max_count, 0, -tick_step)))
    data_cbar_handle.set_ticks(ticks=ticks, labels=[f"{t:d}" for t in ticks])

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")
    axis.grid()
    set_xylim(axis, conf, [min(pos_x), max(pos_x)], [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "correction_count_map.png", conf.get_prefix(data.arams_original.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_correction_count_map)
