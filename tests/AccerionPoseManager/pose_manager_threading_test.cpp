/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include <thread>
#include <future>

#include "AccerionPoseManager/pose_manager.h"


struct SlowPose {
    static SlowPose Identity() {
        return {0};
    }

    SlowPose operator*(const SlowPose &other) const {
        auto result = *this;
        result *= other;
        return result;
    }
    SlowPose& operator*=(const SlowPose& other) {
        value += other.value;
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
        return *this;
    }

    void NormalizeRotation() {}

    SlowPose Inverse() const {
        return {-value};
    }

    SlowPose Interpolate(double ratio, const SlowPose& other) const {
        return {value + (other.value - value) * ratio};
    }

    double value;
};
using SlowPoseManager = accerion::pose_manager::PoseManager<SlowPose>;
using SlowPoseTree = SlowPoseManager::PoseTreeType;

using SlowPoseManagerSetMethod = std::function<void(SlowPoseManager&, SlowPose)>;

// Test disabled as the test may fail for some systems/compilers. The threshold for the time to give the setter the
// chance to acquire the internal lock in the PoseManager before the getter is called is system/compiler dependent.

class PoseManagerThreadingFixture : public ::testing::TestWithParam<SlowPoseManagerSetMethod> {};
TEST_P(PoseManagerThreadingFixture, DISABLED_ThreadSafety) {
    const SlowPoseManager::Id common_id = "common";
    const SlowPoseManager::Id child_id = "child";
    const auto default_pose = SlowPose{1};

    // Set up PoseManager
    SlowPoseTree robot;
    robot.Set(common_id, child_id, default_pose);

    SlowPoseManager manager(std::move(robot), common_id, child_id, child_id);

    // Perform tests
    using namespace std::chrono;
    using Clock = steady_clock;
    const auto start = Clock::now();
    Clock::duration time_diff{0};

    std::size_t tests_performed{0};
    std::size_t no_tests_performed{0};
    while (time_diff <= seconds{3}) {
        const auto curr_time = Clock::now();
        const auto curr_time_d{duration<double>{curr_time.time_since_epoch()}.count()};  // converts to seconds
        time_diff = curr_time - start;
        const auto time_diff_d{duration<double>{time_diff}.count()};  // converts to seconds

        const auto pose = SlowPose{curr_time_d};
        auto future_set = std::async(std::launch::async,
                                     [&manager, pose]() {
                                         // This is where the supplied function parameter is used
                                         const auto test_method = GetParam();
                                         test_method(manager, pose);
                                     });

        // Time to give the previous setter the chance to acquire internal lock before the getter is called
        if (future_set.wait_for(milliseconds{10}) != std::future_status::ready) {
            std::async(std::launch::async,
                       [&manager, &child_id, &curr_time_d, &time_diff_d]() {
                           ASSERT_NEAR(manager.Get(SlowPoseManager::global_id, child_id).value, curr_time_d, time_diff_d / 10.);
                       }).wait();

            future_set.wait();

            ++tests_performed;
        } else {
            ++no_tests_performed;
        }
    }
    std::cout << "Test performed: " << tests_performed << std::endl;
    std::cout << "No test performed: " << no_tests_performed << std::endl;
}

INSTANTIATE_TEST_CASE_P(ThreadSafetyTests,
                        PoseManagerThreadingFixture,
                        ::testing::Values(
                                [](SlowPoseManager& manager, SlowPose pose) { manager.SetGlobal(pose); },
                                [](SlowPoseManager& manager, SlowPose pose) { manager.SetLocal(pose); }
                                ));
