/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionDatabaseHandling/wal_mode.h"
#include "AccerionSensorAPI/utils/enum.h"

#include <chrono>
#include <sstream>
#include <stdexcept>

#include "sqlite3.h"

namespace detail {
    enum class SynchronizationType: int {
        OFF = 0,
        NORMAL = 1,
        FULL = 2,
        EXTRA = 3
    };

    void SetSynchronizationType(const accerion::sqlite::Database& db, const SynchronizationType desired_type) {
        db.Execute(std::string("PRAGMA synchronous = ") + std::to_string(accerion::api::GetEnumValue(desired_type)));

        const SynchronizationType current_type {db.ExecuteAndGetValue<int>("PRAGMA synchronous;")};
        if (current_type != desired_type) {

            std::stringstream ss;
            ss << "Failed to set database synchronization type to '" << accerion::api::GetEnumValue(desired_type)
               << "' , currently still set to '" << accerion::api::GetEnumValue(current_type) << "'";

            throw std::runtime_error(ss.str());
        }
    }
}

namespace accerion {

namespace sqlite {

void SwitchToWalJournaling(const accerion::sqlite::Database& db) {
    db.Execute("PRAGMA journal_mode = WAL;");
    // In Wal mode, setting synchronization to 'NORMAL' provides sufficient safety against corruption while performance
    // is improved (https://www.sqlite.org/pragma.html#:~:text=WAL%20mode.-,NORMAL%20(1),-When%20synchronous%20is)
    detail::SetSynchronizationType(db, detail::SynchronizationType::NORMAL);
}

void SwitchToDeleteJournaling(const accerion::sqlite::Database& db) {
    detail::SetSynchronizationType(db, detail::SynchronizationType::FULL);
    db.Execute("PRAGMA journal_mode = DELETE;");
}

WalCheckpointer::WalCheckpointer(const accerion::sqlite::Database& db)
    : db_(db.GetFilePath(), SQLITE_OPEN_READWRITE) {
    // Check that journaling of provided db handle is set to 'Wal' mode
    const auto journal_mode = db.PrepareStatement("PRAGMA journal_mode;").GetValue<std::string>();
    if (journal_mode != "wal") {
        throw std::runtime_error("Journaling of provided database handle not set to 'Wal' mode");
    }

    // To do manual checkpointing, automatic checkpointing must be deactivated
    db.Execute("PRAGMA wal_autocheckpoint = 0");
    const auto auto_checkpoint = db.PrepareStatement("PRAGMA wal_autocheckpoint;").GetValue<int>();
    if (auto_checkpoint != 0) {
        throw std::runtime_error("Failed to disable automatic checkpointing" );
    }

    // Journaling of 'internal' db handle set to 'Wal' mode
    db_.Execute("PRAGMA journal_mode = WAL");
}

void WalCheckpointer::AttemptToCheckpoint(std::function<void(const std::optional<Info>&)> callback) {
    if (busy_.valid()) {
        switch (busy_.wait_for(std::chrono::seconds(0))) {
            case std::future_status::timeout:
                if (callback) {
                    callback(std::nullopt);
                }
                return;
            case std::future_status::ready:
                busy_.get();
                break;
            default:
                throw std::runtime_error("Implementation error");
        }
    }
    busy_ = std::async(std::launch::async, [this, callback = std::move(callback)]() {
        auto info = Checkpoint();
        if (callback) {
            callback(info);
        }
    });
}

WalCheckpointer::Info WalCheckpointer::Checkpoint() const {
    Info info {};
    if (SQLITE_OK != sqlite3_wal_checkpoint_v2(db_.GetPtr(), nullptr, SQLITE_CHECKPOINT_PASSIVE,
                                               &info.pages_total, &info.pages_checkpointed)) {
        std::stringstream ss;
        ss << "Wal checkpointing failed: " << sqlite3_errmsg(db_.GetPtr());
        throw std::runtime_error(ss.str());
    }
    return info;
}

}  // namespace sqlite
}  // namespace accerion
