#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Optional

from sdk.data.data_container import DataContainer
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v

# All arams-based plotting tools
from sdk.plotting.plot_correction_count_heatmap import plot_correction_count_heatmap
from sdk.plotting.plot_correction_count_histogram import plot_correction_count_histogram
from sdk.plotting.plot_correction_count_map import plot_correction_count_map
from sdk.plotting.plot_correction_velocity_histogram import plot_correction_velocity_histogram
from sdk.plotting.plot_correction_velocity_map import plot_correction_velocity_map
from sdk.plotting.plot_distance_between_corrections_histogram import plot_distance_between_corrections_histogram
from sdk.plotting.plot_distance_between_corrections_map import plot_distance_between_corrections_map
from sdk.plotting.plot_distance_between_signatures_histogram import plot_distance_between_signatures_histogram
from sdk.plotting.plot_distance_between_signatures_histogram_cum import plot_distance_between_signatures_histogram_cum
from sdk.plotting.plot_distance_between_signatures_map import plot_distance_between_signatures_map
from sdk.plotting.plot_external_reference_timeseries import plot_external_reference_timeseries
from sdk.plotting.plot_heading_corrections_heatmap import plot_heading_corrections_heatmap
from sdk.plotting.plot_heading_corrections_histogram import plot_heading_corrections_histogram
from sdk.plotting.plot_heading_corrections_map import plot_heading_corrections_map
from sdk.plotting.plot_map_visualization import plot_map_visualization
from sdk.plotting.plot_mapping_direction import plot_mapping_direction
from sdk.plotting.plot_match_quality_histogram import plot_match_quality_histogram
from sdk.plotting.plot_match_quality_map import plot_match_quality_map
from sdk.plotting.plot_position_corrections_histogram import plot_position_corrections_histogram
from sdk.plotting.plot_position_corrections_map import plot_position_corrections_map
from sdk.plotting.plot_sensor_poses import plot_sensor_poses

# All other plotting tools
from sdk.plotting.plot_residual_position_errors_g2o import plot_residual_position_errors_g2o
from sdk.plotting.plot_residual_heading_errors_g2o import plot_residual_heading_errors_g2o


def plot_all(data: DataContainer,
             output_folder: Optional[Path] = None,
             verbose: bool = False,
             conf: Configuration = Configuration.default()):
    if conf[Configuration.Name.PREFIX] is True:
        # Ensure same prefix for all plots
        conf[Configuration.Name.PREFIX] = conf.get_prefix(data.arams_original.get_input_files())
    scripts = [
        # All plots related to floor-map-coordinates / signature position map files only
        plot_distance_between_signatures_histogram,
        plot_distance_between_signatures_histogram_cum,
        plot_distance_between_signatures_map,
        plot_map_visualization,
        plot_mapping_direction,
        # All plots related to arams files (also a floor-map-coordinates file)
        plot_correction_count_heatmap,
        plot_correction_count_histogram,
        plot_correction_count_map,
        plot_correction_velocity_histogram,
        plot_correction_velocity_map,
        plot_distance_between_corrections_histogram,
        plot_distance_between_corrections_map,
        plot_heading_corrections_heatmap,
        plot_heading_corrections_histogram,
        plot_heading_corrections_map,
        plot_position_corrections_histogram,
        plot_position_corrections_map,
        plot_match_quality_histogram,
        plot_match_quality_map,
        plot_external_reference_timeseries,
        plot_sensor_poses,
        # All plots related to g2o/graph files
        plot_residual_position_errors_g2o,
        plot_residual_heading_errors_g2o
    ]

    print_v(verbose, "=" * 6 + " Arams plots " + "=" * 6)
    for script in scripts:
        script(data, output_folder, conf, verbose=verbose)
    print_v(verbose, "=" * 6 + " Done " + "=" * 6)


def main():
    from sdk.plotting.utils.plotting import parse_plot_arguments
    from sdk.utils.preparations import process_args

    args = parse_plot_arguments()

    conf, input_files, output_folder = process_args(args)
    data = DataContainer.create(input_files, conf)

    plot_all(data, output_folder, verbose=True, conf=conf)


if __name__ == "__main__":
    main()
