#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025 Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from collections.abc import Sequence
from pathlib import Path
from typing import Callable, Optional, Union

import sqlite3

from sdk.database.monitored_data import MonitoredData
from sdk.utils.version import Version, python_version

assert python_version() >= Version("3.8"), \
    "Required for deterministic parameter of sqlite3.connection.create_function"


__all__ = ["Database"]


class Database:
    def __init__(self, db_file: Union[str, Path]):
        assert Path(db_file).exists()
        self.connection = sqlite3.connect(str(db_file), detect_types=sqlite3.PARSE_COLNAMES)
        self.connection.row_factory = sqlite3.Row

        self.__registered_function_names = []

    def __del__(self):
        self.connection.commit()
        self.__unregister_functions()
        self.connection.close()

    def get_cursor(self, query: str) -> sqlite3.Cursor:
        return self.connection.execute(query)

    def select(self, query: str,
               data: Sequence,
               progress_call_back: Optional[Callable] = None) -> sqlite3.Cursor:
        """
        To be used for executing a plain selection query for multiple data entries.

        The data input can be as follows:
          - A dictionary for retrieving a single record, the named-styled way, so
                ```
                db = Database(...)
                db.select("SELECT * FROM table WHERE id = :id", {"id": 5})
                ```
          - A tuple for retrieving a single record, the qmark-styled way, so
                ```
                db.select("SELECT * FROM table WHERE id = ?", (5,))
                ```
          - A list of tuples for retrieving multiple records, the qmark-styled way, so
                ```
                db.select("SELECT * FROM table WHERE id = ?", [(5,), (8,)])
                ```
        Note that retrieving multiple records the named-styled way is not possible.
        """
        if len(data) != 1 and isinstance(data[0], dict):
            raise Exception(
                "Named-style query parametrization cannot be used to retrieve multiple records.")

        query_data = MonitoredData(
            data, progress_call_back) if progress_call_back else data
        return self.connection.execute(query, query_data)

    def execute(self,
                query: str,
                data: Optional[Sequence] = None,
                progress_call_back: Optional[Callable] = None) -> sqlite3.Cursor:
        """
        To be used for executing a manipulation query for multiple data entries.

        Note that the call_back only works when there is data provided.
        """

        if data is None:
            return self.connection.execute(query)
        else:
            query_data = MonitoredData(
                data, progress_call_back) if progress_call_back else data
            return self.connection.executemany(query, query_data)

    def register_function(self, name: str, function: Callable, *, deterministic=False):
        """
        This method will register a function in the database.
        When the database is closed, then the function will be removed.
        """
        from inspect import signature as FunctionSignature
        function_signature = FunctionSignature(function)

        self.connection.create_function(name, len(
            function_signature.parameters), function, deterministic=deterministic)
        self.__registered_function_names.append(name)

    def __unregister_functions(self):
        for function_name in self.__registered_function_names:
            self.connection.create_function(function_name, -1, None)
