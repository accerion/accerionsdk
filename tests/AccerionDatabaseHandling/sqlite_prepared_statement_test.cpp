/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionDatabaseHandling/prepared_statement.h"
#include "AccerionDatabaseHandling/database.h"
#include "AccerionUtilities/temp_file_system.h"


class SqlitePreparedStatementTest : public ::testing::Test {
 protected:
    SqlitePreparedStatementTest() :
            temp_files_{},
            db_{CreateDatabase(temp_files_.Create())},
            base_count_{GetCount()} {}

    [[nodiscard]] std::size_t GetCount() const {
        return db_.ExecuteAndGetValue<std::size_t>(R"SQL(
            SELECT count()
              FROM data;
        )SQL");
    }

    TempFileSystem temp_files_;
    accerion::sqlite::Database const db_;
    std::size_t const base_count_;

 private:
    static accerion::sqlite::Database CreateDatabase(const std::string& path) {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE};

        db.Execute(R"SQL(
                CREATE TABLE IF NOT EXISTS data (
                    id              INTEGER PRIMARY KEY,
                    integral        INTEGER,
                    floating_point  REAL,
                    string          TEXT,
                    blob            BLOB
                );
            )SQL");

        db.Execute(R"SQL(
                INSERT INTO data
                       (id, integral, floating_point, string, blob)
                VALUES (0,  NULL,     NULL,           NULL,   NULL),
                       (1,  1,        1.1,            "1",    NULL),
                       (2,  2,        2.2,            "22",   NULL),
                       (3,  3,        3.3,            "333",  NULL);
            )SQL");

        // To simplify the testing, write the blob using PreparedStatement
        const accerion::sqlite::PreparedStatement stmt = db.PrepareStatement(R"SQL(
                UPDATE data
                   SET blob = ?2
                 WHERE id = ?1;
            )SQL");
        // Write as many values in the blob field as the id field
        const auto update_blob = [&stmt](int id, double value) {
            const std::vector<double> data(id, value);
            const auto bind_blob{accerion::sqlite::PreparedStatement::BindBlob::Create(data.data(),
                                    sizeof(double) * data.size())};

            stmt.Bind(1, id);
            stmt.Bind(2, bind_blob);
            stmt.Step();
            stmt.Reset();
        };
        update_blob(1, 1.11);
        update_blob(2, 2.22);
        update_blob(3, 3.33);

        return db;
    }
};

TEST_F(SqlitePreparedStatementTest, ReadInteger) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT integral
              FROM data
             ORDER BY id;
        )SQL");

    const auto rows = stmt.GetRows<int>();
    EXPECT_EQ(rows.size(), base_count_);

    int expected_value{};
    for (auto row : rows) {
        auto value = std::get<0>(row);
        EXPECT_EQ(value, expected_value);
        ++expected_value;
    }
}

TEST_F(SqlitePreparedStatementTest, WriteInteger) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            INSERT INTO data
                   (integral)
            VALUES (?);
        )SQL");

    const std::vector<int> new_values{25, 61, 87, 14, 762};
    for (auto value: new_values) {
        stmt.Bind(1, value);
        stmt.Step();
        stmt.Reset();
    }
    EXPECT_EQ(GetCount(), base_count_ + new_values.size());
}

TEST_F(SqlitePreparedStatementTest, ReadFloatingPoint) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT floating_point
              FROM data
             ORDER BY id;
        )SQL");

    const auto rows = stmt.GetRows<float>();
    EXPECT_EQ(rows.size(), base_count_);

    float expected_value{};
    for (auto row : rows) {
        auto value = std::get<0>(row);
        EXPECT_EQ(value, expected_value);
        expected_value += 1.1;
    }
}

TEST_F(SqlitePreparedStatementTest, WriteFloatingPoint) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            INSERT INTO data
                   (floating_point)
            VALUES (?);
        )SQL");

    const std::vector<float> new_values{25.25, 61.61, 87.87, 14.14, 762.762};
    for (auto value: new_values) {
        stmt.Bind(1, value);
        stmt.Step();
        stmt.Reset();
    }
    EXPECT_EQ(GetCount(), base_count_ + new_values.size());
}

TEST_F(SqlitePreparedStatementTest, ReadString) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT string
              FROM data
             ORDER BY id;
        )SQL");

    const auto rows = stmt.GetRows<std::string>();
    EXPECT_EQ(rows.size(), base_count_);

    for (std::size_t index = 0; index < rows.size(); ++index) {
        const std::string expected_value(index, '0' + index);
        const auto& row = rows[index];
        auto value = std::get<0>(row);
        EXPECT_EQ(value, expected_value);
    }
}

TEST_F(SqlitePreparedStatementTest, WriteString) {
    const accerion::sqlite::PreparedStatement insert_stmt = db_.PrepareStatement(R"SQL(
            INSERT INTO data
                   (string)
            VALUES (?);
        )SQL");

    // Use large strings to avoid "Small String Optimization"
    const std::vector<std::string> new_values{"", std::string(100, 'x')};
    for (const auto& value: new_values) {
        // Intentionally make a copy, such that the binding to a temporary object is tested
        insert_stmt.Bind(1, std::string{value});
        insert_stmt.Step();
        insert_stmt.Reset();
    }
    EXPECT_EQ(GetCount(), base_count_ + new_values.size());

    const accerion::sqlite::PreparedStatement select_stmt = db_.PrepareStatement(R"SQL(
            SELECT string
              FROM data
             ORDER BY id DESC
             LIMIT 1
        )SQL");
    const auto rows = select_stmt.GetRows<std::string>();
    EXPECT_EQ(rows.size(), 1);
    const auto& data = std::get<0>(rows.front());
    EXPECT_EQ(new_values.back(), data);
}

TEST_F(SqlitePreparedStatementTest, WriteReadBindBlob) {
    // As the writing of blob using BindBlob was already done in the test fixture, only test here that the reading is
    // good

    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT id, blob
              FROM data;
        )SQL");

    const auto rows = stmt.GetRows<std::size_t, accerion::sqlite::PreparedStatement::Blob>();
    EXPECT_EQ(rows.size(), base_count_);

    for (const auto& row : rows) {
        // Check that there as many values in the blob field as the id field
        const auto count = std::get<0>(row);
        const auto& blob = std::get<1>(row);
        const auto data_pointer = reinterpret_cast<const double*>(blob.bytes.data());
        const auto data = std::vector<double>{data_pointer, data_pointer + blob.bytes.size() / sizeof(double)};
        EXPECT_EQ(data.size(), count);
        EXPECT_TRUE(std::all_of(data.cbegin(), data.cend(),
                                [&count](const auto& item) { return item == count * 1.11; }));
    }
}

TEST_F(SqlitePreparedStatementTest, WriteReadBlob) {
    const std::vector<std::uint8_t> test_data{1, 2, 3, 4};
    // Intentionally use a temporary Blob object for the update, such that the binding to a temporary object is tested
    const accerion::sqlite::PreparedStatement update_stmt = db_.PrepareStatement(R"SQL(
                UPDATE data
                   SET blob = @blob
                 WHERE id = 2
            )SQL");
    update_stmt.Bind("@blob", accerion::sqlite::PreparedStatement::Blob{test_data});
    update_stmt.Step();
    update_stmt.Reset();

    const accerion::sqlite::PreparedStatement select_stmt = db_.PrepareStatement(R"SQL(
            SELECT blob
              FROM data
             WHERE id = 2
        )SQL");
    const auto rows = select_stmt.GetRows<accerion::sqlite::PreparedStatement::Blob>();
    EXPECT_EQ(rows.size(), 1);

    const auto& blob = std::get<0>(rows.front());
    EXPECT_EQ(test_data.size(), blob.bytes.size());
    EXPECT_TRUE(std::equal(test_data.cbegin(), test_data.cend(), blob.bytes.cbegin()));
}

TEST_F(SqlitePreparedStatementTest, BindByName) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            INSERT INTO data
                   (integral, floating_point)
            VALUES (@int, @float);
        )SQL");

    const std::vector<float> new_values{25.25, 61.61, 87.87};
    for (auto value: new_values) {
        stmt.Bind("@int", static_cast<int>(value));
        stmt.Bind("@float", static_cast<int>(value));
        stmt.Step();
        stmt.Reset();
    }
    EXPECT_EQ(GetCount(), base_count_ + new_values.size());
}

TEST_F(SqlitePreparedStatementTest, ReadMultipleFieldsUsingStep) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT integral,
                   floating_point,
                   -2 * integral,
                   integral * floating_point
              FROM data;
        )SQL");

    std::tuple<std::uint8_t, float, std::int8_t, double> row;
    while (stmt.Step(row)) {
        const auto unsigned_integer = std::get<0>(row);
        const auto floating_point = std::get<1>(row);
        const auto signed_integer = std::get<2>(row);
        const auto double_floating_point = std::get<3>(row);

        EXPECT_EQ(-2 * unsigned_integer, signed_integer);
        EXPECT_NEAR(unsigned_integer * floating_point, double_floating_point,
                    4 * std::numeric_limits<float>::epsilon());
    }
}

TEST_F(SqlitePreparedStatementTest, ReadMultipleFieldsUsingGetRows) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT integral,
                   floating_point,
                   -2 * integral,
                   integral * floating_point
              FROM data;
        )SQL");

    for (auto values : stmt.GetRows<std::uint8_t, float, std::int8_t, double>()) {
        const auto unsigned_integer = std::get<0>(values);
        const auto floating_point = std::get<1>(values);
        const auto signed_integer = std::get<2>(values);
        const auto double_floating_point = std::get<3>(values);

        EXPECT_EQ(-2 * unsigned_integer, signed_integer);
        EXPECT_NEAR(unsigned_integer * floating_point, double_floating_point,
                    4 * std::numeric_limits<float>::epsilon());
    }
}

TEST_F(SqlitePreparedStatementTest, ReadTooManyFields) {
    const accerion::sqlite::PreparedStatement stmt = db_.PrepareStatement(R"SQL(
            SELECT integral,
                   floating_point
              FROM data;
        )SQL");
    std::tuple<std::uint8_t, float, std::string> row;
    EXPECT_ANY_THROW(stmt.Step(row));
}
