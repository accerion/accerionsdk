/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/connection_manager.h"

#include "AccerionPoseManager/pose_manager.h"

#include "app_utils.h"
#include "pose_manager_examples.h"


// Typical use case where a map is created for Triton using a combined signal of multiple sources or a (LiDAR) SLAM
// package that provides high frequent poses


using PoseManager = accerion::pose_manager::PoseManagerBuffered<accerion::api::Pose, accerion::api::TimePoint>;


int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Please include the sensor ip address, so '" << argv[0] << " 192.168.0.116'" << std::endl;
        return 1;
    }

    // Create the PoseManager instance (define first, because sensor need to be destructed first
    const PoseManager::Id id_sensor = "triton";
    auto pose_manager = PoseManager::CreateSlam("robot",
                                                id_sensor, {0., 0., 0.},
                                                "combined signal", {0., 0., 0.},
                                                PoseManager::PoseTreeType::Container{std::chrono::seconds{100}});

    // Connect to the sensor
    const auto sensor_ip = accerion::api::Address::Parse({argv[1]});
    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(sensor_ip,
                                                                      accerion::api::ConnectionType::kConnectionTcp);
    auto sensor = future_sensor.WaitFor(std::chrono::seconds{1});
    if (!sensor) {
        std::cout << "Could not connect to sensor" << std::endl;
        return 1;
    }

    // Make sure the sensor is configured properly, sensor being in the right state and potentially deleting the
    // existing cluster that will be used for mapping
    const accerion::api::AreaId area_id{25};
    const accerion::api::ClusterId cluster_id{35};
    ConfigureSensor(*sensor, area_id, cluster_id);

    // Mimic a fused pose signal by setting up the absolute pose generator
    // The frequencies used much smaller than typical pose source frequencies, such that the console output is readable
    constexpr double radius{5};  // [meter]
    constexpr double total_duration{10};  // [seconds]
    constexpr double pose_frequency{10};  // [Hertz]

    const auto pose_steps = static_cast<std::size_t>(total_duration * pose_frequency);
    const auto pose_generator = DummyPoseGenerator({radius, 0., 90.}, {0., 0., 360. / pose_steps}, pose_steps - 1);

    // State controlling to detect when to start mapping
    std::atomic_bool sensor_localized{false};

    // Link up the pose generator to the pose manager and sensor
    auto pose_call_back =
            [id_sensor, &pose_manager, &sensor, &sensor_localized](const auto& timestamp, const auto& pose) {
                std::stringstream ss;
                ss << "pose received " << pose << "\n";

                pose_manager.SetGlobal(pose, timestamp);
                ss << "  pose manager updated using SetGlobal\n";

                accerion::api::ExternalReferencePose ext_ref{};
                ext_ref.time_offset = std::chrono::microseconds{0};
                ext_ref.pose = pose_manager.Get(PoseManager::global_id, id_sensor, timestamp);
                sensor->SetExternalReference(ext_ref);
                ss << "  sensor pose set to " << ext_ref.pose << "\n";

                sensor_localized = true;

                std::cout << ss.str() << std::endl;
            };
    auto pose_source_future = CreatePoseSource(pose_generator, pose_frequency, std::move(pose_call_back));

    // When an absolute pose is received and send to the sensor, then the mapping can be started
    while (!sensor_localized) {
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
    }
    StartMapping(*sensor, cluster_id);

    pose_source_future.wait();

    StopMapping(*sensor);

    return 0;
}
