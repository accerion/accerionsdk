#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from os import chdir, getcwd
from pathlib import Path
from typing import Optional


class ChangeDir:
    """
    Context manager to change the directory temporarily. When the context is closed, then the directory is changed back
    to its original.
    """
    def __init__(self, new_dir: [str, Path]):
        self.__new_dir: str = str(new_dir)
        self.__old_dir: Optional[str] = None

    def __enter__(self):
        self.__old_dir = getcwd()
        chdir(self.__new_dir)

    def __exit__(self, exc_type, exc, exc_tb):
        chdir(self.__old_dir)
