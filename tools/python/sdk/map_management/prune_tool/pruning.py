#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass, field
from typing import List, Set, Tuple

import numpy as np
from sklearn.cluster import KMeans
from scipy.spatial import KDTree

from sdk.data.signature import Signature
from sdk.geometry.point import Point
from sdk.map_management.prune_tool.utils import get_max_radius_in_bounding_boxes, BoundingBoxWithRadius


__all__ = ["Settings", "prune"]


@dataclass
class Settings:
    default_radius: float = 0.2  # [meter]
    max_signatures_in_radius: int = 200
    bounding_boxes: List[BoundingBoxWithRadius] = field(default_factory=list)
    cluster_ids_to_keep: Set[int] = field(default_factory=set)


def prune(signatures: List[Signature], settings) -> Set[int]:
    """
    Prune the signature according to the settings. Returns the ids of the signatures to keep.
    """
    assert settings.default_radius > 0

    # Add signatures from 'clusters to keep list'
    all_sig_indices_to_keep = __get_signature_indices_to_keep(signatures, settings.cluster_ids_to_keep)
    all_sig_indices_to_remove = set()

    # Create a simple list with plain position information
    # External libraries require this or to easily create the types required
    # It is also a way for efficient memory handling and computation time (compared to using a dataclass)
    positions = [(s.pose.x, s.pose.y) for s in signatures]

    # Create kdtree to lookup signatures per cell later
    tree = KDTree(positions)

    # Loop over all signatures, determining if a signature should be kept or not
    for signature_index, position in enumerate(positions):
        # Skip if already processed
        if signature_index in all_sig_indices_to_remove or signature_index in all_sig_indices_to_keep:
            continue

        # Find indices within search radius
        radius = get_max_radius_in_bounding_boxes(Point(*position), settings.bounding_boxes, settings.default_radius)
        indices_in_radius_set = set(tree.query_ball_point(np.array(positions[signature_index]), radius))

        # Only look at signatures that were not considered before
        indices_in_radius_to_evaluate = indices_in_radius_set - all_sig_indices_to_remove - all_sig_indices_to_keep
        if len(indices_in_radius_to_evaluate) == 0:
            continue

        # Determine how many signatures to keep in this iteration
        indices_kept_before = indices_in_radius_set & all_sig_indices_to_keep
        signature_count_to_keep = settings.max_signatures_in_radius - len(indices_kept_before)
        if signature_count_to_keep <= 0:
            # No signatures to keep, all should be removed
            all_sig_indices_to_remove.update(indices_in_radius_to_evaluate)
            continue

        # Determine which signatures to keep
        if len(indices_in_radius_to_evaluate) <= signature_count_to_keep:
            # All signatures should be kept
            indices_in_radius_to_keep = indices_in_radius_to_evaluate
        else:
            # Determine points to keep
            indices_in_radius_to_keep = __evenly_distribute_points(
                positions, indices_in_radius_to_evaluate, max_indices=signature_count_to_keep)

            # Determine which signatures to remove
            indices_in_radius_to_remove = indices_in_radius_to_evaluate - indices_in_radius_to_keep
            all_sig_indices_to_remove.update(indices_in_radius_to_remove)

        # Update sets of indices to keep and remove
        all_sig_indices_to_keep.update(indices_in_radius_to_keep)

    return set(signatures[i].id for i in all_sig_indices_to_keep)


def __evenly_distribute_points(positions: List[Tuple[float, float]],
                               position_indices: Set[int],
                               max_indices: int) -> Set[int]:
    """
    Given the list of positions, select the required amount of indices from the index set to have the given maximum remaining
    """
    position_indices_list = sorted(position_indices)
    assert len(position_indices_list) > max_indices

    # Perform k-means clustering
    kmeans_fit = KMeans(n_clusters=max_indices, random_state=0, n_init='auto') \
        .fit([positions[i] for i in position_indices_list])

    # Iterate through k-means clusters and select one point from each
    position_indices_to_keep = set()
    for cluster_center in kmeans_fit.cluster_centers_:
        # Determine the point closest to the k-means cluster center
        closest_position_index = None
        min_distance = float('inf')
        for position_index in position_indices_list:
            distance = (positions[position_index][0] - cluster_center[0]) ** 2 + \
                       (positions[position_index][1] - cluster_center[1]) ** 2
            if distance < min_distance:
                min_distance = distance
                closest_position_index = position_index

        position_indices_to_keep.add(closest_position_index)

    return position_indices_to_keep


def __get_signature_indices_to_keep(signatures: List[Signature], cluster_ids_to_keep: Set[int]) -> Set[int]:
    if not cluster_ids_to_keep:
        return set()

    return set(index for index, sig in enumerate(signatures) if sig.cluster_id in cluster_ids_to_keep)
