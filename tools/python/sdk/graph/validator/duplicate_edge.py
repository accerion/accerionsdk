#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.graph.filter import identify_edges
from sdk.graph.graph import Graph
from sdk.graph.validator.base import Base


class DuplicateEdge(Base):
    """
    Temporary validator that removes the duplicate loop closure edges from the graph. (CSE-595)
    """

    def __init__(self):
        self.__has_duplicate_edges = None

    def is_valid(self, graph: Graph) -> bool:
        self.__has_duplicate_edges = False

        unique_ids = set()
        for i in identify_edges(graph, Graph.Edge.Type.Loop):
            e = graph.edges[i]
            ids = tuple([e.id_begin, e.id_end]) if e.id_begin < e.id_end else tuple([e.id_end, e.id_begin])
            if ids in unique_ids:
                self.__has_duplicate_edges = True
                break
            else:
                unique_ids.add(ids)

        return not self.__has_duplicate_edges

    def correct(self, graph: Graph) -> Graph:
        assert self.__has_duplicate_edges is not None, "Implementation error: is_valid should be called first"

        duplicate_edge_indices = []
        unique_ids = set()
        for i in identify_edges(graph, Graph.Edge.Type.Loop):
            e = graph.edges[i]
            ids = tuple([e.id_begin, e.id_end]) if e.id_begin < e.id_end else tuple([e.id_end, e.id_begin])
            if ids in unique_ids:
                duplicate_edge_indices.append(i)
            else:
                unique_ids.add(ids)

        print(f"There are {len(duplicate_edge_indices)} duplicate edges:")
        print(f"    {sorted([(graph.edges[i].id_begin, graph.edges[i].id_end) for i in duplicate_edge_indices])}")

        graph.edges = [e for i, e in enumerate(graph.edges) if i not in duplicate_edge_indices]

        return graph
