#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Dict

from sdk import ROOT_PATH
from sdk.arams.arams import Arams
from sdk.metrics.reader import Reader as MetricsReader
from sdk.report.create_from_template import create_report
from sdk.report.preparations import prepare_run
from sdk.utils.configuration import Configuration
from sdk.utils.path import separate_common_parent
from sdk.utils.preparations import process_args
from sdk.utils.argument_parser import create_default_arg_parser, add_prefix, add_spec


__all__ = []


def __parse_arguments() -> Dict:
    parser = create_default_arg_parser(output_required = True)
    add_prefix(parser, False)
    add_spec(parser, False)
    return vars(parser.parse_args())


if __name__ == "__main__":
    args = __parse_arguments()

    conf, input_files, output_folder = process_args(args)
    assert output_folder is not None, "Please provide an output folder."
   
    # Generate output file name
    output_file = "report.pdf"
    prefix_original = conf[Configuration.Name.PREFIX]
    if conf[Configuration.Name.PREFIX] is not False:
        prefix = conf.get_prefix(input_files)
        if prefix is not None and len(prefix) != 0:
            output_file = f"{prefix}_{output_file}"
            conf[Configuration.Name.PREFIX] = prefix
            # the prefix needs to be saved in order to load it later during report generation in the md template
            conf.save_configuration(output_folder)
    output_file = output_folder / output_file
    
    # Prepare the data for reporting, done after the prefix processing in order to ensure the same prefix will be used
    # by the report and the plotting functions
    spec_file = args['spec']
    metrics = MetricsReader.read(spec_file) if spec_file is not None else MetricsReader.read_default()
    prepare_run(input_files, metrics=metrics, output_folder=output_folder, master_conf=conf, verbose=True)

    # Create a section at the end of the document stating the following:
    #  - input files used in the reporting
    #  - non-default configuration settings
    document_end_lines = []

    # TODO: not ideal at all, reading all data again, but this does not duplicate any logic
    used_files = Arams(input_files).get_input_files()
    if len(used_files) > 0:
        base_folder, used_files = separate_common_parent([str(Path(f).absolute()) for f in used_files])
        document_end_lines.append("### Input data")
        if len(base_folder) != 0:
            document_end_lines.append(f"Base folder: {base_folder}")
        document_end_lines.extend([f" - {f}" for f in used_files])

    new_lines = ["### Configuration"]
    default_conf = Configuration.default()
    if conf != default_conf:
        for parameter in Configuration.Name:
            if parameter not in conf or parameter not in default_conf:
                continue
            if conf[parameter] != default_conf[parameter]:
                if len(document_end_lines) == 1:
                    new_lines.append("The following configuration parameters are different than default:")
                new_lines.append(f" - {parameter.value}: {conf[parameter]} (default = {default_conf[parameter]})")
    if len(new_lines) == 1:
        new_lines.append("All configuration parameters are the same as the default")
    document_end_lines.extend(new_lines)

    # Actually create the report
    create_report(ROOT_PATH / "resources" / "single_run_report_template.md",
                  output_file,
                  markdown_appendix_lines=document_end_lines)

    if prefix_original != conf[Configuration.Name.PREFIX]:
        conf[Configuration.Name.PREFIX] = prefix_original
        # restore original prefix value
        # we don't want to store the determined prefix value as the user might want to rerun the script with slight changes
        # and keep plots and reports with different timestamp
        conf.save_configuration(output_folder)
