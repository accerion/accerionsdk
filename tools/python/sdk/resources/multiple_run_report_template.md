```python, echo = False, results = 'markdown'
from sdk.pathlib import Path

from sdk.report.markdown_utils import *
from sdk.report.preparations import *
from sdk.utils.configuration import Configuration

# Define all data
curr_version_str = "v5.4.0"
prev_version_str = "v5.3.0"
data_dir = Path("/home/martijn/Seafile/ProductTech/FieldTestData/Automated_field_test_30082021_v540rc10")

fmc_curr_version = data_dir / "v540Map" / "floor_map_coordinates.csv"
g2o_curr_version = data_dir / "v540rc10_optimized.g2o"
runs_curr_version = [
    Run("Run 1, no CM",              data_dir / "v540Map/run1/598100318-30-08-2021-13-07/arams_user_logs"),
    Run("Run 2, no CM",              data_dir / "v540Map/run1_cm/arams_user_logs"),
    Run("Run 1, with CM",            data_dir / "v540Map/run2/arams_user_logs"),
    Run("Run 2, with CM",            data_dir / "v540Map/run2_cm/arams_user_logs"),
    Run("Run 3, no CM, updated map", data_dir / "v540Map/run3_cm_off/arams_user_logs"),
    Run("Run 4, no CM, updated map", data_dir / "v540Map/run4_cm_off/arams_user_logs")
]

fmc_prev_version = data_dir / "v530Map" / "floor_map_coordinates.csv"
runs_prev_version = [
    Run("Run 1, no CM",   data_dir / "v530Map/run1/598100318-30-08-2021-14-54/arams_user_logs"),
    Run("Run 2, with CM", data_dir / "v530Map/run2_cm/598100318-30-08-2021-15-05/arams_user_logs"),
    Run("Run 3, with CM", data_dir / "v530Map/run3_cm/598100318-30-08-2021-15-16/arams_user_logs")
]

conf = Configuration.load_configuration()
# Write the configutaion file (mainly when it was not present already)
conf.save_configuration()

for run in runs_curr_version:
    prepare_run([fmc_curr_version, g2o_curr_version, run.source], sub_folder = run.name, conf = conf)
for run in runs_prev_version:
    prepare_run([fmc_prev_version, run.source], sub_folder = run.name, conf = conf)
```

```python, echo = False, results = 'markdown'
print(f"# AFT Test Report: Triton {curr_version_str} rc 10 - 30-08-'21")
```

* Raw data, map etc: `../SeaDrive/ProductTech/FieldTestData/Automated_field_test_30082021_v540rc10`
* TPS:
* TAR: 

| Non-optimized map | Optimized map |
|:--|:--|
|   |   |
|   |   |

## FQ
No data

```python, echo = False, results = 'markdown'
print(f"## 100m Line mapping ({curr_version_str})")
```

```python, echo = False, results = 'markdown'
print(f"## Grid mapping ({curr_version_str}) and localization ({prev_version_str} & {curr_version_str})")
print(f"### Distance between signatures")
print(f"#### {curr_version_str} map")

print_plots_table(["distance_between_signatures_map",
                   "distance_between_signatures_histogram"],
                  runs_curr_version[0])
```

### Position jumps after PGO
```python, echo = False, results = 'markdown'
print(f"#### {curr_version_str} map")
```
No data

### Distance between corrections
```python, echo = False, results = 'markdown'
print(f"#### {curr_version_str} map")
print_plots_table(["distance_between_corrections_map",
                   "distance_between_corrections_histogram"],
                  runs_curr_version)
```

```python, echo = False, results = 'markdown'
print(f"#### {prev_version_str} map (migrated map)")
print_plots_table(["distance_between_corrections_map",
                   "distance_between_corrections_histogram"],
                  runs_prev_version)
```

### Pose corrections
```python, echo = False, results = 'markdown'
print(f"#### {curr_version_str} map")
print_plots_table(["position_corrections_map",
                   "position_corrections_histogram"],
                  runs_curr_version)
```

```python, echo = False, results = 'markdown'
print(f"#### {prev_version_str} map (migrated map)")
print_plots_table(["position_corrections_map",
                   "position_corrections_histogram"],
                  runs_prev_version)
```

### Heading corrections
```python, echo = False, results = 'markdown'
print(f"#### {curr_version_str} map")
print_plots_table(["heading_corrections_map",
                   "heading_corrections_histogram"],
                  runs_curr_version)
```

```python, echo = False, results = 'markdown'
print(f"#### {prev_version_str} map (migrated map)")
print_plots_table(["heading_corrections_map",
                   "heading_corrections_histogram"],
                  runs_prev_version)
```
