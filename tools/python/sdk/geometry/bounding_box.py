#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from dataclasses import dataclass

from sdk.geometry.interval import Interval
from sdk.geometry.point import Point


__all__ = ["BoundingBox"]


@dataclass
class BoundingBox:
    x_interval: Interval
    y_interval: Interval

    @classmethod
    def create_from_points(cls, *points: Point) -> BoundingBox:
        """Create a BoundingBox from two or more points."""
        if len(points) < 2:
            raise ValueError(
                "At least two points are required to define a bounding box.")

        xs = [p.x for p in points]
        ys = [p.y for p in points]

        x_min = min(xs)
        x_max = max(xs)
        y_min = min(ys)
        y_max = max(ys)

        return cls(Interval(x_min, x_max), Interval(y_min, y_max))

    def __contains__(self, point: Point) -> bool:
        """Check if a point lies within the bounding box"""
        return point.x in self.x_interval and point.y in self.y_interval
