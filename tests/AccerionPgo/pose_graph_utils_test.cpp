/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <vector>

#include "gtest/gtest.h"

#include "AccerionPgo/pose_graph_utils.h"


TEST(Utils, CreateNodeIdToIndexMap) {
    const std::vector<accerion::pgo::PoseGraph::Node> nodes {
            {3}, {13}, {23}, {33}, {43}
    };
    for (const auto& map_iter: CreateNodeIdToIndexMap(nodes)) {
        std::cout << "Test case for " << map_iter.first << " at " << map_iter.second << std::endl;
        const auto& node = nodes.at(map_iter.second);
        ASSERT_EQ(node.id, map_iter.first);
    }
}
