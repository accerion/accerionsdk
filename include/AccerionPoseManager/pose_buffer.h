/* Copyright (c) 2022-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <chrono>
#include <ostream>
#include <map>


namespace accerion {

namespace pose_manager {

template <typename TimeType>
using TimeDiffType = decltype(TimeType{} - TimeType{});

template<typename T>
struct IsTimePoint : std::false_type {};

template<typename Clock, typename Duration>
struct IsTimePoint<std::chrono::time_point<Clock, Duration>> : std::true_type {};

template <typename TimeType, typename = void>
struct TimeDiffCaster {};

template <typename TimeType>
struct TimeDiffCaster<TimeType, typename std::enable_if_t<std::is_arithmetic<TimeDiffType<TimeType>>::value>> {
    static double Cast(const TimeDiffType<TimeType>& time_diff) {
        return static_cast<double>(time_diff);
    }
};

template <typename Clock, typename Duration>
struct TimeDiffCaster<std::chrono::time_point<Clock, Duration>,
                      typename std::enable_if_t<IsTimePoint<std::chrono::time_point<Clock, Duration>>::value>> {
    static double Cast(const Duration& duration) {
        return static_cast<double>(std::chrono::duration_cast<std::chrono::duration<double>>(duration).count());
    }
};


template <typename PoseType, typename TimeType, typename TimeDiffConverter = TimeDiffCaster<TimeType>>
class PoseBuffer_ {
public:
    using TimeDiff = TimeDiffType<TimeType>;

    PoseBuffer_() : buffer_{}, buffer_length_{} {}
    explicit PoseBuffer_(const TimeDiff& buffer_length) : buffer_{}, buffer_length_{buffer_length} {}

    // Add a pose at a certain timestamp in the buffer
    // If the time is before the buffer, then no insertion is done and false is returned
    bool Set(const PoseType& pose, const TimeType& time) {
        // Note that the condition should work for both signed and unsigned types
        if (!buffer_.empty() && time + buffer_length_ < LastTime()) {
            return false;
        }

        // In C++17 insert_or_assign can be used
        auto result = buffer_.emplace(time, std::move(pose));
        if (!result.second) {  // not inserted
            result.first->second = pose;
        }

        Clean();

        return true;
    }

    // Get the pose from the buffer at specified timestamp
    // Interpolation is done when the timestamp is not exactly present in the buffer.
    // When a timestamp before or after the buffer is requested, the first or last pose are returned, respectively.
    PoseType Get(const TimeType& time) const {
        if (buffer_.empty()) {
            return PoseType::Identity();
        }

        // Check if timestamp is in range
        const auto begin = buffer_.begin();
        if (time <= begin->first) {
            return begin->second;
        }
        const auto end = --buffer_.end();
        if (time >= end->first) {
            return end->second;
        }

        const auto after = buffer_.upper_bound(time);
        const auto before = std::prev(after);
        const double ratio =
                TimeDiffConverter::Cast(time - before->first) / TimeDiffConverter::Cast(after->first - before->first);
        return before->second.Interpolate(ratio, after->second);
    }

    friend std::ostream& operator<<(std::ostream& os, const PoseBuffer_<PoseType, TimeType>& buffer) {
        for (const auto& iter: buffer.buffer_) {
            os << +iter.first << ": " << iter.second << "\n";
        }
        return os;
    }

private:
    const TimeType& LastTime() const {
        return buffer_.crbegin()->first;
    }

    void Clean() {
        if (LastTime() - buffer_.begin()->first <= buffer_length_) {
            return;
        }

        const auto iter = buffer_.upper_bound(LastTime() - buffer_length_);
        if (iter != buffer_.cend()) {
            buffer_.erase(buffer_.begin(), std::prev(iter));
        }
    }

    std::map<TimeType, PoseType> buffer_;
    TimeDiff buffer_length_;
};

}  // namespace pose_manager

}  // namespace accerion
