#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import numpy as np
from matplotlib.ticker import FormatStrFormatter

from sdk.metrics.common import ComparisonType
from sdk.metrics.metrics_data import MetricsData
from sdk.metrics.metrics_data_aggregation import MetricDataAggregation
from sdk.plotting.utils.plotting import Colors, add_cumulative_histogram, get_colors_for_bins, scatter_data, set_xylim
from sdk.utils.configuration import Configuration


__all__ = ["Plotting"]


class Plotting:
    __BIN_STEP_SIZE = 2.5  # mm

    def __init__(self, distances, configuration: Configuration):
        assert distances

        self.__values_mm = [1.e3 * i for i in distances]  # [millimeter]
        self.__average_mm = sum(self.__values_mm) / len(self.__values_mm)

        # Get min value, and lower and upper bound for average
        metrics = configuration[Configuration.Name.METRICS]
        self.__constraint_min = 1e3 * metrics.get_constraint_value(MetricsData.Tag.DistSignatures,
                                                                   MetricDataAggregation.Type.Minimum,
                                                                   ComparisonType.Greater)
        self.__constraint_max = 1e3 * metrics.get_constraint_value(MetricsData.Tag.DistSignatures,
                                                                   MetricDataAggregation.Type.Maximum,
                                                                   ComparisonType.Smaller)
        self.__constraint_lower_avg = 1e3 * metrics.get_constraint_value(MetricsData.Tag.DistSignatures,
                                                                         MetricDataAggregation.Type.Average,
                                                                         ComparisonType.Greater)
        self.__constraint_upper_avg = 1e3 * metrics.get_constraint_value(MetricsData.Tag.DistSignatures,
                                                                         MetricDataAggregation.Type.Average,
                                                                         ComparisonType.Smaller)

        n_bins = int(np.ceil(self.__constraint_max / self.__BIN_STEP_SIZE))
        self.__bin_edges = [self.__BIN_STEP_SIZE * i for i in range(0, n_bins + 2)]

        # Combine values outside of bin range in final bin
        self.__values_clipped_mm = np.clip(self.__values_mm, self.__bin_edges[0], self.__bin_edges[-1])

        # Define the color mapping
        self.__color_mapping = self.__define_ranges_and_colors(self.__constraint_min, self.__constraint_max,
                                                               self.__constraint_lower_avg, self.__constraint_upper_avg)

    def calculate_signature_distances_within_range(self) -> int:
        return len([v for v in self.__values_mm if self.__constraint_min <= v <= self.__constraint_max])

    @staticmethod
    def add_map(ax, x_positions, y_positions, values, conf: Configuration):
        _, data_cbar_handle = scatter_data(ax, x_positions, y_positions, values, conf, MetricsData.Tag.DistSignatures)

        data_cbar_handle.ax.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))
        data_cbar_handle.ax.set_title('Distance [m]')

        ax.set_xlabel("X-axis [m]")
        ax.set_ylabel("Y-axis [m]")
        ax.grid()
        set_xylim(ax, conf, [min(x_positions), max(x_positions)], [min(y_positions), max(y_positions)])

    def add_histogram(self, ax):
        hist_arrays, _ = np.histogram(self.__values_clipped_mm, bins=self.__bin_edges)

        # Normalize the histogram to percentages
        hist_percentage = 100 * hist_arrays / sum(hist_arrays)
        bin_width = 0.75 * self.__BIN_STEP_SIZE

        # Leave out first label and append '+' to last label to emphasize all values above
        x_labels = [str(round(x)) for x in self.__bin_edges[0::5]]
        x_labels[-1] += "+"
        ax.set_xticks(self.__bin_edges[0::5])
        ax.set_xticklabels(x_labels)

        # Find corresponding color for each bin
        bin_colors = get_colors_for_bins(self.__color_mapping, self.__bin_edges)
        ax.bar(self.__bin_edges[:-1], hist_percentage, width=bin_width, edgecolor=Colors.BLACK.value, log=True, align='edge',
               color=bin_colors)

        # Limit y-axis to 100%
        ax.set_ylim(top=100)
        # Limit x-axis to last bin
        ax.set_xlim(left=0, right=self.__bin_edges[-1])

        # Set custom tick locations and labels
        custom_ticks = [0.01, 0.1, 1., 10., 100.]  # Example custom tick locations
        custom_labels = ['0.01%', '0.1%', '1%', '10%', '100%']  # Example custom tick labels

        ax.set_yticks(custom_ticks)  # Use FixedLocator to set custom tick locations
        ax.set_yticklabels(custom_labels)  # Use FixedFormatter to set custom tick labels

        # Add line with average of supplied data
        # Color of line depends on whether average falls within target range or not
        line_color = 'g' if self.__constraint_lower_avg < self.__average_mm < self.__constraint_upper_avg else 'r'
        ax.axvline(self.__average_mm, color=line_color, linestyle='dashed', linewidth=1, label="Mean")
        ax.text(self.__average_mm + 3e-4, 40, f'Mean [mm]: {self.__average_mm:.0f}', color=line_color)

    def add_cumulative_histogram(self, ax):
        add_cumulative_histogram(ax, self.__values_clipped_mm, self.__bin_edges[-1], self.__color_mapping)

    @staticmethod
    def __define_ranges_and_colors(min_dist, max_dist, lower_avg, upper_avg):
        assert 0 < min_dist < lower_avg < upper_avg < max_dist

        # Create a color mapping. Scale colors between upper_avg and max_dist linearly.
        target_dist = (lower_avg + upper_avg) / 2
        assert 3 * target_dist < max_dist

        color_mapping = {
            0: Colors.YELLOW,
            min_dist: Colors.YELLOW,
            lower_avg: Colors.LIGHT_GREEN,
            upper_avg: Colors.GREEN,
            2*target_dist: Colors.LIGHT_GREEN,
            3*target_dist: Colors.YELLOW,
            max_dist: Colors.ORANGE,
            np.inf: Colors.DARK_RED
        }

        return color_mapping
