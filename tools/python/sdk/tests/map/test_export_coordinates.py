#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""


import pytest
from sdk.map.export_coordinates import export_coordinates
from sdk.map.map import Map
from sdk.tests.map.test_map import get_db_test_file
from sdk.tests.test_data.files import TestData
from sdk.tests.test_utils.fixture_temp_files import temp_files

TEST_DB_FILES = [
    TestData.File.IMPORT_G2O_DB_200,
]

@pytest.mark.parametrize("test_file", TEST_DB_FILES)
def test_export_coordinates(get_db_test_file, test_file, temp_files):
    test_db = get_db_test_file(test_file)
    output_file = temp_files.create(".csv")

    # Test that no exceptions are raised
    export_coordinates(Map(test_db), output_file)
