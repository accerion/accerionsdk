#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import List

from sdk.csv.versions.base import StrEnum
from sdk.metrics import _functions
from sdk.data.data_container import DataContainer


class MetricsData:
    class Tag(StrEnum):
        CorrectionCount = "correction count"
        DistSignatures = "distance signatures"
        DistCorrections = "distance corrections"
        MatchQuality = "match quality"
        PositionErrors = "position errors"
        OrientationErrors = "orientation errors"
        LoopClosurePositionErrors = "loop closure position errors"
        LoopClosureHeadingErrors = "loop closure heading errors"

    TAG_UNITS = {
        Tag.CorrectionCount: "-",
        Tag.DistSignatures: "m",
        Tag.DistCorrections: "m",
        Tag.MatchQuality: "%",
        Tag.PositionErrors: "m",
        Tag.OrientationErrors: "deg",
        Tag.LoopClosurePositionErrors: "m",
        Tag.LoopClosureHeadingErrors: "deg"
    }

    TAG_FUNCTIONS = {
        Tag.CorrectionCount: _functions.correction_count,
        Tag.DistSignatures: _functions.dist_cons_signatures,
        Tag.DistCorrections: _functions.dist_corrections,
        Tag.MatchQuality: _functions.match_quality,
        Tag.PositionErrors: _functions.position_errors,
        Tag.OrientationErrors: _functions.orientation_errors,
        Tag.LoopClosurePositionErrors: _functions.loop_closure_position_errors,
        Tag.LoopClosureHeadingErrors: _functions.loop_closure_heading_errors,
    }

    def __init__(self, tag: str):
        self.tag = None
        try:
            self.tag = MetricsData.Tag(tag)
        except ValueError:
            raise Exception(f"Supplied data tag '{tag}' is not known")

        assert self.tag in MetricsData.TAG_FUNCTIONS, \
            f"Supplied data tag '{self.tag}' has no corresponding function"
        self.__func = MetricsData.TAG_FUNCTIONS[self.tag]

    def __call__(self, data: DataContainer) -> List[float]:
        return self.__func(data)
