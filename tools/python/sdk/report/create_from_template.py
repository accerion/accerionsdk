#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import os
from argparse import ArgumentParser
from pathlib import Path
from re import compile
from shutil import copyfile
from typing import Dict, List, Union

from markdown import markdownFromFile as md_to_html
from pdfkit import from_file as html_to_pdf
from pweave import weave

from sdk import ROOT_PATH
from sdk.report.markdown_utils import is_list_item
from sdk.utils.path import ensure_directory


__all__ = ["create_report"]


def __parse_arguments() -> Dict:
    parser = ArgumentParser()

    parser.add_argument("-t", "--template", required=True,
                        help="Input markdown template file used to generate a report section of one run.")
    parser.add_argument("-o", "--output", required=True, help="Output folder which is also working directory.")

    return vars(parser.parse_args())


def __clean_markdown_file(input_file: str):
    """
    Somehow, the library pweave sometimes clutters the markdown file with processing statements. This function removes
    those lines from the file.

    Note: reads all lines in memory, so can only be done with small files, should be fine for simple report.
    """

    # Begin with reading all lines from the file
    with open(input_file, "r") as f:
        lines = f.readlines()

    # Then write all lines matching the condition
    with open(input_file, "w") as f:
        reg_ex = compile(r"Processing chunk \d+ named \w+ from line \d+")
        for line in lines:
            if reg_ex.match(line) is None:  # does not match
                f.write(line)
            else:
                print(f"filter line: '{line}'")


def create_report(template: Union[str, Path],
                  output_file_pdf: Union[str, Path],
                  markdown_appendix_lines: List[str] = None):
    template = Path(template).absolute()
    output_file_pdf = Path(output_file_pdf).absolute()
    assert output_file_pdf.suffix == ".pdf", "Report name should have extension '.pdf'."

    # Go to the output directory as the template expects to include results from there as well
    output_folder = output_file_pdf.parent
    ensure_directory(output_folder)
    os.chdir(output_folder)

    # First copy the template file in order to read the plots from the right location
    input_file_md = "report.md"
    copyfile(template, input_file_md)
    output_file_html = "report.html"

    # Update the markdown file by executing the python code in it
    weave(input_file_md, doctype="markdown", figdir="plots")

    # Somehow a side effect of weave is that the stdout is messed up, so from this point prints won't have any effect

    # Clean the processing messages of pweave from the markdown file
    __clean_markdown_file(input_file_md)

    # Add the appendix lines to the report
    if markdown_appendix_lines is not None and len(markdown_appendix_lines) != 0:
        with open(input_file_md, "a") as md:
            md.write(markdown_appendix_lines[0] + '\n')
            for prev_line, curr_line in zip(markdown_appendix_lines[:-1], markdown_appendix_lines[1:]):
                # In case of a list, it is required to have an empty line before it, hence the following step
                if not is_list_item(prev_line) and is_list_item(curr_line):
                    curr_line = '\n' + curr_line
                md.write(curr_line + '\n')

    # Convert the markdown file to pdf, via html
    md_to_html(input=input_file_md, output=output_file_html, extensions=['tables'])

    html_to_pdf_options = {
        "user-style-sheet": ROOT_PATH / "resources" / "style.css",
        "enable-local-file-access": None
    }
    html_to_pdf(output_file_html, output_file_pdf, options=html_to_pdf_options)

    os.remove(input_file_md)
    os.remove(output_file_html)


if __name__ == "__main__":
    args = __parse_arguments()
    create_report(args["template"], Path(args["output"]) / "report.pdf")
