#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path

import numpy as np
from matplotlib.collections import LineCollection
import pandas as pd

from sdk.plotting.plot_map_visualization import plot_clusters
from sdk.plotting.utils.plotting import *
from sdk.processing.generic import get_input_poses
from sdk.utils.configuration import Configuration
from sdk.data.data_container import DataContainer
from sdk.utils.print import print_v


def _get_pose_connections(x_input_: pd.Series, y_input_: pd.Series, x_output_: pd.Series, y_output_: pd.Series):
    df_poses = pd.concat([x_input_, y_input_, x_output_, y_output_], axis=1, join='inner')

    pose_connections = np.zeros((len(df_poses), 2, 2))
    pose_connections[:, 0, 0] = df_poses.iloc[:,0]
    pose_connections[:, 0, 1] = df_poses.iloc[:,1]
    pose_connections[:, 1, 0] = df_poses.iloc[:,2]
    pose_connections[:, 1, 1] = df_poses.iloc[:,3]

    return pose_connections


def plot_sensor_poses(data: DataContainer,
                      output_folder: Optional[Path] = None,
                      conf: Configuration = Configuration.default(),
                      *,
                      verbose: bool = True):
    LDC = data.arams.LDC
    if "LDC" not in data.arams:
        print_v(verbose, __name__, f": Could not find {LDC.full_name()} file or the arams version does not support the file type.")
        return
    df_ldc = data.arams[LDC]

    # Visualize map with clusters
    fig, axis = plt.subplots(nrows=1, ncols=1, figsize=[14, 6])
    fig.suptitle("Map with sensor poses visualization")

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")

    plot_clusters(axis, data.signatures)

    # Plot input poses (if available)
    input_source = "External reference"
    x_input, y_input, _ = drop_nan(*get_input_poses(data.arams))
    if x_input.empty:
        print("No input poses found. Will try to use corrected odometry instead.")

        LRT = data.arams.LRT
        if "LRT" not in data.arams:
            print_v(verbose, __name__,
                    f": Could not find {LRT.full_name()} file or the arams version does not support the file type.")
            return
        df_lrt = data.arams[LRT]

        if df_lrt.empty:
            print_v(verbose, __name__, f": No data in {LRT.full_name()} file or all is filtered out.")
            return

        x_input, y_input = [df_lrt[column].set_axis(df_lrt[LRT.INDEX]) for column in [
            LRT.CORRECTED_ODOM_POSE_X, LRT.CORRECTED_ODOM_POSE_Y]]
        input_source = "Corrected Odometry"

    # Plot input poses with connections to corrections when both are given in one record.
    axis.plot(x_input.to_numpy(), y_input.to_numpy(), marker='.', markersize=7.5, linestyle='None', label=input_source,
              color=Colors.BLUE.value)

    if not df_ldc.empty:
        x_correction, y_correction = [df_ldc[column].set_axis(df_ldc[LDC.INDEX])
                                      for column in [LDC.NEW_XPOS, LDC.NEW_YPOS]]
        axis.plot(x_correction.to_numpy(), y_correction.to_numpy(), marker='.', markersize=2.5, linestyle='None',
                  label="Corrections", color=Colors.ORANGE.value)
        pose_connections = _get_pose_connections(x_input, y_input, x_correction, y_correction)
        axis.add_collection(LineCollection(pose_connections, color=Colors.BLACK.value, linewidth=0.5))
    else:
        print_v(verbose, __name__, f": No data in {LDC.full_name()} file or all is filtered out.")

    # Change the order of the legend items, such that the poses are on top
    legend_handles, legend_labels = axis.get_legend_handles_labels()
    legend_handles = legend_handles[-2:] + legend_handles[:-2]
    legend_labels = legend_labels[-2:] + legend_labels[:-2]
    axis.legend(handles=legend_handles, labels=legend_labels, loc="upper right")

    axis.grid()
    set_xylim(axis, conf, [min(x_input), max(x_input)], [min(y_input), max(y_input)])
    save_or_show(fig, output_folder, "map_with_sensor_poses.png", conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_sensor_poses)
