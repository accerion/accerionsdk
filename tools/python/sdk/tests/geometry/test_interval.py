#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.geometry.interval import Interval


def test_initialisation():
    interval = Interval(0, 1)
    assert 0 == interval.min
    assert 1 == interval.max

    interval = Interval(1, 0)
    assert 0 == interval.min
    assert 1 == interval.max

    interval.min = 2
    assert 1 == interval.min
    assert 2 == interval.max

    interval.max = 0
    assert 0 == interval.min
    assert 1 == interval.max


def test_containment():
    interval = Interval(0, 1)

    assert 0 in interval
    assert 0.5 in interval
    assert 1 in interval

    assert -1 not in interval
    assert 2 not in interval


def test_include():
    interval = Interval(0, 1)
    assert -1 not in interval
    assert 2 not in interval

    interval.include(-1)
    assert -1 in interval
    assert 2 not in interval

    interval.include(2)
    assert 2 in interval


def test_overlap():
    overlap = Interval(-1, 1).overlap(Interval(0, 1))

    assert overlap
    assert 0 == overlap.min and 1 == overlap.max

    inner = Interval(0.25, 0.75)
    outer = Interval(0, 1)

    overlap = inner.overlap(outer)
    assert overlap
    assert overlap == inner

    assert Interval(0, 1) == Interval(0, 1)

    assert not Interval(0, 1).overlap(Interval(2, 3))
