/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionMap/schema_creator.h"

namespace accerion {

namespace map {

SchemaCreator::SchemaCreator(std::map<accerion::api::Version, accerion::map::Schema> schemas) : schemas_{schemas} {}

std::optional<Schema> SchemaCreator::FindSchema(const accerion::api::Version& map_version) const {
    const auto db_schema_iter = schemas_.find(map_version);
    return db_schema_iter == schemas_.end() ? std::nullopt : std::make_optional(db_schema_iter->second);
}

std::optional<TableDescription> Schema::FindTable(const std::string& name) {
    if (tables.empty()) {
        return std::nullopt;
    }
    const auto iter = std::find_if(tables.cbegin(), tables.cend(),
                                   [&name](const auto& table) { return table.name == name; });
    return iter == tables.cend() ? std::nullopt : std::make_optional(*iter);
}

std::optional<IndexDescription> Schema::FindIndex(const std::string& name) {
    if (indices.empty()) {
        return std::nullopt;
    }
    const auto iter = std::find_if(indices.cbegin(), indices.cend(),
                                   [&name](const auto& index) { return index.name == name; });
    return iter == indices.cend() ? std::nullopt : std::make_optional(*iter);
}

void SchemaCreator::AddToDatabase(const accerion::sqlite::Database &db,
                                    const accerion::api::Version &map_version) const {
    const auto& schema = FindSchema(map_version);
    if (!schema) {
        throw std::runtime_error(
                "SchemaCreator::AddToDatabase: No schema for requested map version '" + map_version.String() + "'");
    }
    // Creating the database inside a transaction to prevent the situation in which some tables are created and some are
    // not or the version being set yet, for example in a power cut. This will ease the check if a database was created
    // (just a check that a table is present is sufficient)
    db.Execute("BEGIN TRANSACTION");
    for (const auto& table : schema.value().tables) {
        db.Execute("CREATE TABLE " + table.name + " (" + table.columns + ")");
    }
    db.Execute("INSERT OR REPLACE INTO metadata (key, value) VALUES ('map_version', '" + map_version.String() + "')");
    db.Execute("COMMIT");
}

void SchemaCreator::DropIndices(const accerion::sqlite::Database& db, const accerion::api::Version& map_version) const {
    const auto opt_schema = FindSchema(map_version);
    if (!opt_schema || opt_schema.value().indices.empty()) {
        return;
    }
    for (const auto& index : opt_schema.value().indices) {
        db.Execute("DROP INDEX IF EXISTS " + index.name);
    }
}

void SchemaCreator::CreateIndices(const accerion::sqlite::Database& db,
                                    const accerion::api::Version& map_version) const {
    const auto opt_schema = FindSchema(map_version);
    if (!opt_schema || opt_schema.value().indices.empty()) {
        return;
    }
    for (const auto& index : opt_schema.value().indices) {
        db.Execute("CREATE INDEX IF NOT EXISTS " + index.name + " ON " + index.specification);
    }
}

}  // namespace map
}  // namespace accerion
