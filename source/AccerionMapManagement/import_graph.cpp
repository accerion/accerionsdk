/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <iostream>

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionMapManagement/graph.h"
#include "AccerionPgo/g2o_format.h"

int main(int argc, char *argv[]) {
    // Parse the input parameters
    if (argc != 3) {
        std::cout << "Please provide a map(.db file) as well as a pose graph(.g2o file)\n"
                  << "    e.g. " << argv[0] << " <map to update>  <optimized g2o>" << std::endl;
        return 1;
    }

    const std::string db_path_to_update{argv[1]};
    const std::string optimized_g2o{argv[2]};

    if (!accerion::api::FileExists(db_path_to_update)) {
        std::cout << "Input file `" << db_path_to_update << "`does not exist." << std::endl;
        return 1;
    }
    if (!accerion::api::FileExists(optimized_g2o)) {
        std::cout << "Pose graph (.g2o) file `" << optimized_g2o << "`does not exist." << std::endl;
        return 1;
    }

    auto db = accerion::sqlite::Database(db_path_to_update, SQLITE_OPEN_READWRITE);
    auto pose_graph = accerion::pgo::ReadG2oFile(optimized_g2o);
    if (!pose_graph) {
        std::cout << "Could not process g2o file" << std::endl;
        return 1;
    }
    accerion::map::Import(db, pose_graph.value(),
                                [](std::uint8_t progress) {
                                        std::cout << "    Progress " << +progress << "%" << std::endl;
                                    });
    std::cout << "Import completed" << std::endl;
}
