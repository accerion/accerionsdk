#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.map.versions._map_specification_base import (MapSpecification,
                                                      SignaturesTable)


class MapSpecificationV200(MapSpecification):
    class Signatures(SignaturesTable):
        ID = "id"
        CLUSTER_ID = "cluster_id"
        INDEX_IN_CLUSTER = "index_in_cluster"
        X = "x"
        Y = "y"
        TH = "th"
