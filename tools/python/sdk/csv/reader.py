#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from os import fdopen, remove
from pathlib import Path
from re import compile
from tempfile import mkstemp
from typing import Any, Optional
from warnings import warn

from pandas import DataFrame, read_csv


__all__ = ['read', 'read_lsd']


# Context manager that creates (and deletes) a temporary file. In this temporary file, the content from input file
# is written, with the faulty lines corrected or removed. The path of the temporary file is returned.
# Actions performed for each line are, in execution order:
#  - Any '\0' characters at the beginning of a line are removed.
#  - Line is removed completely when the number of fields (actually the number of ',' characters) is incorrect
#    according to the given file type.
class __CleanedFile(object):
    def __init__(self, input_file_name: Path, file_type):
        output_file_descriptor, self.output_file_name = mkstemp(suffix=".csv")

        # Construction of the regex used:
        #         [^,]*      = zero or more characters being anything but comma
        #       (,[^,]*){n}  = n times ",<anything but comma>"
        # ^[^,]*(,[^,]*){n}$ = full line containing <anything but comma> plus n times ",<anything but comma>"
        regex_comma_count = compile(f"^[^,]*(,[^,]*){{{len(file_type) - 1}}}$")
        line_count = self.__clean(input_file_name, output_file_descriptor, regex_comma_count)
        if line_count <= 1:
            warn(f"Potentially all data is filtered from '{input_file_name}'.")

    def __enter__(self):
        return self.output_file_name

    def __exit__(self, type, value, traceback):
        remove(self.output_file_name)

    @staticmethod
    def __clean(input_file_name, output_file_descriptor, regex_comma_count) -> int:
        # Helper function
        def clean_line(cleaned_line: str, regex_comma_count):
            cleaned_line = cleaned_line.lstrip('\0')
            incorrect_count = regex_comma_count.search(cleaned_line) is None
            return None if incorrect_count else cleaned_line

        count = 0
        with fdopen(output_file_descriptor, 'w') as output_file_handle, \
                open(input_file_name, 'r') as input_file_handle:
            for line in input_file_handle:
                line = clean_line(line, regex_comma_count)
                if line is not None:
                    output_file_handle.write(line)
                    count += 1
        return count


def _read(path: Path, file_type: Any) -> Optional[DataFrame]:
    with __CleanedFile(path, file_type) as cleaned_input_file:
        data_frame = read_csv(cleaned_input_file, index_col=False)

    # Check that all expected columns are present in the read data
    if len(file_type) > len(data_frame.columns) or not all(h.value in data_frame.columns for h in file_type):
        warn(f"Columns in '{path}' do not seem to match specification. Discarding data.")
        return None

    return data_frame


# Open the file, expecting it to be according to the given specification. Returns None in case of failure.
def read(input_file: Path, file_type) -> Optional[DataFrame]:
    input_file = Path(input_file)

    if not input_file.is_file():
        return None

    if file_type.__name__ == "LSD":
        return read_lsd(input_file)

    return _read(input_file, file_type)


def read_lsd(input_file):
    data_dict = read_csv(input_file, index_col=0).to_dict()['value']
    return data_dict
