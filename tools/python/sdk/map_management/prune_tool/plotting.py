#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Dict, List, Optional, Set

import matplotlib.pyplot as plt
import numpy as np

from sdk.data.signature import Signature
from sdk.map_management.prune_tool.plotting_utils import create_custom_cmap, plot_heatmap, plot_bounding_boxes
from sdk.map_management.prune_tool.pruning import Settings
from sdk.map_management.prune_tool.utils import Pixel, SpatialMesh, rasterize_circle, calculate_signature_density_dict
from sdk.plotting.plot_distance_between_signatures import Plotting as PlottingSignatureDistances
from sdk.plotting.utils.plotting import add_cumulative_histogram, get_colors_for_bins, save_or_show
from sdk.processing.distance_between_signatures import get_distance_between_signatures
from sdk.utils.configuration import Configuration


__all__ = ["Plotting"]


MARKER_SIZE = 15
BIN_STEP_SIZE = 20
BIN_COUNT = 21


class Plotting:
    def __init__(self, signatures: List[Signature], selected_signature_ids: Set[int], settings: Settings):
        self.__signatures = signatures
        self.__selected_signatures = [s for s in signatures if s.id in selected_signature_ids]
        self.__removed_signatures = [s for s in signatures if s.id not in selected_signature_ids]

        self.__settings = settings

        # Determine cell size for search radius approximation. Use the smallest radius.
        if len(settings.bounding_boxes) == 0:
            min_radius = settings.default_radius
        else:
            min_radius = min(settings.default_radius, min([box.radius for box in settings.bounding_boxes]))

        cell_size = min_radius / 4
        self.__mesh = SpatialMesh(cell_size, signatures)

        # Calculate patterns
        self.__patterns: Dict[Optional[float], List[Pixel]] = \
            {box.radius: rasterize_circle(box.radius, self.__mesh.cell_size) for box in settings.bounding_boxes}
        self.__patterns[None] = rasterize_circle(settings.default_radius, self.__mesh.cell_size)

        # Calculate densities
        self.__original_density = calculate_signature_density_dict(self.__mesh,
                                                                   signatures,
                                                                   self.__patterns,
                                                                   settings.bounding_boxes)

        self.__pruned_density = calculate_signature_density_dict(self.__mesh,
                                                                 self.__selected_signatures,
                                                                 self.__patterns,
                                                                 settings.bounding_boxes)

    @property
    def pruned_density(self) -> Dict[Pixel, int]:
        return self.__pruned_density

    def plot_prune_overview(self, output_folder: Optional[Path] = None):
        # plot of both original and pruned data
        fig, ((ax00, ax01), (ax10, ax11)) = plt.subplots(2, 2, figsize=(15, 10), sharex='all', sharey='all')
        fig.subplots_adjust(hspace=0.35, wspace=0.2, left=0.04, right=0.98, bottom=0.05, top=0.9)
        axis_list = fig.get_axes()

        # Plot original data (left side)
        self.add_signatures_in_clusters(ax00, self.__signatures)
        upper_bound = self.__add_signature_density_map(ax10,
                                                       self.__signatures,
                                                       max(self.__original_density.values()),
                                                       self.__settings.max_signatures_in_radius,
                                                       self.__original_density)

        # Plot pruned data (right side)
        _ = self.__add_signature_density_map(ax11,
                                             self.__selected_signatures,
                                             upper_bound,
                                             self.__settings.max_signatures_in_radius,
                                             self.__pruned_density)

        # Calculate and plot removed signatures
        removed_sig_density_dict = {
            (column_id, row_id): original_sig_density - self.__pruned_density.get((column_id, row_id), 0)
            for (column_id, row_id), original_sig_density in self.__original_density.items()
        }

        self.__add_removed_signature_density(ax01,
                                             self.__selected_signatures,
                                             self.__removed_signatures,
                                             removed_sig_density_dict)

        fig.suptitle("Comparison of original (left) and pruned (right) datasets", fontsize=16)

        for ax in axis_list:
            # Plot bounding boxes
            plot_bounding_boxes(ax, self.__settings.bounding_boxes)
            # Set axis equal for all axes
            ax.set_aspect('equal')

        save_or_show(fig, output_folder, "prune_overview.png")

    def plot_signature_densities(self, output_folder: Optional[Path] = None):
        # plot of both original and pruned data
        fig, ((ax00, ax01), (ax10, ax11)) = plt.subplots(2, 2, figsize=(15, 10), sharex='all', sharey='row')
        fig.subplots_adjust(hspace=0.35, wspace=0.2, left=0.04, right=0.98, bottom=0.05, top=0.9)
        fig.suptitle("Comparison of original (left) and pruned (right) datasets", fontsize=16)

        # Top: Show signature density as histogram
        upper_bound = self.__add_signature_density_histogram(ax00,
                                                             None,
                                                             self.__settings.max_signatures_in_radius,
                                                             self.__original_density)
        _ = self.__add_signature_density_histogram(ax01,
                                                   upper_bound,
                                                   self.__settings.max_signatures_in_radius,
                                                   self.__pruned_density)

        # Bottom: Show signature density as histogram (cumulative)
        self.__add_signature_density_histogram_cumulative(ax10,
                                                          upper_bound,
                                                          self.__settings.max_signatures_in_radius,
                                                          self.__original_density)
        self.__add_signature_density_histogram_cumulative(ax11,
                                                          upper_bound,
                                                          self.__settings.max_signatures_in_radius,
                                                          self.__pruned_density)

        ax00.set_ylabel("Frequency [%]")
        ax10.set_xlabel("Signatures in search radius [-]")
        ax11.set_xlabel("Signatures in search radius [-]")

        save_or_show(fig, output_folder, "signature_densities.png")

    def plot_signature_distances(self, output_folder: Optional[Path] = None):
        # plot of both original and pruned data
        fig, ((ax00, ax01), (ax10, ax11), (ax20, ax21)) = plt.subplots(3, 2, figsize=(15, 9))
        fig.subplots_adjust(hspace=0.35, wspace=0.2, left=0.04, right=0.98, bottom=0.05, top=0.9)
        fig.suptitle("Comparison of original (left) and pruned (right) datasets", fontsize=16)

        configuration = Configuration.load_configuration()
        # Calculate distance between signatures before and after pruning
        x_positions_original, y_positions_original, distances_original = get_distance_between_signatures(self.__signatures)
        plotting_original = PlottingSignatureDistances(distances_original, configuration)

        x_positions_pruned, y_positions_pruned, distances_pruned = get_distance_between_signatures(self.__selected_signatures)
        plotting_pruned = PlottingSignatureDistances(distances_pruned, configuration)

        # Top: Distance between signatures heatmap/scatterplot
        PlottingSignatureDistances.add_map(ax00, x_positions_original, y_positions_original, distances_original, configuration)
        PlottingSignatureDistances.add_map(ax01, x_positions_pruned, y_positions_pruned, distances_pruned, configuration)
        ax00.sharex(ax01)
        ax00.sharey(ax01)
        # The following is needed, because of the shared axis
        ax00.set_adjustable('box')
        ax01.set_adjustable('box')

        # Mid: distance between signatures histogram
        plotting_original.add_histogram(ax10)
        plotting_pruned.add_histogram(ax11)

        # Bottom: distance between signatures histogram (cumulative)
        plotting_original.add_cumulative_histogram(ax20)
        plotting_pruned.add_cumulative_histogram(ax21)

        ax20.sharex(ax10)
        ax20.set_xlabel('Euclidean distance [mm]')
        ax21.sharex(ax11)
        ax21.set_xlabel('Euclidean distance [mm]')

        save_or_show(fig, output_folder, "signature_distances.png")

    @staticmethod
    def add_signatures_in_clusters(ax, signatures: List[Signature]):
        unique_clusters = sorted(set(s.cluster_id for s in signatures))

        python_default_colours = plt.rcParams['axes.prop_cycle'].by_key()['color']
        num_colours = len(python_default_colours)
        cluster_id_colour = {cluster_id: python_default_colours[i % num_colours]
                             for i, cluster_id in enumerate(unique_clusters)}

        ax.scatter([s.pose.x for s in signatures],
                   [s.pose.y for s in signatures],
                   c=[cluster_id_colour[s.cluster_id] for s in signatures],
                   marker='s', s=MARKER_SIZE)

        ax.set_xlabel("X Position [m]")
        ax.set_ylabel("Y Position [m]")
        ax.set_title("Signatures and their Cluster ID")
        ax.grid(True, alpha=0.3)

    def __add_signature_density_map(self,
                                    ax,
                                    signatures: List[Signature],
                                    upper_bound,
                                    max_signatures_in_radius,
                                    sig_density_dict):
        ax.scatter([s.pose.x for s in signatures], [s.pose.y for s in signatures],
                   marker='s', color='grey', s=MARKER_SIZE, zorder=1)

        # Create a colormap instance
        cmap, upper_bound, _ = create_custom_cmap(max_signatures_in_radius, upper_bound)

        # Normalize the data
        norm = plt.Normalize(0, upper_bound)

        # Plot heatmap and colorbar
        plot_heatmap(sig_density_dict, self.__mesh, cmap, norm, ax)

        ax.set_xlabel("X Position [m]")
        ax.set_ylabel("Y Position [m]")
        ax.set_title("Number of Signatures within Triton's search radius")
        ax.grid(True, alpha=0.5)

        return upper_bound

    @staticmethod
    def __add_signature_density_histogram(ax, upper_bound: Optional[int], max_signatures_in_radius, sig_density_dict):
        sig_density_list = list(sig_density_dict.values())
        density_average = sum(sig_density_list) / len(sig_density_list)  # Compute now, before clipping

        bins_edges = [i * BIN_STEP_SIZE for i in range(BIN_COUNT + 1)]

        # Combine values outside of bin range in final bin
        sig_density_list = np.clip(sig_density_list, bins_edges[0], bins_edges[-1])

        # Count values in bins. Example: first bin is [0, 20) (including 0, excluding 20)
        bin_values, _ = np.histogram(sig_density_list, bins=bins_edges)

        # Normalize
        bin_values_normalized = bin_values / sum(bin_values)
        bin_widths = [0.75 * BIN_STEP_SIZE] * BIN_COUNT

        # Set colors according to colormap
        if upper_bound is None:
            upper_bound = bins_edges[-1]

        # Define the color mapping
        _, upper_bound, color_dict = create_custom_cmap(max_signatures_in_radius, upper_bound)

        # Find corresponding color for each bin
        bin_colors = get_colors_for_bins(color_dict, bins_edges)
        ax.bar(bins_edges[:-1], bin_values_normalized, width=bin_widths,
               edgecolor='black', log=True, align='edge', color=bin_colors)

        # Set custom tick locations and labels
        custom_ticks = [0.001, 0.01, 0.1, 1.]
        custom_labels = ['0.1%','1%', '10%', '100%']

        ax.set_yticks(custom_ticks)  # Use FixedLocator to set custom tick locations
        ax.set_yticklabels(custom_labels)  # Use FixedFormatter to set custom tick labels

        # Add line with average of supplied data
        ax.axvline(density_average, color='k', linestyle='dashed', linewidth=1, label="Mean")
        ax.text(density_average + 3e-4, max(bin_values_normalized), f'Mean : {density_average:.0f}', color='k')
        ax.grid(zorder=0)

        return upper_bound

    @staticmethod
    def __add_signature_density_histogram_cumulative(ax, upper_bound, max_signatures_in_radius, sig_density_dict):
        _, _, color_mapping = create_custom_cmap(max_signatures_in_radius, upper_bound)
        add_cumulative_histogram(ax, list(sig_density_dict.values()), upper_bound, color_mapping)

    def __add_removed_signature_density(self,
                                        ax,
                                        selected_signatures: List[Signature],
                                        removed_signatures: List[Signature],
                                        sig_density_dict):
        ax.scatter([s.pose.x for s in selected_signatures], [s.pose.y for s in selected_signatures],
                   marker='s', color='grey', label="Kept", s=MARKER_SIZE)
        ax.scatter([s.pose.x for s in removed_signatures], [s.pose.y for s in removed_signatures],
                   marker='x', color='grey', label="Removed", s=MARKER_SIZE)

        upper_bound = max(sig_density_dict.values())
        if upper_bound != 0:
            # Normalize the data based on the maximum number of signatures in a cell
            norm = plt.Normalize(0, upper_bound)

            # Create a colormap instance
            cmap, upper_bound, _ = create_custom_cmap(None, upper_bound)

            # Plot heatmap and colorbar
            plot_heatmap(sig_density_dict, self.__mesh, cmap, norm, ax)

        ax.set_xlabel("X Position [m]")
        ax.set_ylabel("Y Position [m]")
        ax.set_title("Number of removed signatures")
        ax.grid(True, alpha=0.5)
        ax.legend(loc='upper left')
