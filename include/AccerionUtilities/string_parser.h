/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/communication/networking.h"
#include "AccerionSensorAPI/utils/optional.h"


accerion::api::Optional<accerion::api::ConnectionType> ParseConnectionType(const std::string& connection_type);
