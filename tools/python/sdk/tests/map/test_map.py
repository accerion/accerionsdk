#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import shutil
from pathlib import Path
from typing import Callable

import pytest

from sdk.graph.graph import Pose
from sdk.map.map import Map
from sdk.tests.test_data.files import TestData
from sdk.tests.test_utils.fixture_temp_files import temp_files

TEST_DB_FILES = [
    TestData.File.IMPORT_G2O_DB_200
]


@pytest.fixture
def get_db_test_file(temp_files) -> Callable[[TestData.File], Path]:
    def create_db_test_file(test_file):
        db_path = TestData.get(test_file)
        temp_db_path = temp_files.create(".db")

        shutil.copyfile(db_path, temp_db_path)

        return temp_db_path

    return create_db_test_file


@pytest.fixture
def _get_test_map(get_db_test_file) -> Callable:
    def create_test_map(test_db_file: TestData.File) -> Map:
        return Map(get_db_test_file(test_db_file))
    return create_test_map


@pytest.mark.parametrize("test_db_file", TEST_DB_FILES)
def test_read_signature(_get_test_map, test_db_file):
    map_db = _get_test_map(test_db_file)

    signature = map_db.get_signature(6, Map.Info.POSE)

    assert signature
    assert signature.id == 6
    assert signature.cluster_id == 0
    assert signature.index_in_cluster == 5
    assert signature.pose == Pose(
        77.45223725809235, -18.83732459117273, 0.743204441005841)


@pytest.mark.parametrize("test_db_file", TEST_DB_FILES)
def test_read_signatures(_get_test_map, test_db_file):
    map_db = _get_test_map(test_db_file)
    signatures = map_db.get_signatures([6, 6], Map.Info.POSE)
    assert signatures[0].id == signatures[1].id


@pytest.mark.parametrize("test_db_file", TEST_DB_FILES)
def test_remove_signatures(_get_test_map, test_db_file):
    map_db = _get_test_map(test_db_file)

    ids = set(signature.id for signature in map_db.get_all_signatures(Map.Info.IDS))
    assert ids

    map_db.remove_signatures(ids)
    assert not map_db.get_all_signatures(Map.Info.IDS)


@pytest.mark.parametrize("test_db_file", TEST_DB_FILES)
def test_reindex_cluster_index(_get_test_map, test_db_file):
    map_db = _get_test_map(test_db_file)

    assert map_db._Map__cluster_reindex_required()
    map_db.reindex_cluster_index()
    assert not map_db._Map__cluster_reindex_required()

def test_foreign_key_constraint_enabled(_get_test_map):
    """
    When deleting the signatures, due to the foreign key constraint and the database 2.0.0 schema definition
    ("ON DELETE CASCADE"), also the data should be deleted.
    """
    map_db = _get_test_map(TestData.File.IMPORT_G2O_DB_200)
    signatures = map_db.get_all_signatures(Map.Info.IDS)
    assert len(signatures) > 0

    data_entries = map_db.database().get_cursor("""SELECT count() FROM data""").fetchone()[0]
    assert data_entries == len(signatures)

    map_db.remove_signatures({s.id for s in signatures})
    data_entries = map_db.database().get_cursor("""SELECT count() FROM data""").fetchone()[0]
    assert data_entries == 0
