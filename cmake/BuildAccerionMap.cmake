project(AccerionMap LANGUAGES CXX)

add_library(${PROJECT_NAME}
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMap/validator.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMap/schema_creator.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMap/signatures.cpp)

target_link_libraries(${PROJECT_NAME} AccerionDatabaseHandling )

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

add_compiler_warnings(${PROJECT_NAME})

add_executable(ValidateMap ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMap/validator_app.cpp)
target_link_libraries(ValidateMap AccerionMap)