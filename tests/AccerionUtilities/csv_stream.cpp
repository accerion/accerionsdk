/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionUtilities/csv_stream.h"

// write a unit test for CsvStream class that tests writing multiple datatypes into a stream
TEST(AccerionUtilities, CsvStream) {
    // instantiate CsvStream object with dummy values
    std::stringstream ss;
    accerion::utilities::CsvStream stream(ss, ',');
    // write a line with multiple datatypes into the stream
    stream.Write<std::string, int, double, float, int, std::uint8_t>("Hello", 1, 2.1, 3.1f, -4, 2);
    // test if the values are correct
    EXPECT_EQ(ss.str(), "Hello,1,2.1,3.1,-4,2\n");
}
