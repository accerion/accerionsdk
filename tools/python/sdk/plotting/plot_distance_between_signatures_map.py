#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Optional

from matplotlib import pyplot as plt

from sdk.data.data_container import DataContainer
from sdk.plotting.plot_distance_between_signatures import Plotting
from sdk.plotting.utils.plotting import call_arams_plot_function, save_or_show
from sdk.processing.distance_between_signatures import get_distance_between_signatures
from sdk.utils.configuration import Configuration


def plot_distance_between_signatures_map(data: DataContainer,
                                         output_folder: Optional[Path] = None,
                                         conf: Configuration = Configuration.default(),
                                         *,
                                         verbose: bool = True):
    x_positions, y_positions, distances = get_distance_between_signatures(data.signatures)
    if not distances:
        return

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    axis.set_title('Map visualization')

    Plotting.add_map(axis, x_positions, y_positions, distances, conf)

    save_or_show(fig, output_folder, "distance_between_signatures_map.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_distance_between_signatures_map)
