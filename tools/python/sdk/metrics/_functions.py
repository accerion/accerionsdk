#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025 Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from math import degrees
from typing import List

from sdk.graph.filter import identify_edges
from sdk.graph.graph import Graph
from sdk.graph.residuals import get_edge_residuals_grouped
from sdk.processing import (get_correction_count,
                            get_distance_between_corrections,
                            get_distance_between_signatures,
                            get_match_quality,
                            get_pose_corrections)
from sdk.utils.configuration import Configuration
from sdk.data.data_container import DataContainer


def correction_count(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []

    # Although the correction count per cluster crossing is configurable, it is undesired in case of metrics.
    # The values calculated as metrics should always be the same.
    _, _, correction_counts, _ = get_correction_count(
        data.arams, Configuration.default(), verbose=False)

    return correction_counts


def dist_cons_signatures(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []
    _, _, distances = get_distance_between_signatures(data.signatures)
    return distances


def dist_corrections(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []
    _, _, distances = get_distance_between_corrections(data.arams, verbose=False)
    return distances


def match_quality(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []
    _, _, match_quality = get_match_quality(data.arams, verbose=False)
    return match_quality


def position_errors(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []
    _, _, error_poses = get_pose_corrections(data.arams, verbose=False)
    return [e.norm for e in error_poses]


def orientation_errors(data: DataContainer) -> List[float]:
    if data.arams is None:
        return []
    _, _, error_poses = get_pose_corrections(data.arams, verbose=False)
    return [abs(degrees(e.th)) for e in error_poses]


def loop_closure_position_errors(data: DataContainer) -> List[float]:
    if data.graph is None:
        return []
    # Calculate all edge residuals per begin vertex
    grouped_residuals = get_edge_residuals_grouped(data.graph, identify_edges(data.graph, Graph.Edge.Type.Loop))
    # Calculate the average edge residuals per begin vertex
    errors = [sum([p.norm for p in poses]) / len(poses) for _, poses in grouped_residuals.values()]
    return errors


def loop_closure_heading_errors(data: DataContainer) -> List[float]:
    """
    Calculate the average heading residuals of all edges per begin vertex, in degrees
    """
    if data.graph is None:
        return []
    # Calculate all edge residuals per begin vertex
    grouped_residuals = get_edge_residuals_grouped(data.graph, identify_edges(data.graph, Graph.Edge.Type.Loop))
    # Calculate the average edge residuals per begin vertex
    errors = [degrees(sum([abs(p.th) for p in poses]) / len(poses)) for _, poses in grouped_residuals.values()]
    return errors
