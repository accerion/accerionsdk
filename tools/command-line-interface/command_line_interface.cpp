/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "command_line_interface.h"

#include <numeric>
#include <utility>

#include <AccerionSensorAPI/utils/string.h>

#include "cli_command.h"
#include "blocking_sensor.h"


void Cli::Run(BlockingSensor&& sensor) {
    Cli cli{std::move(sensor)};
    cli.Loop();
}

void Cli::Run(BlockingSensor&& sensor, const std::string& full_command_str) {
    if (full_command_str.empty()) {
        throw std::runtime_error("Implementation error: No command to execute.");
    }

    Cli cli{std::move(sensor)};
    std::istringstream is{full_command_str};
    cli.Execute(is);
}

Cli::Cli(BlockingSensor&& sensor)
        : commands_{CreateCommands()},
          command_inputs_{CreateCommandInputsMap(commands_)},
          help_message_{GenerateHelpMessage(commands_)},
          sensor_{std::move(sensor)} {}

void Cli::Loop() const {
    std::string full_command_str;

    bool running = true;
    while (running) {
        std::cout << "\n>> " << std::flush;

        std::getline(std::cin, full_command_str);
        std::istringstream is(full_command_str);
        if (full_command_str.empty()) {
            continue;
        }

        std::string command_str;
        is >> command_str;
        accerion::api::ToLower(command_str);

        // Stopping condition
        if (command_str == "quit" || command_str == "exit") {
            running = false;
            continue;
        }
        // Showing help
        else if (command_str == "help") {
            std::cout << help_message_;
            continue;
        }

        // Parse the command and execute it
        is.seekg(0);  // Reset to the beginning
        bool stop_app = Execute(is);
        if (stop_app) {
            std::cout << "Application is finished." << std::endl;
            break;
        }
    }
}

bool Cli::Execute(std::istream& is) const {
    // Get input corresponding to a command name or alias
    std::string command_str;
    is >> command_str;
    accerion::api::ToLower(command_str);

    // Look up command id based on input
    const CliCommandInputsMap::const_iterator command_id_iter = command_inputs_.find(command_str);
    if (command_id_iter == command_inputs_.cend()) {
        std::cout << "Unknown command." << std::endl;
        return false;
    }
    CliCommandId command_id = command_id_iter->second;

    // Actually perform the command
    return commands_.at(command_id)->Execute(sensor_, is);
}

// To add a new command:
//  - create a new derived CliCommand object
//  - add the new command in the CreateCommands function below
// all other containers are generated based on the commands and do not need to be updated.

Cli::CliCommands Cli::CreateCommands() {
    CliCommands commands;
    commands.emplace(CliCommandSetMode::Id, std::make_unique<CliCommandSetMode>());
    commands.emplace(CliCommandReboot::Id, std::make_unique<CliCommandReboot>());
    commands.emplace(CliCommandGetFile::Id, std::make_unique<CliCommandGetFile>());
    commands.emplace(CliCommandSendFile::Id, std::make_unique<CliCommandSendFile>());
    commands.emplace(CliCommandGet::Id, std::make_unique<CliCommandGet>());
    commands.emplace(CliCommandSet::Id, std::make_unique<CliCommandSet>());
    commands.emplace(CliCommandBufferedRecovery::Id, std::make_unique<CliCommandBufferedRecovery>());
    commands.emplace(CliCommandDelete::Id, std::make_unique<CliCommandDelete>());
    commands.emplace(CliCommandDev::Id, std::make_unique<CliCommandDev>());
    return commands;
}

Cli::CliCommandInputsMap Cli::CreateCommandInputsMap(const CliCommands &commands) {
    CliCommandInputsMap inputs;
    for (const CliCommands::value_type &c: commands) {
        const auto id = c.first;
        const auto &command = c.second;

        // Helper function that will add an entry in the final container relating one input to the command id
        const auto add_and_check = [&inputs](const std::string &name, CliCommandId id) {
            if (name.empty()) {
                return;
            }

            auto result = inputs.insert({name, id});
            if (!result.second) {  // not inserted
                std::stringstream ss;
                ss << "Implementation error: duplicate command name found '" << name << "'.";
                throw std::runtime_error(ss.str());
            }
        };

        add_and_check(command->name, id);
        for (const auto &alias_name: c.second->alias) {
            add_and_check(alias_name, id);
        }
    }
    return inputs;
}

std::string Cli::GenerateHelpMessage(const CliCommands &commands) {
    // Helper struct containing all information gathered for formatting
    // Note that the minimal values would be the non-command entries of the help message like 'help' and 'exit'. For now
    // assuming those fields will always be smaller than the command ones.
    struct FormattingInfo {
        std::size_t max_size_inputs;
        std::size_t max_usage;
        std::size_t max_description;
    } formatting_info{};

    // Helper function to calculate the largest string size
    const auto string_max_size_func = [](std::size_t max_size, const std::string &str) {
        std::size_t new_size = str.size();
        if (new_size > max_size) {
            return new_size;
        }
        return max_size;
    };

    // Create the following container once for the concatenation of all input possibilities.
    // Note that it is an ordered map, such that the message will always look the same, sorted on CliCommandId.
    using CommandInputsConcat = std::map<CliCommandId, std::string>;
    CommandInputsConcat inputs_concat;

    // Concatenate all options that can be used for one command, plus track the largest string for formatting later
    std::transform(
            commands.cbegin(), commands.cend(), std::inserter(inputs_concat, inputs_concat.end()),
            [&formatting_info, &string_max_size_func](const CliCommands::value_type &pair) {
                const CliCommandId command_id = pair.first;
                const auto &command = pair.second;

                // Concatenate name and aliases
                std::string command_concat = command->name;
                std::for_each(command->alias.cbegin(), command->alias.cend(),
                              [&command_concat](const std::string &a) {
                                  if (a.empty()) {
                                      return;
                                  }
                                  command_concat += ", " + a;
                              });

                // Determine maximum size
                formatting_info.max_size_inputs = string_max_size_func(formatting_info.max_size_inputs, command_concat);

                return CommandInputsConcat::value_type{command_id, std::move(command_concat)};
            });

    // Determine the largest usage and help string for formatting later
    const auto vector_string_max_size_func = [&string_max_size_func](std::size_t max_size,
                                                                     const std::vector<std::string> &strings) {
        return std::accumulate(strings.cbegin(), strings.cend(), max_size, string_max_size_func);
    };
    formatting_info = std::accumulate(commands.cbegin(), commands.cend(), formatting_info,
                                      [&vector_string_max_size_func](FormattingInfo info,
                                                                     const CliCommands::value_type &pair) {
                                          info.max_usage = vector_string_max_size_func(info.max_usage,
                                                                                       pair.second->usage);
                                          info.max_description = vector_string_max_size_func(info.max_description,
                                                                                             pair.second->description);
                                          return info;
                                      });

    // Actually start building up the help message
    std::stringstream ss;
    const auto add_help_message_entry =
            [&ss,
                    w_inputs = static_cast<int>(formatting_info.max_size_inputs + 4),
                    w_usage = static_cast<int>(formatting_info.max_usage + 4)]
                    (const std::string &inputs, const std::string &usage, const std::string &help) {
                ss << std::left << std::setw(w_inputs) << inputs;
                ss << std::left << std::setw(w_usage) << usage;
                ss << help << std::endl;
            };
    add_help_message_entry("inputs", "usage", "description");
    add_help_message_entry(std::string(formatting_info.max_size_inputs, '-'),
                           std::string(formatting_info.max_usage, '-'),
                           std::string(formatting_info.max_description, '-'));
    add_help_message_entry("help", "", "show this message");
    add_help_message_entry("exit, quit", "", "stop the program");
    std::for_each(
            inputs_concat.begin(), inputs_concat.end(),
            [&commands, &add_help_message_entry](const CommandInputsConcat::value_type &pair) {
                CliCommandId command_id{pair.first};
                const CliCommandPtr &command = commands.at(command_id);
                std::size_t count_usage{command->usage.size()};
                std::size_t count_description{command->description.size()};
                for (std::size_t i = 0; i < std::max(count_usage, count_description); ++i) {
                    std::string inputs{i == 0 ? pair.second : ""};
                    std::string usage{i < count_usage ? command->usage.at(i) : ""};
                    std::string description{i < count_description ? command->description.at(i) : ""};
                    add_help_message_entry(inputs, usage, description);
                }
            });
    return ss.str();
}
