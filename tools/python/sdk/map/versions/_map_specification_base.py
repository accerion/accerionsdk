#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from abc import abstractmethod
from typing import Protocol, Type, runtime_checkable

from sdk.csv.versions.base import ABCEnumMeta, StrEnum


class TableSpecification(StrEnum, metaclass=ABCEnumMeta):
    @staticmethod
    @abstractmethod
    def table_name() -> str:
        pass



class MetaData(TableSpecification):
    KEY = "key"
    VALUE = "value"

    @staticmethod
    def table_name():
        return "metadata"


class SignaturesTable(TableSpecification):
    ID: str
    CLUSTER_ID: str
    INDEX_IN_CLUSTER: str
    X: str
    Y: str
    TH: str

    @staticmethod
    def table_name():
        return "signatures"


@runtime_checkable
class MapSpecification(Protocol):
    MetaData = MetaData
    Signatures: Type[SignaturesTable]
