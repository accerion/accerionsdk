#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from math import degrees

from sdk.metrics.metrics_data import MetricsData
from sdk.plotting.utils.plotting import *
from sdk.processing.pose_corrections import get_pose_corrections
from sdk.utils.configuration import Configuration


def plot_heading_corrections_histogram(data: DataContainer,
                                       output_folder: Optional[Path] = None,
                                       conf: Configuration = Configuration.default(),
                                       *,
                                       verbose: bool = True):
    arams = data.arams  # shallow copy
    
    _, _, error_poses = get_pose_corrections(arams, verbose=verbose)
    if not error_poses:
        return
    errors = [abs(degrees(e.th)) for e in error_poses]

    corrections_total = len(errors)
    corrections_average = sum(errors) / len(errors)

    # Bin ranges; e.g. [5, 6] --> first bin includes values of 5 but excludes values of 6
    max_heading_error = conf[Configuration.Name.METRICS].get_constraint_value(MetricsData.Tag.OrientationErrors,
                                                                              MetricDataAggregation.Type.Maximum,
                                                                              ComparisonType.Smaller)
    n_bins = 10
    bins = [float(x) * max_heading_error / n_bins for x in range(0, n_bins + 1)]

    # Combine values outside of bin range in final bin
    dist = np.clip(errors, bins[0], bins[-1])

    # Generate histogram
    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Heading corrections [deg]", fontweight='bold')
    axis.set_title(f'Total detected signatures: {corrections_total}')

    hist_counts, _, _ = axis.hist(dist, bins=bins, histtype='bar', rwidth=0.75, log=True)

    # Annotate percentages to each bin
    for i, n_corrections in enumerate(hist_counts):
        annotation = f"{n_corrections / corrections_total * 100:.2f}%"
        bin_center = (bins[i] + bins[i + 1]) / 2
        axis.text(bin_center, n_corrections, annotation,
                  horizontalalignment='center',
                  verticalalignment='baseline',
                  fontweight='bold')


    # Append '+' to last label to emphasize all values above
    x_labels = [str(round(x, 1)) for x in bins]
    x_labels[-1] += "+"
    axis.set_xticks(bins)
    axis.set_xticklabels(x_labels)
    axis.set_xlabel('Heading correction L2 norm [deg]')
    # Remove y ticks
    axis.set_yticks([])
    # Relabel the axis as "Frequency"
    axis.set_ylabel("Frequency (log scale)")

    # Visualize mean value
    min_ylim, max_ylim = axis.get_ylim()
    axis.axvline(corrections_average, color=Colors.BLACK.value, linestyle='dashed', linewidth=1, label="Mean")
    axis.text(corrections_average * 1.01, max_ylim * 0.95, f'Mean [deg]: {corrections_average:.3f}')
    axis.set_ylim((min_ylim, max_ylim * 1.2))

    save_or_show(fig, output_folder, "heading_corrections_histogram.png", conf.get_prefix(arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_heading_corrections_histogram)
