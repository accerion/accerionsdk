#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.plotting.utils.plotting import *
from sdk.processing.distance_between_corrections import get_distance_between_corrections
from sdk.utils.configuration import Configuration


def plot_distance_between_corrections_map(data: DataContainer,
                                          output_folder: Optional[Path] = None,
                                          conf: Configuration = Configuration.default(),
                                          *,
                                          verbose: bool = True):
    from sdk.metrics.metrics_data import MetricsData

    pos_x, pos_y, dist = get_distance_between_corrections(data.arams, verbose=verbose)
    if not dist:
        return

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Distance between consecutive corrections [m]", fontweight='bold')
    axis.set_title(f'Total detected signatures: {len(pos_x)}')

    if conf[Configuration.Name.OVERLAY_INPUT_POSES]:
        add_input_poses(axis, data.arams)

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)
    _, data_cbar_handle = scatter_data(axis, pos_x, pos_y, dist, conf, MetricsData.Tag.DistCorrections, factor=50)
    visualize_first_dc(axis, pos_x, pos_y, conf)
    
    data_cbar_handle.ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f')) 
    
    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")
    axis.grid()
    set_xylim(axis, conf, [min(pos_x), max(pos_x)], [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "distance_between_corrections_map.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_distance_between_corrections_map)
