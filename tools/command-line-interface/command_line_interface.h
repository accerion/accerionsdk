/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <numeric>
#include <unordered_map>
#include <string>

#include "AccerionSensorAPI/structs/address.h"

#include "cli_command.h"


class Cli {
public:
    // Run the CLI interactively
    static void Run(BlockingSensor&& sensor);

    // Run the CLI non-interactively, immediately executing the commands provided
    static void Run(BlockingSensor&& sensor, const std::string& full_command_str);

private:
    // Container relating the command id to the actual command instance to perform the action
    using CliCommands = std::unordered_map<CliCommandId, CliCommandPtr>;
    // Container relating all possible inputs (name and aliases) to the corresponding command id
    using CliCommandInputsMap = std::unordered_map<std::string, CliCommandId>;

    explicit Cli(BlockingSensor&& sensor);

    void Loop() const;
    // The return value indicates whether the interactive CLI should be stopped
    bool Execute(std::istream& is) const;

    static CliCommands CreateCommands();
    static CliCommandInputsMap CreateCommandInputsMap(const CliCommands& commands);
    static std::string GenerateHelpMessage(const CliCommands& commands);

    const CliCommands commands_;
    const CliCommandInputsMap command_inputs_;
    const std::string help_message_;

    BlockingSensor sensor_;
};
