/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "blocking_sensor.h"

#include <algorithm>
#include <future>
#include <memory>
#include <string>
#include <utility>

#include "AccerionSensorAPI/callbacks.h"
#include "AccerionSensorAPI/connection_manager.h"
#include "AccerionSensorAPI/utils/reporting.h"
#include "AccerionSensorAPI/utils/optional.h"

#include "AccerionUtilities/coordinates_writer.h"


namespace detail {

// Function that will wait till a done call back is performed and return whether the function was successful or not.
// The function object should still accept a DoneCallback parameter. This function will wrap the user supplied
// DoneCallback to introduce the waiting mechanism. If the done call back is not performed before a certain timeout,
// then no value is returned. A timeout of zero means the function will block / expects a result.
template <typename Function, typename Duration>
accerion::api::Optional<bool> WaitForDoneCallbackOrTimeout(Function function,
                                                           Duration timeout,
                                                           accerion::api::DoneCallback done_call_back = {}) {
    std::promise<bool> promise;
    std::future<bool> future = promise.get_future();
    std::atomic_flag satisfied{false};

    // Trigger the execution of the function
    function([&promise, &done_call_back, &satisfied](bool success, std::string message) {
        if (!satisfied.test_and_set()) {
            if (done_call_back) {
                done_call_back(success, std::move(message));
            }
            promise.set_value(success);
        }
    });

    // Wait for completion or timeout
    if (timeout == Duration::zero() || future.wait_for(timeout) == std::future_status::ready) {
        return future.get();
    }
    return {};
}

// Function that will wait till a done call back is performed and return whether the function was successful or not.
// The function object should still accept a DoneCallback parameter. This function will wrap the user supplied
// DoneCallback to introduce the waiting mechanism.
template <typename Function>
bool WaitForDoneCallback(Function function, accerion::api::DoneCallback done_call_back = {}) {
    return WaitForDoneCallbackOrTimeout(function, std::chrono::seconds::zero(), std::move(done_call_back)).value();
}

// Thin wrapper around the WaitForDoneCallBack above to report on the action performed as call back function.
// Returns whether action was successful.
template <typename Function>
bool WaitForDone(const std::string& action, Function function) {
    auto done_call_back = [&action](bool success, const std::string& message) {
        accerion::api::ReportDone(action, success, message);
    };
    return WaitForDoneCallback(std::move(function), std::move(done_call_back));
}

}  // namespace detail

BlockingSensor::BlockingSensor(const accerion::api::Address& ip)
        : sensor_{accerion::api::ConnectionManager::GetSensor(ip, accerion::api::ConnectionType::kConnectionTcp)
                        .WaitFor(std::chrono::seconds{10})},
          ip_address_{ip} {
    if (!sensor_) {
        throw std::runtime_error("Could not connect to sensor");
    }
}

BlockingSensor::BlockingSensor(const accerion::api::SerialNumber& sn)
        : sensor_{accerion::api::ConnectionManager::GetSensor(sn, accerion::api::ConnectionType::kConnectionTcp)
                          .WaitFor(std::chrono::seconds{10})},
          ip_address_{sensor_->GetIpAddress().WaitFor().value().address} {
    if (!sensor_) {
        throw std::runtime_error("Could not connect to sensor");
    }}

accerion::api::SerialNumber BlockingSensor::GetSerialNumber() const {
    return sensor_->GetSerialNumber();
}

void BlockingSensor::Set(Mode mode, bool enabled) const {
    accerion::api::Optional<accerion::api::Acknowledgement> result;
    std::string mode_str;
    switch (mode) {
        case Mode::Internal:
            result = sensor_->SetInternalMode(enabled).WaitFor();
            mode_str = "internal";
            break;
        case Mode::ContSignatureUpdating:
            result = sensor_->SetContinuousSignatureUpdatingMode(enabled).WaitFor();
            mode_str = "continuous signature updating";
            break;
        case Mode::AbsLocalization:
            result = sensor_->SetLocalizationMode(enabled).WaitFor();
            mode_str = "absolute localization";
            break;
        case Mode::Recording:
            result = sensor_->SetRecordingMode(enabled).WaitFor();
            mode_str = "recording";
            break;
        case Mode::Expert:
            result = sensor_->SetExpertMode(enabled).WaitFor();
            mode_str = "expert";
            break;
        case Mode::Aruco:
            result = sensor_->SetMarkerDetectionMode(enabled, accerion::api::MarkerType::kAruco).WaitFor();
            mode_str = "aruco marker detection";
            break;
        case Mode::Idle:
            result = sensor_->SetIdleMode(enabled).WaitFor();
            mode_str = "idle";
            break;
        case Mode::Reboot:
            if (enabled) {
                result = sensor_->Reboot().WaitFor();
            }
            mode_str = "reboot";
            break;
        default:
            throw std::runtime_error("Implementation error: unhandled mode supplied to BlockingSensor::Set.");
    }
    accerion::api::ReportMode(mode_str, result);
}

void BlockingSensor::SetMappingOn(std::uint16_t cluster_id) const {
    auto result = sensor_->SetMappingMode(true, cluster_id).WaitFor();
    accerion::api::ReportMode("mapping", result);
}

void BlockingSensor::SetMappingOff() const {
    auto result = sensor_->SetMappingMode(false, {}).WaitFor();
    accerion::api::ReportMode("mapping", result);
}

void BlockingSensor::SetLineFollowingOn(std::uint16_t cluster_id) const {
    auto result = sensor_->SetLineFollowingMode(true, cluster_id).WaitFor();
    accerion::api::ReportMode("line following", result);
}

void BlockingSensor::SetLineFollowingOff() const {
    auto result = sensor_->SetLineFollowingMode(false, {}).WaitFor();
    accerion::api::ReportMode("line following", result);
}

void BlockingSensor::GetFile(accerion::api::FileType type, const std::string& file_name) const {
    std::unique_ptr<accerion::api::UpdateService> update_service = nullptr;
    detail::WaitForDone("Get file", [this, &type, &file_name, &update_service](auto done_call_back) {
        switch (type) {
            case accerion::api::FileType::Map:
                sensor_->GetMap(file_name, accerion::api::ReportProgress, done_call_back);
                break;
            case accerion::api::FileType::Logs:
                update_service = accerion::api::ConnectionManager::GetUpdateService(ip_address_).WaitFor(std::chrono::seconds{10});
                update_service->GetLogs(file_name, accerion::api::ReportProgress, [](auto t, auto p) { accerion::api::ReportProgress(p); }, done_call_back);
                break;
            default:
                throw std::runtime_error("Implementation error: unhandled file type supplied to BlockingSensor::GetFile.");
        }
    });
}

void BlockingSensor::GetCoordinatesFile(
        const std::string& file_name, accerion::api::Optional<accerion::api::ClusterId> opt_cluster_id) const {
    // First check that the supplied cluster id is available
    if (opt_cluster_id && !ClusterIdExists(opt_cluster_id.value())) {
        std::cout << "Provided cluster id " << opt_cluster_id.value() << " is not available on sensor." << std::endl;
        return;
    }

    // Actually request the data
    const auto& opt_info = sensor_->GetSignatureInformation(opt_cluster_id).WaitFor(std::chrono::minutes{1});
    if (!opt_info) {
        std::cout << "Did not receive signature information from sensor." << std::endl;
        return;
    }

    accerion::utilities::WriteCoordinates(opt_info.value().value, file_name);
}

void BlockingSensor::GetLoopClosuresFile(const std::string& file_name, double search_radius) const {
    std::vector<accerion::api::LoopClosure> edges;
    sensor_->SubscribeToLoopClosures().Callback([&edges](accerion::api::LoopClosure lc) {
                                                            edges.push_back(lc);
                                                        });
    auto& future_complete = sensor_->SubscribeToLoopClosuresCompleted();
    std::cout << "Searching for loop closures with a search radius of " << search_radius << "." << std::endl;
    sensor_->StartSearchForLoopClosures(search_radius);
    future_complete.WaitFor(std::chrono::hours(24));
    auto result = accerion::api::LoopClosure::WriteLoopClosuresToFile(edges, file_name);
    if (result) {
        std::cout << "Export of .g2o file was successful!" << std::endl;
    } else {
        std::cout << "Could not save .g2o file." << std::endl;
    }
}

void BlockingSensor::GetRecordingsFile(const std::string& file_name, std::vector<std::string> ids) const {
    detail::WaitForDone("Get recording file", [this, &file_name, ids = std::move(ids)](auto done_callback) {
        sensor_->GetRecordings(
            std::move(ids), file_name, accerion::api::ReportProgress, [](auto t, auto p) { accerion::api::ReportProgress(p); },
            done_callback);
    });
}

void BlockingSensor::SendMapFile(const std::string& file_name, accerion::api::FileTransferTask task) const {
    detail::WaitForDone("Send map file", [this, &file_name, &task](auto done_call_back) {
        sensor_->SendMap(file_name, task, accerion::api::ReportProgress, [](auto t, auto p) { accerion::api::ReportProgress(p); }, done_call_back);
    });
}

void BlockingSensor::SendUpdateFile(const std::string& file_name) const {
    auto update_service = accerion::api::ConnectionManager::GetUpdateService(ip_address_).WaitFor(std::chrono::seconds{10});
    detail::WaitForDone("Send update file", [&file_name, &update_service](auto done_call_back) {
        update_service->UpdateSensor(file_name, accerion::api::ReportProgress, [](auto t, auto p) { accerion::api::ReportProgress(p); }, done_call_back);
    });
}

/*
 * All property getting and setting
 */
accerion::api::Areas BlockingSensor::GetAreaIds() const {
    return sensor_->GetAreaIds().WaitFor().value();
}

std::vector<accerion::api::ClusterId> BlockingSensor::GetClusterIds() const {
    return sensor_->GetClusterIds().WaitFor().value().values;
}

accerion::api::Diagnostics BlockingSensor::GetDiagnostics() const {
    return sensor_->SubscribeToDiagnostics().WaitFor().value();
}

accerion::api::IpAddress BlockingSensor::GetIpInfo() const {
    return sensor_->GetIpAddress().WaitFor().value();
}

accerion::api::MapSummary BlockingSensor::GetMapSummary() const {
    return sensor_->GetMapSummary().WaitFor().value();
}

std::vector<std::string> BlockingSensor::GetRecordings() const {
    return sensor_->GetRecordingList().WaitFor().value().value;
}

std::pair<accerion::api::SoftwareVersion, accerion::api::SoftwareDetails> BlockingSensor::GetSoftwareInfo() const {
    auto version_optional = sensor_->GetSoftwareVersion().WaitFor();
    auto details_optional = sensor_->GetSoftwareDetails().WaitFor();
    if (!version_optional || !details_optional) {
        return {};
    }
    return std::make_pair(version_optional.value(), details_optional.value());
}

void BlockingSensor::SetArea(std::uint16_t id) const {
    sensor_->SetArea(id).WaitFor();
}

void BlockingSensor::SetBufferLength(float length) const {
    sensor_->SetBufferLength(length).WaitFor();
}
void BlockingSensor::SetConnectionType(accerion::api::ConnectionType type,
                                       accerion::api::Optional<accerion::api::Address> opt_local_ip) const {
    accerion::api::Address local_ip = opt_local_ip ? opt_local_ip.value() : 
        accerion::api::Address{};

    accerion::api::UdpSettings udp_settings{};
    accerion::api::TcpSettings tcp_settings{};
    std::tie(udp_settings, tcp_settings) = ConfigureConnection(type, local_ip);

    sensor_->SetUdpSettings(udp_settings).WaitFor();
    sensor_->SetTcpSettings(tcp_settings).WaitFor();
}
void BlockingSensor::SetIpInfo(accerion::api::Optional<accerion::api::Address> address,
                               accerion::api::Optional<accerion::api::Address> netmask,
                               accerion::api::Optional<accerion::api::Address> gateway) const {
    if (!address && !netmask && !gateway) {
        // Nothing to do
        return;
    }
    const auto ip_info = GetIpInfo();
    accerion::api::IpAddress new_ip_info{};
    new_ip_info.address = address ? address.value() : ip_info.address;
    new_ip_info.netmask = netmask ? netmask.value() : ip_info.netmask;
    new_ip_info.gateway = gateway ? gateway.value() : ip_info.gateway;
    sensor_->SetIpAddress(new_ip_info).WaitFor();
}

void BlockingSensor::SetPose(accerion::api::Pose pose) const {
    sensor_->SetCorrectedOdometry(pose).WaitFor();
}

void BlockingSensor::DeleteArea(accerion::api::Optional<accerion::api::AreaId> id) const {
    sensor_->DeleteArea(id).WaitFor();
}

void BlockingSensor::DeleteCluster(accerion::api::ClusterId id) const {
    sensor_->DeleteCluster(id).WaitFor();
}

void BlockingSensor::PerformBufferedRecovery(double x, double y, accerion::api::Optional<double> opt_r) const {
    double radius = 0;
    std::string suffix = "searching the whole map";
    if (opt_r) {
        radius = opt_r.value();
        suffix = "using the supplied search radius";
    }
    std::cout << "Performing buffered recovery mode " << suffix << std::endl;

    sensor_->StartBufferedRecovery(x, y, radius, false).WaitFor(std::chrono::hours(2),
        [](accerion::api::BufferProgress bp) {
            accerion::api::Report(bp);
            return bp.message_type == accerion::api::BufferedRecoveryMessageType::kFinished;
        });
}

accerion::api::RawData BlockingSensor::GetFrame(const std::string& dev_key) const {
    return sensor_->CaptureFrame(dev_key).WaitFor().value().value;
}

bool BlockingSensor::ClusterIdExists(accerion::api::ClusterId cluster_id) const {
    const auto cluster_ids = GetClusterIds();
    return std::find(cluster_ids.cbegin(), cluster_ids.cend(), cluster_id) != cluster_ids.cend();
}
