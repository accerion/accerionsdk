/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <optional>
#include <string>

#include "AccerionSensorAPI/connection_manager.h"
#include "AccerionSensorAPI/utils/sensor.h"
#include "AccerionSensorAPI/utils/string.h"

#include "command_line_interface.h"


bool CouldBeIpAddress(const std::string& s) {
    if (std::count(s.begin(), s.end(), '.') != 3) {
        return false;
    }
    try {
        std::ignore = accerion::api::Address::Parse(s);
    }
    catch (...) {
        return false;
    }
    return true;
}
bool CouldBeSerialNumber(const std::string& s) {
    return s.size() == 9 && s.find_first_not_of("0123456789") == std::string::npos;
}

accerion::api::Optional<accerion::api::Address> DetectSensor() {
    const auto sensors{accerion::api::ConnectionManager::GetDetectedSensors()};

    if (sensors.empty()) {
        std::cout << "Could not find any sensor." << std::endl;
        return {};
    } else if (sensors.size() == 1) {
        std::cout << "Found sensor with serial number " << sensors.front().serial_number.value << std::endl;
        return {sensors.front().address};
    } else {
        std::cout << "Found multiple sensors:\n";
        std::cout << "    id    ip address         serial number\n";
        std::cout << "    " << std::string(40, '-') << "\n";
        for (std::size_t i = 0; i < sensors.size(); ++i) {
            const auto& ip = sensors[i].address;
            std::cout << "    ";
            std::cout << std::right << std::setw(2) << std::setfill(' ') << i + 1 << "    ";
            std::cout << ip.AlignedString() << "    ";
            std::cout << std::right << std::setfill(' ') << sensors[i].serial_number.value << "\n";
        }

        while (true) {
            std::cout << "> " << std::flush;

            std::string selection{};
            std::getline(std::cin, selection);

            try {
                const std::size_t sensor_index = std::stoul(selection);
                if (sensor_index < 1 || sensor_index > sensors.size()) {
                    std::cout << "Input not valid. Try again..." << std::endl;
                    continue;
                }
                return {sensors[sensor_index - 1].address};
            } catch (...) {
                std::cout << "Could not parse input. Try again..." << std::endl;
            }
        }
    }
}


int main(int argc, char *argv[]) {
    // First, connect to the desired sensor
    std::string id_str{};
    std::optional<BlockingSensor> sensor{};
    if (argc > 1) {
        id_str = argv[1];
        const bool ip_supplied = CouldBeIpAddress(id_str);
        const bool sn_supplied = CouldBeSerialNumber(id_str);
        if (ip_supplied && sn_supplied) {
            throw std::runtime_error("Could not interpret the first argument. Is it an ip address or a serial number?");
        } else if (ip_supplied) {
            std::cout << "Assuming sensor ip was supplied, using " << id_str << "." << std::endl;
            sensor = BlockingSensor{accerion::api::Address::Parse(id_str)};
        } else if (sn_supplied) {
            std::cout << "Assuming sensor serial number was supplied, using " << id_str << "." << std::endl;
            accerion::api::SerialNumber serial_number{};
            std::stringstream ss{id_str};
            ss >> serial_number.value;
            sensor = BlockingSensor{serial_number};
        } else {
            id_str.clear();
        }
    }

    if (!sensor) {
        std::cout << "No sensor identification was supplied. Going to search the network..." << std::endl;
        auto ip_address = DetectSensor();
        if (ip_address) {
            sensor = BlockingSensor{ip_address.value()};
        } else {
            return 1;
        }
    }

    // Aggregate all command components
    std::string command_str{};
    if (argc > 1 + !id_str.empty()) {
        command_str = accerion::api::Join(argv + 1 + !id_str.empty(), argv + argc, " ");
    }

    // Start the command line interface application
    if (command_str.empty()) {
        Cli::Run(std::move(sensor.value()));
    } else {
        Cli::Run(std::move(sensor.value()), command_str);
    }

    return 0;
}
