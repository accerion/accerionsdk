/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <chrono>
#include <functional>
#include <future>
#include <optional>
#include <string>
#include <thread>

#include "AccerionSensorAPI/sensor.h"
#include "AccerionSensorAPI/structs/areas.h"
#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/structs/time.h"
#include "AccerionSensorAPI/structs/diagnostics.h"


using PoseGenerator = std::function<std::optional<accerion::api::Pose>()>;
using PoseCallBack = std::function<void(const accerion::api::TimePoint&, const accerion::api::Pose&)>;

// Function that spawns a thread providing the poses to the call back
std::future<void> CreatePoseSource(PoseGenerator pose_generator, double frequency, PoseCallBack pose_call_back);


// Convenience dummy pose generator
class DummyPoseGenerator {
public:
    DummyPoseGenerator(accerion::api::Pose start_pose, accerion::api::Pose step_pose, std::size_t steps);

    std::optional<accerion::api::Pose> operator()();

private:
    const std::size_t steps_;
    std::size_t step_counter_;
    accerion::api::Pose step_pose_;
    accerion::api::Pose curr_pose_;
};


bool GetMode(const accerion::api::Diagnostics& diagnostics, const std::string& mode_name);


void ConfigureSensor(accerion::api::Sensor& sensor, accerion::api::AreaId area_id, accerion::api::ClusterId cluster_id);

void StartMapping(accerion::api::Sensor& sensor, accerion::api::ClusterId cluster_id);
void StopMapping(accerion::api::Sensor& sensor);
