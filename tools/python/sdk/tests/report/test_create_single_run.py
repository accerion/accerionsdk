#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import sys

import pytest
from packaging.version import Version
from sdk.tests.test_data.files import TestData
from sdk.tests.test_utils.check_output_files import CheckOutputFiles
from sdk.tests.test_utils.run_script import run_script

# Reuse pytest's fixtures: tmpdir
pytest_plugins = "pytester"


def test_create_single_run(tmpdir, run_script):
    versions_to_test = {
        Version("6.0.0"): [TestData.File.ARAMS_USER_LOGS_V6,
                           TestData.File.FLOOR_MAP_COORDINATES],
        Version("6.1.0"): [TestData.File.ARAMS_USER_LOGS_V61,
                           TestData.File.FLOOR_MAP_COORDINATES_V61],
        Version("6.1.5"): [TestData.File.ARAMS_USER_LOGS_V615,
                           TestData.File.FLOOR_MAP_COORDINATES_V61],
        Version("7.0.0"): [TestData.File.ARAMS_USER_LOGS_V700,
                           TestData.File.FLOOR_MAP_COORDINATES_V700],
    }
    for version, test_data_files in versions_to_test.items():
        _run_test(tmpdir, run_script, test_data_files)
        _check_test_output(tmpdir, version)


def _run_test(tmpdir, run_script, test_data_files):
    command = [
        "sdk.report.create_single_run",
        "-i", TestData.get_list([*test_data_files, TestData.File.POSE_GRAPH]),
        "-o", tmpdir
    ]
    result = run_script(*command)

    assert result.ret == pytest.ExitCode.OK


def _check_test_output(tmpdir, version: Version):
    with CheckOutputFiles(tmpdir) as output_files:
        import subprocess

        # Very coarse checks for now
        # Number of images could be 23 in case we are able to calculate DC per cluster crossing and 18 otherwise.
        if version < Version("6.1.5"):
            assert len(output_files.match("*.png")) in {18, 23}
        else:
            # Adding match quality plots with v6.1.5 of Arams, the number of images can be 20 or 25
            assert len(output_files.match("*.png")) in {20, 25}

        assert "report.pdf" in output_files

        terminal_command = {}
        if sys.platform == "linux" or sys.platform == "linux2":
            terminal_command["grep"] = "grep"
            terminal_command["awk"] = "awk"
        elif sys.platform == "win32":
            terminal_command["grep"] = "powershell FindStr"
            terminal_command["awk"] = "powershell awk"

        page_count = 0
        if terminal_command:
            page_count = int(subprocess.run(f"pdfinfo report.pdf | \
                                            {terminal_command['grep']} 'Pages:' | \
                                            {terminal_command['awk']} '{{print $2}}'",
                                            stdout=subprocess.PIPE,
                                            text=True,
                                            shell=True).stdout)
        assert page_count >= 4, "The created report contains too few pages."
