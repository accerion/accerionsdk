#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass, field
from math import sqrt


__all__ = ["Point"]


@dataclass(frozen=True)
class Point:
    x: float = field(default=0.)
    y: float = field(default=0.)

    def __sub__(self, other):
        return Point(x=self.x - other.x, y=self.y - other.y)

    @property
    def norm_squared(self):
        return self.x ** 2 + self.y ** 2

    @property
    def norm(self):
        return sqrt(self.norm_squared)
