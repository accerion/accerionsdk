#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from typing import List

import pandas as pd

from sdk.arams.arams import Arams


def get_input_poses(arams: Arams) -> List[pd.Series]:
    """
    Based on the Arams version, return input poses from either 'logExtRefInputFast' (versions >=v7.0.0) or 'logRuntimeTrackingFast'.
    """
    from packaging.version import Version

    empty_series = pd.Series()
    empty_list = [empty_series, empty_series, empty_series]
    
    if arams.get_version() >= Version("7.0.0"):
        LER = arams.LER
        if LER not in arams:
            return empty_list

        df_ler = arams[LER]
        df_ler = df_ler.set_index(df_ler[LER.INDEX], drop=False)

        return [df_ler[column]
                for column in [LER.INPUT_POSE_X, LER.INPUT_POSE_Y, LER.INPUT_POSE_TH]]
    else:
        LRT = arams.LRT
        if LRT not in arams:
            return empty_list

        df_lrt = arams[LRT]
        df_lrt = df_lrt.set_index(df_lrt[LRT.INDEX], drop=False)

        return [df_lrt[column] for column in [LRT.INPUT_POSE_X, LRT.INPUT_POSE_Y, LRT.INPUT_POSE_TH]]
