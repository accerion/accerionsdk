function(add_example_lib LIB_NAME LIBS)
    file(GLOB_RECURSE SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

    add_library(${LIB_NAME} ${SOURCES})
    set_base_properties(${LIB_NAME})
    target_link_libraries(${LIB_NAME} ${LIBS})
    target_include_directories(${LIB_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
endfunction()

function(add_example_app APP_NAME LIBS)
    file(GLOB_RECURSE SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

    # Append the API library
    set(LIBS "${LIBS}" AccerionSensorAPI)

    add_executable(${APP_NAME} ${SOURCES})
    set_base_properties(${APP_NAME})
    target_link_libraries(${APP_NAME} ${LIBS})
endfunction()

# Process the subdirectories
add_subdirectory(libs)
add_subdirectory(apps)
