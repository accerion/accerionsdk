/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionPoseManager/pose_tree.h"
#include "AccerionSensorAPI/structs/pose.h"

// Test fixture on a static pose tree (no time dependency) specifically testing the transformations
class PoseTreeFixture : public ::testing::Test {
public:
    using PoseTree = accerion::pose_manager::PoseTree<accerion::api::Pose>;

protected:
    PoseTreeFixture() :
        tree_(CreateTree()),
        p_bd_(tree_.Get("B", "D")),
        p_d0_(tree_.Get("D", "0")) {}

    static PoseTree CreateTree();

    static const accerion::api::Pose p_dc_orig_;

    const PoseTree tree_;
    const accerion::api::Pose p_bd_;
    const accerion::api::Pose p_d0_;
};