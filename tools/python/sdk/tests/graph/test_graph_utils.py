#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.graph.utils import Graph, create_edge_vertex_ids_dict


def __create_graph() -> Graph:
    graph = Graph()
    graph.edges = [
        Graph.Edge(10, 20),
        Graph.Edge(21, 11),
        Graph.Edge(12, 22),
        Graph.Edge(23, 13),
    ]
    return graph


def test_create_edge_vertex_ids_dict():
    graph = __create_graph()

    edge_dict = create_edge_vertex_ids_dict(graph)

    assert len(edge_dict) == len(graph.edges)
    for edge_index, e in enumerate(graph.edges):
        dict_index = edge_dict[(e.id_begin, e.id_end)]
        assert edge_index == dict_index


def test_create_edge_vertex_ids_dict_sorted():
    graph = __create_graph()

    edge_dict = create_edge_vertex_ids_dict(graph, sort=True)

    assert len(edge_dict) == len(graph.edges)
    for edge_index, e in enumerate(graph.edges):
        dict_index = edge_dict[(e.id_begin, e.id_end) if e.id_begin < e.id_end else (e.id_end, e.id_begin)]
        assert edge_index == dict_index
