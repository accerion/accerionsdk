#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import pytest
import sys


@pytest.fixture
def run_script(pytester):
    # Function object to return
    def do_run_script(script: str, *args):
        # Take the same Python executable which is used to run this test, because the subprocess that is going to be
        # run, should use the potential virtual environment of its parent
        command = [sys.executable, "-m", script]
        for a in args:
            if isinstance(a, list):
                command.extend(a)
            else:
                command.append(a)
        return pytester.run(*command)
    return do_run_script
