/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "pose_tree_fixture.h"

PoseTreeFixture::PoseTree PoseTreeFixture::CreateTree() {
    // Tree structure:
    //     0
    //     |
    //     A
    //    / \
    //   B   C
    //       |
    //       D
    PoseTree pt;
    pt.Set("A", "0", {0, 1, 10});
    pt.Set("B", "A", {2, 0, 20});
    pt.Set("C", "A", {0, 3, 30});
    pt.Set("D", "C", p_dc_orig_);
    return pt;
}

const accerion::api::Pose PoseTreeFixture::p_dc_orig_ = {4, 2, 40};