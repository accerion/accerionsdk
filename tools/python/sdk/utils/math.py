#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from math import ceil, floor


def rounded_floor(value: float, precision: float) -> float:
    """
    Apply downwards rounding with a certain precision, like
        rounded_floor(0.543, 0.1) = 0.5
    """
    return floor(value / precision) * precision


def rounded_ceil(value: float, precision: float) -> float:
    """
    Apply upwards rounding with a certain precision, like
        rounded_ceil(0.543, 0.1) = 0.6
    """
    return ceil(value / precision) * precision
