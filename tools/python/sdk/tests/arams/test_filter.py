#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import numpy as np
import pandas as pd

from sdk.arams.filter import RegionFilter


def test_region_filter():
    # Regions contain two rectangles
    #   2.0 |
    #       |
    #   1.5 |     /////////////
    #       |     /////////////
    #   1.0 |     /////////////
    #       |     /////////////
    #   0.5 |     /////////////
    #       |
    #   0.0 +------------------------
    #      0.0   0.5   1.0   1.5   2.0
    region_complete = [
        RegionFilter.Region(x_range=(0.5, 1.5), y_range=(0.5, 1.5))
    ]
    region_parts = [
        RegionFilter.Region(x_range=(0.5, 1.0), y_range=(0.5, 1.0)),
        RegionFilter.Region(x_range=(1.0, 1.5), y_range=(0.5, 1.0)),
        RegionFilter.Region(x_range=(0.5, 1.0), y_range=(1.0, 1.5)),
        RegionFilter.Region(x_range=(1.0, 1.5), y_range=(1.0, 1.5))
    ]

    r = np.arange(0.0, 2.5, 0.5)
    x, y = np.meshgrid(r, r)
    shape = np.product(x.shape)
    data_frame = pd.DataFrame(data={
        "x": np.reshape(x, shape),
        "y": np.reshape(y, shape)
    })

    ref = RegionFilter.filter(data_frame, "x", "y", region_complete)
    tst = RegionFilter.filter(data_frame, "x", "y", region_parts)
    assert ref.equals(tst)
