/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <string>
#include <vector>

// Plain resource managers for deleting of created temporary files and folders
class TempFileSystem {
 public:
    ~TempFileSystem() {
        Clean();
    }

    // Optionally supply an extension, without dot, so like "db" or "png".
    std::string Create(const std::string& ext = "", const std::string& folder = "");

 private:
    void Clean();

    std::vector<std::string> temp_files_{};
};
