#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.graph.graph import Pose
from sdk.processing.time_series import TimeSeries


class PoseTimeSeries(TimeSeries):
    """
    Create a time series of the pose, such that time-based look up is possible.
    If the time value is not exactly present in the series, then interpolation is performed.
    To be used like:
        times = list of floats
        poses = list of poses (Pose object)
        pose_timeseries = PoseTimeSeries(times, poses)

        pose = pose_timeseries[time]
    """

    def __getitem__(self, t: float) -> Pose:
        result = super().__getitem__(t)
        ratio, before, after = result.ratio, result.value_before, result.value_after
        return before if after is None else after if before is None else before.interpolate(after, ratio)

