#!/usr/bin/env python3
"""
 Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from pathlib import Path
from typing import Tuple

from sdk.utils.configuration import Configuration
from sdk.utils.path import PathList, path_list, ensure_directory
from sdk.arams.modifications import *
from sdk.arams.filter import *


def process_args(args) -> Tuple[Configuration, PathList, Path]:
    input_files = path_list(args["input"])
    output_folder = args["output"]
    if output_folder is not None:
        output_folder = Path(output_folder).absolute()
        ensure_directory(output_folder)

    # Retrieve or create configuration
    conf = Configuration.load_configuration(output_folder)
    # Update configuration
    if args["skip"] is not None:
        conf[Configuration.Name.SKIP_INITIAL_DRIFT_CORRECTIONS] = int(args["skip"])

    prefix = args["prefix"]
    if prefix is not None:
        if len(prefix) == 0:
            conf[Configuration.Name.PREFIX] = True
        else:
            conf[Configuration.Name.PREFIX] = prefix

    # Write configuration, just in case it is used by the template
    if output_folder is not None:
        conf.save_configuration(output_folder)

    if args['spec'] is not None:
        from sdk.metrics.reader import Reader as MetricsReader
        conf[Configuration.Name.METRICS] = MetricsReader.read(args['spec'])

    return conf, input_files, output_folder


def apply_configuration(arams, conf):
    # Filters
    PowerCycleFilter.filter_all(arams, conf[Configuration.Name.PLOT_LAST_N_RUNS])
    if conf[Configuration.Name.PLOT_LAST_N_RUNS] == 1:
        TimeIntervalFilter.filter_all(arams, conf[Configuration.Name.PLOT_TIME_INTERVAL])
    else:
        print("Ignoring time interval filter, as PLOT_LAST_N_RUNS is larger than 1.")
    if conf[Configuration.Name.SKIP_INITIAL_DRIFT_CORRECTIONS] != 0:
        InitialRecordFilter.filter_drift_corrections(arams, conf[Configuration.Name.SKIP_INITIAL_DRIFT_CORRECTIONS])
    if conf[Configuration.Name.FIRST_CORRECTION_ON_LINE]:
        InitialCorrectionFilter.filter_unique_consecutive_corrections(arams)

    # Modifications
    if conf[Configuration.Name.CORRECTION_ERROR_FROM_INPUT_POSES]:
        CorrectionErrorModification.modify_drift_correction_with_interpolation(arams)

    return arams
