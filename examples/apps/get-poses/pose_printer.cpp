/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "pose_printer.h"

#include <iomanip>
#include <iostream>
#include <sstream>

#include "AccerionSensorAPI/utils/time.h"


PosePrinter::PosePrinter() {
    is_running_ = true;
    printing_thread_ = std::thread([this]() { RunLoop(); });
}

PosePrinter::~PosePrinter() {
    is_running_ = false;
    if (printing_thread_.joinable()) {
        printing_thread_.join();
    }
}

void PosePrinter::Update(const accerion::api::Odometry& pose) {
    last_odometry_pose_ = pose.pose;
    last_odometry_pose_new_ = true;
}

void PosePrinter::Update(const accerion::api::CorrectedOdometry& pose) {
    last_corrected_odometry_pose_ = pose;
    last_corrected_odometry_pose_new_ = true;
}

void PosePrinter::Update(const accerion::api::DriftCorrection& correction) {
    last_corr_ = correction;
    last_corr_new_ = true;
}

void PosePrinter::RunLoop() {
    while (is_running_) {
        if (last_odometry_pose_new_ || last_corrected_odometry_pose_new_ || last_corr_new_) {
            if (header_counter_ == 0) {
                std::cout << header_ << std::endl;
            }
            Print();
            last_odometry_pose_new_ = false;
            last_corrected_odometry_pose_new_ = false;
            last_corr_new_ = false;
            header_counter_ = (header_counter_ + 1) % header_interval_;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

void PosePrinter::Print() const {
    const auto &c = last_corr_;
    std::cout << Print(last_odometry_pose_) << " " << (last_odometry_pose_new_ ? "[new]" : "     ") << "     "
              << Print(last_corrected_odometry_pose_.pose) << " " << (last_corrected_odometry_pose_new_ ? "[new]" : "     ") << "     "
              << Print(c.delta.x, c.delta.y, c.delta.th) << " " << (last_corr_new_ ? "[new]" : "     ") << std::endl;
}

std::string PosePrinter::Print(accerion::api::Pose p) {
    return Print(p.x, p.y, p.th);
}

std::string PosePrinter::Print(double x, double y, double th) {
    std::stringstream ss;
    ss << "(" << std::fixed << std::setprecision(3)
       << std::setw(6) << std::setfill(' ') << x << ","
       << std::setw(6) << std::setfill(' ') << y << ") " << std::setprecision(1)
       << std::setw(6) << std::setfill(' ') << th;
    return ss.str();
}
