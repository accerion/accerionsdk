#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.graph.g2o_reader import find_g2o_file, Reader

from sdk.tests.test_data.files import TestData


def test_g2o_reader():
    input_file = TestData.get(TestData.File.POSE_GRAPH)

    g2o_file = find_g2o_file(input_file)
    assert g2o_file, "Could not find g2o file"

    graph = Reader.read(g2o_file)

    assert len(graph.vertices) == 12346, "Number of vertices is not correct"
    assert len(graph.edges) == 15971, "Number of edges is not correct"
    assert len(graph.fixes) == 1, "Number of fixes is not correct"
