#!/usr/bin/env python3
"""
 Copyright (c) 2021-2022, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from enum import Enum
from typing import Callable, Dict, Tuple, Optional


NO_AGGREGATION_DATA = -999


class ComparisonType(Enum):
    Equal = '='
    Greater = '>'
    Smaller = '<'


class Result(Enum):
    Pass = "pass"
    Fail = "fail"
    NoData = "no data"

    @staticmethod
    def parse(s: str):
        try:
            result = Result(s)
        except ValueError:
            raise Exception(f"Supplied string '{s}' is not known")
        return result


# Returns the value that is used in the condition (if there is only one) and the function to evaluate the condition
def parse_condition(s: str) -> Tuple[Dict[ComparisonType, float], Callable[[float], Result]]:
    s = s.strip(' ')

    assert '(' not in s and ')' not in s, "Cannot handle brackets in conditions"

    # Processing of composite conditions
    if 'and' in s:
        parts = [p.strip(' ') for p in s.split('and')]
        assert len(parts) == 2, "Only supporting a single 'and' operation"
        comparison1, function1 = parse_condition(parts[0])
        comparison2, function2 = parse_condition(parts[1])
        return ({**comparison1, **comparison2},
                lambda x: Result.Pass if function1(x) == Result.Pass and function2(x) == Result.Pass else Result.Fail)
    elif 'or' in s:
        parts = [p.strip(' ') for p in s.split('or')]
        assert len(parts) == 2, "Only supporting a single 'or' operation"
        comparison1, function1 = parse_condition(parts[0])
        comparison2, function2 = parse_condition(parts[1])
        return ({**comparison1, **comparison2},
                lambda x: Result.Pass if function1(x) == Result.Pass or function2(x) == Result.Pass else Result.Fail)

    # Processing of simple conditions
    assert ' ' in s, f"Condition string '{s}' does not a space"
    parts = s.split(' ')
    assert len(parts) == 2, f"Condition string '{s}' should only contain one space"

    condition, value = parts
    value = float(value)

    assert not s.startswith("<=") and not s.startswith(">="), "Comparisons '<=' and '>=' are not supported"
    if s.startswith('<'):
        return {ComparisonType.Smaller: value}, lambda x: Result.Pass if x < value else Result.Fail
    elif s.startswith('>'):
        return {ComparisonType.Greater: value}, lambda x: Result.Pass if x > value else Result.Fail
    elif s.startswith('='):
        return {ComparisonType.Equal: value}, lambda x: Result.Pass if x == value else Result.Fail
    else:
        raise Exception(f"Unknown condition '{condition}'")
