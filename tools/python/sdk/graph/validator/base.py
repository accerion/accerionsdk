#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from abc import ABC, abstractmethod

from sdk.graph.graph import Graph


class Base(ABC):
    # Return a boolean whether the supplied graph is valid
    @abstractmethod
    def is_valid(self, graph: Graph) -> bool:
        raise NotImplementedError

    # Return a corrected version of the graph
    @abstractmethod
    def correct(self, graph: Graph) -> Graph:
        raise NotImplementedError
