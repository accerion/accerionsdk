#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from typing import Optional

from sdk.data.pose import Pose


__all__ = ["Signature"]


@dataclass
class Signature:
    id: Optional[int] = None
    cluster_id: Optional[int] = None
    index_in_cluster: Optional[int] = None
    pose: Optional[Pose] = None
