/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/string.h"
#include "AccerionUtilities/temp_file_system.h"
#include "AccerionMap/map_version.h"
#include "AccerionMap/schema_creator.h"
#include "AccerionMap/validator.h"

class MapValidationTest : public ::testing::Test {
 protected:
    std::string CreateDatabase() {
        auto path = temp_files_.Create("db");
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE};
        schema_creator_.AddToDatabase(db, accerion::map::LatestVersion());
        return path;
    }

    static void InsertData(const accerion::sqlite::Database& database, std::size_t n) {
        const auto insert_data_statement = database.PrepareStatement(R"SQL(
            INSERT
              INTO data
                   ( id,            a,            b)
            VALUES (@id, zeroblob(10), zeroblob(10))
        )SQL");
        for (auto i = 1; i <= n; ++i) {
            insert_data_statement.Bind("@id", i);
            insert_data_statement.Step();
            insert_data_statement.Reset();
        }
    }

    accerion::api::Acknowledgement CheckIndices(const std::string& path) {
        auto schema = schema_creator_.FindSchema(accerion::map::LatestVersion()).value();

        accerion::sqlite::Database db{path, SQLITE_OPEN_READONLY};
        const auto found_index_names = db.PrepareStatement(R"SQL(
            SELECT name
              FROM sqlite_schema
             WHERE     type = "index"
                   AND sql IS NOT NULL  -- omit internal/automatic indices
        )SQL").GetVector<std::string>();

        // Check for unknown indices
        for (const auto& found_index_name : found_index_names) {
            if (!schema.FindIndex(found_index_name)) {
                return {false, "Unknown index '" + found_index_name + "' present"};
            }
        }

        // Check the known indices
        for (const auto& expected_index : schema.indices) {
            if (std::find(found_index_names.cbegin(), found_index_names.cend(), expected_index.name) == found_index_names.cend()) {
                return {false, "Expected index '" + expected_index.name + "' is not present"};
            }
        }

        return {true, ""};
    }

    TempFileSystem temp_files_{};
    accerion::map::SchemaCreator schema_creator_;
};

TEST_F(MapValidationTest, Valid) {
    const auto path = CreateDatabase();
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_TRUE(result.accepted) << result.message;
}

TEST_F(MapValidationTest, EmptyMapVersion) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("UPDATE metadata SET value = '' WHERE key = 'map_version'");
    }

    const auto result = accerion::map::Validator::ValidateSafety(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "Metadata")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, WrongMapVersion) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("UPDATE metadata SET value = '1.0.0' WHERE key = 'map_version'");
    }

    const auto result = accerion::map::Validator::ValidateSafety(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "Metadata")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, MissingTable) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("DROP TABLE images");
    }

    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "table")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, UnknownTable) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("CREATE TABLE x (x int)");
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "unknown table")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, NoView) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("CREATE VIEW x (x) AS SELECT x FROM signatures");
    }
    const auto result = accerion::map::Validator::ValidateSafety(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "other element")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, NoTrigger) {
    temp_files_.Create("csv");
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("CREATE TRIGGER x INSERT ON signatures BEGIN DELETE FROM DATA; END");
    }
    const auto result = accerion::map::Validator::ValidateSafety(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "other element")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, WrongSetting) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("PRAGMA journal_mode = WAL");
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "pragma")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, NoKeypointData) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute(R"SQL(
            INSERT
              INTO signatures
                   (id, cluster_id, index_in_cluster, x, y, th)
            VALUES ( 1,          1,                0, 0, 0,  0)
        )SQL");
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_, true);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "data")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, IndexInClusterCorrect) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute(R"SQL(
            INSERT
              INTO signatures
                   (id, cluster_id, index_in_cluster, x, y, th)
            VALUES ( 1,          1,                0, 0, 0,  0),
                   ( 2,          1,                1, 0, 0,  0),
                   ( 3,          1,                3, 0, 0,  0),
                   ( 4,          1,                2, 0, 0,  0)
        )SQL");
        InsertData(db, 4);
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_TRUE(result.accepted) << result.message;
}

TEST_F(MapValidationTest, IndexInClusterDuplicateValue) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute(R"SQL(
            INSERT
              INTO signatures
                   (id, cluster_id, index_in_cluster, x, y, th)
            VALUES ( 1,          1,                0, 0, 0,  0),
                   ( 2,          1,                2, 0, 0,  0),
                   ( 3,          1,                2, 0, 0,  0),
                   ( 4,          1,                3, 0, 0,  0)
        )SQL");
        InsertData(db, 4);
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "index_in_cluster")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, IndexInClusterWrongStart) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute(R"SQL(
            INSERT
              INTO signatures
                   (id, cluster_id, index_in_cluster, x, y, th)
            VALUES ( 1,          1,                1, 0, 0,  0),
                   ( 2,          1,                2, 0, 0,  0),
                   ( 3,          1,                3, 0, 0,  0),
                   ( 4,          1,                4, 0, 0,  0)
        )SQL");
        InsertData(db, 4);
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "index_in_cluster")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, IndexInClusterHasGap) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute(R"SQL(
            INSERT
              INTO signatures
                   (id, cluster_id, index_in_cluster, x, y, th)
            VALUES ( 1,          1,                0, 0, 0,  0),
                   ( 2,          1,                1, 0, 0,  0),
                   ( 3,          1,                3, 0, 0,  0),
                   ( 4,          1,                4, 0, 0,  0)
        )SQL");
        InsertData(db, 4);
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "index_in_cluster")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, NoVacuumRequired) {
    // Only testing the relative check, because creating an "empty" database of 1 GB takes too long, about half a minute
    const auto path = temp_files_.Create("db");
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE};
        schema_creator_.AddToDatabase(db, accerion::map::LatestVersion());

        const auto insert_signature_statement = db.PrepareStatement(R"SQL(
            INSERT
              INTO signatures
                   (cluster_id, index_in_cluster, x, y, th)
            VALUES (         1,           @index, 0, 0,  0);
        )SQL");
        const auto insert_data_statement = db.PrepareStatement(R"SQL(
            INSERT
              INTO data
                   (                 id,              a,              b)
            VALUES (last_insert_rowid(), zeroblob(1024), zeroblob(1024))
        )SQL");
        constexpr auto kSignatureCount = 100;
        db.Execute("BEGIN TRANSACTION");
        for (auto index_in_cluster = 0; index_in_cluster < kSignatureCount; ++index_in_cluster) {
            insert_signature_statement.Bind("@index", index_in_cluster);
            insert_signature_statement.Step();
            insert_signature_statement.Reset();

            insert_data_statement.Step();
            insert_data_statement.Reset();
        }
        db.Execute("COMMIT");

        db.Execute(R"SQL(
            DELETE
              FROM signatures
             WHERE id NOT IN (SELECT id
                                FROM signatures
				               WHERE index_in_cluster = 0);
        )SQL");
    }
    const auto result = accerion::map::Validator::Validate(path, schema_creator_);
    ASSERT_FALSE(result.accepted);
    ASSERT_TRUE(accerion::api::DoesStringContain(result.message, "unused space")) << "got '" + result.message + "'";
}

TEST_F(MapValidationTest, NoCleanWhenValid) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        const auto indices = schema_creator_.FindSchema(accerion::map::LatestVersion()).value().indices;
        for (const auto& index : indices) {
            db.Execute("CREATE INDEX IF NOT EXISTS " + index.name + " ON " + index.specification);
        }
    }

    accerion::map::Validator::Clean(path, schema_creator_);

    const auto result = CheckIndices(path);
    ASSERT_TRUE(result.accepted) << result.message;
}

TEST_F(MapValidationTest, CleanUnknownIndex) {
    const auto path = CreateDatabase();
    {
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        db.Execute("CREATE INDEX IF NOT EXISTS unknown_index ON signatures(th)");
    }

    accerion::map::Validator::Clean(path, schema_creator_);

    const auto result = CheckIndices(path);
    ASSERT_TRUE(result.accepted) << result.message;
}

TEST_F(MapValidationTest, CleanWrongIndex) {
    const auto path = CreateDatabase();
    {
        const auto index_name = schema_creator_.FindSchema(accerion::map::LatestVersion()).value().indices.front().name;
        accerion::sqlite::Database db{path, SQLITE_OPEN_READWRITE};
        schema_creator_.DropIndices(db);
        db.Execute("CREATE INDEX IF NOT EXISTS " + index_name + " ON signatures(th)");
    }

    accerion::map::Validator::Clean(path, schema_creator_);

    const auto result = CheckIndices(path);
    ASSERT_TRUE(result.accepted) << result.message;
}
