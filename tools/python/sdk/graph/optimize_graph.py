#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import argparse
from glob import glob
from os import rename, remove
from pathlib import Path
from typing import List, Tuple

from graphslam.graph import Graph as SlamGraph
import matplotlib.pyplot as plt
import numpy as np
from pykdtree.kdtree import KDTree

from sdk.graph.filter import identify_edges
from sdk.graph.graph import Graph
from sdk.graph.g2o_reader import Reader
from sdk.graph.g2o_writer import Writer
from sdk.graph.graph_validator import GraphValidator
from sdk.plotting.plot_graphs import PlotGraphsSettings
from sdk.plotting.utils.graph import add_graph
from sdk.plotting.utils.plotting import add_values
from sdk.processing import get_edge_residuals_grouped
from sdk.utils.print import print_v


def graphslam_optimize(input_file: str, output_file: str):
    graph = SlamGraph.from_g2o(input_file)
    graph.optimize(fix_first_pose=True)
    graph.to_g2o(output_file)


def add_prefix(file_name: str, prefix: str) -> str:
    file_name = Path(file_name)
    file_name = file_name.parent / (prefix + file_name.name)
    return str(file_name)


def add_suffix(file_name: str, suffix: str) -> str:
    file_name = Path(file_name)
    file_name = file_name.parent / (file_name.stem + suffix + file_name.suffix)
    return str(file_name)


def add_presuffix(file_name: str, prefix: str, suffix: str) -> str:
    """Add pre- and suffix, also performed in that order."""
    return add_suffix(add_prefix(file_name, prefix), suffix)


def outlier_ratio(distances: List[float], indices: List[int], errors: List[float]) -> float:
    candidate_index = [i for d, i in zip(distances, indices) if d == 0.0][0]

    # Bug in KDTree? Can get past the end index of size, so just ignore it
    after_end_index = len(errors)
    error_neighbours = [errors[i] for i in indices if i != candidate_index and i != after_end_index]
    average_error_neighbours = sum(error_neighbours) / len(error_neighbours)

    return errors[candidate_index] / average_error_neighbours


def remove_outliers(graph: Graph, verbose: bool = False, debug: bool = False) -> Tuple[int, Graph]:
    class OutlierSettings:
        # Candidate selection threshold of edge residuals, edges with a residual larger that this value will be checked.
        residual_threshold: float = 0.02  # [m]

        # For a candidate, a maximum number of neighbour edges within a certain distance will be checked.
        # The average edge residual of all neighbours is calculated to determine the ratio of the candidate residual
        # with respect to the average neighbours residual.
        neighbour_count: int = 50
        neighbour_max_dist: int = 0.5  # [m]

        # When an edge residual is larger by this factor than its neighbours average, then it is marked as outlier.
        residual_ratio_threshold: float = 20.

    # Calculate the average edge residuals per end vertex
    grouped_residuals = get_edge_residuals_grouped(graph, identify_edges(graph, Graph.Edge.Type.Loop))
    vertices_x = []
    vertices_y = []
    errors = []
    for vertex_index, (_, residuals) in grouped_residuals.items():
        vertex = graph.vertices[vertex_index]
        vertices_x.append(vertex.pose.x)
        vertices_y.append(vertex.pose.y)
        errors.append(sum(r.norm for r in residuals) / len(residuals))

    print_v(debug, f"    loop closures edges count = {len(errors)}")

    candidate_lc_edge_indices = [i for i, e in enumerate(errors) if e > OutlierSettings.residual_threshold]
    if len(candidate_lc_edge_indices) == 0:
        print_v(verbose, "There are no loop closure edge residuals exceeding the threshold of "
                         f"{OutlierSettings.residual_threshold}.")
        return 0, graph
    else:
        print(f"{len(candidate_lc_edge_indices)} candidate outlier(s) exceeding the threshold of "
              f"{OutlierSettings.residual_threshold}.")

    if debug:
        fig, axis = plt.subplots(1, 1)

        settings = PlotGraphsSettings.create(2)
        add_graph(axis, graph, settings[1], draw_vertices=False)

        add_values(axis, vertices_x, vertices_y, errors, factor=500)

        axis.set_aspect('equal', adjustable='datalim')
        axis.set_xlabel("x-coordinate [m]")
        axis.set_ylabel("y-coordinate [m]")
        axis.set_title("Loop closure edges with begin vertex residuals")
        fig.tight_layout()
        plt.show()

    vertices_position = np.array([[x, y] for x, y in zip(vertices_x, vertices_y)])
    tree = KDTree(vertices_position)
    distances_list, tree_indices_list = tree.query(vertices_position[candidate_lc_edge_indices, :],
                                                   k=OutlierSettings.neighbour_count + 1,
                                                   distance_upper_bound=OutlierSettings.neighbour_max_dist)

    lc_edge_indices = identify_edges(graph, Graph.Edge.Type.Loop)
    outlier_lc_edge_indices = []
    for lc_edge_index, distances, tree_indices in zip(candidate_lc_edge_indices, distances_list, tree_indices_list):
        ratio = outlier_ratio(distances, tree_indices, errors)
        if ratio > OutlierSettings.residual_ratio_threshold:
            outlier_lc_edge_index = lc_edge_indices[lc_edge_index]
            e = graph.edges[outlier_lc_edge_index]
            print_v(verbose, f"    outliers identified: edge ids, location, ratio: ({e.id_begin}, {e.id_end}), "
                             f"({vertices_x[lc_edge_index]}, {vertices_y[lc_edge_index]}), {ratio}")
            outlier_lc_edge_indices.append(outlier_lc_edge_index)

    if len(outlier_lc_edge_indices) != 0:
        graph.edges = [e for i, e in enumerate(graph.edges) if i not in outlier_lc_edge_indices]

    return len(outlier_lc_edge_indices), graph


def optimize_graph_outliers(input_file: str, output_file: str,
                            correct_graph: bool, outlier_rejection: bool, verbose: bool = False, debug: bool = False):
    # In this function the input_file is the location of the file with the latest, working-in-progress graph

    result, message = GraphValidator.validate(input_file, correct_graph)
    if result == GraphValidator.Result.Valid:
        print("Graph valid.")
    elif result == GraphValidator.Result.Invalid:
        print("Graph is not valid.")
        print("No optimization done.")
        return
    elif result == GraphValidator.Result.NewFile:
        print("Using corrected graph.")
        input_file = message

    tmp_prefix = "acc_tmp_"

    optimization_counter = 0
    while True:
        # Optimize graph
        optimization_counter += 1
        output_file_iter = add_prefix(output_file, tmp_prefix)
        output_file_iter = add_suffix(output_file_iter, f"_{optimization_counter}")
        graphslam_optimize(input_file, output_file_iter)
        print_v(verbose, f"Optimized g2o file written to: {output_file_iter}")
        input_file = output_file_iter

        # Outlier detection and removal
        if not outlier_rejection:
            break

        print("Determining outliers loop closure edges...")
        graph = Reader.read(input_file)
        outlier_count, graph = remove_outliers(graph, verbose, debug)
        if outlier_count != 0:
            print(f"{outlier_count} outlier(s) found and removed. Next iteration.")
            input_file = add_suffix(input_file, "_filtered")
            Writer.write(graph, input_file)
        else:
            print("No outliers found.")
            break

    # Clean up
    rename(input_file, output_file)
    print("Rename final file and done.")
    for f in glob(add_presuffix(output_file, tmp_prefix, "_*")):
        remove(f)


def __parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", required=True, help="Input g2o file")
    parser.add_argument("-o", "--output", required=True, help="Output g2o file")
    parser.add_argument("-y", "--yes", action='store_true',
                        help="Flag to enable automatic correction of the graph, when applicable")
    parser.add_argument("-r", "--rejection", action='store_true',
                        help="Flag to enable outlier rejection, default false")
    parser.add_argument("-v", "--verbose", action='store_true', help="Flag to enable verbose logging")
    parser.add_argument("-d", "--debug", action='store_true', help="Flag to enable debug logging and plotting")

    return vars(parser.parse_args())


if __name__ == "__main__":
    args = __parse_arguments()

    debug = args["debug"]
    verbose = debug or args["verbose"]

    optimize_graph_outliers(args["input"], args["output"], args["yes"], args["rejection"], verbose, debug)
