/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "pose_manager_examples.h"

#include <bitset>
#include <future>

#include "app_utils.h"


std::future<void> CreatePoseSource(PoseGenerator pose_generator, double frequency, PoseCallBack pose_call_back) {
    return std::async(
            std::launch::async,
            [pose_generator = std::move(pose_generator), frequency, pose_call_back = std::move(pose_call_back)]() {
                using namespace std::chrono;

                const auto iteration_duration{
                    duration_cast<system_clock::duration>(duration<double>{1. / frequency})};

                auto end_iteration_time = system_clock::now();
                while (true) {
                    const auto opt_pose = pose_generator();
                    if (!opt_pose) {
                        // Did not receive a pose anymore, so stop the thread
                        break;
                    }
                    pose_call_back(system_clock::now(), opt_pose.value());

                    // Control the frequency
                    end_iteration_time += iteration_duration;
                    std::this_thread::sleep_until(end_iteration_time);
                }
            });
}


DummyPoseGenerator::DummyPoseGenerator(accerion::api::Pose start_pose, accerion::api::Pose step_pose, std::size_t steps)
        : steps_{steps}, step_counter_{0}, step_pose_{step_pose}, curr_pose_{start_pose} {}

std::optional<accerion::api::Pose> DummyPoseGenerator::operator()() {
    if (step_counter_ >= steps_) {
        return std::nullopt;
    }
    const auto curr_pose = curr_pose_;
    curr_pose_ = step_pose_ * curr_pose_;
    ++step_counter_;
    return curr_pose;
}


bool GetMode(const accerion::api::Diagnostics& diagnostics, const std::string& mode_name) {
    const auto modes = std::bitset<16>(diagnostics.modes);
    const auto iter = std::find_if(
            accerion::api::kModeDescriptionMap.cbegin(), accerion::api::kModeDescriptionMap.cend(),
            [&mode_name](const auto& key_value_pair) { return key_value_pair.second == mode_name; });
    if (iter == accerion::api::kModeDescriptionMap.cend()) {
        throw std::runtime_error("Could not find request mode '" + mode_name + "'");
    }
    return modes[iter->first];
}


void ConfigureSensor(accerion::api::Sensor& sensor, accerion::api::AreaId area_id, accerion::api::ClusterId cluster_id) {
    const auto diagnostics = sensor.SubscribeToDiagnostics().WaitFor();
    if (!diagnostics) {
        throw std::runtime_error("Could not get diagnostics");
    }

    if (GetMode(diagnostics.value(), "internal")) {
        const auto opt_ack = sensor.SetInternalMode(false).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not set internal mode");
        }
    }
    if (GetMode(diagnostics.value(), "continuous signature updating")) {
        const auto opt_ack = sensor.SetContinuousSignatureUpdatingMode(false).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not set continuous signature updating mode");
        }
    }
    if (GetMode(diagnostics.value(), "localization")) {
        const auto opt_ack = sensor.SetLocalizationMode(false).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not set localization mode");
        }
    }
    if (GetMode(diagnostics.value(), "mapping")) {
        const auto opt_ack = sensor.SetMappingMode(false).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not set mapping mode");
        }
    }

    const auto opt_area_ids = sensor.GetAreaIds().WaitFor();
    if (!opt_area_ids) {
        throw std::runtime_error("Could not get area ids");
    }
    if (std::find(opt_area_ids.value().all_areas.cbegin(), opt_area_ids.value().all_areas.cend(), area_id)
        != opt_area_ids.value().all_areas.cend()) {
        std::cout << "Note that area " << area_id << " already exists and "
                  << "it will be used to create a new cluster with id " << cluster_id << "." << std::endl;
    }
    if (opt_area_ids.value().active_area != area_id) {
        const auto opt_ack = sensor.SetArea(area_id).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not set area");
        }
    }

    const auto opt_cluster_ids = sensor.GetClusterIds().WaitFor();
    if (!opt_cluster_ids) {
        throw std::runtime_error("Could not get cluster ids");
    }
    if (std::find(opt_cluster_ids.value().values.cbegin(), opt_cluster_ids.value().values.cend(), cluster_id)
        != opt_cluster_ids.value().values.cend()) {
        std::cout << "Cluster " << cluster_id << " already exists, stop the app to prevent deleting it." << std::endl;
        PauseApp("By pressing enter the app will continue, deleting the cluster...");
        const auto opt_ack = sensor.DeleteCluster(cluster_id).WaitFor();
        if (!opt_ack || !opt_ack.value().accepted) {
            throw std::runtime_error("Could not delete cluster");
        }
    }
}

void StartMapping(accerion::api::Sensor& sensor, accerion::api::ClusterId cluster_id) {
    const auto opt_ack = sensor.SetMappingMode(true, cluster_id).WaitFor();
    if (!opt_ack) {
        throw std::runtime_error("Could not start mapping");
    } else if (!opt_ack.value().accepted) {
        throw std::runtime_error("Could not start mapping: " + opt_ack.value().message);
    }
}

void StopMapping(accerion::api::Sensor& sensor) {
    const auto opt_ack = sensor.SetMappingMode(false).WaitFor();
    if (!opt_ack) {
        throw std::runtime_error("Could not stop mapping");
    } else if (!opt_ack.value().accepted) {
        throw std::runtime_error("Could not stop mapping: " + opt_ack.value().message);
    }
}
