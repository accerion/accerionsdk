/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionPgo/g2o_format.h"

#include <fstream>
#include <iomanip>

namespace accerion {
namespace pgo {

namespace details {

constexpr auto kVertex = "VERTEX_SE2";
constexpr auto kEdge = "EDGE_SE2";
constexpr auto kFix = "FIX";

constexpr auto kPrecision = 6;

template <typename Scalar, int Dimension, typename Matrix = Eigen::Matrix<Scalar, Dimension, Dimension>>
Matrix ReadInformationValues(std::istream& is) {
    Matrix matrix{};

    // Read the upper triangular
    for (int row = 0; row < matrix.rows(); ++row) {
        for (int col = row; col < matrix.cols(); ++col) {
            is >> matrix(row, col);
        }
    }

    // Make matrix symmetric
    matrix.template triangularView<Eigen::StrictlyLower>() = matrix.transpose();

    return matrix;
}

template <typename Derived>
std::string WriteInformationValues(const Eigen::MatrixBase<Derived>& matrix) {
    std::stringstream ss{};
    for (int row = 0; row < matrix.rows(); ++row) {
        for (int col = row; col < matrix.cols(); ++col) {
            ss << " " << std::fixed << std::setprecision(kPrecision) << matrix(row, col);
        }
    }
    return ss.str();
}

PoseGraph::Node ReadNodeFromG2o(std::istream& is) {
    PoseGraph::Node node{};
    is >> node.id >> node.pose.x >> node.pose.y >> node.pose.th;
    node.pose.NormalizeRotation();
    node.type = PoseGraph::NodeType::kUnknown;

    std::string optional_type;
    is >> optional_type;
    if (optional_type == "#") {
        is >> node.type;
    }
    return node;
}

PoseGraph::Edge ReadEdgeFromG2o(std::istream& is) {
    PoseGraph::Edge edge{};
    is >> edge.id_a >> edge.id_b >> edge.offset.x >> edge.offset.y >> edge.offset.th;
    edge.offset.NormalizeRotation();
    edge.info_matrix = ReadInformationValues<double, 3>(is);
    edge.type = PoseGraph::EdgeType::kUnknown;

    std::string optional_type;
    is >> optional_type;
    if (optional_type == "#") {
        is >> edge.type;
    }
    return edge;
}

api::SignatureId ReadFixFromG2o(std::istream& is) {
    api::SignatureId id{};
    is >> id;
    return id;
}

void WriteNodeToG2o(std::ostream& os, const PoseGraph::Node& node) {
    os << kVertex << " "
       << std::fixed << std::setprecision(kPrecision) << node.id << " "
       << std::fixed << std::setprecision(kPrecision) << node.pose.x << " "
       << std::fixed << std::setprecision(kPrecision) << node.pose.y << " "
       << std::fixed << std::setprecision(kPrecision) << node.pose.th;
    if (node.type != PoseGraph::NodeType::kUnknown) {
        os << " " << node.type;
    }
    os << std::endl;
}

void WriteEdgeToG2o(std::ostream& os, const PoseGraph::Edge& edge) {
    os << kEdge << " "
       << std::fixed << std::setprecision(kPrecision) << edge.id_a << " "
       << std::fixed << std::setprecision(kPrecision) << edge.id_b << " "
       << std::fixed << std::setprecision(kPrecision) << edge.offset.x << " "
       << std::fixed << std::setprecision(kPrecision) << edge.offset.y << " "
       << std::fixed << std::setprecision(kPrecision) << edge.offset.th << " "
       << WriteInformationValues(edge.info_matrix);
    if (edge.type != PoseGraph::EdgeType::kUnknown) {
        os << " " << edge.type;
    }
    os << std::endl;
}

void WriteFixToG2o(std::ofstream& os, const api::SignatureId& id) {
    os << kFix << " " << id << std::endl;
}

}  // namespace detail

accerion::api::Optional<PoseGraph> ReadG2oFile(const std::string& input_file) {
    std::ifstream ifs(input_file);
    if (!ifs) {
        return accerion::api::Optional<PoseGraph>{};
    }
    PoseGraph pg;
    std::string line;
    while (std::getline(ifs, line)) {
        if (line.empty()) continue;

        std::istringstream iss(line);
        std::string type;
        if (iss >> type) {
            if (type == details::kVertex) {
                pg.nodes.push_back(details::ReadNodeFromG2o(iss));
            }
            else if (type == details::kEdge) {
                pg.edges.push_back(details::ReadEdgeFromG2o(iss));
            }
            else if (type == details::kFix) {
                pg.fixed_node_ids.push_back(details::ReadFixFromG2o(iss));
            }
        }
    }
    return {std::move(pg)};
}

void WriteG2oFile(const PoseGraph &pose_graph, const std::string& output_file) {
    std::ofstream ofs{output_file};
    std::for_each(pose_graph.nodes.cbegin(), pose_graph.nodes.cend(),
                  [&ofs](const PoseGraph::Node& node) {
                      details::WriteNodeToG2o(ofs, node);
                  });
    std::for_each(pose_graph.edges.cbegin(), pose_graph.edges.cend(),
                  [&ofs](const PoseGraph::Edge& edge) {
                      details::WriteEdgeToG2o(ofs, edge);
                  });
    std::for_each(pose_graph.fixed_node_ids.cbegin(), pose_graph.fixed_node_ids.cend(),
                  [&ofs](const api::SignatureId& id) {
                      details::WriteFixToG2o(ofs, id);
                  });
}

}  // namespace pgo

}  // namespace accerion
