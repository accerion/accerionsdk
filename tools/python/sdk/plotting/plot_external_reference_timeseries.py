#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import warnings

from matplotlib.figure import SubplotParams

from sdk.plotting.utils.plotting import *
from sdk.processing.generic import get_input_poses
from sdk.utils.configuration import Configuration
from sdk.data.data_container import DataContainer
from sdk.utils.print import print_v


MIN_INPUT_POSE_HZ = 20.
TRITON_VELOCITY_MA_WINDOW = 30  # Moving average applied to Triton velocities
INPUT_POSE_MA_WINDOW = 150  # Moving average applied to frequency of input pose

def calculate_input_frequency(poses, time):
    f = np.empty(len(poses))
    f[:] = np.nan

    # Find first non-empty entry
    first_entry_index = np.argwhere(~np.isnan(poses))[0][0]
    t = time[first_entry_index]
    for i in range(first_entry_index+1, len(poses[:])):
        if not np.isnan(poses[i]):
            # Calculate frequency based on difference in time between two non-nan input poses
            dt = (time[i] - t)
            t = time[i]
            f[i] = 1. / dt

    return f


def non_nan_moving_average(data, window_length):
    filtered_data = data
    for i in range(len(data)):
        if i > window_length:
            window = data[i:i + window_length]
            filtered_data[i] = np.nanmean(window) if len(window[~np.isnan(window)]) > 0 else np.nan

    return filtered_data


def plot_external_reference_timeseries(data: DataContainer,
                                       output_folder: Optional[Path] = None,
                                       conf: Configuration = Configuration.default(),
                                       *,
                                       verbose: bool = True):
    LRT = data.arams.LRT
    if "LRT" not in data.arams:
        print_v(verbose, __name__,
                f": Could not find {LRT.full_name()} file or the arams version does not support the file type.")
        return
    df_lrt = data.arams[LRT]
    if df_lrt.empty:
        print_v(verbose, __name__, f": No data in {LRT.full_name()} file or all is filtered out.")
        return

    LDC = data.arams.LDC
    if "LDC" not in data.arams:
        print_v(verbose, __name__,
                f": Could not find {LDC.full_name()} file or the arams version does not support the file type.")
        return
    df_ldc = data.arams[LDC]

    # Use 'logRuntimeTracking' indices to find correspondences with the indices from the input poses series
    indices = df_lrt[LRT.INDEX.value]

    # Specify the desired indices and return NaN for indices that do not exist.
    x_input, y_input, theta_input = [pose_data.reindex(indices) for pose_data in get_input_poses(data.arams)]

    # Use data from input poses, unless no correspondences could be found; 
    # in that case fall back to sensor poses (corrected odometry)
    if x_input.notna().any():
        source = "Input"
        input_poses = [x_input.to_numpy(), y_input.to_numpy(),
                       theta_input.to_numpy()]
    else:
        source = "Sensor"
        warnings.warn(
                "Could not read poses from 'logExtRefInput' file, falling back to 'logRuntimeTracking' for sensor poses.")
        input_poses = [df_lrt[LRT.CORRECTED_ODOM_POSE_X.value].to_numpy(),
                       df_lrt[LRT.CORRECTED_ODOM_POSE_Y.value].to_numpy(),
                       df_lrt[LRT.CORRECTED_ODOM_POSE_TH.value].to_numpy()]

    input_poses = np.array(input_poses)

    output_poses = np.array([df_ldc[LDC.NEW_XPOS.value].to_numpy(),
                             df_ldc[LDC.NEW_YPOS.value].to_numpy(),
                             df_ldc[LDC.NEW_THETA.value].to_numpy()])

    time_LRT = df_lrt[LRT.TIME.value].to_numpy()
    time_LDC = df_ldc[LDC.TIME.value].to_numpy()

    t_end = max(time_LRT[-1], time_LDC[-1])  # Get last time
    time_LRT = (time_LRT - t_end) / 1.e3
    time_LDC = (time_LDC - t_end) / 1.e3

    f = calculate_input_frequency(input_poses[0, :], time_LRT)
    # Apply simple moving average (for default Triton settings: 150hz = 1s window)
    f_ma = non_nan_moving_average(f, 150)

    def create_pose_subplot(axis, axis_label, data_1, data_2):
        axis.set_ylabel(axis_label)
        axis.plot(time_LRT, data_1, marker='.', linestyle='None', markersize=6, color=Colors.BLUE.value)
        if data_2 is not None:
            axis.plot(time_LDC, data_2, marker='.', linestyle='None', markersize=2, color=Colors.ORANGE.value)
        axis.grid()

    fig, axes = plt.subplots(nrows=4, ncols=2, figsize=[8, 8], sharex='all', subplotpars=SubplotParams(hspace=.3, wspace=.3))
    title = "External reference input pose (left) and Triton velocities (right)"
    if source == "Sensor":
        title += "\n\nNote that sensor poses are shown (external reference poses are not available)"
    fig.suptitle(title)

    create_pose_subplot(axes[0, 0], "X-axis [m]", input_poses[0, :], output_poses[0, :])
    create_pose_subplot(axes[1, 0], "Y-axis [m]", input_poses[1, :], output_poses[1, :])
    create_pose_subplot(axes[2, 0], "Heading [deg]", input_poses[2, :], output_poses[2, :])

    # Plot (filtered) input frequency and minimum input frequency required
    create_pose_subplot(axes[3, 0], "Frequency [Hz]", f_ma, None)
    axes[3, 0].set_xlabel("Time since retrieval of logs [s]")
    axes[3, 0].plot(axes[3, 0].get_xlim(), [MIN_INPUT_POSE_HZ, MIN_INPUT_POSE_HZ], linestyle='--', color=Colors.GREEN.value, markersize=2)
    axes[3, 0].set_ylim([0, 200])

    axes[0, 0].legend(labels=[f"{source} pose", "Drift corrections"], loc="best", borderaxespad=1.0)
    axes[3, 0].legend(labels=["Input pose (filtered)", "Requirement"], loc="best", borderaxespad=1.0)


    def create_velocity_subplot(axis, axis_label, data_1):
        axis.set_ylabel(axis_label)
        axis.plot(time_LRT, data_1, marker='.', linestyle='None', markersize=2, color=Colors.RED.value)
        axis.grid()

    # Plot velocities
    triton_velocities = np.array([df_lrt[LRT.ODOM_VEL_X.value].to_numpy(),
                                  df_lrt[LRT.ODOM_VEL_Y.value].to_numpy(),
                                  df_lrt[LRT.ODOM_VEL_TH.value].to_numpy()])

    create_velocity_subplot(axes[0, 1], "X-axis [m/s]",
                            non_nan_moving_average(triton_velocities[0, :], TRITON_VELOCITY_MA_WINDOW))
    create_velocity_subplot(axes[1, 1], "Y-axis [m/s]",
                            non_nan_moving_average(triton_velocities[1, :], TRITON_VELOCITY_MA_WINDOW))
    create_velocity_subplot(axes[2, 1], "Angular [deg/s]",
                            non_nan_moving_average(triton_velocities[2, :], TRITON_VELOCITY_MA_WINDOW))
    axes[2, 1].set_xlabel("Time since retrieval of logs [s]")

    axes[3, 1].axis('off')  # Turn off the last plot

    axes[0, 1].legend(labels=["Triton velocity (filtered)"], loc="best", borderaxespad=1.0)

    save_or_show(fig, output_folder, "external_reference_timeseries.png", conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_external_reference_timeseries)
