# AccerionSDK
This software development kit (SDK) adds components on top of the AccerionSensorAPI.
It is designed to ease the integration and usage of the Accerion sensors.

This document explains how to:
- [What is the structure of the repository](#structure)
- [Build/compile the repository components](#build)
- [Use the libraries in your C++ project](#usage)
- [Use other tools in the repository](#other_tools)


<a name="structure"></a>
## Repository structure
### Dependencies
The SDK requires a compiler supporting C++17 or higher.

The following external libraries are used:
- **AccerionSensorAPI (`accerionsensorapi`)** - Used by multiple components within the repository. Is part of this
  repository as a git submodule.
- **SQLite** - Used by the AccerionDatabaseHandle library. 
- **Eigen3** - Used by the AccerionPgo library.

However, for all dependencies hold that they will be built as part of the SDK, so no additional actions should be
required, other than the ones explained in the [build](#build) process.

### Branches
The repository has two main branches:
- master: The latest released code. Releases and tags can found in the repository corresponding to the software releases.
- development: The development branch on which continuous development is done by Accerion and other contributors.

It should be clear that the master branch should be used by customers. More specifically, the release version of the API
should match that of the sensor firmware version. The development branch should only be used for future feature testing
and in close collaboration with Accerion.


<a name="build"></a>
## Building the repository
The SDK can be compiled on the following OSs:
- Ubuntu 18.04 (amd64 architecture)
- Ubuntu 20.04 (amd64 architecture)
- (not preferred) Windows 10

Other OSs/architectures are not tested and therefore not supported.

To compile specific targets, follow these steps
```bash
mkdir build
cd build
git submodule update --init --recursive
cmake ..
make [<target name(s)>] -j$(nproc)
```

When no targets are provided, then all of them will be built.

The build options available

| Application/library               | Compile flag | Default | Remarks                                                                             |
|-----------------------------------|--------------|---------|-------------------------------------------------------------------------------------|
| PoseManager library               | POSE_MANAGER | on      | See dedicated [readme file](include/AccerionPoseManager/README.md).                 | 
| Utilities library                 | UTILITIES    | off     | Used by tools and examples, so when those are enabled, then also this flag will be. |
| Tools applications                | TOOLS        | on      | Currently only the command-line-interface-like tool can be build.                   |
| Examples applications and library | EXAMPLES     | off     |                                                                                     |
| Unit tests                        | UNIT_TESTS   | off     | The tests are not ran automatically in a build locally.                             |

The built libraries can be found in the root build directory and the applications in the bin folder inside the build directory.


<a name="usage"></a>
## How to use the SDK (with CMake)
### Building your project including the SDK
Make the SDK a submodule of your repository and simply add the following statement to your root CMakeLists.txt file:
```cmake
add_subdirectory(AccerionSDK)
```
Subsequently, the targets will be available for use in your project.

### Building your project using already compiled SDK
Make sure to properly link to the compiled library file and include the libraries of the SDK in your project.
Then your CMakeLists file could look like this:
```cmake
set(LIBRARYPATH "/home/test/AccerionSensorAPI/build/libAccerionSensorAPI.a")
set(INCLUDEPATH "/home/test/AccerionSensorAPI/include")

find_library(ACCAPI ${LIBRARYPATH})
include_directories(${INCLUDEPATH})
target_link_libraries(yourApplication ${LIBRARYPATH})
```
You would include the file like this:
```cpp
#include "AccerionPoseManager/pose_manager.h"
```

<a name="other_tools"></a>
## Other tools 

There is a [command-line-interface-like tool](tools/command-line-interface/README.md) which is a C++ application (part
of repository build) that can be used to perform basic operations on a sensor.

The tools folder also contains [Python tooling](tools/python/README.md) of which the main components are:
- g2o pose graph analysis and optimization
- ARAMS logging reading and processing framework
- Plotting and metrics calculation based on ARAMS
- Report generation framework incorporating plots and metric results
