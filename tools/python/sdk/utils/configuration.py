#!/usr/bin/env python3
"""
 Copyright (c) 2021-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from enum import Enum
from typing import Dict, Union, Optional, List
from pathlib import Path
from os.path import join, basename, dirname, exists
from glob import glob
from yaml import dump, safe_load

from sdk import ROOT_PATH
from sdk.utils.path import determine_prefix


__all__ = ["Configuration"]


_CONF_FILE_NAME = "conf.yml"


class Configuration:
    class Name(Enum):
        # maps plotting
        OVERLAY_MAP = "overlay map"
        OVERLAY_INPUT_POSES = "overlay input poses"
        ZOOM_TO_REGION_OF_INTEREST = "zoom to region of interest"
        VISUALIZE_OUTLIERS = "visualize outliers"
        VISUALIZE_FIRST_DRIFT_CORRECTION = "visualize first drift correction"

        # parsing of errors
        FIRST_CORRECTION_ON_LINE = "first correction on line"
        CORRECTION_ERROR_FROM_INPUT_POSES = "drift correction error based on external reference"

        # arams filter
        PLOT_LAST_N_RUNS = "plot last N runs"
        PLOT_TIME_INTERVAL = "plot time interval"
        SKIP_INITIAL_DRIFT_CORRECTIONS = "skip initial drift corrections"

        # cluster crossing filter
        MAX_SIGN_IDS_PER_CLUSTER_CROSSING = "maximum signature ids per cluster crossing"
        MIN_DIST_BETWEEN_CLUSTER_CROSSINGS = "minimum distance between cluster crossings"

        # report generation
        REUSE_RESULTS = "reuse results"
        OMIT_PASSED_RESULTS = "omit passed results"
        METRICS = "metrics"
        PREFIX = "prefix"

        @classmethod
        def _key_exists(cls, value):
            try:
                cls(value)
            except ValueError:
                return False
            return True

    @staticmethod
    def default() -> Configuration:
        file_name = ROOT_PATH / "resources" / "conf_template.yml"
        conf = Configuration.__read(file_name)
        return conf

    @staticmethod
    def __read(file_name):
        conf = Configuration()
        with open(file_name, 'r') as input_file:
            new_data = {Configuration.Name(k): v for k, v in safe_load(input_file).items()}
            conf.__update(new_data)
        return conf

    def __write(self, file_name: str):
        with open(file_name, 'w') as output_file:
            output_file.write(dump({k.value: v for k, v in self.__data.items() if k != Configuration.Name.METRICS},
                                   indent=4, default_flow_style=False, sort_keys=False))

    def __init__(self):
        self.__data = {}

    def __len__(self):
        return len(self.__data)

    def __contains__(self, k: Union[str, Name]):
        k = Configuration.Name(k)
        return k in self.__data

    def __eq__(self, other):
        return self.__data == other.__data

    def __ne__(self, other):
        return not self == other

    def __getitem__(self, k: Union[str, Name]):
        k = Configuration.Name(k)
        assert k in self, f"Requested key '{k}' is not found in configuration."
        return self.__data[k]

    def __setitem__(self, k: Union[str, Name], v):
        k = Configuration.Name(k)
        self.__data[k] = v

    def __update(self, new_data: Dict):
        assert all(Configuration.Name._key_exists(k) for k in new_data.keys()), \
            "Please provide only valid configuration keys. Check the README for the valid options"
        self.__data.update(new_data)

    def update_conf(self, conf: Configuration):
        self.__update(conf.__data)

    @staticmethod
    def load_configuration(folder: Optional[Path] = None):
        from sdk.metrics.reader import Reader as MetricsReader
        from sdk.report.preparations import METRICS_RESULTS_FILE_NAME

        if folder is None:
            file_path = _CONF_FILE_NAME
        else:
            folder = folder if basename(folder) != "plots" else dirname(folder)
            file_path = join(folder, _CONF_FILE_NAME)

        conf_file = glob(file_path)
        assert len(conf_file) in [0, 1], "Expecting one or no configuration file."
        conf = Configuration.default()
        if len(conf_file) == 1:
            user_conf =  Configuration.__read(conf_file[0])
            conf.update_conf(user_conf)

        # Find test specification
        input_file_spec = join(str(folder), METRICS_RESULTS_FILE_NAME)
        if not exists(input_file_spec) or not conf[Configuration.Name.REUSE_RESULTS]:
            conf[Configuration.Name.METRICS] = MetricsReader.read_default()
        else:
            conf[Configuration.Name.METRICS] = MetricsReader.read(input_file_spec)

        return conf

    def save_configuration(self, output_folder: Path = Path('.')):
        self.__write(str(output_folder / _CONF_FILE_NAME))

    def get_prefix(self, input_files: List[Path] = None) -> Optional[str]:
        prefix = None
        if isinstance(self.__data[Configuration.Name.PREFIX], str):
            prefix = self.__data[Configuration.Name.PREFIX]
        elif self.__data[Configuration.Name.PREFIX]:
            prefix = determine_prefix(input_files)

        return prefix
