#!/usr/bin/env python3
"""
 Copyright (c) 2021-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from pathlib import Path
from typing import List

from pandas import concat as df_concat, DataFrame
from tabulate import tabulate

from sdk.metrics.common import NO_AGGREGATION_DATA, Result
from sdk.metrics.metrics import Metrics
from sdk.metrics.metrics_data import MetricsData


FIELD_NAME_TEST_ID = "Test id"
FIELD_NAME_DESCRIPTION = "Test description"
FIELD_NAME_MEASURED_VALUE = "Measured value"
FIELD_NAME_CONDITION = "Condition"
FIELD_NAME_RESULT = "Result"


def add_html_color(text: str, color: str):
    return f'<span style="color:{color}">{text}</span>'


def __add_html_color_result(result: Result):
    # Map the result enum to a html color
    result_colors = {
        Result.NoData: 'black',
        Result.Fail: 'red',
        Result.Pass: 'green'
    }
    return add_html_color(result.value, result_colors[result])


def __create_table(metrics: Metrics, data_tag: MetricsData.Tag):
    """
    Create a dataframe table containing all the metrics results for given data tag.
    """
    conditions = metrics.get_conditions(data_tag)
    table = DataFrame(conditions)

    # Rename the column headers
    table.rename(columns={
        'name': FIELD_NAME_TEST_ID,
        'aggregation_result': FIELD_NAME_MEASURED_VALUE,
        'condition': FIELD_NAME_CONDITION,
        'condition_result': FIELD_NAME_RESULT
    }, inplace=True)

    # Create new column
    table[FIELD_NAME_DESCRIPTION] = f"{data_tag.value} [{MetricsData.TAG_UNITS[data_tag]}], " + \
                                    table['aggregation'].astype(str)

    # Update measured data when there is no data
    table[FIELD_NAME_MEASURED_VALUE] = \
        table.apply(lambda row: 'no data' if row[FIELD_NAME_MEASURED_VALUE] == NO_AGGREGATION_DATA else
                                str(row[FIELD_NAME_MEASURED_VALUE]),
                    axis=1)

    # Convert the result enum to its value string with color
    table[FIELD_NAME_RESULT] = table.apply(lambda row: __add_html_color_result(row[FIELD_NAME_RESULT]), axis=1)

    # Only keep the columns of interest
    table = table[
        [FIELD_NAME_TEST_ID, FIELD_NAME_DESCRIPTION, FIELD_NAME_MEASURED_VALUE, FIELD_NAME_CONDITION, FIELD_NAME_RESULT]
    ]

    return table


def print_metrics_table(metrics: Metrics, data_tag: MetricsData.Tag):
    """
    Create a markdown formatted table from the dataframe table (output from __create_table) containing all the metrics
    results for given data tag.
    """
    table = __create_table(metrics, data_tag)

    # Sort the table based on the id column
    table.sort_values(FIELD_NAME_TEST_ID, inplace=True)

    print('\n' + table.to_markdown(index=False) + '\n')


def print_metrics_overview_table(metrics: Metrics):
    """
    Create a markdown formatted table from the dataframe table (output from __create_table) containing all the metrics
    results for given data tag.
    """
    table = DataFrame()
    for data in metrics.data_conditions.keys():
        table = df_concat([table, __create_table(metrics, data.tag)])

    def create_dict(test_id: str, description: str):
        return {
            FIELD_NAME_TEST_ID: test_id,
            FIELD_NAME_DESCRIPTION: description,
            FIELD_NAME_MEASURED_VALUE: '',
            FIELD_NAME_CONDITION: '',
            FIELD_NAME_RESULT: ''
        }

    # Add headers to table
    table = df_concat([table, DataFrame([create_dict("9A", "Mapping")])], ignore_index=True)
    table = df_concat([table, DataFrame([create_dict("9B", "Localization")])], ignore_index=True)

    # Sort the table based on the id column
    table.sort_values(FIELD_NAME_TEST_ID, inplace=True)

    print('\n' + table.to_markdown(index=False) + '\n')


def create_plot(name: str, size: int = None, folder: str = None):
    if size is None:
        size = 450
    if folder is None:
        folder = "."
    return f'<img src="{folder}/plots/{name}.png" width="{size}">'


def print_plot(*args, **kwargs):
    print(create_plot(*args, **kwargs))


def print_table(rows: List[List[str]], header: List[str] = None):
    # Check that all rows have the same length
    n_columns = len(header) if header is not None else len(rows[0])
    assert all(len(row) == n_columns for row in rows), "Expecting all rows to have the same number of fields."
    if header is None:
        table = tabulate(rows, [""] * n_columns, tablefmt="pipe")
        # There is no header, but still the header-rows division is there, so remove that as well
        # table = table.partition('\n')[2]
    else:
        table = tabulate(rows, header, tablefmt="pipe")
    print('\n' + table + '\n')


@dataclass
class Run:
    name: str
    source: Path


def print_plots_table(plot_names: List[str], runs_list: List[Run], *,
                      top_row: List[str] = None, left_column: List[str] = None):
    """
    Function to create a table with the figures having the given names for the runs that are supplied.

    Optionally an alternative left column can be supplied, otherwise the run names will be used.
    Similarly, an optional top row can be supplied, if not, then no row is added.
    """
    # Being user-friendly
    if isinstance(runs_list, Run):
        runs_list = [runs_list]

    table_content = []
    if top_row is not None:
        assert len(top_row) == len(plot_names), "Custom top_row should have as many fields as the number of plots."
        table_content.append(["", *top_row])

    if left_column is None:
        left_column = [r.name for r in runs_list]

    table_content.extend([
        [lc,
         *[create_plot(plot_name, folder=run.name) for plot_name in plot_names]
        ]
        for lc, run in zip(left_column, runs_list)
    ])

    print_table(table_content)


def is_list_item(line: str) -> bool:
    return line.lstrip().startswith(('-', '*', '+'))
