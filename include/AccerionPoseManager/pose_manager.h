/* Copyright (c) 2022, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <mutex>

#include "pose_tree.h"


namespace accerion {

namespace pose_manager {


// There are two trees in the PoseManager:
//   - One robot description, provided by the user
//   - One mapping tree defined like global -> local -> common
// The connection between the two trees is the root node of the robot description tree.
// ALL POSES ARE GIVEN FROM POINT PERSPECTIVE. SEE https://accknowledgeportal.refined.site/space/AKB/681083087/Coordinate+transformations
template <typename PoseType, typename ContainerType>
class PoseManager_ {
public:
    using StaticPoseTree = PoseTree<PoseType>;
    using PoseTreeType = PoseTree_<PoseType, ContainerType>;
    using Id = typename PoseTreeType::Id;

    // Helper/convenience functions that will build the robot description pose tree as part of the creation of the pose
    // manager. The id_sensor input describes a 3rd sensor which does not provide neither local nor global positioning 
    // (i.e. during mapping a lidar could provide global positioning, wheel encoders the local positioning and Triton would be the 3rd sensor)
    static PoseManager_ Create(Id id_common,
                               Id id_sensor, PoseType p_common_sensor,
                               Id id_global_child, PoseType p_common_global,
                               Id id_local_child, PoseType p_common_local,
                               ContainerType default_container = {}) {
        StaticPoseTree robot_description;
        robot_description.Set(std::move(id_sensor), id_common, std::move(p_common_sensor));
        robot_description.Set(id_global_child, id_common, std::move(p_common_global));
        robot_description.Set(id_local_child, id_common, std::move(p_common_local));

        PoseManager_ pose_manager(std::move(robot_description),
                                  std::move(id_common),
                                  std::move(id_global_child),
                                  std::move(id_local_child),
                                  std::move(default_container));
        return pose_manager;
    }
    // This create function assumes that the external reference sensor provides poses that already incorporate SLAM.
    // Subsequently, only global positioning (including corrections) is provided by the external reference.
    static PoseManager_ CreateSlam(Id id_common,
                                   Id id_sensor, PoseType p_common_sensor,
                                   Id id_global_child, PoseType p_common_global,
                                   ContainerType default_container = {}) {
        return Create(std::move(id_common),
                      std::move(id_sensor), std::move(p_common_sensor),
                      std::move(id_global_child), std::move(p_common_global),
                      "local_dummy", PoseType::Identity(),
                      std::move(default_container));
    }

    // Create a PoseManager object, given the robot description, its root id and the local and global child id's.
    // Note that the root id will be the common id between the robot description and the map tree.
    // Id's with an underscore prefix are not allowed as they are reserved for internal use.
    PoseManager_(StaticPoseTree&& robot_description,
                 Id common, Id global_child, Id local_child,
                 ContainerType default_container = {})
            : common_id{std::move(common)},
              global_child_id{std::move(global_child)},
              local_child_id{std::move(local_child)},
              map_{std::move(default_container)},
              mutex_{},
              map_initialized_{false},
              robot_{std::move(robot_description)},
              p_common_global_child_{robot_.Get(common_id, global_child_id)},
              p_common_local_child_{robot_.Get(common_id, local_child_id)} {
        if (common_id.front() == '_') {
            throw std::runtime_error("Common id should not start with an underscore.");
        }
        if (global_child_id.front() == '_') {
            throw std::runtime_error("Global child id should not start with an underscore.");
        }
        if (local_child_id.front() == '_') {
            throw std::runtime_error("Local child id should not start with an underscore.");
        }
    }

    // For thread safety, the class uses a mutex, hence the move and other operators need to be explicitly implemented,
    // when required. Implementation after https://stackoverflow.com/a/29988626
    ~PoseManager_() = default;
    PoseManager_(const PoseManager_&) = delete;
    PoseManager_& operator=(const PoseManager_&) = delete;
    PoseManager_(PoseManager_&& other) noexcept
            : common_id{std::move(other.common_id)},
              global_child_id{std::move(other.global_child_id)},
              local_child_id{std::move(other.local_child_id)},
              map_{},
              mutex_{},
              map_initialized_{other.map_initialized_},
              robot_{std::move(other.robot_)},
              p_common_global_child_{std::move(other.p_common_global_child_)},
              p_common_local_child_{std::move(other.p_common_local_child_)} {
        std::lock_guard<std::mutex> other_lock(other.mutex_);
        map_ = std::move(other.map_);
    }
    PoseManager_& operator=(PoseManager_&&) = delete;

    // Incorporates the global pose that is given as the transformation that transforms point from the global child to
    // the global system. Internally it will update the global -> local transformation.
    template <typename... Args>
    void SetGlobal(const PoseType& p_global_child, Args&&... args) {
        std::lock_guard<std::mutex> guard(mutex_);

        EnsureInitializationMap(std::forward<Args>(args)...);

        // In the structure global -> local -> common -> ... -> global child the goals is to update global -> local with the
        // given transformation for global child -> global.
        const PoseType p_local_common = map_.Get(local_id, common_id, std::forward<Args>(args)...);
        PoseType p_local_global = p_local_common * p_common_global_child_ * p_global_child.Inverse();

        map_.Set(local_id, global_id, std::move(p_local_global), std::forward<Args>(args)...);
    }

    // Incorporates the local pose that is given as the transformation that transforms point from the local child to the
    // local system. Internally it will update the local -> common transformation.
    template <typename... Args>
    void SetLocal(const PoseType& p_local_child, Args&&... args) {
        std::lock_guard<std::mutex> guard(mutex_);

        EnsureInitializationMap(std::forward<Args>(args)...);

        // In the structure local -> common -> ... -> local child the goals is to update local -> common with the given
        // transformation for local child -> local.
        PoseType p_common_local = p_common_local_child_ * p_local_child.Inverse();
        map_.Set(common_id, local_id, std::move(p_common_local), std::forward<Args>(args)...);
    }

    template <typename... Args>
    PoseType Get(const Id& id_to, const Id& id_from, Args&&... args) const {
        std::lock_guard<std::mutex> guard(mutex_);

        if (id_to == id_from) {
            return PoseType::Identity();
        }

        EnsureInitializationMap(std::forward<Args>(args)...);

        // For each id it should be known in which pose tree it resides, because that will determine if the trees need to
        // be combined or querying one tree will be sufficient. Note that the common id is in both trees.
        const bool id_to_is_common_id = id_to == common_id;
        const bool id_to_in_map = id_to_is_common_id || map_.Contains(id_to);
        assert(id_to_in_map || robot_.Contains(id_to));
        const bool id_from_is_common_id = id_from == common_id;
        const bool id_from_in_map = id_from_is_common_id || map_.Contains(id_from);
        assert(id_from_in_map || robot_.Contains(id_from));

        if (id_to_in_map && id_from_in_map) {
            // Both id's are in map pose tree
            return map_.Get(id_to, id_from, std::forward<Args>(args)...);
        }
        if ((id_to_is_common_id || !id_to_in_map) && (id_from_is_common_id || !id_from_in_map)) {
            // Both id's are in the robot pose tree
            return robot_.Get(id_to, id_from);
        }

        // At this point it is known that the id's are in separate trees and both are not the common id
        assert(!id_to_is_common_id && !id_from_is_common_id);
        const PoseType p_to_common = (id_to_in_map ? map_.Get(id_to, common_id, std::forward<Args>(args)...)
                                                   : robot_.Get(id_to, common_id));
        const PoseType p_common_from = (id_from_in_map ? map_.Get(common_id, id_from, std::forward<Args>(args)...)
                                                       : robot_.Get(common_id, id_from));
        return p_to_common * p_common_from;
    }

    // Identifiers for the global and local frames. Used only for requesting a pose.
    static constexpr auto global_id = "_global";
    static constexpr auto local_id = "_local";

    // The common id between the robot and map tree
    const Id common_id;

    const Id global_child_id;
    const Id local_child_id;

private:
    // When this class is instantiated, there is no initialization of the map. Before the map is going to be used, then
    // it needs some sensible values. Entries that are not there, cannot be requested and will fail. Therefore, right
    // before the map is going to be used, ensure that there is something there.
    // Main reason is that sensible metadata is required for initialization and this information is only supplied when a
    // request is done. Therefore, only then the map can be filled.
    template <typename... Args>
    void EnsureInitializationMap(Args&&... args) const {
        if (!map_initialized_) {
            map_.Set(local_id, global_id, PoseType::Identity(), std::forward<Args>(args)...);
            map_.Set(common_id, local_id, PoseType::Identity(), std::forward<Args>(args)...);
            map_initialized_ = true;
        }
    }

    mutable PoseTreeType map_;  // mutable because initialization needs to be "right in time", just before it is accessed
    mutable std::mutex mutex_;
    mutable bool map_initialized_;
    const StaticPoseTree robot_;

    const PoseType p_common_global_child_;
    const PoseType p_common_local_child_;
};

// Convenience types
template <typename PoseType>
using PoseManager = PoseManager_<PoseType, PlainPoseContainer<PoseType>>;

template <typename PoseType, typename TimeType, typename TimeDiffConverter = TimeDiffCaster<TimeType>>
using PoseManagerBuffered = PoseManager_<PoseType, PoseBuffer_<PoseType, TimeType, TimeDiffConverter>>;

}  // namespace pose_manager

}  // namespace accerion
