/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionPgo/pose_graph_utils.h"


namespace accerion {
namespace pgo {

Eigen::Matrix3d CreateInformationMatrix(double standard_deviation_position, double standard_deviation_rotation) {
    const auto information_value_position = 1. / standard_deviation_position / standard_deviation_position;
    return Eigen::DiagonalMatrix<double, 3>{information_value_position,
                                            information_value_position,
                                            1. / standard_deviation_rotation / standard_deviation_rotation};
}

std::unordered_map<accerion::api::SignatureId, std::size_t> CreateNodeIdToIndexMap(
        const std::vector<accerion::pgo::PoseGraph::Node>& nodes) {
    std::unordered_map<accerion::api::SignatureId, std::size_t> result;
    for (std::size_t i = 0; i < nodes.size(); ++i) {
        result.emplace(nodes.at(i).id, i);
    }
    return result;
}

std::vector<accerion::pgo::PoseGraph::Edge> IdentifyLoopClosureEdges(
        const std::vector<accerion::pgo::PoseGraph::Edge>& edges) {
    std::vector<accerion::pgo::PoseGraph::Edge> loops{};
    for (const auto& edge: edges) {
        if (edge.type == accerion::pgo::PoseGraph::EdgeType::kLoopClosure ||
            (edge.type == accerion::pgo::PoseGraph::EdgeType::kUnknown &&
             std::llabs(static_cast<std::int64_t>(edge.id_a) - static_cast<std::int64_t>(edge.id_b)) > 1)) {
            loops.push_back(edge);
        }
    }
    return loops;
}

}  // namespace pgo
}  // namespace accerion
