#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from glob import glob
from os import mkdir
from pathlib import Path
from typing import List, Optional

from sdk.data.data_container import DataContainer
from sdk.metrics.metrics import Metrics
from sdk.metrics.reader import Reader as MetricsReader
from sdk.metrics.writer import Writer as MetricsWriter
from sdk.plotting.plot_all import plot_all
from sdk.utils.configuration import Configuration
from sdk.utils.path import ensure_directory


__all__ = ["METRICS_RESULTS_FILE_NAME", "get_metrics", "prepare_run"]


METRICS_RESULTS_FILE_NAME = "metrics_results.yml"


def get_metrics(folder: Optional[Path] = None) -> Metrics:
    file_path = METRICS_RESULTS_FILE_NAME if folder is None else folder / METRICS_RESULTS_FILE_NAME

    files = glob(file_path)
    assert len(files) == 1, f"Expecting exactly one metrics results file"
    return MetricsReader.read(files[0])


def prepare_run(input_files: List[Path],
                *,
                metrics: Metrics,
                output_folder: Optional[Path] = None,
                sub_folder: Optional[str] = None,
                master_conf: Optional[Configuration] = None,
                verbose: bool = False):
    assert isinstance(metrics, Metrics)

    if output_folder is None:
        output_folder = Path(".").resolve()
    if sub_folder is not None and len(sub_folder) != 0:
        output_folder /= sub_folder

    # Check the section output folder exists
    ensure_directory(output_folder)

    # To determine the configuration:
    #  - Take the one that is present in the output folder already, otherwise take the default
    #  - Update the configuration by overwriting with settings from the master configuration
    # Regardless, the actually used configuration is saved in the output folder.
    conf = Configuration.load_configuration(output_folder)
    conf.update_conf(master_conf)
    conf.save_configuration(output_folder)

    reuse = conf[Configuration.Name.REUSE_RESULTS]

    # Only read the data when the results can not be reused
    data = None

    # Calculate metrics
    output_metrics_file = output_folder / METRICS_RESULTS_FILE_NAME
    if not reuse or not output_metrics_file.exists():
        if data is None:
            data = DataContainer.create(input_files, conf)
        metrics.evaluate(data)
        MetricsWriter.write(metrics, output_metrics_file)

    # Generate plots
    plots_folder = output_folder / "plots"
    if not plots_folder.exists():
        mkdir(plots_folder)
    if not reuse or len(glob(str(plots_folder / "*"))) == 0:
        if data is None:
            data = DataContainer.create(input_files, conf)
        plot_all(data, plots_folder, verbose, conf)
