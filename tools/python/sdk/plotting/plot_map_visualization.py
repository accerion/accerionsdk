#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.data.signature import Signature
from sdk.plotting.utils.plotting import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_clusters(axis, signatures: List[Signature]):
    from matplotlib.colors import LinearSegmentedColormap

    if not signatures:
        return

    # Create a continuous colormap from Green to Yellow to Red
    color_map = LinearSegmentedColormap.from_list(COLOR_MAP_GYR.name, COLOR_MAP_GYR.colors)

    cluster_ids_distinct = list(sorted(set(s.cluster_id for s in signatures)))

    # Normalize cluster IDs to range [0, 1] for the colormap
    color_map_indices = np.linspace(0, 1, len(cluster_ids_distinct))

    for cluster_id, color_map_index in zip(cluster_ids_distinct, color_map_indices):
        x_pos = [s.pose.x for s in signatures if s.cluster_id == cluster_id]
        y_pos = [s.pose.y for s in signatures if s.cluster_id == cluster_id]

        # Get a continuous color from the colormap
        axis.scatter(x_pos, y_pos, color=color_map(color_map_index), marker="s", s=50,
                     label=f"cluster id {cluster_id}")

    return cluster_ids_distinct


def plot_map_visualization(data: DataContainer,
                           output_folder: Optional[Path] = None,
                           conf: Configuration = Configuration.default(),
                           *,
                           verbose: bool = True):
    if not data.signatures:
        print_v(verbose, __name__,
                ": No signature information was provided (no or empty floor map coordinates or map .db file)")
        return

    fig, axis = plt.subplots(nrows=1, ncols=1, figsize=[14, 6])
    fig.suptitle("Map visualization")

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")

    cluster_ids_distinct = plot_clusters(axis, data.signatures)

    axis.set_aspect('equal', adjustable='datalim')
    axis.legend(labels=[f"cluster id {cluster_id}" for cluster_id in cluster_ids_distinct], loc="upper right")

    axis.grid()
    save_or_show(fig, output_folder, "map_visualization.png", conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_map_visualization)
