#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from dataclasses import dataclass
import math
from typing import Optional

# Dependency checks
from sdk.utils.version import python_version, Version
assert python_version() >= Version("3.7"), "Compatibility constraint, use of library dataclasses"


__all__ = ['Pose']


@dataclass
class Pose:
    x: float = 0
    y: float = 0
    th: float = 0  # radians

    @property
    def norm_squared(self) -> float:
        return self.x ** 2 + self.y ** 2

    @property
    def norm(self) -> float:
        return math.sqrt(self.norm_squared)

    @staticmethod
    def parse(s: str) -> Pose:
        parts = s.split(',')
        assert len(parts) == 3, "Expecting exactly 3 comma-separated pose arguments"

        p = Pose()
        p.x = float(parts[0].strip())
        p.y = float(parts[1].strip())
        p.th = float(parts[2].strip())
        return p.normalized()

    def __mul__(self, o) -> Pose:
        assert isinstance(o, Pose), "Pose can only be multiplied with another Pose"

        cos_th = math.cos(self.th)
        sin_th = math.sin(self.th)
        return Pose(
            self.x + cos_th * o.x - sin_th * o.y,
            self.y + sin_th * o.x + cos_th * o.y,
            self.th + o.th
        ).normalized()

    def inverse(self) -> Pose:
        """
        For pose P with rotation R and translation t, the inverse P_i is given by
        P = [R|t] -> P_i = [R_t | - R_t t]
        where R_t is the rotation matrix transposed
        """
        cos_th = math.cos(self.th)
        sin_th = math.sin(self.th)
        return Pose(
            - ( cos_th * self.x + sin_th * self.y),
            - (-sin_th * self.x + cos_th * self.y),
            - self.th
        )

    def is_approx(self, other, *, rel_tol: Optional[float] = None, abs_tol: Optional[float] = None) -> bool:
        # Construct to not having to copy the default values and directly rely on the ones from math.isclose itself
        args = dict()
        if rel_tol:
            args["rel_tol"] = rel_tol
        if abs_tol:
            args["abs_tol"] = abs_tol

        return all([math.isclose(self.x, other.x, **args),
                    math.isclose(self.y, other.y, **args),
                    math.isclose(self.th, other.th, **args)])

    def normalized(self) -> Pose:
        """
        Return a copy Pose with the angle value normalized to be in the interval -pi and pi
        """
        return Pose(self.x, self.y, normalize_angle_rad(self.th))

    def interpolate(self, after: Pose, ratio: float) -> Pose:
        """
        Interpolate between this pose and another pose based on the given ratio.
        :param self: The before Pose to interpolate with.
        :param after: The after Pose to interpolate with.
        :param ratio: Interpolation factor (0 returns self, 1 returns after).
        :return: Interpolated Pose.
        """
        assert 0.0 <= ratio <= 1.0, "Ratio must be between 0.0 and 1.0"

        # Early out when interpolation is not needed
        if ratio <= 0.0:
            return self
        elif ratio >= 1.0:
            return after

        # Perform interpolation
        return Pose(
            x=self.x + ratio * (after.x - self.x),
            y=self.y + ratio * (after.y - self.y),
            th=self.th + ratio * normalize_angle_rad(after.th - self.th)).normalized()

def normalize_angle_rad(th):
    """
    Normalize angle to be in the interval -pi and pi
    """
    return (th + math.pi) % (2 * math.pi) - math.pi