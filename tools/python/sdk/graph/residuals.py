#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import Dict, List, Tuple

from sdk.graph.graph import Graph, Pose
from sdk.graph.utils import create_vertex_id_dict


def calc_edge_residual_pose(pose_begin: Pose, pose_end: Pose, edge: Pose) -> Pose:
    # Calculate `end` pose based on the edge, by applying it to the `begin` pose
    pose_end_edge = pose_begin * edge
    # Calculate the different between the given `end` pose and the one based on the edge
    return Pose(x=pose_end.x - pose_end_edge.x,
                y=pose_end.y - pose_end_edge.y,
                th=pose_end.th - pose_end_edge.th).normalized()


def get_edge_residuals(graph: Graph, edges: List[Graph.Edge]) -> Tuple[List[int], List[Pose]]:
    """
    Given a pose graph, the residual error pose is calculated for the edges supplied

    The error pose is the absolute difference of the end vertex pose from applying the edge to the begin vertex and the
    actual end vertex pose.

    :returns: index of the end vertex and the residual error for every edge
    """

    # For proper and fast look up of vertices
    vertex_id_to_index = create_vertex_id_dict(graph)

    # Containers with results
    end_vertex_indices = []
    residuals = []

    for edge in edges:
        vertex_begin_index = vertex_id_to_index[edge.id_begin]
        vertex_end_index = vertex_id_to_index[edge.id_end]

        pose_begin = graph.vertices[vertex_begin_index].pose
        pose_end = graph.vertices[vertex_end_index].pose

        residual = calc_edge_residual_pose(pose_begin, pose_end, edge.pose)

        end_vertex_indices.append(vertex_end_index)
        residuals.append(residual)

    return end_vertex_indices, residuals


def get_edge_residuals_grouped(graph: Graph, edge_indices: List[int]) -> Dict[int, Tuple[List[int], List[Pose]]]:
    """
    Given a pose graph, the residual error pose is calculated for the edges identified by the supplied edge indices,
    grouped on end vertex. (It performs get_edge_residuals and groups the result, based on the end vertex index.)

    :returns: A mapping relating the end vertex pose index to the linked edge indices and residual errors
    """

    end_vertex_indices, residuals = get_edge_residuals(graph, [graph.edges[i] for i in edge_indices])

    result = {}
    for end_vertex_id, edge_index, residual in zip(end_vertex_indices, edge_indices, residuals):
        if end_vertex_id not in result:
            result[end_vertex_id] = ([], [])
        result[end_vertex_id][0].append(edge_index)
        result[end_vertex_id][1].append(residual)
    return result
