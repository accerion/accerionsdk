#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.graph.g2o_reader import G2oTags
from sdk.graph.graph import Graph
from sdk.utils.path import ensure_parent_directory


class Writer:
    @staticmethod
    def write(graph: Graph, file_name: str):
        Writer(graph, file_name)

    def __init__(self, graph: Graph, file_name: str):
        ensure_parent_directory(file_name)
        self.output_file = open(file_name, 'w')
        self.__write_file(graph)

    def __del__(self):
        self.output_file.close()

    def __write_file(self, graph: Graph):
        [self.__write_fix(f) for f in graph.fixes]
        [self.__write_vertex(v) for v in graph.vertices]
        [self.__write_edge(e) for e in graph.edges]

    def __write_vertex(self, vertex):
        line = f"{G2oTags.VERTEX.value} {vertex.id} {Writer.__print_pose(vertex.pose)}"
        if vertex.type != Graph.Vertex.Type.Unknown:
            line += f" # {vertex.type.value}"
        self.output_file.write(line + '\n')

    def __write_edge(self, edge):
        line = f"{G2oTags.EDGE.value} {edge.id_begin} {edge.id_end} " \
               f"{Writer.__print_pose(edge.pose)} {Writer.__print_inf(edge.inf)}"
        if edge.type != Graph.Edge.Type.Unknown:
            line += f" # {edge.type.value}"
        self.output_file.write(line + '\n')

    def __write_fix(self, fix):
        self.output_file.write(
            f"{G2oTags.FIX.value} {fix.id}\n")

    @staticmethod
    def __print_pose(pose):
        return f"{pose.x} {pose.y} {pose.th}"

    @staticmethod
    def __print_inf(inf):
        return f"{inf.x_x} {inf.x_y} {inf.x_th} {inf.y_y} {inf.y_th} {inf.th_th}"
