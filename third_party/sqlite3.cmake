message(STATUS "Preparing third-party library 'sqlite3'")

# Avoid warning about DOWNLOAD_EXTRACT_TIMESTAMP by FetchContent_Declare
if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.24")
    cmake_policy(SET CMP0135 NEW)
endif()

include(FetchContent)
FetchContent_Declare(
    sqlite3
    URL "https://www.sqlite.org/2022/sqlite-amalgamation-3380100.zip"
)

# Check 'Populated' variable to prevent creating duplicated target libraries
if (NOT sqlite3_POPULATED)
    FetchContent_MakeAvailable(sqlite3)

    # Create target to make 'sqlite3' available as library
    add_library(sqlite3 "${sqlite3_SOURCE_DIR}/sqlite3.c")
    target_link_libraries(sqlite3 PRIVATE ${CMAKE_DL_LIBS})
    if (UNIX)
        find_package(Threads REQUIRED)
        target_link_libraries(sqlite3 PRIVATE ${CMAKE_THREAD_LIBS_INIT})
    endif()
    # Make include file available through 'sqlite3' target
    target_include_directories(sqlite3 PUBLIC "${sqlite3_SOURCE_DIR}")
endif()
