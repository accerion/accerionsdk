#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.plotting.utils.plotting import *
from sdk.processing.correction_count import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_correction_count_heatmap(data: DataContainer,
                                  output_folder: Optional[Path] = None,
                                  conf: Configuration = Configuration.default(),
                                  *,
                                  verbose: bool = True):
    pos_x, pos_y, dc_counts, velocity_bounds = get_correction_count(data.arams_original, conf, verbose=verbose)
    if not dc_counts:
        print_v(verbose, "No drift correction per cluster crossing.")
        return

    # Create plot
    fig, axis = plt.subplots(1, 1)

    if conf[Configuration.Name.OVERLAY_MAP]:
        # adding alpha channel to prevent the heatmap from being lost under the scatter map
        add_signature_poses(axis, data.signatures, (.5, .5, .7, .15))

    array = create_binned_data(pos_x, pos_y, dc_counts, aggregation_method=min)
    heatmap_handle = add_heat_map(axis, pos_x, pos_y, array, reverse=True, min_colorbar=1)
    data_cbar_handle = fig.colorbar(heatmap_handle, ax=axis)

    max_count = max(dc_counts)
    tick_step = 1
    if max_count > 20:
        tick_step = ceil(max_count / 20)
    ticks = list(reversed(range(max_count, 0, -tick_step)))
    data_cbar_handle.set_ticks(ticks=ticks, labels=[f"{t:d}" for t in ticks])

    axis.set_xlabel("x-coordinate [m]")
    axis.set_ylabel("y-coordinate [m]")
    fig.suptitle("Minimum number of drift corrections per cluster crossing", fontweight='bold')
    axis.set_title(f'Cluster crossings: {len(dc_counts)}, velocity interval: ({velocity_bounds[0]:.3f},{velocity_bounds[1]:.3f})')
    set_xylim(axis, conf, [min(pos_x), max(pos_x)], [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "correction_count_heatmap.png", conf.get_prefix(data.arams_original.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_correction_count_heatmap)
