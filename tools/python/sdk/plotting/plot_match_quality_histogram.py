#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.plotting.utils.plotting import *
from sdk.processing.match_quality import get_match_quality
from sdk.utils.configuration import Configuration


def plot_match_quality_histogram(data: DataContainer,
                                 output_folder: Optional[Path] = None,
                                 conf: Configuration = Configuration.default(),
                                 *,
                                 verbose: bool = True):
    _, _, match_quality = get_match_quality(data.arams, verbose=verbose)
    if not match_quality:
        return

    corrections_total = len(match_quality)
    match_quality_average = sum(match_quality) / corrections_total

    # Bin ranges; e.g. first bin range of [0, 10) includes values of 0 but excludes values of 10
    n_bins = 10
    max_match_quality = 100
    bins = [x * int(max_match_quality) /
                  n_bins for x in range(n_bins + 1)]

    fig = plt.figure()
    axis = fig.subplots(1, 1)

    axis.set_title(f'Total detected signatures: {corrections_total}')

    hist_counts, _, _ = axis.hist(
        match_quality, bins=bins, histtype='bar', rwidth=0.75, log=True)

    # Annotate percentages to each bin
    for i, n_corrections in enumerate(hist_counts):
        annotation = f"{n_corrections / corrections_total * 100:.2f}%"
        bin_center = (bins[i] + bins[i + 1]) / 2
        axis.text(bin_center, n_corrections, annotation,
                  horizontalalignment='center',
                  verticalalignment='baseline',
                  fontweight='bold')

    # Label x axis
    x_labels = [str(round(v)) for v in bins]
    axis.set_xticks(bins)
    axis.set_xticklabels(x_labels)
    axis.set_xlabel('Match quality [%]')

    # Remove y ticks
    axis.set_yticks([])
    axis.set_ylabel("Frequency (log scale)")

    # Visualize mean value
    min_ylim, max_ylim = axis.get_ylim()
    axis.axvline(match_quality_average, color=Colors.BLACK.value,
                 linestyle='dashed', linewidth=1, label="Mean")
    axis.text(match_quality_average * 1.01, max_ylim * 0.95,
              f'Mean [%]: {match_quality_average:.2f}')
    axis.set_ylim((min_ylim, max_ylim * 1.2))

    save_or_show(fig, output_folder, "match_quality_histogram.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_match_quality_histogram)
