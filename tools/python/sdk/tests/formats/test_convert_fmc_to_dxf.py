#!/usr/bin/env python3
"""
 Copyright (c) 2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.data.signature_reader import SignatureReader
from sdk.formats.convert_fmc_to_dxf import convert_fmc_to_dxf
from sdk.tests.test_data.files import TestData


def test_convert_fmc_to_dxf(tmpdir):
    input_file = TestData.get(TestData.File.FLOOR_MAP_COORDINATES_V700)
    signatures = SignatureReader.read([input_file])
    assert signatures, "No signature information is provided"

    convert_fmc_to_dxf(signatures, tmpdir / "test.dxf")

    # The dxf files have metadata including the time of writing the file. (This could not be disabled easily)
    # Moreover, some floating point values differ between multiple conversions.
    # Hence, at this point, this test does not actually test anything else than a crash
