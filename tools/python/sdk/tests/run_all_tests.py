#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import os
import sys
from argparse import ArgumentParser
from pathlib import Path
from typing import List, Optional

import pytest

from sdk import ROOT_PATH
from sdk.tests.test_utils.environment_variables import PYTHON_PATH


def run_all_tests(python_folder: str,
                  results_file: Optional[str] = None,
                  remainder_args = Optional[List[str]]) -> pytest.ExitCode:
    """
    Wrapper around the standard pytest executable. The default directory for pytest is the current one. It is possible
    to supply the python directory to search that for tests, but then still the working directory is not correct, such
    that test data cannot be found. Also, the results file (in case of MR builds) can be located somewhere else.
    """
    args = [python_folder]
    if results_file is not None:
        args.append("--junitxml")
        args.append(str(Path(results_file).absolute()))
    if remainder_args:
        args.extend(remainder_args)

    return pytest.main(args)


def main():
    parser = ArgumentParser()
    parser.add_argument("-r", "--result", required=False, help="Output test results xml file")
    known_args, unknown_args = parser.parse_known_args()
    args = vars(known_args)

    # PyTest completely depends on the PYTHON_PATH variable to be set. Therefore, it will be set here, regardless

    if PYTHON_PATH in os.environ:
        # Update environment variable to make all paths absolute.
        python_paths = os.environ[PYTHON_PATH].split(os.pathsep)
        python_paths = [str(Path(p).absolute()) for p in python_paths]
        os.environ[PYTHON_PATH] = os.pathsep.join(python_paths)

        python_folders = [p for p in python_paths if p.endswith("python")]
        assert len(python_folders) >= 1, f"Expecting at least one 'python' folder in {PYTHON_PATH}"
        python_folder = python_folders[0]
    else:
        python_folder = ROOT_PATH.parent
    os.environ[PYTHON_PATH] = python_folder

    exit_code = run_all_tests(python_folder, args["result"], unknown_args)
    sys.exit(exit_code.value)


if __name__ == "__main__":
    main()
