/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionPoseManager/pose_manager.h"
#include "AccerionSensorAPI/structs/pose.h"

#include "pose_utils.h"

// Somehow, somewhere <windows.h> is included. Problem is that min and max macros are defined in there that interfere
// with the numeric_limits min and max functions, at least under Windows. Hence, the undef statements here.
#if _WIN32
    #undef max
    #undef min
#endif


using namespace accerion;

template <typename T>
api::Pose CreatePose(T value) {
    auto double_value = static_cast<double>(value);
    return {double_value, double_value, double_value};
}

template <typename T>
class PoseBufferTest : public testing::Test {
protected:
    using Time = T;
    using PoseBuffer = pose_manager::PoseBuffer_<api::Pose, Time>;
};

using PoseBufferTestTypes = ::testing::Types<double, uint64_t, int>;
TYPED_TEST_SUITE(PoseBufferTest, PoseBufferTestTypes);

TYPED_TEST(PoseBufferTest, EmptyBuffer) {
    const typename TestFixture::PoseBuffer buffer{10};
    const typename TestFixture::Time t{1};

    ASSERT_EQ(buffer.Get(t), api::Pose::Identity());
}

TYPED_TEST(PoseBufferTest, Overwritability) {
    typename TestFixture::PoseBuffer buffer{10};
    const typename TestFixture::Time t{1};

    const auto p1 = CreatePose(0);
    buffer.Set(p1, t);
    ASSERT_PRED2(PosesAreApproxEqual, buffer.Get(t), p1);

    const auto p2 = CreatePose(10);
    buffer.Set(p2, t);
    ASSERT_PRED2(PosesAreApproxEqual, buffer.Get(t), p2);
}

TYPED_TEST(PoseBufferTest, BasicFunctionality) {
    std::vector<api::Pose> poses;
    typename TestFixture::PoseBuffer buffer{100};
    for (auto t: {10, 20, 30}) {
        poses.push_back(CreatePose(t));
        buffer.Set(poses.back(), t);
    }

    struct InterpolationTest{
        typename TestFixture::Time time;
        api::Pose expectation;
    };

    const std::vector<InterpolationTest> tests {
            {0, poses[1-1]},
            {10, poses[1-1]},
            {20, poses[2-1]},
            {30, poses[3-1]},
            {40, poses[3-1]},
            {15, {15, 15, 15}},
            {21, {21, 21, 21}}
    };

    for (const auto& t: tests) {
        const api::Pose result = buffer.Get(t.time);
        ASSERT_PRED2(PosesAreApproxEqual, result, t.expectation);
    }
}

TEST(PoseBufferTest, BasicFunctionalityWithChrono) {
    using Duration = std::chrono::duration<double, std::milli>;
    using Time = std::chrono::time_point<std::chrono::steady_clock, Duration>;
    using PoseBuffer = pose_manager::PoseBuffer_<api::Pose, Time>;

    std::vector<api::Pose> poses;
    PoseBuffer buffer{Duration{100}};
    for (auto d: {10, 20, 30}) {
        auto t = Time{Duration{d}};
        poses.push_back(CreatePose(d));
        buffer.Set(poses.back(), t);
    }

    struct InterpolationTest{
        Time time;
        api::Pose expectation;
    };

    const std::vector<InterpolationTest> tests {
            {Time{Duration{ 0}}, poses[1-1]},
            {Time{Duration{10}}, poses[1-1]},
            {Time{Duration{20}}, poses[2-1]},
            {Time{Duration{30}}, poses[3-1]},
            {Time{Duration{40}}, poses[3-1]},
            {Time{Duration{15}}, {15, 15, 15}},
            {Time{Duration{21}}, {21, 21, 21}}
    };

    for (const auto& t: tests) {
        const api::Pose result = buffer.Get(t.time);
        ASSERT_PRED2(PosesAreApproxEqual, result, t.expectation);
    }
}

TEST(PoseBufferTest, BasicFunctionalityWithCustomType) {
    // This is the minimal interface to be implemented for a custom time type
    class CustomTime {
    public:
        CustomTime() : value_{} {}
        explicit CustomTime(double value) : value_{value} {}

        bool operator==(const CustomTime& other) const { return value_ == other.value_; }

        bool operator<(const CustomTime& other) const { return value_ < other.value_; }
        bool operator<=(const CustomTime& other) const { return *this == other || *this < other; }

        bool operator>(const CustomTime& other) const { return value_ > other.value_; }
        bool operator>=(const CustomTime& other) const { return *this == other || *this > other; }

        CustomTime operator+(const CustomTime& other) const { return CustomTime{value_ + other.value_}; }
        CustomTime operator-(const CustomTime& other) const { return CustomTime{value_ - other.value_}; }

        double Get() const { return value_; }

    private:
        double value_;
    };

    // If the difference between CustomTime instances would result in a std::chrono::duration or plain type, then the
    // following is not needed
    struct TimeDiffCaster {
        static double Cast(const CustomTime& duration) {
            return duration.Get();
        }
    };

    using PoseBuffer = pose_manager::PoseBuffer_<api::Pose, CustomTime, TimeDiffCaster>;

    std::vector<api::Pose> poses;
    PoseBuffer buffer{CustomTime{100.}};
    for (auto d: {10., 20., 30.}) {
        auto t = CustomTime{d};
        poses.push_back(CreatePose(d));
        buffer.Set(poses.back(), t);
    }

    struct InterpolationTest{
        CustomTime time;
        api::Pose expectation;
    };

    const std::vector<InterpolationTest> tests {
            {CustomTime{ 0}, poses[1-1]},
            {CustomTime{10}, poses[1-1]},
            {CustomTime{20}, poses[2-1]},
            {CustomTime{30}, poses[3-1]},
            {CustomTime{40}, poses[3-1]},
            {CustomTime{15}, {15, 15, 15}},
            {CustomTime{21}, {21, 21, 21}}
    };

    for (const auto& t: tests) {
        const api::Pose result = buffer.Get(t.time);
        ASSERT_PRED2(PosesAreApproxEqual, result, t.expectation);
    }
}

TYPED_TEST(PoseBufferTest, LimitedSize) {
    using Time = typename TestFixture::Time;
    using PoseBuffer = typename TestFixture::PoseBuffer;

    const typename PoseBuffer::TimeDiff buffer_size = 10;
    PoseBuffer buffer{buffer_size};
    const Time start{2};
    for (auto i = static_cast<std::size_t>(start); i < 25; ++i) {
        const auto t = static_cast<Time>(i);
        const auto d = static_cast<double>(i);

        buffer.Set({d, d, d}, t);

        const Time t_begin = t < buffer_size ? start : std::max(t - buffer_size, start);
        const auto d_begin = static_cast<double>(t_begin);
        ASSERT_EQ(buffer.Get(t_begin - 1).x, d_begin);  // before begin
        ASSERT_EQ(buffer.Get(t_begin).x, d_begin);  // begin
        ASSERT_EQ(buffer.Get(t).x, d);  // end
        ASSERT_EQ(buffer.Get(t + 1).x, d);  // after end
    }
}

TYPED_TEST(PoseBufferTest, InsertBeforeBuffer) {
    using PoseBuffer = typename TestFixture::PoseBuffer;

    const typename PoseBuffer::TimeDiff buffer_size = 3.;
    PoseBuffer buffer{buffer_size};

    ASSERT_TRUE(buffer.Set(CreatePose(2), 1));
    ASSERT_TRUE(buffer.Set(CreatePose(0), 0));
    ASSERT_TRUE(buffer.Set(CreatePose(4), 4));
    ASSERT_FALSE(buffer.Set(CreatePose(0), 0));
    ASSERT_TRUE(buffer.Set(CreatePose(6), 6));
}

// For CheckForPrecisionError test values are taken from an actual crash due to integer overflow
TEST(PoseBufferTest, CheckForPrecisionError) {
    using Time = std::uint64_t;

    const Time base = 18446744072294180000u;
    const Time current_time = base + 3994u;
    std::vector<Time> times {base + 3783u, base + 4009u};
    pose_manager::PoseBuffer_<api::Pose, Time> buffer{};

    for (std::size_t i = 0; i < times.size(); ++i) {
        buffer.Set(CreatePose(i), times[i]);
    }

    auto pose = buffer.Get(current_time);
    auto ratio = (pose.x - buffer.Get(times.front()).x) / (buffer.Get(times.back()).x - buffer.Get(times.front()).x);
    Time interpolated_timestamp = times.front() + static_cast<Time>(ratio * (times.back() - times.front()));
    EXPECT_NEAR(current_time, interpolated_timestamp, 1);
}
