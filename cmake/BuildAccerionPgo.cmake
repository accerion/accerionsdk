project(AccerionPgo LANGUAGES CXX)

include(${CMAKE_CURRENT_SOURCE_DIR}/third_party/eigen.cmake)

add_library(${PROJECT_NAME}
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionPgo/g2o_format.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionPgo/pose_graph_utils.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionPgo/pose_graph.cpp)

target_link_libraries(${PROJECT_NAME} AccerionSensorAPI Eigen3::Eigen)

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

add_compiler_warnings(${PROJECT_NAME})
