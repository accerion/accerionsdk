# Get all published pose data
This app showcases the pose data that is published by the sensor. Note that internal mode is always on in this
example to not require an external reference sensor.

The app should be called with the ip address of the sensor:
```
./getPoses <ip address>
```

For example, the full command on the terminal could look like this:
```
user@pc:~/accerionsensorapi/build/bin$ ./getPoses 192.168.178.100
```
