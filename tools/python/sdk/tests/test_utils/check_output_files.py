#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import os
from pathlib import Path
from typing import List, Union

from sdk.tests.test_utils.change_dir import ChangeDir


class CheckOutputFiles(ChangeDir):
    """
    Context manager to access the test directory.
    """
    def __init__(self, test_dir: [str, Path]):
        super().__init__(test_dir)
        self.__files: List[Path] = []

    def __enter__(self):
        super().__enter__()
        self.__files = self.__explore_folder()
        return self

    @staticmethod
    def __explore_folder() -> List[Path]:
        all_files = []
        for root, folders, files in os.walk("."):
            root = Path(root)
            all_files += [root / f for f in files]
        return all_files

    def __exit__(self, exc_type, exc, exc_tb):
        super().__exit__(exc_type, exc, exc_tb)

    def __contains__(self, path: Union[str, Path]) -> bool:
        if isinstance(path, str):
            path = Path(path)
        return path in self.__files

    def match(self, pattern: str) -> List[Path]:
        return [f for f in self.__files if f.match(pattern)]
