#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from unittest.mock import patch

from sdk.data.data_container import DataContainer
from sdk.plotting.plot_all import plot_all
from sdk.tests.test_data.files import TestData
from sdk.utils.configuration import Configuration


def test_plot_all():
    files_to_test = ([TestData.File.ARAMS_USER_LOGS_V61, TestData.File.FLOOR_MAP_COORDINATES_V61],
                     [TestData.File.ARAMS_USER_LOGS_V612, TestData.File.FLOOR_MAP_COORDINATES_V61],
                     [TestData.File.ARAMS_USER_LOGS_V615, TestData.File.FLOOR_MAP_COORDINATES_V61],
                     [TestData.File.ARAMS_USER_LOGS_V700, TestData.File.FLOOR_MAP_COORDINATES_V700])

    conf = Configuration.load_configuration()
    for file_list in files_to_test:
        print(f"Plot all input files: {file_list}")
        data = DataContainer.create(TestData.get_list(file_list), conf)
        # To prevent plot visualization, mock the 'plot' function from the 'matplotlib.pyplot' module
        with patch("matplotlib.pyplot.show"):
            plot_all(data, conf=conf)
