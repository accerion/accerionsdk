#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from argparse import ArgumentParser

from sdk.data.pose import Pose
from sdk.graph.graph import Graph
from sdk.graph.g2o_reader import Reader as G2oReader
from sdk.graph.g2o_writer import Writer as G2oWriter


def transform_graph(graph: Graph, transformation: Pose) -> Graph:
    for i, v in enumerate(graph.vertices):
        graph.vertices[i].pose = transformation * v.pose
    return graph


def __main(arguments):
    input_graph = G2oReader.read(arguments["input"])
    transformation = Pose.parse(arguments["transformation"])
    print(f"parsed transformation: {transformation}")
    output_file_name = arguments["output"]

    transformed_graph = transform_graph(input_graph, transformation)
    G2oWriter.write(transformed_graph, output_file_name)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, type=str, help="input .g2o file")
    parser.add_argument("-t", "--transformation", required=True, type=str, help='transformation pose as "x,y,th" with angle in radians, for example "2,-.2,1.57".')
    parser.add_argument("-o", "--output", required=True, type=str, help="output transformed .g2o file")
    args = vars(parser.parse_args())

    __main(args)
