#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import List, Tuple

from sdk.data.pose import Pose
from sdk.data.signature import Signature


def get_distance_between_signatures(signatures: List[Signature]) -> Tuple[List[float], List[float], List[float]]:
    sig_x, sig_y, dist = [], [], []
    for prev, curr in zip(signatures[:-1], signatures[1:]):
        if prev.cluster_id != curr.cluster_id:
            continue

        sig_x.append(curr.pose.x)
        sig_y.append(curr.pose.y)
        dist.append(Pose(x=curr.pose.x - prev.pose.x, y=curr.pose.y - prev.pose.y).norm)

    return sig_x, sig_y, dist
