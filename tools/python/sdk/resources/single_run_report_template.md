#### Overview
```python, echo = False, results = 'markdown'
from glob import glob

from sdk.metrics.metrics_data import MetricsData
from sdk.report.markdown_utils import *
from sdk.report.preparations import *
from sdk.utils.configuration import Configuration

conf = Configuration.load_configuration()
prefix = conf[Configuration.Name.PREFIX] + "_" if isinstance(conf[Configuration.Name.PREFIX], str) else ""
print_all = not conf[Configuration.Name.OMIT_PASSED_RESULTS]

metrics = get_metrics()

any_failed = metrics.any(Result.Fail)
if any_failed:
    print(f"There are tests that {add_html_color('failed', 'red')}!")
else:
    print(f"All tests {add_html_color('pass', 'green')}.")

print_metrics_overview_table(metrics)

if not print_all:
    if any_failed:
        print("Details will be given only for the tests that failed.")
else:
    print("Details will be given for all tests having data available.")
    
# helper function
def should_print(tag: MetricsData.Tag):
    any_failed = tag in metrics and metrics.any(Result.Fail, tag)
    data_available = any_failed or (tag in metrics and metrics.any(Result.Pass, tag))
    return data_available and (print_all or any_failed)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.LoopClosurePositionErrors
if should_print(tag):
    print("#### Pose graph position residuals")
    print_plot(prefix + "residual_position_errors_g2o_map")
    print_plot(prefix + "residual_position_errors_g2o_hist")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.LoopClosureHeadingErrors
if should_print(tag):
    print("#### Pose graph heading residuals")
    print_plot(prefix + "residual_heading_errors_g2o_map")
    print_plot(prefix + "residual_heading_errors_g2o_hist")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.DistSignatures
if should_print(tag):
    print("#### Distance between signatures")
    print_plot(prefix + "distance_between_signatures_map")
    print_plot(prefix + "distance_between_signatures_histogram")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.DistCorrections
if should_print(tag):
    print("#### Distance between corrections")
    print_plot(prefix + "distance_between_corrections_map")
    print_plot(prefix + "distance_between_corrections_histogram")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.PositionErrors
if should_print(tag):
    print("#### Position corrections")
    print_plot(prefix + "position_corrections_map")
    print_plot(prefix + "position_corrections_histogram")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.OrientationErrors
if should_print(tag):
    print("#### Orientation corrections")
    print_plot(prefix + "heading_corrections_map")
    print_plot(prefix + "heading_corrections_histogram")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.MatchQuality
if should_print(tag):
    print("#### Match quality")
    print_plot(prefix + "match_quality_map")
    print_plot(prefix + "match_quality_histogram")
    print_metrics_table(metrics, tag)
```

```python, echo = False, results = 'markdown'
tag = MetricsData.Tag.CorrectionCount
if should_print(tag):
    print("#### Correction count")
    print_plot(prefix + "correction_count_map")
    print_plot(prefix + "correction_count_histogram")
    print_metrics_table(metrics, tag)
```
