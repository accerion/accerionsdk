project(AccerionUtilities LANGUAGES CXX)

file(GLOB_RECURSE SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/source/${PROJECT_NAME}/*.cpp)

add_library(${PROJECT_NAME} "${SOURCES}")

target_link_libraries(${PROJECT_NAME} AccerionSensorAPI)

target_include_directories(${PROJECT_NAME} SYSTEM PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

add_compiler_warnings(${PROJECT_NAME})
