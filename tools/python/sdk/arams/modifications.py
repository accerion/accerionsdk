#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from math import degrees
import numpy as np

from sdk.arams.arams import Arams
from sdk.data.pose import normalize_angle_rad
from sdk.graph.graph import Pose
from sdk.processing.pose_time_series import PoseTimeSeries


class CorrectionErrorModification:
    """
    Class that modifies the drift correction error from the logDriftCorrections in an ARAMS class instance by using
    interpolated external reference poses instead.
    """

    @classmethod
    def modify_drift_correction_with_interpolation(cls, arams: Arams):
        """
        Calculates drift correction using interpolated external reference poses and change the ARAMS instance accordingly.

        If either 'LER' (Log External Reference) or 'LDC' (Log Drift Corrections) data is missing,
        this method logs a warning and does nothing.

        Parameters:
        - arams (Arams): An ARAMS instance containing the necessary data.

        Behavior:
        - If both 'LER' and 'LDC' data types are available, drift corrections are applied.
        - If any required data is missing, the method exits gracefully.
        """
        required_types = ["LER", "LDC"]
        missing_types = [t for t in required_types if not arams.has_type(t) or t not in arams]

        if missing_types:
            print(f"Warning: Missing required data types: {', '.join(missing_types)}. No corrections applied.")
            return

        # Call the private method to perform the actual modification
        cls._modify(arams)

    @staticmethod
    def _modify(arams: Arams):
        """
        Private method that performs the actual correction using interpolated poses.
        Assumes both 'LER' and 'LDC' data types are present in the ARAMS object.
        """
        LDC = arams.LDC
        df_ldc = arams[LDC]

        LER = arams.LER
        df_ler = arams[LER]


        ler_times = df_ler[LER.TIME].to_list()

        # Compose list with external reference poses, convert angles to radians (according to Pose definition)
        ler_poses = [Pose(x=x, y=y, th=th)
                     for x, y, th in zip(df_ler[LER.INPUT_POSE_X],
                                         df_ler[LER.INPUT_POSE_Y],
                                         np.deg2rad(df_ler[LER.INPUT_POSE_TH]))]

        input_pose_timeseries = PoseTimeSeries(ler_times, ler_poses)

        error_x = []
        error_y = []
        error_th = []

        ldc_headings_rad = np.deg2rad(df_ldc[LDC.NEW_THETA])

        for t, x_ldc, y_ldc, th_ldc in zip(df_ldc[LDC.TIME], df_ldc[LDC.NEW_XPOS].to_list(),
                                           df_ldc[LDC.NEW_YPOS].to_list(), ldc_headings_rad):
            input_pose = input_pose_timeseries[t]
            error_x.append(x_ldc - input_pose.x)
            error_y.append(y_ldc - input_pose.y)
            # Convert back to degrees as original arams object also uses degrees
            error_th.append(degrees(normalize_angle_rad(th_ldc - input_pose.th)))

        arams[LDC][LDC.ERROR_X] = error_x
        arams[LDC][LDC.ERROR_Y] = error_y
        arams[LDC][LDC.ERROR_THETA] = error_th