#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import Optional

from matplotlib.colors import LinearSegmentedColormap, to_rgb
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from sdk.plotting.utils.plotting import COLOR_MAP_GYR, Colors


def create_custom_cmap(max_allowed: Optional[int], max_measured: int):
    """
    Create a custom colormap based on maximum allowed and measured values.

    Parameters:
    - max_allowed (int): Maximum allowed value for colormap scaling.
    - max_measured (int): Maximum measured value for colormap scaling.

    Returns:
    - dict: A dictionary mapping values to colors.
    """

    # Define a default color map in case no upper bound is provided
    color_dict = {}
    max_cmap_value = None
    cmap = COLOR_MAP_GYR
    colors = [to_rgb(c) for c in cmap.colors]
    custom_cmap = LinearSegmentedColormap.from_list(cmap.name, colors)

    # Define the color and value pairs in case an upper bound is provided
    if max_allowed is not None:
        # Create color mapping based on computed values. Overwrite values >=1 if needed.
        color_dict = {
            0: Colors.DARK_GREEN,
            max(1, max_allowed): Colors.GREEN,
            max(1, round(1.2 * max_allowed)): Colors.LIGHT_GREEN,
            max(1, round(1.5 * max_allowed)): Colors.YELLOW,
            max(1, round(2 * max_allowed)): Colors.RED,
        }

        # Check if max_measured exceeds twice the max_allowed and add dark red if necessary
        if max_measured > 2 * max_allowed:
            color_dict[max_measured] = Colors.DARK_RED

        # Get the highest value in the color mapping
        max_cmap_value = max(color_dict.keys())

        # Extract values and colors from the dictionary
        tick_values = np.array(list(color_dict.keys()))
        colors = [to_rgb(color.value) for color in color_dict.values()]

        # Normalize tick_values to range [0, 1]
        norm_values = (tick_values - tick_values.min()) / (max_cmap_value - tick_values.min())

        # Create a custom LinearSegmentedColormap
        custom_cmap = LinearSegmentedColormap.from_list("Custom color map", list(zip(norm_values, colors)))

    return custom_cmap, max_cmap_value, color_dict


def plot_heatmap(density_dict, mesh, cmap, norm, ax):
    """Plot a density map as heatmap using imshow."""
    # Determine the grid size
    column_ids = [cell_id[0] for cell_id in density_dict.keys()]
    row_ids = [cell_id[1] for cell_id in density_dict.keys()]

    min_col = min(column_ids)
    max_col = max(column_ids)
    min_row = min(row_ids)
    max_row = max(row_ids)

    # Create a 2D array of densities
    grid_width = max_col - min_col + 1
    grid_height = max_row - min_row + 1
    density_grid = np.full((grid_height, grid_width), np.nan)

    for (col, row), density in density_dict.items():
        density_grid[row - min_row, col - min_col] = density

    # Display the density grid as an image
    extent = [mesh.min_cell_x + min_col * mesh.cell_size,
              mesh.min_cell_x + (max_col + 1) * mesh.cell_size,
              mesh.min_cell_y + min_row * mesh.cell_size,
              mesh.min_cell_y + (max_row + 1) * mesh.cell_size]

    ax.imshow(density_grid, cmap=cmap, norm=norm, extent=extent, origin='lower', alpha=0.5, zorder=2)

    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])  # Empty array to make the color bar show the full range
    plt.colorbar(sm, ax=ax, label='Number of signatures')


def plot_bounding_boxes(ax, bounding_boxes):
    for box in bounding_boxes:
        rect = patches.Rectangle((box.bounding_box.x_interval.min, box.bounding_box.y_interval.min),
                                 box.bounding_box.x_interval.length, box.bounding_box.y_interval.length,
                                 linewidth=2, linestyle='dashed', edgecolor='black', facecolor='none',
                                 label=f"{box.radius} m")
        ax.add_patch(rect)
