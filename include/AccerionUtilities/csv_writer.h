/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <mutex>
#include <numeric>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "AccerionUtilities/csv_stream.h"
#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/string.h"
namespace accerion {

namespace utilities {

template <typename... Types>
class CsvWriter {
 public:
    explicit CsvWriter(std::string file_location, std::vector<std::string>
                        headers, char delimiter = ',',
                        std::ios_base::openmode mode = std::ios::trunc):
                file_handle_(file_location, std::ios::out | mode),
                file_location_(file_location),
                mode_(std::ios::out | mode),
                stream_(file_handle_, delimiter),
                writable_headers_(accerion::api::Join(headers.begin(),
                                    headers.end(), std::string{delimiter})) {
        constexpr std::size_t n = sizeof...(Types);
        if (n != headers.size()) {
            std::stringstream ss;
            ss << "Mismatch between amount of columns and headers " <<
                                        "provided " << file_location;
            throw std::runtime_error(ss.str());
        }
        WriteHeaders();
    }

    ~CsvWriter() {
        std::scoped_lock<std::recursive_mutex> lk(mutex_);
        CloseFile();
    }

    void WriteLine(Types... nextColumns) const {
        std::scoped_lock<std::recursive_mutex> lk(mutex_);
        AssertOpen();
        stream_.Write<Types...>(nextColumns...);
    }

    void CloseFile() {
        std::scoped_lock<std::recursive_mutex> lk(mutex_);
        if (IsOpen()) {
            file_handle_.close();
        }
    }

    void ReopenFile() {
        std::scoped_lock<std::recursive_mutex> lk(mutex_);
        CloseFile();
        file_handle_.open(file_location_, mode_);
        WriteHeaders();
    }

 private:
    static_assert(sizeof...(Types) > 0, "Min. 1 type required in template");

    void AssertOpen() const {
        if (!IsOpen()) {
            throw std::runtime_error("File is not opened");
        }
    }

    bool IsOpen() const {
        return file_handle_.is_open();
    }

    void WriteHeaders() {
        AssertOpen();
        std::uint64_t file_size = accerion::api::GetFileSize(file_location_);
        if (file_size == 0) {
            stream_.Write(writable_headers_);
        }
    }

    std::ofstream file_handle_;
    std::string file_location_;
    std::ios_base::openmode mode_;
    CsvStream stream_;
    std::string writable_headers_;
    mutable std::recursive_mutex mutex_;
};

}  // namespace utilities

}  // namespace accerion
