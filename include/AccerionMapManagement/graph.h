/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once
#include <optional>
#include "AccerionDatabaseHandling/database.h"
#include "AccerionPgo/pose_graph.h"

namespace accerion {
namespace map {

void Import(accerion::sqlite::Database& db, const accerion::pgo::PoseGraph& graph,
                                const std::function<void(std::uint8_t)>& report_progress = {});

}  //  namespace map
}  //  namespace accerion