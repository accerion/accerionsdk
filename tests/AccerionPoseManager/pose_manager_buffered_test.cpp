/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionPoseManager/pose_manager.h"
#include "AccerionSensorAPI/structs/pose.h"

#include "pose_utils.h"


TEST(PoseManagerBuffered, CreateWithChrono) {
    // Simplified version of test "PoseManager.Create", to confirm that all works when using chrono types
    using Clock = std::chrono::steady_clock;
    using Duration = std::chrono::duration<double, std::milli>;
    using TimePoint = std::chrono::time_point<Clock, Duration>;
    using PoseManager = accerion::pose_manager::PoseManagerBuffered<accerion::api::Pose, TimePoint>;

    const PoseManager::Id global_child_id = "global_child";

    // Set up PoseManager using Create
    auto manager = PoseManager::Create("common",
                                       "dummy", accerion::api::Pose::Identity(),
                                       global_child_id, {-3, 0, 0},
                                       "local_child", {1, 0, 90});
    manager.SetLocal({0, -2, 0}, Clock::now());
    manager.SetGlobal({2, 0, 0}, Clock::now());

    // Perform tests
    const accerion::api::Pose p{2, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id, Clock::now()));
}


using Timestamp = std::uint8_t;
using PoseManager2d = accerion::pose_manager::PoseManagerBuffered<accerion::api::Pose, Timestamp>;
using StaticPoseTree2d = PoseManager2d::StaticPoseTree;
using PoseBuffer2d = PoseManager2d::PoseTreeType::Container;

TEST(PoseManagerBuffered, SetGetDifferentChild) {
    // Trees:          |------- map ------|- robot -|
    // Tree id's:    global -> local -> common -> local_child
    // Distances:           4        2         1
    // Tree id's:                       common -> global_child
    // Distances:                              3

    const PoseManager2d::Id common_id = "common";
    const PoseManager2d::Id local_child_id = "local_child";
    const PoseManager2d::Id global_child_id = "global_child";
    const double d_local_child{2 + 1};
    const double d_global_child{4 + 2 + 3};

    // First update local and then global
    //       |
    //   0.2 ┤           o-----------o
    //       │      l  /        ....
    //       │       /      ....
    //       │     /    ....  g
    //       │   /  ....
    //       │ /....
    //     d ┼─────┬─────┬─────┬─────┬─────┬---
    //       t     1     2     3     4     5
    //
    //   d = 0    0.1   0.2   0.2   0.2   0.2    local part
    //   d = 0    0.05  0.1   0.15  0.2   0.2    global part
    // Set values
    const Timestamp t{10};
    const Timestamp t_2{t + 2};   const double d_2{0.2};
    const Timestamp t_4{t + 4};   const double d_4{0.4};
    // Test values
    const Timestamp t_1{t + 1};   const double d_l_1{0.1};   const double d_g_1{d_l_1 + 0.05};
    const Timestamp t_3{t + 3};   const double d_l_3{0.2};   const double d_g_3{d_l_3 + 0.15};
    const Timestamp t_5{t + 5};   const double d_l_5{0.2};   const double d_g_5{d_l_5 + 0.2};

    // Set up PoseManager
    StaticPoseTree2d robot;
    robot.Set(local_child_id, common_id, {1, 0, 0});
    robot.Set(global_child_id, common_id, {3, 0, 0});
    PoseManager2d manager(std::move(robot), common_id, global_child_id, local_child_id, PoseBuffer2d{10});

    // Feed data to PoseManager
    manager.SetLocal({d_local_child, 0, 0}, t);
    manager.SetGlobal({d_global_child, 0, 0}, t);
    manager.SetLocal({d_local_child + d_2, 0, 0}, t_2);
    manager.SetGlobal({d_global_child + d_4, 0, 0}, t_4);

    // Perform tests
    accerion::api::Pose p = {d_local_child + d_l_1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id, t_1));
    p = {d_global_child + d_g_1, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id, t_1));

    p = {d_local_child + d_l_3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id, t_3));
    p = {d_global_child + d_g_3, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id, t_3));

    p = {d_local_child + d_l_5, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.local_id, local_child_id, t_5));
    p = {d_global_child + d_g_5, 0, 0};
    ASSERT_PRED2(PosesAreApproxEqual, p, manager.Get(manager.global_id, global_child_id, t_5));
}
