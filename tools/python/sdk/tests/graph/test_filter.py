#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.data.pose import Pose
from sdk.graph.filter import identify_edges
from sdk.graph.graph import Graph


def test_filter():
    graph = Graph()
    # Legacy
    graph.edges = [
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Unknown),
        Graph.Edge(id_begin=1, id_end=2, pose=Pose(), type=Graph.Edge.Type.Unknown),
        Graph.Edge(id_begin=0, id_end=2, pose=Pose(), type=Graph.Edge.Type.Unknown)
    ]

    assert identify_edges(graph, Graph.Edge.Type.Unknown) == [0, 1, 2]
    assert identify_edges(graph, Graph.Edge.Type.Relative) == [0, 1]
    assert identify_edges(graph, Graph.Edge.Type.Absolute) == []
    assert identify_edges(graph, Graph.Edge.Type.Identity) == []
    assert identify_edges(graph, Graph.Edge.Type.Loop) == [2]

    # Mixed
    graph.edges[-1].type = Graph.Edge.Type.Loop

    assert identify_edges(graph, Graph.Edge.Type.Unknown) == [0, 1]
    assert identify_edges(graph, Graph.Edge.Type.Relative) == []
    assert identify_edges(graph, Graph.Edge.Type.Absolute) == []
    assert identify_edges(graph, Graph.Edge.Type.Identity) == []
    assert identify_edges(graph, Graph.Edge.Type.Loop) == [2]

    # Default
    graph.edges = [
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Relative),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Relative),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Absolute),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Absolute),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Identity),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Identity),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Loop),
        Graph.Edge(id_begin=0, id_end=1, pose=Pose(), type=Graph.Edge.Type.Loop)
    ]

    assert identify_edges(graph, Graph.Edge.Type.Unknown) == []
    assert identify_edges(graph, Graph.Edge.Type.Relative) == [0, 1]
    assert identify_edges(graph, Graph.Edge.Type.Absolute) == [2, 3]
    assert identify_edges(graph, Graph.Edge.Type.Identity) == [4, 5]
    assert identify_edges(graph, Graph.Edge.Type.Loop) == [6, 7]
