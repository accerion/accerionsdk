/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/utils/optional.h"

#include "pose_graph.h"


namespace accerion {

namespace pgo {

[[nodiscard]] accerion::api::Optional<PoseGraph> ReadG2oFile(const std::string& input_file);

void WriteG2oFile(const PoseGraph& pose_graph, const std::string& output_file);

}  // namespace pgo

}  // namespace accerion
