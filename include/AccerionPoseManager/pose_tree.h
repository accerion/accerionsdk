/* Copyright (c) 2022-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <algorithm>
#include <cassert>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>

#include "pose_buffer.h"

namespace accerion {

namespace pose_manager {

template <typename PoseType, typename ContainerType>
class PoseTree_ {
public:
    // The type Id that is used for addressing certain poses, cannot be empty.
    using Id = std::string;
    using Container = ContainerType;

    PoseTree_() : default_container_{}, tree_{} {}
    explicit PoseTree_(ContainerType default_container)
            : default_container_{ std::move(default_container) }, tree_{} {}

    // It is assumed that transformations are given as if points are transformed from system "id_from" to system "id_to"
    template <typename... Args>
    void Set(Id id_to, Id id_from, PoseType pose_to_from, Args&&... args) {
        CheckValid(id_from);
        CheckValid(id_to);

        if (id_from == id_to) {
            std::stringstream ss;
            ss << "Ids must be different, got '" << id_from << "' twice.";
            throw std::runtime_error(ss.str());
        }

        // Check if intended change respects all tree properties
        if (!tree_.empty()) {
            if (!Contains(id_to) && !Contains(id_from)) {
                // Both nodes are not present in the tree
                std::stringstream ss;
                ss << "Provided ids '" << id_from << "' and '" << id_to << "' would create an isolated tree.";
                throw std::runtime_error(ss.str());
            } else if (Contains(id_to) && Contains(id_from)) {
                // Both nodes are present in the tree
                auto const result = IsDirectLinkReversed(id_to, id_from);
                bool const direct_link = result.first;
                bool const reversed = result.second;
                if (!direct_link) {
                    std::stringstream ss;
                    ss << "Provided ids '" << id_from << "' and '" << id_to << "' would create cyclic branches.";
                    throw std::runtime_error(ss.str());
                }
                if (reversed) {
                    std::swap(id_to, id_from);
                    pose_to_from = pose_to_from.Inverse();
                }
            } else {
                // Only one node is present in the tree, only allowed when
                //   - the id_from is in the tree, or
                //   - the id_to is in the from ids, and the id_to is not in the to ids
                if (!Contains(id_from) && !(FindIdFrom(id_to) != tree_.end() && FindIdTo(id_to) == tree_.end())) {
                    std::stringstream ss;
                    ss << "Provided ids '" << id_from << "' and '" << id_to << "' would create additional root node.";
                    throw std::runtime_error(ss.str());
                }
            }
        }

        // The poses will be stored like id_from -> id_to -> pose
        auto tree_iter = tree_.find(id_from);
        if (tree_iter == tree_.end()) {
            // New entry in tree, so including new sub tree
            auto c{default_container_};
            const auto result = c.Set(std::move(pose_to_from), std::forward<Args>(args)...);
            if (!result) {
                std::stringstream ss;
                ss << "Adding pose for ids '" << id_from << "' and '" << id_to << "' to container failed.";
                throw std::runtime_error(ss.str());
            }
            SubTree sub_tree = {std::make_pair(std::move(id_to), std::move(c))};
            tree_.emplace(std::move(id_from), std::move(sub_tree));
        } else {
            // Entry in tree is present
            auto& sub_tree = tree_iter->second;
            auto sub_tree_iter = sub_tree.find(id_to);
            if (sub_tree_iter == sub_tree.end()) {
                // New entry in sub tree
                auto c{default_container_};
                const auto result = c.Set(std::move(pose_to_from), std::forward<Args>(args)...);
                if (!result) {
                    std::stringstream ss;
                    ss << "Adding pose for ids '" << id_from << "' and '" << id_to << "' to container failed.";
                    throw std::runtime_error(ss.str());
                }
                sub_tree.emplace(std::move(id_to), std::move(c));
            } else {
                // Entry in sub tree is present
                const auto result = sub_tree_iter->second.Set(std::move(pose_to_from), std::forward<Args>(args)...);
                if (!result) {
                    std::stringstream ss;
                    ss << "Adding pose for ids '" << id_from << "' and '" << id_to << "' to container failed.";
                    throw std::runtime_error(ss.str());
                }
            }
        }
    }

    template <typename... Args>
    PoseType Get(const Id& id_to, const Id& id_from, Args&&... args) const {
        CheckPresence(id_from);
        CheckPresence(id_to);

        // Get the two chains of ids going from the root to the respective ids, like:
        //   c -> f -> id_from
        //   c -> g -> id_to
        auto chains = GetChains(id_to, id_from);
        std::list<Id> ids_to = chains.first;
        std::list<Id> ids_from = chains.second;

        // Calculate the transformation for each chain of ids and the final result
        PoseType pose_from_parent = TransformationFrom(ids_from, std::forward<Args>(args)...);
        PoseType pose_to_parent = TransformationFrom(ids_to, std::forward<Args>(args)...);
        return pose_to_parent * pose_from_parent.Inverse();
    }

    // Check if the supplied id is present in the tree
    bool Contains(const Id& id) const {
        // Present as from id
        if (FindIdFrom(id) != tree_.cend()) {
            return true;
        }
        // Present as to id
        if (FindIdTo(id) != tree_.cend()) {
            return true;
        }
        return false;
    }

private:
    // Poses are stored like id_from -> id_to -> pose_container, so map<id_from, map<id_to, pose_container>>
    using SubTree = std::map<Id, ContainerType>;
    using Tree = std::map<Id, SubTree>;

    // Get the two chains of ids going from the common id to the respective ids, like:
    //   c -> f -> id_from
    //   c -> g -> id_to
    std::pair<std::list<Id>, std::list<Id>> GetChains(const Id& id_to, const Id& id_from) const {
        // Get the two chains going from the root to the respective ids, like:
        //   root -> ... -> c -> f -> id_from
        //   root -> ... -> c -> g -> id_to
        std::list<Id> ids_from = GetChain(id_from);
        std::list<Id> ids_to = GetChain(id_to);

        if (ids_from.front() != ids_to.front()) {
            std::stringstream ss;
            ss << "Implementation error: Found different root nodes in PoseTree for '" << id_from << "' and '" << id_to << "'.";
            throw std::runtime_error(ss.str());
        }

        // Find common parent (c in example)
        // Remove the first id from both lists when the second id is also the same in both lists
        while (ids_from.size() >= 2 && ids_to.size() >= 2 && *(++ids_from.cbegin()) == *(++ids_to.cbegin())) {
            ids_from.pop_front();
            ids_to.pop_front();
        }

        return std::make_pair(ids_to, ids_from);
    }

    // Get the chain of ids from the root id to the supplied id
    std::list<Id> GetChain(const Id& id) const {
        std::list<Id> ids;
        ids.push_back(id);

        Id node = id;
        while (true) {
            const auto& tree_iter = FindIdTo(node);
            if (tree_iter == tree_.cend()) {
                // Could not find supplied id in any subtree, so id must be root node.
                return ids;
            }
            node = tree_iter->first;
            ids.push_front(node);
        }
    }

    // Returns an iterator to the tree where the subtree contains the supplied id
    typename Tree::const_iterator FindIdFrom(const Id& id) const {
        return tree_.find(id);
    };

    // Returns an iterator to the tree where the subtree contains the supplied id
    typename Tree::const_iterator FindIdTo(const Id& id) const {
        return std::find_if(
                tree_.cbegin(), tree_.cend(),
                [&id](const typename Tree::value_type& iter) -> bool {
                    const SubTree& sub_tree = iter.second;
                    return sub_tree.find(id) != sub_tree.cend();
                });
    };

    // Returns the transformation from a to n where the list of ids contains a->b->...->n
    template <typename... Args>
    PoseType TransformationFrom(const std::list<Id>& ids, Args&&... args) const {
        // What is done here, given ids a, b and c, then the transformations are stored like
        // a -> b -> p_ba
        // b -> c -> p_cb
        // and so the combined transformation should be p_cb * p_ba. Therefore the looping through the id pairs will be
        // backwards to result in transformation p_ca.
        PoseType result = PoseType::Identity();
        Id prev_id = ids.back();
        for (auto id_iter = ++ids.crbegin(); id_iter != ids.crend(); ++id_iter) {
            result *= tree_.at(*id_iter).at(prev_id).Get(std::forward<Args>(args)...);
            prev_id = *id_iter;
        }
        return result;
    }

    friend std::ostream& operator<<(std::ostream& os, const PoseTree_& tree) {
        for (const auto& tree_iter: tree.tree_) {
            os << tree_iter.first << ": \n";
            for (const auto& sub_tree_iter: tree_iter.second) {
                os << "    " << sub_tree_iter.first << ": " << sub_tree_iter.second << "\n";
            }
        }
        return os;
    }

    static void CheckValid(const Id& id) {
        if (id.empty()) {
            throw std::runtime_error("Empty id provided.");
        }
    }

    void CheckPresence(const Id& id) const {
        if (!Contains(id)) {
            std::stringstream ss;
            ss << "Provided id '" << id << "' is unknown.";
            throw std::runtime_error(ss.str());
        }
    }

    // Returned a pair of booleans representing:
    //   - Are the nodes directly connected via link?
    //   - If so, is the order of the inputs correct, or reversed?
    std::pair<bool, bool> IsDirectLinkReversed(const Id& id_to, const Id& id_from) const {
        auto const iter_from = FindIdFrom(id_from);
        auto const iter_to = FindIdFrom(id_to);
        bool direct_link = false;
        if (iter_from != tree_.cend()) {
            auto const& sub_tree = iter_from->second;
            if (sub_tree.find(id_to) != sub_tree.end()) {
                direct_link = true;
            }
        }
        if (!direct_link && iter_to != tree_.cend()) {
            auto const& sub_tree = iter_to->second;
            if (sub_tree.find(id_from) != sub_tree.end()) {
                direct_link = true;
            }
        }
        return std::make_pair(direct_link, iter_from == tree_.cend());
    }

    ContainerType default_container_;
    Tree tree_;
};

template <typename PoseType>
class PlainPoseContainer {
public:
    bool Set(const PoseType& pose) {
        pose_ = std::move(pose);
        return true;
    }

    PoseType Get() const {
        return pose_;
    }

private:
    friend std::ostream& operator<<(std::ostream& os, const PlainPoseContainer& c) {
        return os << "    PlainPoseContainer: " << c.pose_;
    }

    PoseType pose_ = PoseType::Identity();
};

// Convenience types
template <typename PoseType>
using PoseTree = PoseTree_<PoseType, PlainPoseContainer<PoseType>>;

template <typename PoseType, typename TimeType, typename TimeDiffConverter = TimeDiffCaster<TimeType>>
using PoseTreeBuffered = PoseTree_<PoseType, PoseBuffer_<PoseType, TimeType, TimeDiffConverter>>;

}  // namespace pose_manager

}  // namespace accerion
