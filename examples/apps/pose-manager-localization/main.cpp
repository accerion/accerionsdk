/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/connection_manager.h"

#include "AccerionPoseManager/pose_manager.h"

#include "app_utils.h"
#include "pose_manager_examples.h"


using PoseManager = accerion::pose_manager::PoseManagerBuffered<accerion::api::Pose, accerion::api::TimePoint>;
using PoseBuffer = PoseManager::PoseTreeType::Container;

PoseManager CreatePoseManager(const PoseManager::Id& id_sensor) {
    PoseManager::Id id_robot = "robot";

    PoseManager::StaticPoseTree robot_description{};
    robot_description.Set(id_sensor, id_robot, {0., 0., 0.});  // scout

    PoseBuffer default_buffer{std::chrono::seconds{10}};
    return {std::move(robot_description), std::move(id_robot), id_sensor, id_sensor, default_buffer};
}

int main(int argc, char **argv) {
    if (!(argc == 2 || argc == 5)) {
        std::cout << "Please include the sensor ip address and optionally a start pose,"
                     " so '" << argv[0] << " 192.168.0.116', possibly with '0 0.5 180' appended" << std::endl;
        return 1;
    }

    // Parse inputs
    const auto sensor_ip = accerion::api::Address::Parse({argv[1]});
    const auto opt_initial_pose =
            argc == 5 ? accerion::api::Pose::Parse(std::string{argv[2]} + " " + std::string{argv[3]} + " " + std::string{argv[4]})
                      : accerion::api::Optional<accerion::api::Pose>{};

    // Create the PoseManager instance (define first, because sensor need to be destructed first
    const PoseManager::Id id_sensor = "triton";
    auto pose_manager = CreatePoseManager(id_sensor);

    // Connect to the sensor
    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(sensor_ip,
                                                                      accerion::api::ConnectionType::kConnectionTcp);
    auto sensor = future_sensor.WaitFor(std::chrono::seconds{1});
    if (!sensor) {
        std::cout << "Could not connect to sensor" << std::endl;
        return 1;
    }

    if (opt_initial_pose) {
        pose_manager.SetGlobal(opt_initial_pose.value(), sensor->SubscribeToOdometry().WaitFor().value().timestamp);
        sensor->SetExternalReference({opt_initial_pose.value(), std::chrono::microseconds{0}});
    }

    // Odometry messages are sent, regardless of the internal mode setting. When the mode is set, then the
    // Triton will process corrections internally already, updating the corrected pose. This would make the setting of
    // the external reference redundant. Therefore, this mode is not set here.
    const auto diagnostics = sensor->SubscribeToDiagnostics().WaitFor();
    if (!diagnostics) {
        throw std::runtime_error("Could not get diagnostics.");
    }
    if (GetMode(diagnostics.value(), "internal")) {
        const auto set_int_mode_ack = sensor->SetInternalMode(false).WaitFor();
        if (!set_int_mode_ack || !set_int_mode_ack.value().accepted) {
            throw std::runtime_error("Could not set internal mode.");
        }
    }
    if (!GetMode(diagnostics.value(), "localization")) {
        const auto set_loc_mode_ack = sensor->SetLocalizationMode(true).WaitFor();
        if (!set_loc_mode_ack || !set_loc_mode_ack.value().accepted) {
            throw std::runtime_error("Could not set localization mode.");
        }
    }

    // Set the required call backs
    sensor->SubscribeToOdometry().Callback([id_sensor, &pose_manager, &sensor](auto odometry) {
        std::stringstream ss;
        ss << "odometry pose received " << odometry.pose << "\n";

        pose_manager.SetLocal(odometry.pose, odometry.timestamp);
        ss << "  pose manager updated using SetLocal\n";

        // Retrieve the sensor pose and send it to the sensor
        const auto pose = pose_manager.Get(PoseManager::global_id, id_sensor, odometry.timestamp);
        sensor->SetExternalReference({pose, std::chrono::microseconds{0}});
        ss << "  sensor pose set to " << pose << "\n";

        std::cout << ss.str() << std::endl;
    });
    sensor->SubscribeToCompensatedDriftCorrection().Callback([&pose_manager](auto drift_correction) {
        // Note that this can only happen when the sensor is located on a signature known in the current map
        std::stringstream ss;
        ss << "drift correction received " << drift_correction.pose << "\n";

        pose_manager.SetGlobal(drift_correction.pose, drift_correction.timestamp);
        ss << "  pose manager updated using SetGlobal\n";

        std::cout << ss.str() << std::endl;
    });

    PauseApp();

    sensor->SetLocalizationMode(false).WaitFor();

    return 0;
}
