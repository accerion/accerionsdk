/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <cstdint>
#include <utility>

#include "blocking_sensor.h"


// The order of the commands in the help message is based on their id, which is defined here
enum class CliCommandId : std::uint8_t {
    SetMode,
    Reboot,
    GetFile,
    SendFile,
    Get,
    Set,
    BufferedRecovery,
    Remove,
    Dev
};

struct CliCommand {
public:
    CliCommand(std::string n, std::vector<std::string> a, std::vector<std::string> u, std::vector<std::string> d);

    // Actually parse the command arguments and execute the command.
    // The return value indicates whether the application should stop.
    virtual bool Execute(const BlockingSensor& sensor, std::istream& is) = 0;

    const std::string name;

    // Other aliases for the command
    const std::vector<std::string> alias;

    // Short indication of how command should be used
    const std::vector<std::string> usage;

    // Full explanation of what command does
    const std::vector<std::string> description;
};
using CliCommandPtr = std::unique_ptr<CliCommand>;


class CliCommandSetMode : public CliCommand {
public:
    explicit CliCommandSetMode();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::SetMode};
};

class CliCommandReboot : public CliCommand {
public:
    explicit CliCommandReboot();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::Reboot};
};

class CliCommandGetFile : public CliCommand {
public:
    explicit CliCommandGetFile();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::GetFile};
    static const std::string kNoLoopsOption;
};

class CliCommandSendFile : public CliCommand {
public:
    explicit CliCommandSendFile();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::SendFile};
    static const std::string kNoImportOption;
};

class CliCommandGet : public CliCommand {
public:
    explicit CliCommandGet();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::Get};
};

class CliCommandSet : public CliCommand {
public:
    explicit CliCommandSet();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::Set};
};

class CliCommandBufferedRecovery : public CliCommand {
public:
    explicit CliCommandBufferedRecovery();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::BufferedRecovery};
};

class CliCommandDelete : public CliCommand {
public:
    explicit CliCommandDelete();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::Remove};
    static const std::string kAllOption;
};

class CliCommandDev : public CliCommand {
public:
    explicit CliCommandDev();

    bool Execute(const BlockingSensor& sensor, std::istream& is) override;

    static constexpr CliCommandId Id{CliCommandId::Dev};
};
