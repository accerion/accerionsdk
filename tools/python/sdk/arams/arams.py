#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import copy
import os
from enum import Enum, EnumMeta

from packaging.version import Version
from pathlib import Path
from typing import Union

import pandas as pd

from sdk.utils.path import AnyPath, path_list
from sdk.csv.reader import read as read_any_log
from sdk.csv.versions import determine_version, get_supported_version_module


__all__ = ["Arams"]


class Arams:
    """
    Class that reads ARAMS files that can also be accessed through this class.

    Because the file specifications are added to this class dynamically, prevent the respective warning issued by the
    Python code analysis: @DynamicAttrs
    """

    def __init__(self, input_paths: AnyPath):
        """
        All files and folders in the input path list will be searched for ARAMS files and subsequently read.
        In future this behaviour could be updated to use lazy loading (load file only when requested).
        Only one file per file type is expected. When multiple files are present, then the reading will fail.

        Provide as input a list of paths or a string with one path.
        """
        self._data = dict()
        self._input_files = []
        
        input_paths = path_list(input_paths)
        self._version = determine_version(input_paths)
        self._get_supported_types()
        self._set_supported_types()

        for input_path in input_paths:
            if Path(input_path).is_file():
                self._read_file(input_path)
            else:
                self._read_folder(input_path)

    def __contains__(self, file_type: Union[str, EnumMeta]) -> bool:
        """
        Accepts both string and enum.EnumMeta. The string is converted to the corresponding enum.EnumMeta type. 
        Check whether data is present for the supplied file type.
        Usage:
            arams = ARAMS("~/folder")
            is_fmt_available = FMT in arams
        """
        assert (isinstance(file_type, str) or isinstance(file_type, EnumMeta)), \
            "file_type is expected to be of type string or enum.EnumMeta."
        if isinstance(file_type, str):
            if not self.has_type(file_type):
                return False
            file_type = self.get_type(file_type)
        return file_type in self._data

    def __getitem__(self, file_type: EnumMeta) -> pd.DataFrame:
        """
        Retrieve the data frame corresponding to the supplied file type.

        Usage:
            arams = ARAMS("~/folder")
            data_frame_ldc = arams[LDC]

        Will fail when the requested file type is not present. This should be checked up front by the user.
        String file_type can be converted to its enum.EnumMeta equivalent using get_type(string_name).
        """
        assert isinstance(file_type, EnumMeta), "file_type is expected to be of type enum.EnumMeta."
        assert file_type in self, "Requested file type not found in inputs."
        return self._data[file_type]

    def __setitem__(self, file_type: EnumMeta, new_data: pd.DataFrame):
        """
        Set the data frame corresponding to the supplied file type.

        Usage:
            arams = ARAMS("~/folder")
            arams[FMT] = new_data

        Will fail when the requested file type is not present in file specification.
        String file_type can be converted to its enum.EnumMeta equivalent using get_type(string_name).
        """
        assert isinstance(file_type, EnumMeta), "file_type is expected to be of type enum.EnumMeta."
        assert file_type in self._files_supported, "Requested file type not found in supported files."
        self._data[file_type] = new_data

    def _read_folder(self, input_folder: Path):
        for root, _, files in os.walk(input_folder):
            for name in files:
                self._read_file(Path(root) / name)

    # The read data will be a dataframe that is stored internally
    def _read_file(self, input_file: Path):
        file_type = Arams._find_file_type(input_file, self._files_supported)
        if file_type is None:
            return

        data = read_any_log(input_file, file_type)
        if data is None:
            return

        update_method = Arams.UpdateMethod.NEW
        if file_type in self._data:
            update_method = Arams.UpdateMethod.PREPEND if input_file.match("*.1.*") else Arams.UpdateMethod.APPEND
        
        self._input_files.append(input_file)
        return self._update_data({file_type: data}, update_method)

    @staticmethod
    def _find_file_type(file_name: Path, file_types):
        file_name_base = Path(file_name).name
        for file_type in file_types:
            if file_type.can_parse(file_name_base):
                return file_type
        return None

    class UpdateMethod(Enum):
        NEW = 0
        APPEND = 1
        PREPEND = 2

    def _update_data(self, new_data, update_method: UpdateMethod = UpdateMethod.NEW):
        if update_method in [Arams.UpdateMethod.APPEND, Arams.UpdateMethod.PREPEND]:
            assert all(k in self._data for k in new_data.keys()), "Explicitly expecting data to be present already."
            for key in new_data.keys():
                self._data[key] = \
                    pd.concat((self._data[key], new_data[key]), axis=0) if update_method == Arams.UpdateMethod.APPEND \
                    else pd.concat((new_data[key], self._data[key]), axis=0)
        else:
            assert not any(k in self._data for k in new_data.keys()), "Explicitly expecting no data to be present."
            self._data.update(new_data)

    def get_input_files(self):
        """
        The returned input files include the processed arams logs file paths and the map_coordinate csv file,
        but not the g2o file path as that is not processed by the Arams object.
        """
        return self._input_files
        
    def _get_supported_types(self):
        version_module = get_supported_version_module(self._version)

        self._files_supported = version_module.FILES_SUPPORTED.copy()
        self.ARAMS_INDEX = version_module.ARAMS_INDEX
        self.ARAMS_TIME = version_module.ARAMS_TIME

    def _set_supported_types(self):
        """
        Create the enum classes from _files_supported as attributes of the Arams object.
        Those enums can be later addressed as arams_obj.XYZ.
        Before using them it is good to check whether they exist by invoking arams_obj.has_type("XYZ")
        """
        for file_type in self._files_supported:
            setattr(self, file_type.__name__, file_type)

    def get_version(self):
        return self._version

    def has_type(self, file_type: str):
        """
        Checks whether the requested arams type exists in the file specification
        """    
        assert isinstance(file_type, str), "file_type is expected to be of type string."
        return file_type.upper() in set(x.__name__ for x in self._files_supported)

    def get_type(self, file_type: str):
        """
        Get file type object based on the name.
        Throws an error if type is not present in the file specification. Check this using has_type.
        """
        assert isinstance(file_type, str), "file_type is expected to be of type string."
        assert self.has_type(file_type), "Requested file type is not part of this ARAMS version, " \
                                         "use has_type() before calling this method to check for type existence."
        return getattr(self, file_type.upper())

    def get_copy(self):
        arams_copy = copy.deepcopy(self)
        return arams_copy
