/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <memory>
#include <string>

#include "prepared_statement.h"
#include "sqlite3.h"

namespace accerion {

namespace sqlite {

class Database {
 public:
    explicit Database(const std::string& path, int flags = SQLITE_OPEN_READONLY);

    ~Database() = default;
    Database(const Database& other) = delete;
    Database& operator=(const Database& other) = delete;
    Database(Database&& other) = default;
    Database& operator=(Database&& other) = default;

    [[deprecated("This is a temporary method to get a sqlite3 pointer.")]]
    sqlite3* GetPtr() const;

    PreparedStatement PrepareStatement(const std::string& query) const;

    void Execute(const std::string& query) const;

    template <typename Type>
    Type ExecuteAndGetValue(const std::string& query) const {
        return PreparedStatement{*this, query}.GetValue<Type>();
    }

    std::string GetFilePath() const;

 private:
    struct SqlitePtrDeleter {
        void operator()(sqlite3* db) {
            sqlite3_close_v2(db);
        }
    };
    using SqlitePtr = std::unique_ptr<sqlite3, SqlitePtrDeleter>;

    static SqlitePtr OpenDatabase(const std::string& path, const int flags);

    SqlitePtr db_;  // Should be treated as const, except that it can't as it needs to be movable
};

}  // namespace sqlite
}  // namespace accerion
