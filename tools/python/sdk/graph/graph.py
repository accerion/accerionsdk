#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from dataclasses import dataclass, field
from math import sqrt
from typing import List, Optional

from sdk.csv.versions.base import StrEnum
from sdk.data.pose import Pose


__all__ = ['Graph', 'InfMatrix', 'Pose']


@dataclass
class InfMatrix:
    x_x: float = 0  # [1 / meter ^ 2]
    x_y: float = 0  # [1 / meter ^ 2]
    x_th: float = 0  # [1 / (meter * radian)]
    y_y: float = 0  # [1 / meter ^ 2]
    y_th: float = 0  # [1 / (meter * radian)]
    th_th: float = 0  # [1 / radian ^ 2]

    @staticmethod
    def from_std_dev(std_dev_pos: float, std_dev_rot: Optional[float] = None) -> InfMatrix:
        inf = InfMatrix()
        inf.x_x = std_dev_pos ** -2
        inf.y_y = inf.x_x
        inf.th_th = inf.x_x if std_dev_rot is None else std_dev_rot ** -2
        return inf

    def to_std_dev_pos(self) -> float:
        assert self.no_cross_terms
        return sqrt(1 / self.x_x + 1 / self.y_y)

    def to_std_dev_rot(self) -> float:
        assert self.no_cross_terms
        return 1 / sqrt(self.th_th)

    @property
    def no_cross_terms(self) -> bool:
        return self.x_y == 0 and self.x_th == 0 and self.y_th == 0


@dataclass
class Graph:
    @dataclass
    class Vertex:
        class Type(StrEnum):
            Unknown = "unknown"
            Signature = "signature"
            Absolute = "absolute"
            Fixed = "fixed"
            Interpolated = "interpolated"

        id: int = 0
        pose: Pose = field(default_factory=Pose)
        type: Type = Type.Unknown

    @dataclass
    class Edge:
        class Type(StrEnum):
            Unknown = "unknown"
            Relative = "relative"
            Absolute = "absolute"
            Loop = "loop"
            Identity = "identity"

        """
        The geometry of an edge specifies how to transform a point in the end system to the begin one, i.e.
        P_edge = P_begin^-1 * P_end
        Or, how is the origin of the end system seen from the begin system.
        """
        id_begin: int = 0
        id_end: int = 0
        pose: Pose = field(default_factory=Pose)
        inf: InfMatrix = field(default_factory=InfMatrix)
        type: Type = Type.Unknown

    @dataclass
    class Fix:
        id: int = 0

    vertices: List[Vertex] = field(default_factory=list)
    edges: List[Edge] = field(default_factory=list)
    fixes: List[Fix] = field(default_factory=list)
