#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import List

from sdk.graph.graph import Graph


def identify_edges(graph: Graph, edge_type: Graph.Edge.Type) -> List[int]:
    """
    Return the list of indices to the edges of desired type

    If the graph edges all have type unknown, then the legacy way is used (only relevant for relative and loop closure
    edges).
    """

    legacy = all(edge.type == Graph.Edge.Type.Unknown for edge in graph.edges)

    if legacy:
        if edge_type == Graph.Edge.Type.Unknown:
            return list(range(len(graph.edges)))
        elif edge_type == Graph.Edge.Type.Relative:
            return [index for index, edge in enumerate(graph.edges) if abs(edge.id_end - edge.id_begin) == 1]
        elif edge_type == Graph.Edge.Type.Loop:
            return [index for index, edge in enumerate(graph.edges) if abs(edge.id_end - edge.id_begin) != 1]
        else:
            return []
    else:
        return [index for index, edge in enumerate(graph.edges) if edge.type == edge_type]
