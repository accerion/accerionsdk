#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from argparse import ArgumentParser
import math
from typing import List

import ezdxf

from sdk.data.signature import Signature
from sdk.data.signature_reader import SignatureReader


SHOW_LINES = True   # Whether to show lines between consecutive signatures
SHOW_POINTS = False # Whether to show individual signatures as a points
MAX_POINTS = 10000  # Max number of points to use for output (int), or None if using all points in floor map coordinates
COLOR = (86, 57, 134)  # Accerion purple to visualize lines and points in dxf


def __add_lines_and_points(point_list, modelspace, line_color, point_color, points_per_cluster):
    if points_per_cluster and len(point_list) > points_per_cluster:  # Reduce number of points in list
        # Calculate step size that will lead to desired number of points per cluster.
        step_size = max(1, math.ceil((len(point_list) - 1) / max(1, points_per_cluster - 1)))
        filtered_point_list = point_list[::step_size] + [point_list[-1]]
    else:
        filtered_point_list = point_list[:]

    if SHOW_LINES and len(filtered_point_list) > 1:  # Need at least two points to draw a line
        for i in range(len(filtered_point_list) - 1):
            line = modelspace.add_line(start=filtered_point_list[i], end=filtered_point_list[i+1])
            line.rgb = line_color

    if SHOW_POINTS:
        for point in filtered_point_list:
            point = modelspace.add_point(point)
            point.rgb = point_color

    return modelspace


def convert_fmc_to_dxf(signatures: List[Signature], output_file: str):
    doc = ezdxf.new()
    msp = doc.modelspace()  # Create an empty modelspace

    # To reduce the file size, the user can configure a limit to the number of points used in the dxf file.
    cluster_count = len(set(s.cluster_id for s in signatures))

    # Based on the max number of points and the total number of clusters, we determine how much lines to use per cluster.
    # A minimum of one point is needed to visualize a cluster
    points_per_cluster = max(1, math.floor(MAX_POINTS/cluster_count)) if MAX_POINTS else None

    point_list = []
    prev_cluster_id = None

    # For each cluster: add line segments and points to the modelspace
    for signature in signatures:
        if prev_cluster_id is not None and prev_cluster_id != signature.cluster_id:  # New cluster detected
            # Process cluster points collected so far
            msp = __add_lines_and_points(point_list, msp, COLOR, COLOR, points_per_cluster)
            point_list.clear()
        point_list.append((signature.pose.x, signature.pose.y))
        prev_cluster_id = signature.cluster_id

    # Add lines for last cluster
    msp = __add_lines_and_points(point_list, msp, COLOR, COLOR, points_per_cluster)
    doc.saveas(output_file)


def __main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, nargs="+",
                        help="Input files and folders including floor map coordinates or database")
    parser.add_argument("-o", "--output", required=True,
                        help="Output file (.dxf)")

    arguments = vars(parser.parse_args())

    signatures = SignatureReader.read(arguments["input"])
    assert signatures, "No signature information is provided"
    convert_fmc_to_dxf(signatures, arguments["output"])


if __name__ == "__main__":
    __main()
