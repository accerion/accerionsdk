
# Easy access to libs and apps
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

# Helper functions
function(add_compiler_warnings TARGET_NAME)
    set(FLAGS "")
    if(UNIX)
        # Treat all warnings as errors
        list(APPEND ${FLAGS} -Werror)
    endif()

    get_target_property(TARGET_TYPE ${TARGET_NAME} TYPE)
    if(${TARGET_TYPE} STREQUAL "INTERFACE_LIBRARY")
        target_compile_options(${TARGET_NAME} INTERFACE ${FLAGS})
    else()
        target_compile_options(${TARGET_NAME} PUBLIC ${FLAGS})
    endif()
endfunction()

function(set_base_properties TARGET_NAME)
    add_compiler_warnings(${TARGET_NAME})
endfunction()
