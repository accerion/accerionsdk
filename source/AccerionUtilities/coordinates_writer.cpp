/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUtilities/coordinates_writer.h"

#include <algorithm>

#include "AccerionUtilities/csv_writer.h"

namespace accerion {
namespace utilities {

void WriteCoordinates(const std::vector<api::Signature>& signatures, const std::string& path, char delimiter) {
    const std::vector<std::string> headers = {"signature_id", "cluster_id", "x[m]", "y[m]", "th[deg]"};
    CsvWriter<api::SignatureId, api::ClusterId, double, double, double> writer{path, headers, delimiter};
    std::for_each(
            signatures.cbegin(),
            signatures.cend(),
            [&writer](const auto& s) {
                writer.WriteLine(s.signature_id, s.cluster_id, s.pose.x, s.pose.y, s.pose.th);
            });
}

}  // namespace utilities
}  // namespace accerion
