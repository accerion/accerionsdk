/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <vector>

#include "AccerionSensorAPI/sensor.h"
#include "AccerionSensorAPI/update_service.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/utils/sensor.h"
#include "AccerionSensorAPI/utils/optional.h"

class BlockingSensor {
public:
    explicit BlockingSensor(const accerion::api::Address& ip);
    explicit BlockingSensor(const accerion::api::SerialNumber& sn);

    ~BlockingSensor() = default;

    BlockingSensor(BlockingSensor&&) = default;
    BlockingSensor& operator=(BlockingSensor&&) = default;
    BlockingSensor(const BlockingSensor&) = delete;
    BlockingSensor& operator=(const BlockingSensor&) = delete;

    accerion::api::SerialNumber GetSerialNumber() const;

    // All mode toggling
    enum class Mode : std::uint8_t {
        Internal = 0,
        Mapping,
        ContSignatureUpdating,
        AbsLocalization,
        LineFollowing,
        Recording,
        Expert,
        Aruco,
        Idle,
        Reboot
    };
    void Set(Mode mode, bool enabled) const;
    void SetMappingOn(std::uint16_t cluster_id) const;
    void SetMappingOff() const;
    void SetLineFollowingOn(std::uint16_t cluster_id) const;
    void SetLineFollowingOff() const;

    // All file up- and downloading
    void GetFile(accerion::api::FileType type, const std::string& file_name) const;
    // If the cluster id is not provided, then all will be exported
    void GetCoordinatesFile(const std::string& file_name, accerion::api::Optional<accerion::api::ClusterId> opt_cluster_id) const;
    void GetLoopClosuresFile(const std::string& file_name, double search_radius) const;
    void GetRecordingsFile(const std::string& file_name, std::vector<std::string> ids) const;
    void SendMapFile(const std::string& file_name, accerion::api::FileTransferTask task) const;
    void SendUpdateFile(const std::string& file_name) const;

    // All property getting and setting
    accerion::api::Areas GetAreaIds() const;
    std::vector<accerion::api::ClusterId> GetClusterIds() const;
    accerion::api::Diagnostics GetDiagnostics() const;
    accerion::api::IpAddress GetIpInfo() const;
    accerion::api::MapSummary GetMapSummary() const;
    std::vector<std::string> GetRecordings() const;
    std::pair<accerion::api::SoftwareVersion, accerion::api::SoftwareDetails> GetSoftwareInfo() const;

    void SetArea(std::uint16_t id) const;
    void SetBufferLength(float length) const;
    void SetConnectionType(
            accerion::api::ConnectionType type,
            accerion::api::Optional<accerion::api::Address> local_ip = {}) const;
    void SetIpInfo(
            accerion::api::Optional<accerion::api::Address> address,
            accerion::api::Optional<accerion::api::Address> netmask,
            accerion::api::Optional<accerion::api::Address> gateway) const;
    void SetPose(accerion::api::Pose pose) const;

    // All map management functionality
    void DeleteArea(accerion::api::Optional<accerion::api::AreaId> id) const;
    void DeleteCluster(accerion::api::ClusterId id) const;

    // All remaining functionality

    // In case the optional radius is not provided, then a full map search is performed
    void PerformBufferedRecovery(double x, double y, accerion::api::Optional<double> r) const;

    // Dev functionality
    accerion::api::RawData GetFrame(const std::string& dev_key) const;

 private:
    bool ClusterIdExists(accerion::api::ClusterId cluster_id) const;

    std::unique_ptr<accerion::api::Sensor> sensor_;
    accerion::api::Address ip_address_;
};
