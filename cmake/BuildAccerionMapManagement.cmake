project(AccerionMapManagement LANGUAGES CXX)

add_library(${PROJECT_NAME}
            ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMapManagement/graph.cpp)

target_link_libraries(${PROJECT_NAME} AccerionPgo AccerionMap)

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

add_compiler_warnings(${PROJECT_NAME})

add_executable(ImportGraph ${CMAKE_CURRENT_SOURCE_DIR}/source/AccerionMapManagement/import_graph.cpp)
target_link_libraries(ImportGraph AccerionMapManagement AccerionPgo AccerionDatabaseHandling)
add_compiler_warnings(ImportGraph)