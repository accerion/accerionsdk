#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.metrics.common import parse_condition, Result


def test_metrics_parse_condition():
    class Test:
        def __init__(self, c: str, v: float, r: Result):
            self.condition: str = c
            self.value: float = v
            self.result: Result = r

    tests = [
        Test("< 1", 0.99, Result.Pass),
        Test("< 1", 1.0, Result.Fail),
        Test("> 1", 1.01, Result.Pass),
        Test("> 1", 1.0, Result.Fail),
        Test("> 0.016 and < 0.024", 0.01, Result.Fail),
        Test("> 0.016 and < 0.024", 0.02, Result.Pass),
        Test("> 0.016 and < 0.024", 0.03, Result.Fail),
        Test("> 0.024 or < 0.016", 0.01, Result.Pass),
        Test("> 0.024 or < 0.016", 0.02, Result.Fail),
        Test("> 0.024 or < 0.016", 0.03, Result.Pass)
    ]
    for test in tests:
        condition_test_function = parse_condition(test.condition)[1]
        assert condition_test_function(test.value) == test.result, \
            f"Test '{test.condition}' on value {test.value} did not result in {test.result}"
