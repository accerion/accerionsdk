#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from math import isclose, pi, radians, degrees
from typing import Set

from sdk.graph.graph import Pose


def test_pose_normalized():
    @dataclass
    class Test:
        input: float
        expectations: Set[float]

    tests = [
        Test(-181, { 179}),
        Test(-180, {-180, 180}),
        Test(-179, {-179}),
        Test(-  1, {-  1}),
        Test(   0, {   0}),
        Test(   1, {   1}),
        Test( 179, { 179}),
        Test( 180, {-180, 180}),
        Test( 181, {-179}),
    ]

    for test in tests:
        output = Pose(th=test.input / 180 * pi).normalized().th / pi * 180
        assert any(isclose(output, expectation) for expectation in test.expectations)


def test_pose_interpolate_rotation():
    @dataclass
    class Test:
        input_a: float
        input_b: float
        input_ratio: float
        expectations: Set[float]

    tests = [
        Test(-50., 50., 0.0, {-50}),
        Test(-50., 50., 0.2, {-30}),
        Test(- 50.,  50., 0.5, {0}),
        Test(- 90.,  90., 0.2, {-126, -54}),
        Test(- 90.,  90., 0.5, {-180, 0, 180}),
        Test(-100.,  80., 0.2, {-136, -64}),
        Test(-100.,  80., 0.5, {-10, 170}),
        Test(-130., 130., 0.2, {-150}),
        Test(-130., 130., 0.5, {-180, 180})
    ]

    for test in tests:
        p_a = Pose(th=radians(test.input_a))
        p_b = Pose(th=radians(test.input_b))

        # Test interpolated pose
        output = degrees(p_a.interpolate(p_b, test.input_ratio).th)
        assert any(isclose(output, expectation) for expectation in test.expectations)

        # Test inversely interpolated pose
        output = degrees(p_b.interpolate(p_a, 1. - test.input_ratio).th)
        assert any(isclose(output, expectation) for expectation in test.expectations)


def test_pose_interpolate_position():
    @dataclass
    class Test:
        input: float
        expectation: Pose

    p_a = Pose(1., 3., 0.)
    p_b = Pose(-1., 13., 0.)

    tests = [
        Test(0.0, p_a),
        Test(0.2, Pose(0.6, 5., 0.)),
        Test(0.5, Pose(0.0, 8., 0.)),
        Test(1.0, p_b)
    ]

    for test in tests:

        # Test interpolated pose
        output = p_a.interpolate(p_b, test.input)
        assert output.is_approx(test.expectation)

        # Test inversely interpolated pose
        output = p_b.interpolate(p_a, 1. - test.input)
        assert output.is_approx(test.expectation)
