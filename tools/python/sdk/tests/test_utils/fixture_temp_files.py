#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import os
from pathlib import Path
from tempfile import mkstemp
from typing import List

import pytest


class _TempFiles:
    def __init__(self):
        self.__files: List[Path] = []

    def create(self, suffix=None) -> Path:
        fd, file_path_str = mkstemp(suffix=suffix)
        # Closing the file descriptor that is internally created by os.open(..)
        os.close(fd)

        self.__files.append(Path(file_path_str))
        return self.__files[-1]

    def cleanup(self):
        for f in self.__files:
            os.remove(f)


@pytest.fixture
def temp_files():
    # Setup
    temp_files_instance = _TempFiles()

    # Using "yield" is preferred over a custom "__del__" destructor,
    # as garbage collection is unpredictable and may never be called.
    yield temp_files_instance

    # Teardown
    temp_files_instance.cleanup()
