#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import os
from pathlib import Path
from typing import Dict, List

from sdk.csv.versions.base import StrEnum


class TestData:
    """
    Structure to address the data is available for testing.

    Reason of this class is twofold:
     - Improve traceability. Using this class and enum, it is easy to find out which tests are using certain files.
     - PyTest can be run from anywhere, so using this object automatically returns the right data path.
    """

    class File(StrEnum):
        IMPORT_G2O_DB_200 = "import_g2o_200.db"
        IMPORT_G2O_G2O = "import_g2o.g2o"
        FLOOR_MAP_COORDINATES_V700 = "v700_floor_map_coordinates.csv"
        # Only kept a couple of lines
        ARAMS_USER_LOGS_V700 = "v700_arams_user_logs"
        ARAMS_USER_LOGS_V615 = "v615_arams_user_logs"
        # empty arams logs for version 6.1.2-r
        ARAMS_USER_LOGS_V612 = "v612_arams_user_logs"
        # source, Seafile, ProductTech/Testing/ACC-2235_use_signature_id_instead_of_index
        ARAMS_USER_LOGS_V61 = "v61_arams_user_logs"
        # Regenerate after finishing ACC-2302
        FLOOR_MAP_COORDINATES_V61 = "v61_floor_map_coordinates.csv"
        # source: Seafile, ProductTech/FieldTestData/Autonmated_field_test_30082021_v540rc10/v540Map/run2
        ARAMS_USER_LOGS_V6 = "v6_arams_user_logs"
        FLOOR_MAP_COORDINATES = "floor_map_coordinates.csv"
        # source: Seafile, ProductTech/FieldTestData/Automated_field_test_30082021_v540rc10
        POSE_GRAPH = "optimized.g2o"

        @staticmethod
        def get_data_dir():
            return Path(__file__).absolute().parent

    __file_to_path_dict: Dict[File, Path] = {}

    @classmethod
    def register(cls, file_enum) -> None:
        cls.__file_to_path_dict.update(
            {f: Path(file_enum.get_data_dir() / f.value) for f in file_enum})

        # Check that paths are actually correct
        for e, p in cls.__file_to_path_dict.items():
            assert os.path.exists(p), f"Test data file '{e.name}' with path '{p}' does not exist."

    @classmethod
    def get(cls, f) -> Path:
        return cls.__file_to_path_dict[f]

    @classmethod
    def get_list(cls, files) -> List[Path]:
        return [cls.get(f) for f in files]


TestData.register(TestData.File)
