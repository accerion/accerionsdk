#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from packaging.version import Version

from sdk.arams.arams import Arams
from sdk.tests.test_data.files import TestData


def test_files_supported():
    files_to_test = (
        (Version("6.1.5"), [TestData.File.ARAMS_USER_LOGS_V615, TestData.File.FLOOR_MAP_COORDINATES_V61]),
        (Version("7.0.0"), [TestData.File.ARAMS_USER_LOGS_V700, TestData.File.FLOOR_MAP_COORDINATES_V700])
    )

    for version, files in files_to_test:
        arams = Arams(TestData.get_list(files))
        assert arams.get_version() == version
        for file in arams._files_supported:
            assert file in arams
