#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from argparse import ArgumentParser
import logging
from logging.handlers import RotatingFileHandler
from pathlib import Path
from shutil import copy as copy_file
import sys
from typing import List, Mapping, Optional, Set

from sdk import ROOT_PATH
from sdk.geometry.bounding_box import BoundingBox
from sdk.geometry.point import Point
from sdk.map.map import Map
from sdk.map_management.prune_tool.plotting import Plotting
from sdk.map_management.prune_tool.pruning import Settings, prune
from sdk.map_management.prune_tool.utils import BoundingBoxWithRadius, Pixel
from sdk.metrics.common import Result
from sdk.metrics.metrics import Metrics, MetricCondition, MetricDataAggregation
from sdk.utils.argument_parser import ValidateInputPaths
from sdk.utils.path import add_to_stem, ensure_directory


def create_pruned_map(input_db_path: Path, output_db_path: Path, sig_ids_to_remove: Set[int]):
    """
    Create a new database with only the signatures to keep
    """
    assert input_db_path != output_db_path

    # Create a fresh map to be filled with the signatures to keep
    copy_file(ROOT_PATH / "resources" / "empty_map_v700.db", output_db_path)

    pruned_map = Map(output_db_path)
    pruned_map.database().execute(f"""
        attach '{input_db_path.resolve()}' as input;
    """)

    sig_ids_to_remove_list = ','.join(str(i) for i in sig_ids_to_remove)
    pruned_map.database().execute("BEGIN TRANSACTION")
    pruned_map.database().execute(f"""
        INSERT INTO main.signatures
        SELECT *
          FROM input.signatures
         WHERE id NOT IN ({sig_ids_to_remove_list});
    """)
    pruned_map.database().execute(f"""
        INSERT INTO main.data
        SELECT *
          FROM input.data
         WHERE id NOT IN ({sig_ids_to_remove_list});
    """)
    pruned_map.database().connection.commit()
    pruned_map.database().execute("BEGIN TRANSACTION")
    pruned_map.database().execute(f"""
        INSERT INTO main.images
        SELECT *
          FROM input.images
         WHERE id NOT IN ({sig_ids_to_remove_list});
    """)
    pruned_map.database().connection.commit()

    pruned_map.reindex_cluster_index()
    pruned_map.database().connection.commit()


def parse_bounding_boxes(arguments):
    bounding_boxes = []
    if arguments["bounding_box"]:
        for bounding_box_str in arguments["bounding_box"]:
            bounding_box_vals = bounding_box_str.split(',')
            if len(bounding_box_vals) != 5:
                raise ValueError(f"Invalid bounding box format: {bounding_box_str}")

            # Strip any leading/trailing whitespace or quotes and convert to float
            x_min, x_max, y_min, y_max, radius = map(lambda x: float(x.strip().strip("'\"")), bounding_box_vals)

            # Create the BoundingBoxWithRadius object
            bounding_box_with_radius = BoundingBoxWithRadius(
                bounding_box=BoundingBox.create_from_points(Point(x_min, y_min), Point(x_max, y_max)),
                radius=radius
            )
            bounding_boxes.append(bounding_box_with_radius)

    return bounding_boxes


def create_metrics(settings: Settings,
                   signature_density: Mapping[Pixel, int]) -> List[Metrics.Condition]:
    def calculate_result(metric: Metrics.Condition, values) -> Metrics.Condition:
        metric.aggregation_result = metric.aggregation(values)
        metric.condition_result = metric.condition(metric.aggregation_result)
        return metric

    return [
        calculate_result(Metrics.Condition(name="signature density",
                                           aggregation=MetricDataAggregation("maximum"),
                                           aggregation_result=0,
                                           condition=MetricCondition(f"< {2 * settings.max_signatures_in_radius}"),
                                           condition_result=Result.NoData),
                         signature_density.values()),
        calculate_result(Metrics.Condition(name="signature density",
                                           aggregation=MetricDataAggregation(f"percentile < {1.5 * settings.max_signatures_in_radius}"),
                                           aggregation_result=0,
                                           condition=MetricCondition(f"> 95"),
                                           condition_result=Result.NoData),
                         signature_density.values()),
        calculate_result(Metrics.Condition(name="signature density",
                                           aggregation=MetricDataAggregation("average"),
                                           aggregation_result=0,
                                           condition=MetricCondition(f"< {settings.max_signatures_in_radius}"),
                                           condition_result=Result.NoData),
                         signature_density.values())
    ]


def report_metrics(metrics: List[Metrics.Condition], logger: logging.Logger) -> bool:
    logger.info("\nThe quality check results:")
    for m in metrics:
        if m.condition_result == Result.NoData:
            continue
        logger.info(f"    {m.name}, {m.aggregation}: {m.condition_result.value.upper()} "
                    f"(actual {m.aggregation_result} {m.condition} expected)")
    any_failure = any(m.condition_result == Result.Fail for m in metrics)
    if any_failure:
        logger.info("There are metrics that are failing")
    else:
        logger.info("All metrics passed")
    return any_failure


def __main(arguments):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    output_folder: Optional[Path] = arguments["output_folder"]
    if output_folder is not None:
        ensure_directory(output_folder)
        logger.addHandler(RotatingFileHandler(output_folder / "output.log", mode = 'w'))
        # Only report the command when logging to file
        logger.info("Command run:\n    " + "\n    ".join(sys.argv) + "\n")
    # Add logging to console after potentially logging the command used to run this script
    logger.addHandler(logging.StreamHandler(sys.stdout))

    # Gather the settings
    prune_settings = Settings()
    logger.info(f"The maximum number of loaded signatures is: {prune_settings.max_signatures_in_radius}")
    if arguments['radius'] is not None:
        prune_settings.default_radius = arguments['radius']
    logger.info(f"The default search radius used is: {prune_settings.default_radius} meter")

    prune_settings.bounding_boxes = parse_bounding_boxes(arguments)

    # Import signature data from input .db file
    input_db_path = arguments["input_db_path"]
    map_db = Map(input_db_path)
    signatures = map_db.get_all_signatures(Map.Info.POSE)

    # Validate clusters to keep
    clusters_to_keep = arguments["keep_cluster_ids"]
    if clusters_to_keep is not None:
        prune_settings.cluster_ids_to_keep = set(clusters_to_keep)
        known_cluster_rows = map_db.database().get_cursor(f"""
                SELECT DISTINCT cluster_id
                  FROM signatures
            """)
        unknown_clusters = prune_settings.cluster_ids_to_keep - set(row[0] for row in known_cluster_rows)
        logger.warning(f"The following provided cluster ids are not found in the map: "
                       f"{', '.join(str(c) for c in unknown_clusters)}")

    # Prune map
    selected_signature_ids = prune(signatures, prune_settings)
    logger.info(f"There are {len(signatures) - len(selected_signature_ids)} signatures removed, "
                f"out of {len(signatures)}")

    # If output path is provided, the results will be saved
    if output_folder is not None:
        sig_ids_to_remove = set(s.id for s in signatures) - selected_signature_ids
        create_pruned_map(input_db_path,
                          output_folder / add_to_stem(input_db_path, suffix="_pruned").name,
                          sig_ids_to_remove)

    # Report on results
    plotting = Plotting(signatures, selected_signature_ids, prune_settings)

    metrics = create_metrics(prune_settings, plotting.pruned_density)
    any_failure = report_metrics(metrics, logger)

    plotting.plot_prune_overview(output_folder)
    plotting.plot_signature_densities(output_folder)
    plotting.plot_signature_distances(output_folder)

    exit(any_failure)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--input_db_path", required=True, type=Path, action=ValidateInputPaths,
                        help="Path to map file to prune (.db)")
    parser.add_argument("-o", "--output_folder", required=False, type=Path,
                        help="Path to output results folder, if not supplied tool is run interactively"
                             " (without saving the map)")
    parser.add_argument("-r", "--radius", required=False, type=float,
                        help="Default search radius [m]")
    parser.add_argument("-k", "--keep_cluster_ids", required=False, nargs="+", type=int,
                        help="List of cluster IDs to keep")
    parser.add_argument("-b", "--bounding_box", required=False, nargs="+", type=str,
                        help="Bounding box and density in the format 'x_min,x_max,y_min,y_max,radius'. "
                             "Can be used multiple times.")

    __main(vars(parser.parse_args()))
