#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path
from typing import Optional

from matplotlib import pyplot as plt

from sdk.data.data_container import DataContainer
from sdk.plotting.plot_distance_between_signatures import Plotting
from sdk.plotting.utils.plotting import call_arams_plot_function, save_or_show
from sdk.processing.distance_between_signatures import get_distance_between_signatures
from sdk.utils.configuration import Configuration


__all__ = ["plot_distance_between_signatures_histogram_cum"]


def plot_distance_between_signatures_histogram_cum(data: DataContainer,
                                                  output_folder: Optional[Path] = None,
                                                  conf: Configuration = Configuration.default(),
                                                  *,
                                                  verbose: bool = True):
    _, _, distances = get_distance_between_signatures(data.signatures)  # [meter]
    if not distances:
        return

    fig = plt.figure()
    axis = fig.subplots(1, 1)

    plotting = Plotting(distances, conf)
    plotting.add_cumulative_histogram(axis)

    fig.suptitle("Distance between consecutive signatures (cumulative)", fontweight='bold')
    perc_within_range = plotting.calculate_signature_distances_within_range() / len(distances) * 100.
    axis.set_title(f'Signatures between min/max distance: {perc_within_range:.1f} %')
    axis.set_xlabel('Euclidean distance [mm]')

    save_or_show(fig, output_folder, "distance_between_signatures_histogram_cumulative.png",
                 conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_distance_between_signatures_histogram_cum)
