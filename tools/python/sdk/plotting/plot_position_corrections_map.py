#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.plotting.utils.plotting import *
from sdk.processing.pose_corrections import get_pose_corrections
from sdk.utils.configuration import Configuration


def plot_position_corrections_map(data: DataContainer,
                                  output_folder: Optional[Path] = None,
                                  conf: Configuration = Configuration.default(),
                                  *,
                                  verbose: bool = True):
    from sdk.metrics.metrics_data import MetricsData

    pos_x, pos_y, error_poses = get_pose_corrections(data.arams, verbose=verbose)
    if not error_poses:
        return
    errors = [e.norm for e in error_poses]

    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Position corrections [m]", fontweight='bold')
    axis.set_title(f'Total detected signatures: {len(pos_x)}')

    if conf[Configuration.Name.OVERLAY_INPUT_POSES]:
        add_input_poses(axis, data.arams)

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)
    scatter_data(axis, pos_x, pos_y, errors, conf, MetricsData.Tag.PositionErrors,
                                                      factor=500)
    visualize_first_dc(axis, pos_x, pos_y, conf)

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")
    axis.grid()
    set_xylim(axis, conf, [min(pos_x), max(pos_x)], [min(pos_y), max(pos_y)])

    save_or_show(fig, output_folder, "position_corrections_map.png", conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_position_corrections_map)
