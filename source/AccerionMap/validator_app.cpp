/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <iomanip>
#include <iostream>

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionMap/validator.h"

int main(int argc, char *argv[]) {
    // Parse the input parameters
    if (argc != 2) {
        std::cout << "Please provide input .db file,\n"
                  << "    e.g. " << argv[0] << " <input map>" << std::endl;
        return 255;
    }

    std::string db_path{argv[1]};
    if (!accerion::api::FileExists(db_path)) {
        std::cout << "Input file does not exist." << std::endl;
        return 255;
    }

    // Perform validation on database
    accerion::map::SchemaCreator creator;

    const auto safety_result = accerion::map::Validator::ValidateSafety(db_path, creator, true);  // Perform all checks
    if (!safety_result.accepted) {
        std::cout << "The first issue found: " << std::quoted(safety_result.message) << "\n"
                  << "Validation stopped, but there could be more issues";
        return 1;
    }
    const auto result = accerion::map::Validator::Validate(db_path, creator, true);  // Perform all checks
    std::cout << "Validator is ran with all checks enabled" << std::endl;
    if (result.accepted) {
        std::cout << "No issues are found" << std::endl;
    } else {
        std::cout << "The first issue found: " << std::quoted(result.message) << "\n"
                  << "Validation stopped, but there could be more issues";
    }
    return result.accepted ? 0 : 1;
}
