/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDatabaseHandling/integrity.h"

namespace accerion {

namespace sqlite {

void SetForeignKeysConstraint(const Database& db, const bool enabled) {
    // Unfortunately, it is not possible to bind parameters to precompiled 'PRAGMA' statements.
    db.Execute(std::string("PRAGMA foreign_keys = ") + (enabled ? "ON" : "OFF"));

    const auto foreign_keys = db.ExecuteAndGetValue<bool>("PRAGMA foreign_keys;");
    if (foreign_keys != enabled) {
        std::stringstream ss;
        ss << std::boolalpha << "Failed to set foreign keys constraint to " << enabled << std::endl;
        throw std::runtime_error(ss.str());
    }
}

bool AnyForeignKeyViolations(const Database& db) {
    const auto stmt = db.PrepareStatement("PRAGMA foreign_key_check");

    // One row for each foreign key violation (https://www.sqlite.org/pragma.html#pragma_foreign_key_check)
    const auto& foreign_key_violations = stmt.GetRows<>();

    return !foreign_key_violations.empty();
}

bool IsCorrupt(const std::string& path) {
    sqlite3* connection;
    // "SQLITE_OPEN_READWRITE" is required to open db with 'hot' journal
    // (https://www.sqlite.org/atomiccommit.html#:~:text=4.2.%20Hot%20Rollback%20Journals)
    sqlite3_open_v2(path.c_str(), &connection, SQLITE_OPEN_READWRITE, nullptr);
    int rc = sqlite3_exec(connection, "PRAGMA schema_version;", nullptr, nullptr, nullptr);
    sqlite3_close_v2(connection);
    return rc != SQLITE_OK;
}

}  // namespace sqlite
}  // namespace accerion
