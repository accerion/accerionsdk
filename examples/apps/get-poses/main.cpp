/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/connection_manager.h"

#include "app_utils.h"

#include "pose_printer.h"


int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cout << "Please include sensor ip address ; e.g. getPoses 10.42.0.116" << std::endl;
        return 1;
    }
    const auto sensor_ip = accerion::api::Address::Parse({argv[1]});

    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(sensor_ip,
                                                                      accerion::api::ConnectionType::kConnectionTcp);
    auto sensor = future_sensor.WaitFor(std::chrono::seconds{1});
    if (!sensor) {
        std::cout << "Could not connect to sensor" << std::endl;
        return 1;
    }

    sensor->SetInternalMode(true);

    PosePrinter printer;
    sensor->SubscribeToOdometry().Callback(
            [&printer](const accerion::api::Odometry &p) { printer.Update(p); });
    sensor->SubscribeToCorrectedOdometry().Callback(
            [&printer](const accerion::api::CorrectedOdometry &p) { printer.Update(p); });
    sensor->SubscribeToCompensatedDriftCorrection().Callback(
            [&printer](const accerion::api::DriftCorrection &c) { printer.Update(c); });

    PauseApp();

    sensor->SetInternalMode(false);

    return 0;
}
