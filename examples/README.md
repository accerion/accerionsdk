# Overview of provided examples
The examples provided showcase the usage of different parts of the Accerion Sensor C++ API.

For more basic usage of the sensor, check the [command line interface](../tools/command-line-interface/README.md) under tools.
The internals of the tool can be treated as an advance example as well.

Below the available example applications are listed with a small description.
More information can be found in the documentation of the respective example folder in the folder `apps`.

| Example                        | Description                                                                                                                                                                          |
|:-------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| get-line-following-data        | Get line following data and print them                                                                                                                                               |
| get-poses                   	  | Get all published pose data                                                                                                                                                          |
| pose-manager-localization      | Use of the pose manager to localize with Triton using Triton's odometry and drift corrections                                                                                        |
| pose-manager-mapping-1-source  | Use of the pose manager create a map with Triton using a dummy absolute localization source (representing the output of a LiDAR SLAM package, for example)                           |
| pose-manager-mapping-2-sources | Use of the pose manager create a map with Triton using a dummy relative and absolute localization source (representing the output of a wheel odometry and lidar source, for example) |

Some applications use the example library code that can be found in the folder `libs`. Below an overview of the library classes are given.

| Library    | Description                                                                               |
|------------|-------------------------------------------------------------------------------------------|
| ExampleLib | Contains many small bits of functionality that are used by multiple example applications. |
