#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt

from sdk.data.data_container import DataContainer
from sdk.plotting.utils.plotting import save_or_show
from sdk.processing.correction_count import *
from sdk.utils.configuration import Configuration
from sdk.utils.math import rounded_ceil, rounded_floor
from sdk.utils.print import print_v

from sdk.utils.version import python_version, Version
assert python_version() >= Version("3.7"), "Assumption that dictionary keys are ordered."


def plot_correction_velocity_histogram(data: DataContainer,
                                       output_folder: Optional[Path] = None,
                                       conf: Configuration = Configuration.default(),
                                       *,
                                       verbose: bool = True):
    arams = data.arams_original  # shallow copy

    info = get_drift_correction_info_cluster(arams, conf, verbose=verbose)
    if info is None or len(info) == 0:
        print_v(verbose, "No drift correction per cluster crossing.")
        return
    velocity_data = [i.mean_velocity for i in info]

    rounding = 0.1
    min_velocity = rounded_floor(min(velocity_data), rounding)
    max_velocity = rounded_ceil(max(velocity_data), rounding)
    n_bins = int((max_velocity - min_velocity) / rounding)
    bins = list(min_velocity + v / n_bins * (max_velocity - min_velocity) for v in range(0, n_bins))

    total = len(velocity_data)

    # Combine values outside of bin range in final bin
    errors = np.clip(velocity_data, bins[0], bins[-1])

    # Generate histogram
    fig = plt.figure()
    axis = fig.subplots(1, 1)
    fig.suptitle("Distribution of velocities at cluster crossing", fontweight='bold')
    axis.set_title(f'Total detected signatures: {total}')

    axis.hist(errors, bins=bins, histtype='bar', rwidth=0.75)

    # Append '+' to last label to emphasize all values above
    x_labels = [f"{b:.1f}" for b in bins]
    x_labels[-1] += "+"
    axis.set_xticks(bins)
    axis.set_xticklabels(x_labels)

    axis.set_xlabel('velocity [m/s]')
    axis.set_ylabel("Frequency")

    save_or_show(fig, output_folder, "correction_velocity_histogram.png", conf.get_prefix(arams.get_input_files()))


if __name__ == "__main__":
    from sdk.plotting.utils.plotting import call_arams_plot_function
    call_arams_plot_function(plot_correction_velocity_histogram)
