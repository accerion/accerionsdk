# Command Line Interface (CLI)

A CLI-like application written in C++, depending on the `AccerionSensorAPI` and `AccerionUtilities`.

Basic sensor operations can be performed using this tool as will be shown in the usage section.


## Building and installing

Building should be done as part of building the AccerionSDK, which the CLI depends on.
Therefore the following steps should be performed from the repository root directory. 

```bash
mkdir -p build
cd build
cmake ..
make acc-cli
```

Note that the CLI is build by default, but could be enforced by using the compile option `-DTOOLS=ON` to the cmake command.

## Usage

The application can be called in multiple ways:
```
acc-cli
acc-cli <ip address>
acc-cli <serial number>
```

For example, the full command on the terminal could look like this:
```
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli 192.168.178.100
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli 598100500
```

When an ip address or serial number is supplied, then that sensor will be used.
When nothing is provided, then automatically the network will be searched for any sensors.
When there is only one found, then this sensor is used by the tool. This will also be mentioned by the tool.
When there are multiple sensors available, then the tool will ask the user to select one sensor.

### Commands
After this a command prompt will be shown. To quit the application, type `exit` or `quit`.
The help text showing the commands and explaining their usages can be triggered by typing `help`:
```
>> help
```
This will display the following message (excluding development commands):

```
inputs          usage                                                                       description
------------    ------------------------------------------------------------------------    ---------------------------------------------------------------------------
help                                                                                        show this message
exit, quit                                                                                  stop the program
setmode, sm     internal <on/off>                                                           set internal mode
                mapping <on/off> <cluster id, when enabling>                                set mapping mode
                cont-sig-updating <on/off>                                                  set continuous signature updating mode
                localizing <on/off>                                                         set localization mode
                line <on/off> <cluster id, when enabling>                                   set line following mode
                recording <on/off>                                                          set recording mode
                aruco <on/off>                                                              set aruco marker detection mode
                idle <on/off>                                                               set idle mode
                expert <on/off>                                                             set expert mode
reboot                                                                                      reboot the sensor, cli will stop
getfile, gf     <map/logs> [<output file/folder name/path>]                                 retrieve file
                coords [<output file/folder name/path>] [<cluster id>]                      retrieve map coordinates, default for all clusters
                loops [<output file/folder name/path>] <search radius [m]>                  retrieve loop closures
                recordings [<output file/folder name/path>] [<recording ids>]               retrieve recordings, default retrieve all recordings
sendfile, sf    map <input file name/path> [<replace, merge, update>]                       send map, default strategy replace
                update <input file name/path>                                               send asu file firmware update
get, g          areas                                                                       get information
                clusters                                                                    
                diagnostics                                                                 
                ip                                                                          
                map-info [<file name/path>]                                                 
                recordings                                                                  
                software                                                                    
set, s          area <area id>                                                              set property value
                buffer <buffered recovery buffer length [m]>                                
                connection <tcp/tcp_no_udp/udp_broad/udp_uni/udp_uni_no_hb> [<local ip>]    local ip is only needed for udp
                ip <address/netmask/gateway> <address> [...]                                
                pose <x [m]> <y [m]> <theta [deg]>                                          
recover         <x [m]> <y [m]> [<radius [m]>]                                              perform buffered recovery, no radius means full map
delete, d       area <id/all>                                                               delete one area or delete all areas
                cluster <id>                                                                delete one cluster
```
Note:
- Usage options in square brackets are optional arguments.
- File names can be in quotes (using `"` only) in case of spaces in the file path.

As an example, to retrieve the g2o file with loop closures from the sensor, then the following command can be used:
```
getfile loops /home/user/loop_closures.g2o 0.25
```
This will trigger the loop closure search with a search radius of 0.25 meters.

Regarding the output file names for the `getfile` commands:
* When a full path is provided, then that path will be used to store the file.
* When a directory is given, then a file name will be generated.
* When no argument is supplied at all, then a file name will be generated and the file will be placed in the current directory.

The file name that is generated will contain the following components:
* file identifier prefix, referring to the type of data (for example, "logs" or "map")
* sensor serial number
* date and time, formatted like "year-month-day-hours-minutes"

So a full example file name could look like "logs-598100475-2022-03-30-11-19.zip".

### Non-interactive mode
A command can also immediately be supplied to the CLI, such that the tool is non-interactive:
```
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli get software
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli 192.168.178.100 get software
user@pc:~/accerionsensorapi/build/bin$ ./acc-cli 598100500 get software
```
In this way the CLI will only execute that action and finish.
