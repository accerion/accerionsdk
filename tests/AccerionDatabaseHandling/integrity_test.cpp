/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionDatabaseHandling/integrity.h"
#include "AccerionUtilities/temp_file_system.h"

class SqliteIntegrityTest : public ::testing::Test {
 protected:
    SqliteIntegrityTest() : temp_files_ {}, db_ {CreateDatabase(temp_files_.Create())} {}

    TempFileSystem temp_files_;
    const accerion::sqlite::Database db_;

 private:
    static accerion::sqlite::Database CreateDatabase(const std::string& path) {
        accerion::sqlite::Database db {path, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE};
        // Table 'data_1' that has 'id' column as primary key
        db.Execute(R"SQL(
            CREATE TABLE IF NOT EXISTS data_1 (
                id      INTEGER PRIMARY KEY,
                value   TEXT)
        )SQL");

        db.Execute(R"SQL(
            INSERT INTO data_1
                (id) VALUES (0)
        )SQL");
        return db;
    }
};

TEST_F(SqliteIntegrityTest, ForeignKeyViolation){
    // Create table 'data_2' with foreign key constraint
    db_.Execute(R"SQL(
        CREATE TABLE IF NOT EXISTS data_2 (
            id              INTEGER,
            FOREIGN KEY(id) REFERENCES data_1(id))
    )SQL");

    // Disable constraint in order to violate foreign keys constraint
    accerion::sqlite::SetForeignKeysConstraint(db_, false);

    // Insert record with non-existent foreign key parent
    EXPECT_NO_THROW(db_.Execute(R"SQL(
        INSERT INTO data_2 (id)
        VALUES (1)
    )SQL"));
    
    EXPECT_TRUE(accerion::sqlite::AnyForeignKeyViolations(db_));
}