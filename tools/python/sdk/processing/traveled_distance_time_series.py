#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from itertools import accumulate
from typing import List, Tuple

from pandas import isna

from sdk.graph.graph import Pose
from sdk.processing.time_series import TimeSeries


class TraveledDistanceTimeSeries(TimeSeries):
    """
    Create a time series of the distance traveled, such that time-based look up is possible.
    If the time value is not exactly present in the series, then interpolation is performed.
    To be used like:
        LRT = arams.get_type("LRT")
        df_lrt = arams[LRT]
        traveled_distances = TraveledDistanceTimeSeries(df_lrt[LRT.TIME].to_list(),
                                                        [Pose(x, y) for x, y in zip(df_lrt[LRT.CORRECTED_ODOM_POSE_X],
                                                                                    df_lrt[LRT.CORRECTED_ODOM_POSE_Y])])
        traveled_distance = traveled_distances[time]
    """

    def __init__(self, time_values: List[float], pose_values: List[Pose]):
        assert len(time_values) == len(pose_values)

        times, distances = self.__calculate_traveled_distances(time_values, pose_values)
        super().__init__(times, distances)

    def __getitem__(self, t: float) -> float:
        result = super().__getitem__(t)
        return result.value_before if result.ratio == 0 else result.value_after if result.ratio == 1 else \
            result.value_before + result.ratio * (result.value_after - result.value_before)

    @staticmethod
    def __calculate_traveled_distances(times: List[float], poses: List[Pose]) -> Tuple[List[float], List[float]]:
        result_times = []
        result_poses = []

        # Filter out the "not a number" values
        for t, p in zip(times, poses):
            if isna(p.x) or isna(p.y):
                continue
            result_times.append(t)
            result_poses.append(p)

        if len(result_poses) == 0:
            return result_times, []

        relative_distances = [Pose(curr_pose.x - prev_pose.x, curr_pose.y - prev_pose.y).norm
                              for prev_pose, curr_pose in zip(result_poses[:-1], result_poses[1:])]
        relative_distances.insert(0, 0)
        return result_times, list(accumulate(relative_distances))
