#!/usr/bin/env python3
"""
 Copyright (c) 2022, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""


def parse_float(s: str):
    parts = s.split(' ')
    values = []
    for part in parts:
        if is_digit(part):
            values.append(float(part))
    return values


def is_digit(x):
    try:
        float(x)
        return True
    except ValueError:
        return False