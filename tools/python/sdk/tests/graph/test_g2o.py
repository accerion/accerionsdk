#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.graph.g2o_reader import Reader
from sdk.graph.g2o_writer import Writer

from sdk.tests.test_data.files import TestData


def test_g2o_roundtrip(tmpdir):
    input_file_path = TestData.get(TestData.File.POSE_GRAPH)
    input_graph = Reader.read(input_file_path)

    input_file_copy_path = tmpdir / (input_file_path.stem + "_copy" + input_file_path.suffix)
    Writer.write(input_graph, str(input_file_copy_path))

    output_graph = Reader.read(input_file_copy_path)

    assert input_graph == output_graph
