/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionDatabaseHandling/prepared_statement.h"
#include <iostream>
#include "AccerionDatabaseHandling/database.h"

namespace accerion {

namespace sqlite {

PreparedStatement::PreparedStatement(const Database& db, const std::string& query)
        : database_{db.GetPtr()}, statement_{nullptr, SqliteStatementPtrDeleter {database_}} {
    sqlite3_stmt* statement = nullptr;
    const auto return_code = sqlite3_prepare_v2(database_, query.c_str(), static_cast<int>(query.size()),
                                                &statement, nullptr);
    if (return_code != SQLITE_OK) {
        std::stringstream ss;
        ss << "Prepare statement error\n"
           << "  error: " << sqlite3_errmsg(database_) << "\n"
           << "  query: " << query << "\n";
        throw std::runtime_error(ss.str());
    }
    statement_.reset(statement);
}

void PreparedStatement::Reset() const {
    sqlite3_reset(statement_.get());
    sqlite3_clear_bindings(statement_.get());
}

bool PreparedStatement::Step() const {
    last_return_code_ = sqlite3_step(statement_.get());
    if (last_return_code_ == SQLITE_MISUSE) {
        std::stringstream ss;
        ss << "Prepare statement stepping misuse detected\n"
           << "  error: " << sqlite3_errmsg(database_) << "\n"
           << "  query: " << sqlite3_sql(statement_.get()) << "\n";
        throw std::runtime_error(ss.str());
    }
    return last_return_code_ != SQLITE_DONE | last_return_code_ == SQLITE_ROW;
}

int PreparedStatement::LastReturnCode() const { return last_return_code_; }

PreparedStatement::SqliteStatementPtrDeleter::SqliteStatementPtrDeleter(sqlite3* database)
        : database_{database} {}

void PreparedStatement::SqliteStatementPtrDeleter::operator()(sqlite3_stmt* statement) const {
    const auto return_code = sqlite3_finalize(statement);
    if (return_code != SQLITE_OK) {
        std::cerr << "Finalize statement error\n"
                  << "  error: " << sqlite3_errmsg(database_) << "\n"
                  << "  query: " << sqlite3_sql(statement) << std::endl;
    }
}

}  // namespace sqlite
}  // namespace accerion
