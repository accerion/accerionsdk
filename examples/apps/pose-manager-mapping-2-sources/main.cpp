/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/connection_manager.h"

#include "AccerionPoseManager/pose_manager.h"

#include "app_utils.h"
#include "pose_manager_examples.h"


// Typical use case where a map is created for Triton using two pose sources:
//   - relative (like wheel odometry)
//   - absolute (like LiDAR)


using PoseManager = accerion::pose_manager::PoseManagerBuffered<accerion::api::Pose, accerion::api::TimePoint>;


PoseManager CreatePoseManager(const PoseManager::Id& id_sensor) {
    // Robot coordinate frame
    PoseManager::Id id_robot = "robot";
    PoseManager::Id id_rel_sensor = "rel. sensor";
    PoseManager::Id id_abs_sensor = "abs. sensor";

    PoseManager::StaticPoseTree robot_description{};
    robot_description.Set(id_sensor, id_robot, accerion::api::Pose{0., 0., 0.});
    robot_description.Set(id_rel_sensor, id_robot, accerion::api::Pose{0., 0., 0.});
    robot_description.Set(id_abs_sensor, id_robot, accerion::api::Pose{0., 0., 0.});

    PoseManager::PoseTreeType::Container default_buffer{std::chrono::seconds{100}};
    return {std::move(robot_description),
            std::move(id_robot), std::move(id_abs_sensor), std::move(id_rel_sensor),
            default_buffer};
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Please include the sensor ip address, so '" << argv[0] << " 192.168.0.116'" << std::endl;
        return 1;
    }

    // Create the PoseManager instance (define first, because sensor need to be destructed first
    const PoseManager::Id id_sensor = "triton";
    auto pose_manager = CreatePoseManager(id_sensor);

    // Connect to the sensor
    const auto sensor_ip = accerion::api::Address::Parse({argv[1]});
    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(sensor_ip,
                                                                      accerion::api::ConnectionType::kConnectionTcp);
    auto sensor = future_sensor.WaitFor(std::chrono::seconds{1});
    if (!sensor) {
        std::cout << "Could not connect to sensor" << std::endl;
        return 1;
    }

    // Make sure the sensor is configured properly, sensor being in the right state and potentially deleting the
    // existing cluster that will be used for mapping
    const accerion::api::AreaId area_id{25};
    const accerion::api::ClusterId cluster_id{35};
    ConfigureSensor(*sensor, area_id, cluster_id);

    // Mimic pose sensor by setting up relative and absolute pose generators
    // The frequencies used much smaller than typical pose source frequencies, such that the console output is readable
    constexpr double radius{5};  // [meter]
    constexpr double total_duration{10};  // [seconds]
    constexpr double rel_pose_frequency{10};  // [Hertz]
    constexpr double abs_pose_frequency{10. / 8.};  // [Hertz]

    constexpr auto circumference{2 * accerion::api::kPi * radius};  // [meter]
    const auto rel_pose_steps = static_cast<std::size_t>(total_duration * rel_pose_frequency);
    // Intentionally introduce an error factor in the pose step angle, to mimic typical odometry drift
    const auto rel_pose_generator = DummyPoseGenerator(accerion::api::Pose::Identity(),
                                                       {circumference / rel_pose_steps, 0., 360. / rel_pose_steps * .8},
                                                       rel_pose_steps - 1);

    const auto abs_pose_steps = static_cast<std::size_t>(total_duration * abs_pose_frequency);
    const auto abs_pose_generator = DummyPoseGenerator({radius, 0., 90.},
                                                       {0., 0., 360. / abs_pose_steps},
                                                       abs_pose_steps);

    // State controlling to detect when to start mapping
    std::atomic_bool abs_pose_available{false};
    std::atomic_bool sensor_localized{false};

    // Link up the pose generators to the pose manager and sensor
    auto rel_pose_call_back =
            [id_sensor, &pose_manager, &sensor, &abs_pose_available, &sensor_localized]
            (const auto& timestamp, const auto& pose) {
                std::stringstream ss;
                ss << "relative pose received " << pose << "\n";

                pose_manager.SetLocal(pose, timestamp);
                ss << "  pose manager updated using SetLocal\n";

                accerion::api::ExternalReferencePose ext_ref{};
                ext_ref.time_offset = std::chrono::microseconds{0};
                ext_ref.pose = pose_manager.Get(PoseManager::global_id, id_sensor, timestamp);
                sensor->SetExternalReference(ext_ref);
                ss << "  sensor pose set to " << ext_ref.pose << "\n";

                if (abs_pose_available) {
                    sensor_localized = true;
                }

                std::cout << ss.str() << std::endl;
            };
    auto rel_pose_source_future =
            CreatePoseSource(rel_pose_generator, rel_pose_frequency, std::move(rel_pose_call_back));

    auto abs_pose_call_back = [&pose_manager, &abs_pose_available](const auto& timestamp, const auto& pose) {
        std::stringstream ss;
        ss << "absolute pose received " << pose << "\n";

        pose_manager.SetGlobal(pose, timestamp);
        abs_pose_available = true;
        ss << "  pose manager updated using SetGlobal\n";

        std::cout << ss.str() << std::endl;
    };
    auto abs_pose_source_future =
            CreatePoseSource(abs_pose_generator, abs_pose_frequency, std::move(abs_pose_call_back));

    // When an absolute pose is received and send to the sensor, then the mapping can be started
    while (!sensor_localized) {
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
    }
    StartMapping(*sensor, cluster_id);

    rel_pose_source_future.wait();
    abs_pose_source_future.wait();

    StopMapping(*sensor);

    return 0;
}
