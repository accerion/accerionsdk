/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionDatabaseHandling/database.h"

namespace accerion {

namespace sqlite {

void SetForeignKeysConstraint(const Database& db, const bool enabled);

bool AnyForeignKeyViolations(const Database& db);

bool IsCorrupt(const std::string& path);

}  // namespace sqlite
}  // namespace accerion
