#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict, List, NamedTuple,  Union

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.lines as mlines

from sdk.graph.graph import Graph, Pose


__all__ = ["Name", "PlotGraphsSettings", "PlotElements", "add_graph", "add_legend"]


class Name(Enum):
    VERTICES = "vertices"
    EDGES_GEOM = "edges_geom"
    EDGES_REL = "edges_rel"


@dataclass
class PlotGraphsSettings:
    color: str
    style: Any
    size: int

    HEADING_LINE_SIZE = 0.001

    # Line style sequences denoted as (draw, don't draw, ...)
    DASHED = (0, (5, 5))
    LOOSELY_DOTTED = (0, (1, 5))

    @classmethod
    def create(cls, size: int):
        assert (1 <= size <= 3), "Currently only supporting between 1 and 3 graphs"
        return [cls.__set_1(), cls.__set_2(), cls.__set_3()][:size]

    @classmethod
    def __set_1(cls):
        return {
            Name.VERTICES:   cls('r', 'o', 4)._get_plt_dict_vertices(),  # r = red
            Name.EDGES_GEOM: cls('k', '-', 1)._get_plt_dict_edges(),     # k = black
            Name.EDGES_REL:  cls('k', cls.LOOSELY_DOTTED, 1)._get_plt_dict_edges()      # k = black
        }

    @classmethod
    def __set_2(cls):
        return {
            Name.VERTICES:   cls('g', 'o', 3)._get_plt_dict_vertices(),  # g = green
            Name.EDGES_GEOM: cls('b', '-', 1)._get_plt_dict_edges(),     # b = blue
            Name.EDGES_REL:  cls('b', cls.LOOSELY_DOTTED, 1)._get_plt_dict_edges()      # b = blue
        }

    @classmethod
    def __set_3(cls):
        return {
            Name.VERTICES:   cls('y', 'o', 2)._get_plt_dict_vertices(),  # y = yellow
            Name.EDGES_GEOM: cls('m', '-', 1)._get_plt_dict_edges(),     # m = magenta
            Name.EDGES_REL:  cls('m', cls.LOOSELY_DOTTED, 1)._get_plt_dict_edges()      # m = magenta
        }

    def _get_plt_dict_vertices(self) -> Dict:
        return {
            "linestyle": ' ',
            "markerfacecolor": self.color,
            "markeredgecolor": self.color,
            "marker": self.style,
            "markersize": self.size
        }

    def _get_plt_dict_edges(self) -> Dict:
        return {
            "color": self.color,
            "linestyle": self.style,
            "linewidth": self.size
        }


class PlotElements(NamedTuple):
    names: List[str] = []
    settings: List[Dict] = []
    handles: List[List[Any]] = []

    def append(self, n: str, s: Dict, h: Union[Any, List[Any]]):
        if not isinstance(h, list):
            h = [h]
        self.names.append(n)
        self.settings.append(s)
        self.handles.append(h)

    def __str__(self):
        """Handy for debugging"""
        result = "PlotElements[\n"
        for n, s, h in zip(self.names, self.settings, self.handles):
            result += f"  {n}, {s}, {h}\n"
        result += "]\n"
        return result


def add_graph(ax, graph: Graph, settings: dict, *,
              draw_vertices: bool = True, draw_heading: bool = False) -> List[Any]:
    """
    Function plots the graph vertices and edges in the supplied axis, note that filtering should be done by caller.
    For example, if only the loop closure edges should be displayed, then the supplied graph should only contain those.
    """
    handles = []

    vertex_id_to_index = {v.id: i for i, v in enumerate(graph.vertices)}

    heading_line = Pose(x=PlotGraphsSettings.HEADING_LINE_SIZE, y=0, th=0)

    # Draw vertices
    if draw_vertices and graph.vertices:
        vertices = np.array([[v.pose.x, v.pose.y] for v in graph.vertices])
        handles.extend(ax.plot(vertices[:, 0], vertices[:, 1], **settings[Name.VERTICES]))
        if draw_heading:
            vertices_heads = np.zeros((len(graph.vertices), 2, 2))
            vertices_heads[:, 0, :] = vertices

            heads = [v.pose * heading_line for v in graph.vertices]
            vertices_heads[:, 1, 0] = [h.x for h in heads]
            vertices_heads[:, 1, 1] = [h.y for h in heads]

            handles += tuple([ax.add_collection(LineCollection(vertices_heads,
                                                               color=settings[Name.VERTICES]["markerfacecolor"],
                                                               linestyle=PlotGraphsSettings.DASHED,
                                                               linewidths=1))])

    # Draw edges
    n_edges = len(graph.edges)
    # Array containing all line segments, consisting of a start and end points, where each point has two coordinates,
    # so the array is used like "array[line_index, point_index, coordinate_index]"
    edges_rel = np.zeros((n_edges, 2, 2))
    edges_geom = np.zeros((n_edges, 2, 2))
    edges_heads = np.zeros((n_edges, 2, 2))
    for index, edge in enumerate(graph.edges):
        p_a = graph.vertices[vertex_id_to_index[edge.id_begin]].pose  # pose a
        p_b = graph.vertices[vertex_id_to_index[edge.id_end]].pose  # pose b
        p_b_e = p_a * edge.pose  # pose b from applying the edge/pose change to pose a

        # Define line segments
        # Define starting point of line segment
        edges_rel[index, 0, :] = [p_a.x, p_a.y]
        edges_geom[index, 0, :] = [p_a.x, p_a.y]

        # Define ending point of line segment
        edges_rel[index, 1, :] = [p_b.x, p_b.y]
        edges_geom[index, 1, :] = [p_b_e.x, p_b_e.y]

        if draw_heading:
            p_b_e_head = p_b_e * heading_line
            edges_heads[index, 0, :] = [p_b_e.x, p_b_e.y]
            edges_heads[index, 1, :] = [p_b_e_head.x, p_b_e_head.y]

    handles += tuple([ax.add_collection(LineCollection(edges_rel, **settings[Name.EDGES_REL]))])
    handles += tuple([ax.add_collection(LineCollection(edges_geom, **settings[Name.EDGES_GEOM]))])
    if draw_heading:
        handles += tuple([ax.add_collection(LineCollection(edges_heads,
                                                           color=settings[Name.EDGES_REL]["color"],
                                                           linestyle=PlotGraphsSettings.DASHED,
                                                           linewidths=1))])

    return handles


def add_legend(fig, ax, plot_elements: PlotElements):
    # Method below to get the correct legend: https://matplotlib.org/2.0.2/users/legend_guide.html
    handles = []
    for s in plot_elements.settings:
        legend_settings = s[Name.VERTICES]
        legend_settings.update(s[Name.EDGES_GEOM])
        handles.append(mlines.Line2D([], [], **legend_settings))
    legend1 = plt.legend(handles, plot_elements.names, loc='upper right')
    ax.add_artist(legend1)

    # Add visibility toggling functionality
    legend1_items = legend1.get_lines()
    for item in legend1_items:
        item.set_picker(True)
        item.set_pickradius(10)
    legend_plots_map = dict(zip(legend1_items, plot_elements.handles))
    fig.canvas.mpl_connect('pick_event', lambda event: __toggle_visibility(legend_plots_map, event))

    # Add separate legend for distinction of line types
    handles = [
        mlines.Line2D([], [], **plot_elements.settings[0][Name.EDGES_REL]),
        mlines.Line2D([], [], **plot_elements.settings[0][Name.EDGES_GEOM])
    ]
    legend2 = plt.legend(handles, ["actual/relational", "expectation/geometrical"], loc='lower right')
    ax.add_artist(legend2)


def __toggle_visibility(map_legend_item_to_handle, event):
    # Toggling functionality based on:
    # https://learndataanalysis.org/source-code-how-to-toggle-graphs-visibility-by-clicking-legend-label-in-matplotlib/
    legend_item = event.artist

    # Filter event from other parts of the figure (in case of multiple plots in one figure)
    if legend_item in map_legend_item_to_handle:
        plot_handles = map_legend_item_to_handle[legend_item]
        if len(plot_handles) != 0:
            new_visibility = not plot_handles[0].get_visible()
            [h.set_visible(new_visibility) for h in plot_handles]
            legend_item.set_alpha(1.0 if new_visibility else 0.2)

            event.canvas.draw_idle()
