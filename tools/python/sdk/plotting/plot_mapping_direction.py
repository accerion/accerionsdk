#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
import pandas as pd

from sdk.geometry.interval import Interval
from sdk.plotting.utils.plotting import *
from sdk.data.data_container import DataContainer
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v


def plot_mapping_direction(data: DataContainer,
                           output_folder: Optional[Path] = None,
                           conf: Configuration = Configuration.default(),
                           *,
                           verbose: bool = True):
    if not data.signatures:
        print_v(verbose, __name__,
                ": No signature information was provided (no or empty floor map coordinates or map .db file)")
        return

    fig, axis = plt.subplots(nrows=1, ncols=1, figsize=[14, 6])
    fig.suptitle("Mapping direction")

    axis.set_xlabel("X-axis [m]")
    axis.set_ylabel("Y-axis [m]")

    cluster_ids_distinct = sorted(set(s.cluster_id for s in data.signatures))

    # Ideally the index in cluster should be used to create this plot, but if not available, then use the signature id
    use_ids = any(s.index_in_cluster is None for s in data.signatures)

    cax = None
    for cluster_id in cluster_ids_distinct:
        cluster_signatures = [s for s in data.signatures if s.cluster_id == cluster_id]
        x_pos = [s.pose.x for s in cluster_signatures]
        y_pos = [s.pose.y for s in cluster_signatures]
        indices = [s.id if use_ids else s.index_in_cluster for s in cluster_signatures]

        cax = axis.scatter(x_pos, y_pos, c=indices, marker="s", cmap=COLOR_MAP_GYR.name)

    cbar = fig.colorbar(cax)
    cbar.ax.set_title('Cluster')
    cbar.set_ticks(cbar.ax.get_ylim(), labels=['start', 'end'])

    axis.set_aspect('equal', adjustable='datalim')
    axis.grid()

    save_or_show(fig, output_folder, "mapping_direction.png", conf.get_prefix(data.arams.get_input_files()))


if __name__ == "__main__":
    call_arams_plot_function(plot_mapping_direction)
