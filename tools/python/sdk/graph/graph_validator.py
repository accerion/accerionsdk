#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from enum import Enum
from pathlib import Path
from typing import Tuple

from sdk.graph.g2o_reader import Reader
from sdk.graph.g2o_writer import Writer
from sdk.graph.validator.connectivity import Connectivity
from sdk.graph.validator.duplicate_edge import DuplicateEdge


class GraphValidator:
    class Result(Enum):
        Valid = "Valid"
        Invalid = "Invalid"
        NewFile = "NewFile"

    @staticmethod
    def validate(input_file: str, correct_graph: bool) -> Tuple[Result, str]:
        # Read the graph
        input_file = Path(input_file)
        graph = Reader.read(input_file)

        # Create all validation objects
        validators = [Connectivity(), DuplicateEdge()]

        # Use all validators to check the graph validity
        print("Validate graph...")
        validator_results = []
        for v in validators:
            print(f"  - {type(v).__name__}: ", end='')
            validator_results.append(v.is_valid(graph))
            print("valid" if validator_results[-1] else "not valid")

        # If all validators found the graph valid, then done
        if all(validator_results):
            return GraphValidator.Result.Valid, ""

        # Some validators found the graph to be invalid, should the graph be corrected?
        apply_correction = False
        if correct_graph:
            print("Automatic correction of the graph.")
            apply_correction = True
        else:
            while True:
                print("Do you want to correct the graph? [y/n]", end='   ')
                answer = input().lower()
                if answer not in ['y', 'n']:
                    print("    Invalid input")
                    continue
                apply_correction = answer == 'y'
                break

        # Correct the graph when desired
        if apply_correction:
            for validator_result, validator in zip(validator_results, validators):
                if not validator_result:  # not valid
                    graph = validator.correct(graph)

            new_path = input_file.parent / (input_file.stem + "_corrected" + input_file.suffix)
            Writer.write(graph, new_path)
            print(f"Corrected g2o file written to: {new_path}")
            return GraphValidator.Result.NewFile, str(new_path)
        else:
            return GraphValidator.Result.Invalid, ""
