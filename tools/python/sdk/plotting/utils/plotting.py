#!/usr/bin/env python3
"""
 Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from dataclasses import dataclass
from enum import Enum
from math import ceil, floor
from os.path import join
from pathlib import Path
from typing import Dict, Callable, List, Optional, Tuple
from warnings import warn

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgb
from matplotlib.ticker import FormatStrFormatter
import numpy as np

from sdk.data.data_container import DataContainer
from sdk.data.signature import Signature
from sdk.metrics.metrics import ComparisonType, MetricsData, MetricDataAggregation, Metrics
from sdk.utils.argument_parser import ArgumentParser, create_default_arg_parser, add_prefix, add_spec
from sdk.utils.configuration import Configuration
from sdk.utils.path import ensure_directory
from sdk.utils.preparations import process_args


def create_default_plotting_arg_parser() -> ArgumentParser:
    parser = create_default_arg_parser(output_required=False)
    add_prefix(parser, False)
    add_spec(parser, False)
    return parser


def parse_plot_arguments() -> Dict:
    parser = create_default_plotting_arg_parser()
    return vars(parser.parse_args())


def call_arams_plot_function(plot_function: Callable, args: Optional[Dict] = None) -> None:
    """
    Call the supplied plotting function with the expected signature:
        plot_function(data: DataContainer, output_folder: Optional[str], conf: Configuration = Configuration.default())

    The arguments are assumed to be the ones parsed using the function above 'parse_plot_arguments'.
    """
    if not args:
        args = parse_plot_arguments()
    conf, input_files, output_folder = process_args(args)
    data = DataContainer.create(input_files, conf)

    plot_function(data, output_folder, conf)


def save_or_show(fig, output_folder: Optional[Path] = None, file_name: Optional[str] = None, prefix: Optional[str] = None):
    if output_folder is not None and file_name is not None:
        ensure_directory(output_folder)
        if prefix is not None and len(prefix) != 0:
            file_name = prefix + "_" + file_name
        fig.savefig(join(output_folder, file_name))
        plt.close(fig)
    else:
        # keep it to plt.show(), fig.show() causes the images to close immediately in interactive mode (-o omitted from start command)
        plt.show()


def add_signature_poses(axis, signatures: List[Signature], color=(.5, .5, .7)):
    """
    Add map coordinates to the current plot, when data is available.

    :param axis: Axis object in which the data should be plotted
    :param signatures: List of signatures to be plotted
    :param color: Colour to plot the coordinates in, default is blueish-grey
    """
    if not signatures:
        return

    # Scale floor map visualization based on number of signatures in map
    return axis.scatter([s.pose.x for s in signatures], [s.pose.y for s in signatures],
                        color=color, s=ceil(15000/len(signatures)))

# Define color constants based on Color Universal Design (colorblind-friendly)
class Colors(Enum):
    DARK_GREEN = (0.0, 0.45, 0.70)  # Deep Blue-Green (#0072B2)
    GREEN = (0.0, 0.6, 0.0)  # True Green (#009E73)
    LIGHT_GREEN = (0.5, 0.8, 0.5)  # Soft Green (#8FBC8F)
    YELLOW = (0.94, 0.90, 0.26)  # Gold Yellow (#F0E442)
    ORANGE = (0.91, 0.60, 0.00)  # Light Orange (#E69F00)
    RED = (0.84, 0.28, 0.15)  # Vermilion Red (#D73027)
    DARK_RED = (0.50, 0.00, 0.00)  # Dark Brown Red (#7F0000)
    BLUE = (0.12, 0.56, 1.00)  # Dodger Blue (#1E90FF)
    BLACK = (0., 0., 0.)  # Black
    GREY = (0.5, 0.5, 0.5)  # Grey (Medium Gray)

def add_input_poses(axis, arams, color=Colors.GREY.value):
    """
    Add input poses from LRT to the current plot, when data is available.

    :param arams: Arams object used for accessing the input poses
    """
    if "LRT" not in arams:
        print('Could not find logRuntimeTrackingFast file or the arams version does not support the file type.')
        return

    LRT = arams.LRT
    df_lrt = arams[LRT]

    # Plot input poses
    x, y = drop_nan(df_lrt[LRT.INPUT_POSE_X.value], df_lrt[LRT.INPUT_POSE_Y.value])
    return axis.plot(x.to_numpy(), y.to_numpy(), color=color, marker='None', linestyle='dashed', label="InputPose")


@dataclass
class ColorMap:
    name: str
    colors: List[tuple]

COLOR_MAP_GYR = ColorMap("Custom color map gyr", [Colors.GREEN.value, Colors.YELLOW.value, Colors.RED.value])
COLOR_MAP_RYG = ColorMap("Custom color map ryg", list(reversed(COLOR_MAP_GYR.colors)))
COLOR_MAP_RB = ColorMap("Custom color map rb", [Colors.RED.value, Colors.BLACK.value])
COLOR_MAP_BR = ColorMap("Custom color map br", list(reversed(COLOR_MAP_RB.colors)))
COLOR_MAPS = [COLOR_MAP_GYR, COLOR_MAP_RYG, COLOR_MAP_RB, COLOR_MAP_BR]

def _register_heat_maps():
    from matplotlib.colors import LinearSegmentedColormap

    for cmap in COLOR_MAPS:
        # Create a colormap object
        colors = [to_rgb(c) for c in cmap.colors]
        map_object = LinearSegmentedColormap.from_list(cmap.name, colors)

        # Register this new color map with matplotlib
        if cmap.name not in mpl.colormaps:
            mpl.colormaps.register(name=cmap.name, cmap=map_object)
_register_heat_maps()  # Actually run it once


DEFAULT_BIN_SIZE = 0.1


def create_binned_data(xs: List[float], ys: List[float], data: List[float], *,
                       bin_size: float = DEFAULT_BIN_SIZE, aggregation_method: Callable = max):
    """
    Add heat map based on the three arrays containing the x, y and values to be aggregated.
    When there are no data point in a bin, then the array wil contain the value np.nan.

    The aggregation_method parameter could min, max, mean, etc.
    """

    x_min = min(xs)
    x_max = max(xs)
    y_min = min(ys)
    y_max = max(ys)

    cols = ceil((x_max - x_min) / bin_size) + 1
    rows = ceil((y_max - y_min) / bin_size) + 1

    def bin_index(x_pos, y_pos):
        return (
            floor((x_pos - x_min) / bin_size),
            floor((y_pos - y_min) / bin_size)
        )

    aggregation_info: Dict[Tuple[int, int], List[float]] = {}
    for x, y, value in zip(xs, ys, data):
        key = bin_index(x, y)
        if key not in aggregation_info:
            aggregation_info[key] = []
        aggregation_info[key].append(value)

    array_agg = np.full([rows, cols], np.nan, dtype=float)
    for key, values in aggregation_info.items():
        array_agg[key[1], key[0]] = aggregation_method(values)

    # The indexing for the y coordinate is 0, 1, 2, ... going from top to bottom, which is reverse to the natural y
    # direction, therefore flip array vertically
    return np.flipud(array_agg)


def extract_argument(name, kwargs, default=None):
    value = default
    if name in kwargs:
        value = kwargs[name]
        kwargs.pop(name)
    return value


def add_heat_map(axis, xs, ys, array, *args, **kwargs):
    reverse = extract_argument("reverse", kwargs, False)
    color_map = COLOR_MAP_RYG if reverse else COLOR_MAP_GYR

    min_colorbar = extract_argument("min_colorbar", kwargs)
    if min_colorbar is not None:
        kwargs["vmin"] = min_colorbar
    max_colorbar = extract_argument("max_colorbar", kwargs)
    if max_colorbar is not None:
        kwargs["vmax"] = max_colorbar
    if min(xs) == max(xs) or min(ys) == max(ys):
        extent = None
    else:
        extent = (min(xs), max(xs), min(ys), max(ys))
    return axis.imshow(array, extent=extent, cmap=color_map.name, *args, **kwargs)

def add_values(axis, xs, ys, values, factor = 20, *args, **kwargs):
    reverse = extract_argument("reverse", kwargs, False)
    color_map = COLOR_MAP_RYG if reverse else COLOR_MAP_GYR

    min_colorbar = extract_argument("min_colorbar", kwargs)
    if min_colorbar is not None:
        kwargs["vmin"] = min_colorbar
    max_colorbar = extract_argument("max_colorbar", kwargs)
    if max_colorbar is not None:
        kwargs["vmax"] = max_colorbar

    if len(values) != 0:
        values, xs, ys = zip(*sorted(zip(values, xs, ys), reverse=reverse))

    return axis.scatter(xs, ys, c=values, s=[max(1, v * factor) for v in values], cmap=color_map.name, **kwargs)


def set_xylim(axis, conf, lim_x, lim_y):
    """
    if manual mode: Zoom in to area defined in config file. If all values of xlim and ylim are 0 then keep the limits as they are. 
    if auto mode: Zoom in automatically to the region with drift corrections
    """

    roi = conf[Configuration.Name.ZOOM_TO_REGION_OF_INTEREST]
    base_modes = ['manual', 'auto', 'equal']

    def set_aspect_equal():
        axis.set_aspect('equal', adjustable='datalim')

    if not any([x in roi['mode'] for x in base_modes]):
        warn("Unknown mode for 'Zoom to region of interest' parameter. Falling back to 'equal' mode.")
        set_aspect_equal()
        return
    
    if 'manual' in roi['mode']:
        if not all(v == 0 for v in roi['xlim']) and \
           not all(v == 0 for v in roi['ylim']):
            axis.set_xlim(roi['xlim'])
            axis.set_ylim(roi['ylim'])
        else:
            set_aspect_equal()
    if 'auto' in roi['mode']:
        extra_space = 0.5
        axis.set_xlim([lim_x[0]-extra_space, lim_x[1]+extra_space])
        axis.set_ylim([lim_y[0]-extra_space, lim_y[1]+extra_space])
    if 'equal' in roi['mode']:
        set_aspect_equal()


def plot_outliers(axis, pos_x, pos_y, data, metrics: Metrics, tag: MetricsData.Tag, *,
                  factor=20, reverse=False, min_colorbar = None, max_colorbar = None):
    if min_colorbar is None:
        min_colorbar = metrics.get_constraint_value(tag, MetricDataAggregation.Type.Minimum, ComparisonType.Greater)
    if min_colorbar is None:
        min_colorbar = min(data)

    if max_colorbar is None:
        max_colorbar = metrics.get_constraint_value(tag, MetricDataAggregation.Type.Maximum, ComparisonType.Smaller)
    if max_colorbar is None:
        max_colorbar = max(data)

    pos_x = np.array(pos_x)
    pos_y = np.array(pos_y)
    data = np.array(data)
    outliers_ind = (min_colorbar > data) | (data > max_colorbar)
    kwargs = {"vmin": min_colorbar, "vmax": max_colorbar, 'reverse': reverse}
    handle = add_values(axis, 
                        pos_x[~outliers_ind], 
                        pos_y[~outliers_ind], 
                        data[~outliers_ind], factor=factor, **kwargs)
                        
    outliers_cax, data_cax = _create_cbar_axes(axis, reverse) if any(outliers_ind) else (None, None)
    data_cbar_handle = _plot_data_cbar(axis, handle, min_colorbar, max_colorbar, data_cax=data_cax)

    if any(outliers_ind):
        outliers_cmap = COLOR_MAP_BR if reverse else COLOR_MAP_RB
        outliers_handle = axis.scatter(pos_x[outliers_ind], pos_y[outliers_ind], c=data[outliers_ind], 
                                       cmap=outliers_cmap.name, marker = "x")
        _plot_outliers_cbar(axis, outliers_handle, outliers_cax, data[outliers_ind])
        
    return handle, data_cbar_handle, min_colorbar, max_colorbar


def _create_cbar_axes(axis, reverse):
    """
    This function creates two new axes on the figure - one for the data color bar and one for the outliers color bar.
    """
    fig = axis.get_figure()

    # leave space on right for colorbar and label
    fig.subplots_adjust(left=0.125, right=0.8, top=0.9, bottom=0.1)

    axis_pos = axis.get_position()
    cbar_width = 0.03
    cbar_height = axis_pos.height
    cbar_x0 = axis_pos.x1 + cbar_width
    outliers_ratio = 0.3
    data_ratio = 1 - outliers_ratio

    # if reverse place outlier colorbar on bottom else place at end of data cbar
    outliers_cbar_y0 = axis_pos.y0 if reverse else axis_pos.y0 + cbar_height * data_ratio + 0.05
    outliers_cax = fig.add_axes([cbar_x0, outliers_cbar_y0, cbar_width, cbar_height * outliers_ratio])

    # data colorbar axis
    data_cbar_y0 = axis_pos.y0 + cbar_height * outliers_ratio + 0.05 if reverse else axis_pos.y0
    data_cax = fig.add_axes([cbar_x0, data_cbar_y0, cbar_width, cbar_height * data_ratio])

    return outliers_cax, data_cax


def _plot_outliers_cbar(axis, outliers_handle, outliers_cax, outliers):
    fig = axis.get_figure()

    # outlier colorbar
    num_outliers = len(outliers)

    outliers_cbar = fig.colorbar(outliers_handle, cax=outliers_cax,
                                 ticks=np.linspace(min(outliers), max(outliers), min(num_outliers, 5)))
    outliers_cbar.ax.set_ylabel("Outliers, num: "+str(num_outliers), rotation=90)
    outliers_cbar.ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))


def _plot_data_cbar(axis, handle, min_colorbar, max_colorbar, data_cax=None):
    fig = axis.get_figure()
    
    data_cbar_handle = fig.colorbar(handle, ax=axis, cax=data_cax, ticks=np.linspace(min_colorbar, max_colorbar, 10))
    data_cbar_handle.ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))

    return data_cbar_handle


def scatter_data(axis, pos_x, pos_y, data, conf: Optional[Configuration] = None, tag: Optional[MetricsData.Tag] = None, *,
                 factor=20, reverse=False, min_colorbar=None, max_colorbar=None):
    """
    When the configuration and tag are supplied, then outliers will be plotted in a different style.
    """
    if conf and tag and conf[Configuration.Name.VISUALIZE_OUTLIERS]:
        handle, data_cbar_handle, min_colorbar, max_colorbar = plot_outliers(axis, pos_x, pos_y, data,
                                                                     conf[Configuration.Name.METRICS], tag,
                                                                     factor=factor, reverse=reverse,
                                                                     min_colorbar=min_colorbar, max_colorbar=max_colorbar)
    else:
        if min_colorbar is None:
            min_colorbar = min(data)
        if max_colorbar is None:
            max_colorbar = max(data)
        handle = add_values(axis, pos_x, pos_y, data,
                            factor=factor, reverse=reverse,
                            min_colorbar=min_colorbar, max_colorbar=max_colorbar)
        data_cbar_handle = _plot_data_cbar(axis, handle, min_colorbar, max_colorbar)

    return handle, data_cbar_handle


def visualize_first_dc(axis, pos_x, pos_y, conf):
    if conf[Configuration.Name.VISUALIZE_FIRST_DRIFT_CORRECTION]:
        axis.plot(pos_x[0], pos_y[0], marker='o', markersize=20, markeredgewidth=2, linewidth=.0, fillstyle='none', label="first DC")
        axis.legend(loc="upper left", bbox_to_anchor=(1, -.02))


def drop_nan(*args):
    """
    Takes multiple (2 or more) pandas series and synchronizes the removal of all the nan values from all series.
    """
    assert len(args) >= 2
    mask = args[0].notna()
    for arg in args[1:]:
        mask &= arg.notna()
    return tuple(arg[mask] for arg in args)


def add_cumulative_histogram(ax, values, upper_bound, color_mapping):
    # Create bins. Add one to range to have last value included to.
    bin_count = 200
    bin_edges = [i / bin_count * upper_bound for i in range(bin_count + 1)]
    bin_values, _, _ = ax.hist(values, bins=bin_edges, density=True, cumulative=True, histtype='step')

    # Find corresponding color for each bin
    bin_colors = get_colors_for_bins(color_mapping, bin_edges)

    # Draw colored planes according to color mapping
    color_plane_y_min = 0
    for i, bin_value in enumerate(bin_values[:-1]):
        # Check if the colour for the bin edge before and after this bin value are different
        if bin_colors[i] != bin_colors[i + 1]:
            ax.axhspan(color_plane_y_min, bin_value, facecolor=bin_colors[i], alpha=0.5)
            color_plane_y_min = bin_value

    # Show last interval (if applicable)
    if bin_values[-1] > color_plane_y_min:
        ax.axhspan(color_plane_y_min, bin_values[-1], facecolor=color_mapping[max(color_mapping)].value, alpha=0.5)

    ax.set(xlim=(0, upper_bound), ylim=(0, 1))

    # Set custom tick locations and labels
    custom_ticks = [0., 0.2, 0.4, 0.6, 0.8, 1.]
    custom_labels = ['0%', '20%', '40%', '60%', '80%', '100%']

    ax.set_yticks(custom_ticks)  # Use FixedLocator to set custom tick locations
    ax.set_yticklabels(custom_labels)  # Use FixedFormatter to set custom tick labels

def get_colors_for_bins(color_dict, value_bin_edges):
    """
    For each bin center defined by the bin edges, determine the colour based on the given colour mapping
    """

    # To avoid confusion, there are two types of bins referred to here:
    #  - The value bins supplied as argument that represent some data partitioning
    #  - The colour bins calculated, they partition the values and are derived from the supplied color mapping
    # The former (the seconds parameter to this function) could just have been values to be looked up, but as this
    # function is only used for bins, the bin centers are calculated here for convenience.

    # Get the sorted thresholds (bins) for the colors
    sorted_keys = sorted(color_dict.keys())
    color_bin_edges = np.array(sorted_keys)

    # Find the index of the bin in which value bin center belongs
    value_bin_centers = [(begin + end) / 2 for begin, end in zip(value_bin_edges[:-1], value_bin_edges[1:])]
    indices = np.digitize(value_bin_centers, color_bin_edges, right=True)

    # Return the corresponding color
    return [color_dict[sorted_keys[idx]].value for idx in indices]
