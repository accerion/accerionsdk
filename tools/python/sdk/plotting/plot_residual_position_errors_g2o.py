#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.graph.filter import identify_edges
from sdk.graph.graph import Graph
from sdk.graph.residuals import get_edge_residuals_grouped
from sdk.graph.utils import create_vertices
from sdk.plotting.utils.plotting import *
from sdk.utils.configuration import Configuration
from sdk.utils.print import print_v

import matplotlib.pyplot as plt


def plot_residual_position_errors_g2o(data: DataContainer,
                             output_folder: Optional[Path] = None,
                             conf: Configuration = Configuration.default(),
                             *,
                             verbose: bool = True):
    from sdk.plotting.utils.plotting import scatter_data
    from sdk.plotting.utils.plotting import save_or_show

    if data.graph is None:
        print_v(verbose, __name__, ": No graph object in data container.")
        return None

    # Clip position jumps to this value
    pos_jump_lim = conf[Configuration.Name.METRICS].get_constraint_value(MetricsData.Tag.LoopClosurePositionErrors,
                                                                              MetricDataAggregation.Type.Maximum,
                                                                              ComparisonType.Smaller)

    # Check if .g2o file contains vertices
    if len(data.graph.vertices) == 0:
        # Check if there are signatures
        if len(data.signatures) == 0:
            print_v(verbose, __name__, "No vertices in .g2o file and no signatures available.")
            return
        # Use signatures to create vertices and add to graph
        print_v(verbose, __name__, "No vertices in .g2o file. Creating vertices from signatures.")
        data.graph.vertices = create_vertices(data.signatures)

    # Calculate all edge residuals per end vertex
    grouped_residuals = get_edge_residuals_grouped(data.graph, identify_edges(data.graph, Graph.Edge.Type.Loop))

    if not grouped_residuals:
        print("No loop closures in graph.")
        return None

    # Calculate the average edge residuals per end vertex
    pos_x = []
    pos_y = []
    errors = []
    for vertex_index, (_, residuals) in grouped_residuals.items():
        vertex = data.graph.vertices[vertex_index]
        pos_x.append(vertex.pose.x)
        pos_y.append(vertex.pose.y)
        errors.append(sum(r.norm for r in residuals) / len(residuals))

    prefix = conf.get_prefix()

    #
    # Create scatter plot where point color and size indicate the magnitude of the error
    #
    fig = plt.figure()
    axis = fig.subplots(1, 1)
    axis.set_aspect('equal', adjustable='datalim')

    if conf[Configuration.Name.OVERLAY_MAP]:
        add_signature_poses(axis, data.signatures)

    scatter_data(axis, pos_x, pos_y, errors, conf, MetricsData.Tag.LoopClosurePositionErrors, factor=500)

    axis.set_title('Average residual error of loop closures edges [m]')
    axis.set_xlabel("X [m]")
    axis.set_ylabel("Y [m]")
    axis.grid()

    save_or_show(fig, output_folder, 'residual_position_errors_g2o_map.png', prefix)

    #
    # Create histogram
    #
    errors_average = sum(errors) / len(errors)
    n_bins = 100

    fig = plt.figure()
    axis = fig.subplots(1, 1)

    axis.hist(errors, bins=n_bins)
    axis.axvline(errors_average, color=Colors.BLACK.value, linestyle='dashed', linewidth=1, label="Mean")

    min_ylim, max_ylim = axis.get_ylim()
    axis.text(errors_average * 1.1, max_ylim * 0.9, f'Mean [m]: {errors_average:.3f}')

    fig.suptitle('Average residual error of loop closures edges')
    axis.set_title(f'Mean [m]: {errors_average:.3f}, Min [m]: {min(errors):.3f}, Max [m]: {max(errors):.3f}')
    axis.set_xlabel("Average residual error [m]")
    axis.set_ylabel("Frequency [-]")
    axis.legend()
    axis.set_xlim(0, pos_jump_lim)

    save_or_show(fig, output_folder, 'residual_position_errors_g2o_hist.png', prefix)


def main():
    from sdk.plotting.utils.plotting import parse_plot_arguments
    from sdk.utils.preparations import process_args

    args = parse_plot_arguments()
    conf, input_files, output_folder = process_args(args)

    data = DataContainer.create(input_files, conf)
    if data.graph is None:
        raise Exception("Could not find g2o file.")
    plot_residual_position_errors_g2o(data, output_folder, conf, verbose=True)


if __name__ == "__main__":
    main()
