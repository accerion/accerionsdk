#!/usr/bin/env python3
"""
 Copyright (c) 2024-2025 Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.metrics.metrics import MetricsData

def test_metrics_tag_functions():
    for tag in MetricsData.Tag:
        assert tag in MetricsData.TAG_UNITS, "Metrics tag '{tag}' has no corresponding unit"
        assert tag in MetricsData.TAG_FUNCTIONS, "Metrics tag '{tag}' has no corresponding function"
