#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from copy import deepcopy

from sdk.data.pose import Pose
from sdk.graph.graph_compare import Graph, get_edge_diff_sets


def test_get_edge_diff_sets():
    # Initially create two identical graphs
    expected_same_edges = 5
    flip_edges = 3
    graph_1 = Graph()
    for i in range(expected_same_edges):
        ids = [i, 50 + i] if i >= flip_edges else [50 + i, i]
        graph_1.edges.append(Graph.Edge(ids[0], ids[1], Pose()))
    graph_2 = deepcopy(graph_1)

    # Introduce some differences
    expected_diff_edge_indices = [1, 2]
    expected_diff_edges = len(expected_diff_edge_indices)
    for i in expected_diff_edge_indices:
        graph_2.edges[i].pose.x = 1.0

    # Lastly add unique ones to each graph
    expected_additional_edges_1 = 2
    for i in range(expected_additional_edges_1):
        graph_1.edges.append(Graph.Edge(100 + i, 105 + i, Pose()))
    expected_additional_edges_2 = 3
    for i in range(expected_additional_edges_2):
        graph_2.edges.append(Graph.Edge(200 + i, 205 + i, Pose()))


    both_same, both_diff, only_a, only_b = get_edge_diff_sets(graph_1, list(range(len(graph_1.edges))),
                                                              graph_2, list(range(len(graph_2.edges))))

    assert len(both_same) == expected_same_edges - expected_diff_edges
    assert len(both_diff) == expected_diff_edges
    assert len(only_a) == expected_additional_edges_1
    assert len(only_b) == expected_additional_edges_2
