#!/usr/bin/env python3
"""
 Copyright (c) 2017-2025, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from typing import List, Tuple

from sdk.graph.graph import Graph
from sdk.graph.filter import identify_edges
from sdk.graph.utils import create_edge_vertex_ids_dict, create_vertex_id_dict


POSE_ABS_TOL = 5.e-3  # Absolute tolerance on Pose values, 5 cm


# Convenience types
IndexList = List[int]
IndexPair = Tuple[int, int]
IndexPairList = List[IndexPair]


def __sort_ids(e: Graph.Edge) -> Graph.Edge:
    if e.id_begin < e.id_end:
        return e
    return Graph.Edge(id_begin=e.id_end, id_end=e.id_begin, pose=e.pose.inverse(), inf=e.inf)


def get_edge_diff_sets(graph_a: Graph, loop_a: List[int], graph_b: Graph, loop_b: List[int],
                       abs_tolerance: float = POSE_ABS_TOL) -> Tuple[IndexPairList, IndexPairList, IndexList, IndexList]:
    both_same = []
    both_diff = []
    only_a = []
    only_b = []

    dict_edge_to_index_a = create_edge_vertex_ids_dict(graph_a, sort=True)
    dict_edge_to_index_b = create_edge_vertex_ids_dict(graph_b, sort=True)

    for edge_index_a in loop_a:
        edge_a = __sort_ids(graph_a.edges[edge_index_a])
        edge_vertex_ids_a = (edge_a.id_begin, edge_a.id_end)
        if edge_vertex_ids_a in dict_edge_to_index_b:
            edge_index_b = dict_edge_to_index_b[edge_vertex_ids_a]
            edge_b = __sort_ids(graph_b.edges[edge_index_b])
            if edge_a.pose.is_approx(edge_b.pose, abs_tol=abs_tolerance):
                both_same.append((edge_index_a, edge_index_b))
            else:
                both_diff.append((edge_index_a, edge_index_b))
        else:
            only_a.append(edge_index_a)

    for edge_index_b in loop_b:
        edge_b = __sort_ids(graph_b.edges[edge_index_b])
        edge_vertex_ids_b = (edge_b.id_begin, edge_b.id_end)
        if edge_vertex_ids_b not in dict_edge_to_index_a:
            only_b.append(edge_index_b)

    return both_same, both_diff, only_a, only_b


def report_edge_diff(graph_a: Graph, loop_a: List[int], graph_b: Graph, loop_b: List[int]):
    both_same, both_diff, only_a, only_b = get_edge_diff_sets(graph_a, loop_a, graph_b, loop_b)

    map_vertex_id_to_index = create_vertex_id_dict(graph_a)

    print(f"Same edges, same pose (components differ less than {POSE_ABS_TOL}):")
    print(f"    Found {len(both_same) if len(both_same) != 0 else 'no'} edges in this category")
    print("")

    print(f"Same edges, different pose (components differ more than {POSE_ABS_TOL}):")
    print(f"    Found {len(both_diff) if len(both_diff) != 0 else 'no'} edges in this category")
    if len(both_diff) != 0:
        print("    [index a, index b, vertex ids, pose a, edge a, edge b]: ", end='')
        for (i_a, i_b) in both_diff:
            e_a = graph_a.edges[i_a]
            e_b = graph_b.edges[i_b]
            print(f"[{i_a}, {i_b}, {(e_a.id_begin, e_a.id_end)}, "
                  f"{graph_a.vertices[map_vertex_id_to_index[e_a.id_begin]].pose}, {e_a.pose} ,{e_b.pose}] ", end='')
        print("")
    print("")

    print("Unique edges in a:")
    print(f"    Found {len(only_a) if len(only_a) != 0 else 'no'} edges in this category")
    if len(only_a) != 0:
        print("    [edge a index, edge vertex ids]: ", end='')
        for i in only_a:
            e = graph_a.edges[i]
            print(f"[{i}, {(e.id_begin, e.id_end)}] ", end='')
        print("")
    print("")

    print("Unique edges in b:")
    print(f"    Found {len(only_b) if len(only_b) != 0 else 'no'} edges in this category")
    if len(only_b) != 0:
        print("    [edge b index, edge vertex ids]: ", end='')
        for i in only_b:
            e = graph_b.edges[i]
            print(f"[{i}, {(e.id_begin, e.id_end)}] ", end='')
        print("")
    print("")


def graph_compare(graph_a: Graph, graph_b: Graph):
    odom_a = identify_edges(graph_a, Graph.Edge.Type.Relative)
    loop_a = identify_edges(graph_a, Graph.Edge.Type.Loop)
    odom_b = identify_edges(graph_b, Graph.Edge.Type.Relative)
    loop_b = identify_edges(graph_b, Graph.Edge.Type.Loop)

    print("Amounts: reference vs. test (difference)")
    print(f"    vertices:              {len(graph_a.vertices)} vs {len(graph_b.vertices)} ({len(graph_b.vertices) - len(graph_a.vertices)})")
    print(f"    fixes:                 {len(graph_a.fixes)} vs {len(graph_b.fixes)} ({len(graph_b.fixes) - len(graph_a.fixes)})")
    print(f"    odometry edges:        {len(odom_a)} vs {len(odom_b)} ({len(odom_b) - len(odom_a)})")
    print(f"    loop closure edges:    {len(loop_a)} vs {len(loop_b)} ({len(loop_b) - len(loop_a)})")
    print("")

    if abs(len(loop_b) - len(loop_a)) != 0:
        report_edge_diff(graph_a, loop_a, graph_b, loop_b)


def __main():
    from argparse import ArgumentParser
    from sdk.graph.g2o_reader import Reader, is_g2o_file
    from sdk.utils.path import path_list

    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True, nargs="+", help="Input file(s) and folder(s)")
    args = vars(parser.parse_args())

    input_files = [f for f in path_list(args["input"]) if is_g2o_file(f)]
    print(f"Reading {len(input_files)} graphs.\n")
    graphs = [Reader.read(f) for f in input_files]

    input_file_a = input_files[0]
    graph_a = graphs[0]
    for input_file_b, graph_b in zip( input_files[1:], graphs[1:]):
        print("Comparison done for:")
        print(f"    Reference '{input_file_a}'")
        print(f"    Test      '{input_file_b}'")
        print("")
        graph_compare(graph_a, graph_b)
        print("")

        # Reuse the test data
        input_file_a = input_file_b
        graph_a = graph_b


if __name__ == "__main__":
    __main()
