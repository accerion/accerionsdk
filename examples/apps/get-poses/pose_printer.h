/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <atomic>
#include <string>
#include <thread>

#include "AccerionSensorAPI/structs/corrected_odometry.h"
#include "AccerionSensorAPI/structs/drift_correction.h"
#include "AccerionSensorAPI/structs/odometry.h"

class PosePrinter {
public:
    PosePrinter();
    ~PosePrinter();

    void Update(const accerion::api::Odometry& pose);
    void Update(const accerion::api::CorrectedOdometry& pose);
    void Update(const accerion::api::DriftCorrection& correction);

private:
    void RunLoop();

    void Print() const;
    static std::string Print(accerion::api::Pose p);
    static std::string Print(double x, double y, double th);

    std::atomic_bool is_running_{false};
    std::thread printing_thread_{};

    accerion::api::Pose last_odometry_pose_{};
    std::atomic_bool last_odometry_pose_new_{false};
    accerion::api::CorrectedOdometry last_corrected_odometry_pose_{};
    std::atomic_bool last_corrected_odometry_pose_new_{false};
    accerion::api::DriftCorrection last_corr_{};
    std::atomic_bool last_corr_new_{false};

    std::size_t header_counter_{0};
    static constexpr std::size_t header_interval_{15};
    static constexpr auto header_ = "uncorrected pose,                corrected pose,                  correction                   ->   (x, y) angle";
};
