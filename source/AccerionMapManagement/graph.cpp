/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionMapManagement/graph.h"
#include "AccerionMap/schema_creator.h"
namespace accerion {
namespace map {

void Import(accerion::sqlite::Database& db, const accerion::pgo::PoseGraph& graph,
                                const std::function<void(std::uint8_t)>& report_progress) {
    const auto& n_nodes = graph.nodes.size();
    const auto update_pose_stmt = db.PrepareStatement(R"SQL(
            UPDATE signatures
               SET x = @x,
                   y = @y,
                   th = @th
             WHERE id = @id;
        )SQL");

    if (report_progress) {
        report_progress(0);
    }
    SchemaCreator creator;
    creator.DropIndices(db);
    db.Execute("BEGIN TRANSACTION");
    std::size_t progress_counter = 0;
    std::uint8_t previous_progress = 0;
    for (const auto& node : graph.nodes) {
            update_pose_stmt.Bind("@x", node.pose.x);
            update_pose_stmt.Bind("@y", node.pose.y);
            update_pose_stmt.Bind("@th", node.pose.th);
            update_pose_stmt.Bind("@id", node.id);
            update_pose_stmt.Step();
            update_pose_stmt.Reset();

        if (report_progress) {
            ++progress_counter;
            auto progress_value = static_cast<std::uint8_t>(std::round(
                100. * static_cast<double>(progress_counter) / static_cast<double>(n_nodes)));
            if (progress_value != previous_progress) {
                report_progress(progress_value);
                previous_progress = progress_value;
            }
        }
    }

    if (report_progress && previous_progress != 100) {
        report_progress(100);
    }
    db.Execute("COMMIT");
    creator.CreateIndices(db);
}

}  //  namespace map
}  //  namespace accerion
