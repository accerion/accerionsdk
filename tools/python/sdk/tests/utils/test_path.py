#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from __future__ import annotations

from dataclasses import dataclass

from sdk.utils.path import *


def test_add_to_stem():
    @dataclass
    class Test:
        path: str
        prefix: Optional[str]
        suffix: Optional[str]
        expectation: str

    tests = [
        Test("/folder/file", None, None, "/folder/file"),
        Test("/folder/file.txt", None, None, "/folder/file.txt"),
        Test("/folder/file.txt1.txt2", None, None, "/folder/file.txt1.txt2"),
        Test("/folder/file", "p_", None, "/folder/p_file"),
        Test("/folder/file.txt", "p_", None, "/folder/p_file.txt"),
        Test("/folder/file.txt1.txt2", "p_", None, "/folder/p_file.txt1.txt2"),
        Test("/folder/file", None, "_s", "/folder/file_s"),
        Test("/folder/file.txt", None, "_s", "/folder/file_s.txt"),
        Test("/folder/file.txt1.txt2", None, "_s", "/folder/file_s.txt1.txt2"),
        Test("/folder/file", "p_", "_s", "/folder/p_file_s"),
        Test("/folder/file.txt", "p_", "_s", "/folder/p_file_s.txt"),
        Test("/folder/file.txt1.txt2", "p_", "_s", "/folder/p_file_s.txt1.txt2"),
        Test("file", "p_", "_s", "p_file_s"),
        Test("file.txt", "p_", "_s", "p_file_s.txt"),
        Test("file.txt1.txt2", "p_", "_s", "p_file_s.txt1.txt2"),
    ]

    for index, test in enumerate(tests):
        expected_result = Path(test.expectation)
        actual_result = add_to_stem(Path(test.path), prefix=test.prefix, suffix=test.suffix)

        assert actual_result == expected_result, f"Test at index '{index}' failed"


def test_separate_common_parent():
    class Test:
        def __init__(self, b: str, p: List[str], *, expectation: Test = None):
            self.base_path: str = b
            self.paths: List[str] = p
            self.expectation: Optional[Test] = expectation

    tests = [
        Test("", []),
        Test("", ["file"]),
        Test("", ["file1.png",
                  "file2.png"]),
        Test("folder" + os.sep, ["file1.png",
                                 "file2.png"]),
        Test(os.path.join("folder1", "folder2") + os.sep, ["file1.png",
                                                           "file2.png"]),
        Test("folder" + os.sep, [os.path.join("folder1", "file1.png"),
                                 "file2.png"]),
        Test(os.sep + "folder" + os.sep, ["file1.png",
                                          "file2.png"]),
        Test(os.sep + "folder" + os.sep, [os.path.join("folder1", "file1.png"),
                                          "file2.png"]),
        Test("", [os.path.join("common", "additional", "file1.png"),
                  os.path.join("common", "file2.png")],
             expectation = Test("common", [os.path.join("additional", "file1.png"),
                                           "file2.png"])),
        Test("folder" + os.sep, [os.path.join("common", "additional", "file1.png"),
                                 os.path.join("common", "file2.png")],
             expectation = Test(os.path.join("folder", "common"), [os.path.join("additional", "file1.png"),
                                                                   "file2.png"])),
        Test(os.sep + "folder" + os.sep, [os.path.join("common", "additional", "file1.png"),
                                          os.path.join("common", "file2.png")],
             expectation = Test(os.sep + os.path.join("folder", "common"), [os.path.join("additional", "file1.png"),
                                                                            "file2.png"]))
    ]
    for index, test in enumerate(tests):
        test_input = [test.base_path + p for p in test.paths]
        actual_result_base_path, actual_result_paths = separate_common_parent(test_input)

        expected_result = test.expectation
        if expected_result is None:
            expected_result = Test(test.base_path.rstrip(os.sep), test.paths)

        assert actual_result_base_path == expected_result.base_path, f"Test '{index}' failed on 'base_path' value"
        assert actual_result_paths == expected_result.paths, f"Test '{index}' failed on 'paths' value"
