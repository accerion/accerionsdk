/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDatabaseHandling/database.h"

#include <sstream>
#include <stdexcept>

#include "AccerionDatabaseHandling/prepared_statement.h"
#include "AccerionDatabaseHandling/integrity.h"


namespace accerion {

namespace sqlite {

Database::Database(const std::string& path, const int flags) : db_ {OpenDatabase(path, flags)} {
    // Need to enable explicitly for each db handle, due to backwards compatability within SQLite database verions
    // (https://www.sqlite.org/foreignkeys.html#:~:text=2.%20Enabling%20Foreign%20Key%20Support)
    SetForeignKeysConstraint(*this, true);
}

sqlite3* Database::GetPtr() const { return db_.get(); }

PreparedStatement Database::PrepareStatement(const std::string& query) const {
    return PreparedStatement{*this, query};
}

void Database::Execute(const std::string& query) const {
    const auto result = sqlite3_exec(db_.get(), query.c_str(), nullptr, nullptr, nullptr);
    if (result != SQLITE_OK) {
        std::stringstream ss;
        ss << "SQL error: " << sqlite3_errmsg(db_.get()) << "\n"
           << "Query: " << query << "\n";
        throw std::runtime_error(ss.str());
    }
}

std::string Database::GetFilePath() const { return sqlite3_db_filename(db_.get(), nullptr); }

Database::SqlitePtr Database::OpenDatabase(const std::string& path, const int flags) {
    sqlite3* db = nullptr;
    const auto return_code = sqlite3_open_v2(path.c_str(), &db, flags, nullptr);
    if (return_code != SQLITE_OK) {
        const std::string error_message{sqlite3_errmsg(db)};
        SqlitePtrDeleter()(db);
        throw std::runtime_error("SqliteDatabase can't open database '" + path + "': " + error_message);
    }
    return SqlitePtr{db};
}

}  // namespace sqlite
}  // namespace accerion
