#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

from sdk.csv.versions.base import LogFileSpecification
from sdk.csv.versions.file_specifications_v61 import ARAMS_INDEX, ARAMS_TIME, FMC, LRT, LSD


__all__ = ["ARAMS_INDEX", "ARAMS_TIME", "FMC", "FILES_SUPPORTED"]


# Log drift corrections .csv file column headers used
class LDC(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME

    SIGNATURE_ID = "signature_id"
    CLUSTER_ID = "cluster_id"
    NEW_XPOS = "sensor_pos_x[m]"
    NEW_YPOS = "sensor_pos_y[m]"
    NEW_THETA = "sensor_pos_theta[deg]"
    ERROR_X = "error_x[m]"
    ERROR_Y = "error_y[m]"
    ERROR_THETA = "error_theta[deg]"
    MATCH_QUALITY = "match_quality[%]"
    CUMU_DIST = "cumulative_distance[m]"
    CUMU_YAW = "cumulative_yaw[deg]"

    @staticmethod
    def full_name():
        return "logDriftCorrections"


# Log external reference input .csv file column headers used
class LER(LogFileSpecification):
    INDEX = ARAMS_INDEX
    TIME = ARAMS_TIME

    TIME_OFFSET = "time_offset(s)"
    INPUT_POSE_X = "input_pos_x[m]"
    INPUT_POSE_Y = "input_pos_y[m]"
    INPUT_POSE_TH  = "input_pos_theta[deg]"
    STD_DEV_POS_X = "std_dev_pos_x[m]"
    STD_DEV_POS_Y = "std_dev_pos_y[m]"
    STD_DEV_POS_THETA = "std_dev_pos_theta[deg]"

    @staticmethod
    def full_name():
        return "logExtRefInputFast"


# List of supported ARAMS files
FILES_SUPPORTED = [LRT, LDC, LER, LSD]

