#!/usr/bin/env python3
"""
 Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from sdk.graph.graph import Graph
from sdk.graph.validator.base import Base


class Connectivity(Base):
    def __init__(self):
        self.__disconnected_vertices = None

    def is_valid(self, graph: Graph) -> bool:
        connected = set([f.id for f in graph.fixes] if len(graph.fixes) != 0 else [graph.vertices[0].id])

        prev_n_connected = 0
        while len(connected) not in (len(graph.vertices), prev_n_connected):
            prev_n_connected = len(connected)

            # To improve, looping over all edges everytime
            for e in graph.edges:
                if e.id_begin in connected or e.id_end in connected:
                    connected.add(e.id_begin)
                    connected.add(e.id_end)

        self.__disconnected_vertices = set(v.id for v in graph.vertices if v.id not in connected)
        return len(self.__disconnected_vertices) == 0

    def correct(self, graph: Graph) -> Graph:
        assert self.__disconnected_vertices is not None, "Implementation error: is_valid should be called first"

        print(f"There are {len(self.__disconnected_vertices)} vertices not connected to the main graph:")
        print(f"    {sorted(self.__disconnected_vertices)}")

        graph.vertices = [v for v in graph.vertices if v.id not in self.__disconnected_vertices]
        graph.edges = [e for e in graph.edges if
                       all(v not in self.__disconnected_vertices for v in [e.id_begin, e.id_end])]

        return graph
