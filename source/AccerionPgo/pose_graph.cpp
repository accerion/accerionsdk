/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionPgo/pose_graph.h"

namespace accerion {

namespace pgo {

std::ostream& operator<<(std::ostream& os, PoseGraph::NodeType node_type) {
    std::string id{};
    switch (node_type) {
        case PoseGraph::NodeType::kUnknown:
            id = "unknown";
            break;
        case PoseGraph::NodeType::kSignature:
            id = "signature";
            break;
        case PoseGraph::NodeType::kAbsolute:
            id = "absolute";
            break;
        case PoseGraph::NodeType::kFixed:
            id = "fixed";
            break;
        case PoseGraph::NodeType::kInterpolated:
            id = "interpolated";
            break;
    }
    return os << "# " << id;
}

std::istream& operator>>(std::istream& is, PoseGraph::NodeType& type) {
    std::string id{};
    is >> id;
    if (id == "unknown") type = PoseGraph::NodeType::kUnknown;
    else if (id == "signature") type = PoseGraph::NodeType::kSignature;
    else if (id == "absolute") type = PoseGraph::NodeType::kAbsolute;
    else if (id == "fixed") type = PoseGraph::NodeType::kFixed;
    else if (id == "interpolated") type = PoseGraph::NodeType::kInterpolated;
    return is;
}

std::ostream& operator<<(std::ostream& os, PoseGraph::EdgeType edge_type) {
    std::string id{};
    switch (edge_type) {
        case PoseGraph::EdgeType::kUnknown:
            id = "unknown";
            break;
        case PoseGraph::EdgeType::kRelative:
            id = "relative";
            break;
        case PoseGraph::EdgeType::kAbsolute:
            id = "absolute";
            break;
        case PoseGraph::EdgeType::kLoopClosure:
            id = "loop";
            break;
        case PoseGraph::EdgeType::kIdentity:
            id = "identity";
            break;
    }
    return os << "# " << id;
}

std::istream& operator>>(std::istream& is, PoseGraph::EdgeType& type) {
    std::string id{};
    is >> id;
    if (id == "unknown") type = PoseGraph::EdgeType::kUnknown;
    else if (id == "relative") type = PoseGraph::EdgeType::kRelative;
    else if (id == "absolute") type = PoseGraph::EdgeType::kAbsolute;
    else if (id == "loop") type = PoseGraph::EdgeType::kLoopClosure;
    else if (id == "identity") type = PoseGraph::EdgeType::kIdentity;
    return is;
}

}  // namespace pgo

}  // namespace accerion
