# PoseManager library

## Components
The main components from the library are described below.

### PoseBuffer
Type: `PoseBuffer_<PoseType, TimeType, TimeDiffConverter>`

This class is mainly a timeseries container.
It stores poses of type `PoseType` with their corresponding timestamp of type `TimeType`.

Note that the `TimeDiffConverter` is only required for time types other than std::chrono::time_point or a plain arithmetic type. 

### PoseTree
Type: `PoseTree_<PoseType, ContainerType>`

Convenience types:
* `PoseTree<PoseType> = PoseTree_<PoseType, PlainPoseContainer<PoseType>>`
* `PoseTreeBuffered<PoseType, TimeType, TimeDiffConverter> = PoseTree_<PoseType, PoseBuffer_<PoseType, TimeType, TimeDiffConverter>>`

This class acts as a tree-like data structure in which poses between different frames can be stored.
Frames are identified by `std::string` identifiers. The pose data is stored in a `ContainerType` instance.
This tree class will forward any arguments while setting or getting to the container class.

The way the pose data is expected to be supplied and retrieved is that they following the point coordinate interpretation, as described on the Knowledge Base.
That means that a supplied pose transforms point in the frame `id_from` to frame `id_to`. 

Two containers are supplied in the library, namely the already discussed `PoseBuffer_` and the `PlainPoseContainer`.
The latter is just a simple container with a single pose.
Hence, the convenience type `PoseTree` (using the `PlainPoseContainer`) can be used for static pose trees, for example describing the fixed robot configuration.


### PoseManager
Type: `PoseManager_<PoseType, ContainerType>` 

Convenience types:
* `PoseManager<PoseType> = PoseManager_<PoseType, PlainPoseContainer<PoseType>>;`
* `PoseManagerBuffered<PoseType, TimeType, TimeDiffConverter> = PoseManager_<PoseType, PoseBuffer_<PoseType, TimeType, TimeDiffConverter>>;`

This is the main interfacing class to be used. The class and interface are documented in the code / Doxygen.
The pose interpretation is the same as for the PoseTree.

This class contains two `PoseTree` instances:
* static robot description, provided by the user, having common as root node
* internal mapping tree, defined like common -> local -> global

The manager class has one get and two set functions. There is a setter for global sensor data information and the local one.
Details are described on the Knowledge Base.


## Customizations
The classes `Pose` in the API and `Pose3` in the SDK example pose-manager can be used by the PoseManager library immediately.

In case a custom pose type is desired, then there are a couple of interface requirements for the type, in order to be usable by the library.
The following methods are expected to be present for the custom `PoseType`:
```C++
class PoseType {
public:
    static PoseType Identity();
    
    PoseType operator*(const PoseType &other) const;
    PoseType& operator*=(const PoseType& other);
    
    void NormalizeRotation();
    
    PoseType Inverse() const;
    
    PoseType Interpolate(double ratio, const PoseType& other) const;
    
    ...
};
```
The multiplication (and inversion) rules follow the calculations as described on the Knowledge Base page on coordinate transformations.

For the container that is used to store the dat in te PoseTree their interface should look like:
```C++
template <typename PoseType, typename... Args>
class CustomContainer {
public:
    bool Set(const PoseType& pose, Args&&... args);

    PoseType Get(Args&&... args) const;
    
    ...
};
```
The return value of Set should indicate if setting succeeded or not.

The template arguments for `Args` can be
* nothing, as already done for the `PlainPoseContainer` class, or
* one or multiple arguments that are required to uniquely identify a pose.
  An example is the PoseBuffer_ that requires a time argument.

When the time type is not std::chrono::time_point or a plain arithmetic type, then the unit
`testPoseBufferTest.BasicFunctionalityWithCustomType` shows an example of the minimal interface that needs to be
available for that type.

When printing of any of the custom types is desired, then also implement the streaming operator:
```C++
class CustomType {
public:
    friend std::ostream& operator<<(std::ostream& os, const CustomType& pose);
    
    ...
};
```
