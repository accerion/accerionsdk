#!/usr/bin/env python3
"""
 Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
from collections.abc import Iterable


def flatten(data):
    if isinstance(data, Iterable):
        for x in data:
            for y in flatten(x):
                yield y
    else:
        yield data
