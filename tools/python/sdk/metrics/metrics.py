#!/usr/bin/env python3
"""
 Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import copy
from dataclasses import dataclass, field
from typing import Dict, List

from sdk.metrics.common import ComparisonType, Result
from sdk.metrics.metrics_data import MetricsData
from sdk.data.data_container import DataContainer
from sdk.metrics.metrics_data_aggregation import MetricDataAggregation
from sdk.metrics.metrics_condition import MetricCondition


@dataclass
class Metrics:
    @dataclass
    class Condition:
        name: str
        aggregation: MetricDataAggregation
        aggregation_result: float
        condition: MetricCondition
        condition_result: Result

        @staticmethod
        def create(name: str, cond_tag: str, cond_str: str, cond_result: str, agg_result: float):
            return Metrics.Condition(
                name = name,
                aggregation = MetricDataAggregation(cond_tag),
                aggregation_result = agg_result,
                condition = MetricCondition(cond_str),
                condition_result = Result.parse(cond_result))
        
    data_conditions: Dict[MetricsData, List[Condition]] = field(default_factory=dict)

    def __contains__(self, data_tag: MetricsData.Tag):
        return any(d.tag == data_tag for d in self.data_conditions.keys())

    def get_conditions(self, data_tag: MetricsData.Tag):
        assert data_tag in self, "Requested data is not available in metrics."
        return [c for d, c in self.data_conditions.items() if d.tag == data_tag][0]

    # To be used like
    #     metrics = ...  # considered given
    #     metrics.get_constraint_value(MetricsData.Tag.DistSignatures,
    #                                  MetricDataAggregation.Type.Minimum,
    #                                  ComparisonType.Greater)
    def get_constraint_value(self, data_tag: MetricsData.Tag, agg_type: MetricDataAggregation.Type, comp_type: ComparisonType):
        value = None
        for condition in self.get_conditions(data_tag):
            if condition.aggregation.type == agg_type:
                comparisons = condition.condition.comparisons
                if comp_type in comparisons:
                    value = comparisons[comp_type]
                    break
        return value

    def evaluate(self, data: DataContainer):
        evaluated_data = copy.deepcopy(data)
        for metric_data, conditions in self.data_conditions.items():
            values = metric_data(evaluated_data)
            for c in conditions:
                c.aggregation_result = c.aggregation(values)
                c.condition_result = c.condition(c.aggregation_result)

    def all(self, result: Result, data_tag: MetricsData.Tag = None):
        return all(self.__get_results(result, data_tag))

    def any(self, result: Result, data_tag: MetricsData.Tag = None):
        return any(self.__get_results(result, data_tag))

    def __get_results(self, result: Result, data_tag: MetricsData.Tag):
        if data_tag is None:
            for cs in self.data_conditions.values():
                for c in cs:
                    yield c.condition_result == result
        else:
            for c in self.get_conditions(data_tag):
                yield c.condition_result == result
